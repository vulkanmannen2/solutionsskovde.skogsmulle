﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct GenericInterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// AboutScreen
struct AboutScreen_t2183797299;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// System.String
struct String_t;
// animateTexture
struct animateTexture_t712563028;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.Material
struct Material_t340375123;
// System.Collections.Generic.List`1<UnityEngine.Texture>
struct List_1_t839070149;
// UnityEngine.Texture
struct Texture_t3661962703;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// animationScaling
struct animationScaling_t1595398591;
// UnityEngine.Transform
struct Transform_t3600365921;
// AsyncSceneLoader
struct AsyncSceneLoader_t621267272;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0
struct U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// buttonColorToggle
struct buttonColorToggle_t2066501000;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// settingsManager
struct settingsManager_t2086358119;
// System.Collections.Generic.List`1<SettingsList>
struct List_1_t3355509319;
// SettingsList
struct SettingsList_t1883434577;
// buttonMoveToScene
struct buttonMoveToScene_t2573241184;
// buttonTriggerAnimation
struct buttonTriggerAnimation_t4121295644;
// UnityEngine.Animation
struct Animation_t3648466861;
// buttonTriggerAnimation/<AnimationCooldown>c__Iterator0
struct U3CAnimationCooldownU3Ec__Iterator0_t1141888348;
// UnityEngine.AnimationClip
struct AnimationClip_t2318505987;
// CameraController
struct CameraController_t3346819214;
// UnityEngine.Touch[]
struct TouchU5BU5D_t1849554061;
// System.Func`2<UnityEngine.Touch,UnityEngine.Vector2>
struct Func_2_t528868469;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>
struct IEnumerable_1_t1136082412;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Touch>
struct IEnumerable_1_t901709757;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// CameraFocusController
struct CameraFocusController_t1033776956;
// Vuforia.VuforiaARController
struct VuforiaARController_t1876945237;
// System.Action
struct Action_t1264377477;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// Vuforia.CameraDevice
struct CameraDevice_t960297568;
// CameraSettings
struct CameraSettings_t3152619780;
// CameraSettings/<RestoreOriginalFocusMode>c__Iterator0
struct U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229;
// Vuforia.ITrackerManager
struct ITrackerManager_t607206903;
// CharacterCycler
struct CharacterCycler_t3521054542;
// characterLoader
struct characterLoader_t1326495876;
// characterSlot
struct characterSlot_t990761438;
// DefaultInitializationErrorHandler
struct DefaultInitializationErrorHandler_t3109936861;
// VuforiaMonoBehaviour
struct VuforiaMonoBehaviour_t1150221792;
// Vuforia.VuforiaRuntime
struct VuforiaRuntime_t1949122020;
// System.Action`1<Vuforia.VuforiaUnity/InitError>
struct Action_1_t3593217305;
// UnityEngine.GUI/WindowFunction
struct WindowFunction_t3146511083;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// Vuforia.VuforiaConfiguration
struct VuforiaConfiguration_t1763229349;
// Vuforia.VuforiaConfiguration/GenericVuforiaConfiguration
struct GenericVuforiaConfiguration_t3697830469;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.GUIStyleState
struct GUIStyleState_t1397964415;
// UnityEngine.Font
struct Font_t1956802104;
// UnityEngine.RectOffset
struct RectOffset_t1369453676;
// DefaultTrackableEventHandler
struct DefaultTrackableEventHandler_t1588957063;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1113559212;
// Vuforia.ITrackableEventHandler
struct ITrackableEventHandler_t1495975588;
// onScanController
struct onScanController_t1472212342;
// UnityEngine.Behaviour
struct Behaviour_t1437897464;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t3210418286;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t4234922487;
// UnityEngine.Canvas[]
struct CanvasU5BU5D_t682926938;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// destoryOnAwake
struct destoryOnAwake_t1274961527;
// EnableMouthAnim
struct EnableMouthAnim_t2443685136;
// findTutorialManager
struct findTutorialManager_t1037408675;
// FpsCount
struct FpsCount_t2935661334;
// FrameRateSettings
struct FrameRateSettings_t3598747490;
// Vuforia.VuforiaRenderer
struct VuforiaRenderer_t3433045970;
// InitErrorHandler
struct InitErrorHandler_t2159361531;
// LoadingScreen
struct LoadingScreen_t2154736699;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843;
// UnityEngine.UI.RawImage[]
struct RawImageU5BU5D_t1576731517;
// Math3d
struct Math3d_t1427253148;
// UnityEngine.Camera
struct Camera_t4157153871;
// MenuAnimator
struct MenuAnimator_t2112910832;
// MenuOptions
struct MenuOptions_t1951716431;
// TrackableSettings
struct TrackableSettings_t2862243993;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// ModelSwap
struct ModelSwap_t1632145241;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.Animator
struct Animator_t434523843;
// symbolPrefabDefiner
struct symbolPrefabDefiner_t2798204667;
// onScanController/<WaitAndPlayShowTail>c__Iterator0
struct U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349;
// pageTurn
struct pageTurn_t132420247;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// setSettings
struct setSettings_t4116128020;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Audio.AudioMixer
struct AudioMixer_t3521020193;
// SmoothCamera
struct SmoothCamera_t4222009864;
// System.Collections.Generic.Queue`1<UnityEngine.Quaternion>
struct Queue_1_t2148187825;
// System.Collections.Generic.Queue`1<UnityEngine.Vector3>
struct Queue_1_t3568572958;
// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t2151848540;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>
struct UnityAction_2_t1262235195;
// symbolsSwitch
struct symbolsSwitch_t3635132447;
// TapHandler
struct TapHandler_t334234343;
// TouchCamera
struct TouchCamera_t3780128488;
// Vuforia.StateManager
struct StateManager_t1982749557;
// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour>
struct IEnumerable_1_t93412101;
// Vuforia.ImageTargetBehaviour
struct ImageTargetBehaviour_t2200418350;
// Vuforia.ImageTarget
struct ImageTarget_t3707016494;
// Vuforia.MultiTargetBehaviour
struct MultiTargetBehaviour_t2061511750;
// Vuforia.MultiTarget
struct MultiTarget_t2016089265;
// Vuforia.CylinderTargetBehaviour
struct CylinderTargetBehaviour_t822809409;
// Vuforia.CylinderTarget
struct CylinderTarget_t4265049602;
// Vuforia.ObjectTargetBehaviour
struct ObjectTargetBehaviour_t728125005;
// Vuforia.ObjectTarget
struct ObjectTarget_t3212252422;
// Vuforia.VuMarkBehaviour
struct VuMarkBehaviour_t1178230459;
// Vuforia.VuMarkTemplate
struct VuMarkTemplate_t3623118391;
// Vuforia.ObjectTracker
struct ObjectTracker_t4177997237;
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet>
struct IEnumerable_1_t2265887763;
// System.Collections.Generic.List`1<Vuforia.DataSet>
struct List_1_t463142320;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2059959053;
// Vuforia.DataSet
struct DataSet_t3286034874;
// System.Char[]
struct CharU5BU5D_t3528271667;
// triggerAudio
struct triggerAudio_t1885395141;
// triggerAudio/<SoundCooldown>c__Iterator0
struct U3CSoundCooldownU3Ec__Iterator0_t604438267;
// tutorialManager
struct tutorialManager_t3421782323;
// SettingsList[]
struct SettingsListU5BU5D_t4213907788;
// Vuforia.DataSet[]
struct DataSetU5BU5D_t4052198943;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>
struct Dictionary_2_t2272543;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>
struct List_1_t2585633954;
// Vuforia.VuMarkManager
struct VuMarkManager_t2982459596;
// Vuforia.DeviceTrackingManager
struct DeviceTrackingManager_t3849131975;
// Vuforia.IExtendedTrackingManager
struct IExtendedTrackingManager_t821947488;
// Vuforia.IlluminationManager
struct IlluminationManager_t3960931838;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t2571361770;
// UnityEngine.Texture[]
struct TextureU5BU5D_t908295702;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Byte
struct Byte_t1134296376;
// System.Double
struct Double_t594665363;
// System.UInt16
struct UInt16_t2177724958;
// System.Void
struct Void_t1185182177;
// Vuforia.ImageTargetBuilder
struct ImageTargetBuilder_t2430893908;
// Vuforia.TargetFinder
struct TargetFinder_t2439332195;
// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_t1617499438;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Action`1<UnityEngine.Font>
struct Action_1_t2129269699;
// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t2467502454;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>
struct Dictionary_2_t1340713322;
// System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>
struct List_1_t905170877;
// Vuforia.ICameraConfiguration
struct ICameraConfiguration_t283990539;
// Vuforia.DigitalEyewearARController
struct DigitalEyewearARController_t1054226036;
// Vuforia.VideoBackgroundManager
struct VideoBackgroundManager_t2198727358;
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct Dictionary_2_t3630620928;
// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
struct List_1_t386988881;
// Vuforia.IWebCam
struct IWebCam_t3734514901;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// Vuforia.VuforiaConfiguration/DigitalEyewearConfiguration
struct DigitalEyewearConfiguration_t546560202;
// Vuforia.VuforiaConfiguration/VideoBackgroundConfiguration
struct VideoBackgroundConfiguration_t3392414655;
// Vuforia.VuforiaConfiguration/DeviceTrackerConfiguration
struct DeviceTrackerConfiguration_t721467671;
// Vuforia.VuforiaConfiguration/SmartTerrainConfiguration
struct SmartTerrainConfiguration_t1514074484;
// Vuforia.VuforiaConfiguration/WebCamConfiguration
struct WebCamConfiguration_t1101614731;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t3309123499;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t190067161;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1258266594;
// UnityEngine.AudioSourceExtension
struct AudioSourceExtension_t3064908834;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// characterSlot[]
struct characterSlotU5BU5D_t2121073899;
// System.Nullable`1<UnityEngine.Vector2>[]
struct Nullable_1U5BU5D_t3148003288;
// System.Action`1<Vuforia.VuforiaBehaviour>
struct Action_1_t2324316135;
// Vuforia.Trackable
struct Trackable_t2451999991;
// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
struct List_1_t2968050330;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// UnityEngine.UI.ToggleGroup
struct ToggleGroup_t123837990;
// UnityEngine.UI.Toggle/ToggleEvent
struct ToggleEvent_t1873685584;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// Vuforia.VuMarkTarget
struct VuMarkTarget_t1129573803;
// UnityEngine.UI.FontData
struct FontData_t746620069;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButtonBehaviour>
struct Dictionary_2_t325039782;

extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3525588294;
extern String_t* _stringLiteral4188196043;
extern const uint32_t AboutScreen_OnStartAR_m2896384759_MetadataUsageId;
extern RuntimeClass* Input_t1431474628_il2cpp_TypeInfo_var;
extern const uint32_t AboutScreen_Update_m1006106731_MetadataUsageId;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisRenderer_t2627027031_m2222376672_RuntimeMethod_var;
extern const uint32_t animateTexture_Start_m3979085971_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Item_m1945629867_RuntimeMethod_var;
extern const uint32_t animateTexture_changeToTexture_m3561338433_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Count_m314570365_RuntimeMethod_var;
extern const uint32_t animateTexture_changeToNextTexture_m2676168445_MetadataUsageId;
extern RuntimeClass* U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170_il2cpp_TypeInfo_var;
extern const uint32_t AsyncSceneLoader_LoadNextSceneAfter_m1751933459_MetadataUsageId;
extern RuntimeClass* WaitForSeconds_t1699091251_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoadNextSceneAfterU3Ec__Iterator0_MoveNext_m3855908786_MetadataUsageId;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CLoadNextSceneAfterU3Ec__Iterator0_Reset_m3376656304_RuntimeMethod_var;
extern const uint32_t U3CLoadNextSceneAfterU3Ec__Iterator0_Reset_m3376656304_MetadataUsageId;
extern String_t* _stringLiteral3454842849;
extern String_t* _stringLiteral133445133;
extern const uint32_t buttonColorToggle__ctor_m3550269698_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponentInChildren_TisText_t1901882714_m396351542_RuntimeMethod_var;
extern const uint32_t buttonColorToggle_Start_m2208318078_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var;
extern String_t* _stringLiteral3452614528;
extern const uint32_t buttonColorToggle_toggleColor_m646326456_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m3797380788_RuntimeMethod_var;
extern String_t* _stringLiteral537141514;
extern const uint32_t buttonColorToggle_checkIfOn_m1439767701_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisAnimation_t3648466861_m3766528019_RuntimeMethod_var;
extern const uint32_t buttonTriggerAnimation_PlayAnimation_m1140260127_MetadataUsageId;
extern RuntimeClass* U3CAnimationCooldownU3Ec__Iterator0_t1141888348_il2cpp_TypeInfo_var;
extern const uint32_t buttonTriggerAnimation_AnimationCooldown_m4119330121_MetadataUsageId;
extern const uint32_t U3CAnimationCooldownU3Ec__Iterator0_MoveNext_m786066803_MetadataUsageId;
extern const RuntimeMethod* U3CAnimationCooldownU3Ec__Iterator0_Reset_m4271818490_RuntimeMethod_var;
extern const uint32_t U3CAnimationCooldownU3Ec__Iterator0_Reset_m4271818490_MetadataUsageId;
extern RuntimeClass* CameraController_t3346819214_il2cpp_TypeInfo_var;
extern RuntimeClass* Func_2_t528868469_il2cpp_TypeInfo_var;
extern const RuntimeMethod* CameraController_U3CUpdateU3Em__0_m3793165627_RuntimeMethod_var;
extern const RuntimeMethod* Func_2__ctor_m3243656238_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Select_TisTouch_t1921856868_TisVector2_t2156229523_m1617180933_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_ToArray_TisVector2_t2156229523_m3384610930_RuntimeMethod_var;
extern const uint32_t CameraController_Update_m2075811523_MetadataUsageId;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern const uint32_t CameraController_UpdateRotate_m1805720568_MetadataUsageId;
extern const uint32_t CameraController_UpdatePanning_m509325418_MetadataUsageId;
extern RuntimeClass* VuforiaARController_t1876945237_il2cpp_TypeInfo_var;
extern RuntimeClass* Action_t1264377477_il2cpp_TypeInfo_var;
extern RuntimeClass* Action_1_t269755560_il2cpp_TypeInfo_var;
extern const RuntimeMethod* CameraFocusController_OnVuforiaStarted_m1082443761_RuntimeMethod_var;
extern const RuntimeMethod* CameraFocusController_OnPaused_m3022290520_RuntimeMethod_var;
extern const RuntimeMethod* Action_1__ctor_m2079027267_RuntimeMethod_var;
extern const uint32_t CameraFocusController_Start_m4173619726_MetadataUsageId;
extern RuntimeClass* CameraDevice_t960297568_il2cpp_TypeInfo_var;
extern const uint32_t CameraFocusController_OnVuforiaStarted_m1082443761_MetadataUsageId;
extern const uint32_t CameraFocusController_OnPaused_m3022290520_MetadataUsageId;
extern const RuntimeMethod* CameraSettings_OnVuforiaStarted_m924645186_RuntimeMethod_var;
extern const RuntimeMethod* CameraSettings_OnPaused_m1743728020_RuntimeMethod_var;
extern const uint32_t CameraSettings_Start_m1913056950_MetadataUsageId;
extern RuntimeClass* Boolean_t97287965_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral328110920;
extern String_t* _stringLiteral3558393413;
extern const uint32_t CameraSettings_SwitchFlashTorch_m1540789541_MetadataUsageId;
extern String_t* _stringLiteral1601691866;
extern String_t* _stringLiteral1647848890;
extern String_t* _stringLiteral3666098040;
extern const uint32_t CameraSettings_SwitchAutofocus_m1104105893_MetadataUsageId;
extern const uint32_t CameraSettings_TriggerAutofocusEvent_m898525935_MetadataUsageId;
extern const uint32_t CameraSettings_OnPaused_m1743728020_MetadataUsageId;
extern RuntimeClass* U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229_il2cpp_TypeInfo_var;
extern const uint32_t CameraSettings_RestoreOriginalFocusMode_m3433876528_MetadataUsageId;
extern RuntimeClass* CameraDirection_t637748435_il2cpp_TypeInfo_var;
extern const RuntimeMethod* ITrackerManager_GetTracker_TisObjectTracker_t4177997237_m1548597255_RuntimeMethod_var;
extern String_t* _stringLiteral4178167562;
extern String_t* _stringLiteral3976947749;
extern String_t* _stringLiteral1322537578;
extern const uint32_t CameraSettings_RestartCamera_m1627822698_MetadataUsageId;
extern const uint32_t U3CRestoreOriginalFocusModeU3Ec__Iterator0_MoveNext_m3953872261_MetadataUsageId;
extern const RuntimeMethod* U3CRestoreOriginalFocusModeU3Ec__Iterator0_Reset_m884766291_RuntimeMethod_var;
extern const uint32_t U3CRestoreOriginalFocusModeU3Ec__Iterator0_Reset_m884766291_MetadataUsageId;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern const uint32_t CharacterCycler_Start_m401767514_MetadataUsageId;
extern const uint32_t DefaultInitializationErrorHandler__ctor_m2145257936_MetadataUsageId;
extern RuntimeClass* VuforiaRuntime_t1949122020_il2cpp_TypeInfo_var;
extern RuntimeClass* Action_1_t3593217305_il2cpp_TypeInfo_var;
extern const RuntimeMethod* DefaultInitializationErrorHandler_OnVuforiaInitializationError_m512807497_RuntimeMethod_var;
extern const RuntimeMethod* Action_1__ctor_m2713332384_RuntimeMethod_var;
extern const uint32_t DefaultInitializationErrorHandler_Awake_m1713298888_MetadataUsageId;
extern RuntimeClass* WindowFunction_t3146511083_il2cpp_TypeInfo_var;
extern RuntimeClass* GUI_t1624858472_il2cpp_TypeInfo_var;
extern const RuntimeMethod* DefaultInitializationErrorHandler_DrawWindowContent_m2208378571_RuntimeMethod_var;
extern const uint32_t DefaultInitializationErrorHandler_OnGUI_m2338842741_MetadataUsageId;
extern const uint32_t DefaultInitializationErrorHandler_OnDestroy_m3668093536_MetadataUsageId;
extern String_t* _stringLiteral2016908147;
extern String_t* _stringLiteral3483484711;
extern const uint32_t DefaultInitializationErrorHandler_DrawWindowContent_m2208378571_MetadataUsageId;
extern RuntimeClass* InitError_t3420749710_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3279329212;
extern String_t* _stringLiteral3325583105;
extern String_t* _stringLiteral1253325676;
extern String_t* _stringLiteral2959890895;
extern String_t* _stringLiteral2293327149;
extern String_t* _stringLiteral2746077084;
extern String_t* _stringLiteral2746058527;
extern String_t* _stringLiteral491174246;
extern String_t* _stringLiteral3183081100;
extern String_t* _stringLiteral868600955;
extern String_t* _stringLiteral3122929577;
extern String_t* _stringLiteral3567432369;
extern String_t* _stringLiteral229317972;
extern String_t* _stringLiteral3452614641;
extern String_t* _stringLiteral2072581803;
extern String_t* _stringLiteral2642543365;
extern String_t* _stringLiteral3752705136;
extern String_t* _stringLiteral3453007782;
extern const uint32_t DefaultInitializationErrorHandler_SetErrorCode_m599033302_MetadataUsageId;
extern RuntimeClass* VuforiaConfiguration_t1763229349_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1431967569;
extern String_t* _stringLiteral3797279721;
extern String_t* _stringLiteral1108443480;
extern String_t* _stringLiteral2072975055;
extern String_t* _stringLiteral1498400317;
extern const uint32_t DefaultInitializationErrorHandler_getKeyInfo_m1864640064_MetadataUsageId;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern RuntimeClass* GUIStyle_t3956901511_il2cpp_TypeInfo_var;
extern RuntimeClass* RectOffset_t1369453676_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Resources_GetBuiltinResource_TisFont_t1956802104_m2738776830_RuntimeMethod_var;
extern String_t* _stringLiteral2974894664;
extern const uint32_t DefaultInitializationErrorHandler_SetupGUIStyles_m3863535424_MetadataUsageId;
extern RuntimeClass* Texture2D_t3840446185_il2cpp_TypeInfo_var;
extern const uint32_t DefaultInitializationErrorHandler_CreateSinglePixelTexture_m424000749_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisTrackableBehaviour_t1113559212_m1736119408_RuntimeMethod_var;
extern const uint32_t DefaultTrackableEventHandler_Start_m796446126_MetadataUsageId;
extern const uint32_t DefaultTrackableEventHandler_OnDestroy_m1080906236_MetadataUsageId;
extern String_t* _stringLiteral3820270571;
extern String_t* _stringLiteral3073488411;
extern String_t* _stringLiteral3483481617;
extern const uint32_t DefaultTrackableEventHandler_OnTrackableStateChanged_m77027111_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisonScanController_t1472212342_m2197168753_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponentsInChildren_TisRenderer_t2627027031_m2673895911_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponentsInChildren_TisCollider_t1773347010_m2667952426_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponentsInChildren_TisCanvas_t3310196443_m1457345007_RuntimeMethod_var;
extern const uint32_t DefaultTrackableEventHandler_OnTrackingFound_m4202593607_MetadataUsageId;
extern const uint32_t DefaultTrackableEventHandler_OnTrackingLost_m424172778_MetadataUsageId;
extern const uint32_t destoryOnAwake_Awake_m2265884017_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponentInChildren_TisAnimation_t3648466861_m4170904281_RuntimeMethod_var;
extern const uint32_t EnableMouthAnim_Start_m2930014436_MetadataUsageId;
extern RuntimeClass* Text_t1901882714_il2cpp_TypeInfo_var;
extern const uint32_t FpsCount__ctor_m3739041898_MetadataUsageId;
extern RuntimeClass* Single_t1397266774_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3990009627;
extern const uint32_t FpsCount_Update_m3597445316_MetadataUsageId;
extern const RuntimeMethod* FrameRateSettings_OnVuforiaStarted_m2069567078_RuntimeMethod_var;
extern const uint32_t FrameRateSettings_Start_m580618101_MetadataUsageId;
extern RuntimeClass* VuforiaRenderer_t3433045970_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3233180876;
extern String_t* _stringLiteral4120677989;
extern const uint32_t FrameRateSettings_OnVuforiaStarted_m2069567078_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponentsInParent_TisCanvas_t3310196443_m1039379662_RuntimeMethod_var;
extern const RuntimeMethod* InitErrorHandler_OnInitError_m2884619828_RuntimeMethod_var;
extern const uint32_t InitErrorHandler_Awake_m3752167262_MetadataUsageId;
extern String_t* _stringLiteral3258121531;
extern String_t* _stringLiteral3820000162;
extern String_t* _stringLiteral2293294349;
extern String_t* _stringLiteral2177475950;
extern String_t* _stringLiteral3262298861;
extern String_t* _stringLiteral3078096941;
extern String_t* _stringLiteral3034338515;
extern const uint32_t InitErrorHandler_ShowErrorMessage_m1141628225_MetadataUsageId;
extern const uint32_t LoadingScreen_Update_m69320718_MetadataUsageId;
extern const RuntimeMethod* Object_FindObjectsOfType_TisRawImage_t3182918964_m2400832436_RuntimeMethod_var;
extern String_t* _stringLiteral1395798546;
extern const uint32_t LoadingScreen_FindSpinnerImage_m2988402308_MetadataUsageId;
extern RuntimeClass* GameObject_t1113636619_il2cpp_TypeInfo_var;
extern RuntimeClass* Math3d_t1427253148_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral2613755077;
extern String_t* _stringLiteral2078730823;
extern const uint32_t Math3d_Init_m2456345004_MetadataUsageId;
extern const uint32_t Math3d_AddVectorLength_m3019376001_MetadataUsageId;
extern const uint32_t Math3d_SetVectorLength_m3782886289_MetadataUsageId;
extern RuntimeClass* Quaternion_t2301928331_il2cpp_TypeInfo_var;
extern const uint32_t Math3d_SubtractRotation_m1474400458_MetadataUsageId;
extern const uint32_t Math3d_PlanePlaneIntersection_m1345259878_MetadataUsageId;
extern const uint32_t Math3d_LinePlaneIntersection_m869610868_MetadataUsageId;
extern const uint32_t Math3d_LineLineIntersection_m2101611845_MetadataUsageId;
extern const uint32_t Math3d_ClosestPointsOnTwoLines_m2440915867_MetadataUsageId;
extern const uint32_t Math3d_AreQuaternionsClose_m3116464126_MetadataUsageId;
extern const uint32_t Math3d_ProjectPointOnLine_m4192705973_MetadataUsageId;
extern const uint32_t Math3d_ProjectPointOnLineSegment_m2179139403_MetadataUsageId;
extern const uint32_t Math3d_ProjectPointOnPlane_m447207244_MetadataUsageId;
extern const uint32_t Math3d_ProjectVectorOnPlane_m3889860530_MetadataUsageId;
extern const uint32_t Math3d_SignedDistancePlanePoint_m2566831789_MetadataUsageId;
extern const uint32_t Math3d_SignedDotProduct_m2541652821_MetadataUsageId;
extern const uint32_t Math3d_AngleVectorPlane_m4246505615_MetadataUsageId;
extern const uint32_t Math3d_DotProductAngle_m4066372586_MetadataUsageId;
extern const uint32_t Math3d_PlaneFrom3Points_m1928210570_MetadataUsageId;
extern const uint32_t Math3d_GetForwardVector_m983852590_MetadataUsageId;
extern const uint32_t Math3d_GetUpVector_m2891280427_MetadataUsageId;
extern const uint32_t Math3d_GetRightVector_m493994844_MetadataUsageId;
extern RuntimeClass* Vector4_t3319028937_il2cpp_TypeInfo_var;
extern const uint32_t Math3d_QuaternionFromMatrix_m3657783017_MetadataUsageId;
extern const uint32_t Math3d_LookRotationExtended_m2189285815_MetadataUsageId;
extern const uint32_t Math3d_TransformWithParent_m2022261480_MetadataUsageId;
extern const uint32_t Math3d_PreciseAlign_m2550822458_MetadataUsageId;
extern const uint32_t Math3d_VectorsToTransform_m1296114369_MetadataUsageId;
extern const uint32_t Math3d_PointOnWhichSideOfLineSegment_m3215589412_MetadataUsageId;
extern const uint32_t Math3d_MouseDistanceToLine_m163400490_MetadataUsageId;
extern const uint32_t Math3d_MouseDistanceToCircle_m2071200245_MetadataUsageId;
extern const uint32_t Math3d_IsLineInRectangle_m2110013125_MetadataUsageId;
extern const uint32_t Math3d_IsPointInRectangle_m50583159_MetadataUsageId;
extern const uint32_t Math3d_AreLineSegmentsCrossing_m616452860_MetadataUsageId;
extern const uint32_t Math3d_AverageQuaternion_m2118685977_MetadataUsageId;
extern const uint32_t MenuAnimator__ctor_m2002699033_MetadataUsageId;
extern const RuntimeMethod* Object_FindObjectOfType_TisMenuOptions_t1951716431_m1050906446_RuntimeMethod_var;
extern const uint32_t MenuAnimator_Start_m1688760679_MetadataUsageId;
extern const uint32_t MenuAnimator_Update_m3695532741_MetadataUsageId;
extern const uint32_t MenuAnimator_Show_m982174260_MetadataUsageId;
extern const RuntimeMethod* Object_FindObjectOfType_TisCameraSettings_t3152619780_m4203198741_RuntimeMethod_var;
extern const RuntimeMethod* Object_FindObjectOfType_TisTrackableSettings_t2862243993_m1852430619_RuntimeMethod_var;
extern const RuntimeMethod* Object_FindObjectOfType_TisMenuAnimator_t2112910832_m1830680649_RuntimeMethod_var;
extern const RuntimeMethod* MenuOptions_OnPaused_m1134986695_RuntimeMethod_var;
extern const uint32_t MenuOptions_Start_m2289807458_MetadataUsageId;
extern String_t* _stringLiteral3521912105;
extern const uint32_t MenuOptions_ShowAboutPage_m3278612116_MetadataUsageId;
extern const RuntimeMethod* MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125_RuntimeMethod_var;
extern String_t* _stringLiteral683363693;
extern const uint32_t MenuOptions_ToggleAutofocus_m677591208_MetadataUsageId;
extern String_t* _stringLiteral351316669;
extern const uint32_t MenuOptions_ToggleTorch_m1503051540_MetadataUsageId;
extern const uint32_t MenuOptions_SelectCamera_m2158115138_MetadataUsageId;
extern String_t* _stringLiteral1531511922;
extern const uint32_t MenuOptions_ToggleExtendedTracking_m3362773293_MetadataUsageId;
extern const uint32_t MenuOptions_ActivateDataset_m942511626_MetadataUsageId;
extern String_t* _stringLiteral3549741419;
extern String_t* _stringLiteral3872780051;
extern String_t* _stringLiteral3544302583;
extern String_t* _stringLiteral451598385;
extern const uint32_t MenuOptions_UpdateUI_m2695051465_MetadataUsageId;
extern const uint32_t MenuOptions_CloseMenu_m2198615225_MetadataUsageId;
extern const uint32_t MenuOptions_OnPaused_m1134986695_MetadataUsageId;
extern String_t* _stringLiteral3058346340;
extern String_t* _stringLiteral3974699883;
extern const uint32_t ModelSwap_Start_m1562037199_MetadataUsageId;
extern const uint32_t ModelSwap_Update_m1133952060_MetadataUsageId;
extern RuntimeClass* onScanController_t1472212342_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TischaracterLoader_t1326495876_m1035246574_RuntimeMethod_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t1113636619_m4130575780_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponentInChildren_TisAnimation_t3648466861_m235625869_RuntimeMethod_var;
extern String_t* _stringLiteral778478698;
extern String_t* _stringLiteral3670285835;
extern String_t* _stringLiteral2682697484;
extern String_t* _stringLiteral321671482;
extern const uint32_t onScanController_Awake_m195501164_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisAudioSource_t3935305588_m625814604_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var;
extern String_t* _stringLiteral4045970737;
extern const uint32_t onScanController_Update_m190167415_MetadataUsageId;
extern RuntimeClass* IEnumerator_t1853284238_il2cpp_TypeInfo_var;
extern RuntimeClass* Transform_t3600365921_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t3640265483_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TisCanvas_t3310196443_m1808013672_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisText_t1901882714_m2114913816_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TissymbolPrefabDefiner_t2798204667_m4248518593_RuntimeMethod_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t1113636619_m3215236302_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var;
extern String_t* _stringLiteral2952584548;
extern String_t* _stringLiteral3987835886;
extern String_t* _stringLiteral2002597352;
extern String_t* _stringLiteral1035174073;
extern String_t* _stringLiteral1628612613;
extern const uint32_t onScanController_OnEnable_m2731938639_MetadataUsageId;
extern RuntimeClass* U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349_il2cpp_TypeInfo_var;
extern const uint32_t onScanController_WaitAndPlayShowTail_m1087383620_MetadataUsageId;
extern const uint32_t U3CWaitAndPlayShowTailU3Ec__Iterator0_MoveNext_m3400634910_MetadataUsageId;
extern const RuntimeMethod* U3CWaitAndPlayShowTailU3Ec__Iterator0_Reset_m351113022_RuntimeMethod_var;
extern const uint32_t U3CWaitAndPlayShowTailU3Ec__Iterator0_Reset_m351113022_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Item_m3743125852_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m2812834599_RuntimeMethod_var;
extern const uint32_t pageTurn_turnToNextPage_m922167039_MetadataUsageId;
extern const uint32_t pageTurn_turnToPage_m4215582371_MetadataUsageId;
extern String_t* _stringLiteral3588954345;
extern String_t* _stringLiteral1836739017;
extern const uint32_t setSettings_Start_m1838433502_MetadataUsageId;
extern const uint32_t setSettings_toggleSetting_m2511861156_MetadataUsageId;
extern const uint32_t setSettings_toggleSettingIfItCantCeOnAtTheSameTimeAsOtherSetting_m3029310343_MetadataUsageId;
extern const uint32_t setSettings_checkIfOn_m1363969882_MetadataUsageId;
extern const uint32_t settingsManager_Awake_m2253256640_MetadataUsageId;
extern String_t* _stringLiteral3964750823;
extern const uint32_t settingsManager_Start_m3801860366_MetadataUsageId;
extern String_t* _stringLiteral3787301066;
extern const uint32_t settingsManager_toggleSetting_m1197959296_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Count_m3659879520_RuntimeMethod_var;
extern String_t* _stringLiteral1938806281;
extern const uint32_t settingsManager_findSetting_m2352407313_MetadataUsageId;
extern const uint32_t settingsManager_mute_m3259797164_MetadataUsageId;
extern const RuntimeMethod* Queue_1_get_Count_m566549244_RuntimeMethod_var;
extern const RuntimeMethod* Queue_1_Dequeue_m127687118_RuntimeMethod_var;
extern const RuntimeMethod* Queue_1_Dequeue_m3534639416_RuntimeMethod_var;
extern const RuntimeMethod* Queue_1_Enqueue_m309039112_RuntimeMethod_var;
extern const RuntimeMethod* Queue_1_Enqueue_m2554143986_RuntimeMethod_var;
extern const RuntimeMethod* Queue_1_GetEnumerator_m3198989551_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2950039238_RuntimeMethod_var;
extern const RuntimeMethod* Queue_1_Peek_m1284364542_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m459395817_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m557667343_RuntimeMethod_var;
extern const RuntimeMethod* Queue_1_GetEnumerator_m676935694_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m4117045769_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m2202395141_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m1218804216_RuntimeMethod_var;
extern const RuntimeMethod* Queue_1_get_Count_m2376062550_RuntimeMethod_var;
extern const uint32_t SmoothCamera_OnTrackablesUpdated_m2235135197_MetadataUsageId;
extern RuntimeClass* Queue_1_t2148187825_il2cpp_TypeInfo_var;
extern RuntimeClass* Queue_1_t3568572958_il2cpp_TypeInfo_var;
extern RuntimeClass* UnityAction_2_t1262235195_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Queue_1__ctor_m1117100023_RuntimeMethod_var;
extern const RuntimeMethod* Queue_1__ctor_m3566777400_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisVuforiaBehaviour_t2151848540_m3060796760_RuntimeMethod_var;
extern const RuntimeMethod* SmoothCamera_OnInitialized_m1148221488_RuntimeMethod_var;
extern const RuntimeMethod* SmoothCamera_OnTrackablesUpdated_m2235135197_RuntimeMethod_var;
extern const RuntimeMethod* SmoothCamera_UnRegister_m1885363057_RuntimeMethod_var;
extern const RuntimeMethod* UnityAction_2__ctor_m608474170_RuntimeMethod_var;
extern const uint32_t SmoothCamera_Start_m244894020_MetadataUsageId;
extern String_t* _stringLiteral3886504256;
extern String_t* _stringLiteral2619271551;
extern const uint32_t SmoothCamera_UnRegister_m1885363057_MetadataUsageId;
extern const uint32_t TapHandler_Start_m554074027_MetadataUsageId;
extern const uint32_t TapHandler_Update_m2328515667_MetadataUsageId;
extern const uint32_t TapHandler_HandleTap_m1322678531_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponentInChildren_TisCameraSettings_t3152619780_m2209036194_RuntimeMethod_var;
extern const uint32_t TapHandler_OnSingleTapConfirmed_m3372274260_MetadataUsageId;
extern const uint32_t TapHandler_OnDoubleTap_m2928608177_MetadataUsageId;
extern RuntimeClass* Nullable_1U5BU5D_t3148003288_il2cpp_TypeInfo_var;
extern const uint32_t TouchCamera__ctor_m2214619079_MetadataUsageId;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2U5BU5D_t1457185986_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Nullable_1_get_HasValue_m1317965445_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1__ctor_m680077965_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1_GetValueOrDefault_m3722452406_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1_get_Value_m963759207_RuntimeMethod_var;
extern const uint32_t TouchCamera_Update_m4288923361_MetadataUsageId;
extern RuntimeClass* ITrackerManager_t607206903_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerable_1_t93412101_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_1_t1546129680_il2cpp_TypeInfo_var;
extern RuntimeClass* ImageTargetBehaviour_t2200418350_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectTarget_t3212252422_il2cpp_TypeInfo_var;
extern RuntimeClass* MultiTargetBehaviour_t2061511750_il2cpp_TypeInfo_var;
extern RuntimeClass* CylinderTargetBehaviour_t822809409_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectTargetBehaviour_t728125005_il2cpp_TypeInfo_var;
extern RuntimeClass* VuMarkBehaviour_t1178230459_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral2304731189;
extern const uint32_t TrackableSettings_SwitchExtendedTracking_m1275921245_MetadataUsageId;
extern const RuntimeMethod* Enumerable_ToList_TisDataSet_t3286034874_m1984750843_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m3978133574_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_ElementAt_TisDataSet_t3286034874_m1304043092_RuntimeMethod_var;
extern String_t* _stringLiteral3452614529;
extern String_t* _stringLiteral3932363835;
extern const uint32_t TrackableSettings_GetActiveDatasetName_m1771968640_MetadataUsageId;
extern RuntimeClass* IEnumerable_1_t2265887763_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_1_t3718605342_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_GetEnumerator_m6144716_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m1064371760_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m1234730920_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m1247034833_RuntimeMethod_var;
extern const uint32_t TrackableSettings_ActivateDataSet_m4118878993_MetadataUsageId;
extern const uint32_t triggerAudio_OnEnable_m4092205599_MetadataUsageId;
extern RuntimeClass* U3CSoundCooldownU3Ec__Iterator0_t604438267_il2cpp_TypeInfo_var;
extern const uint32_t triggerAudio_SoundCooldown_m3107703534_MetadataUsageId;
extern const uint32_t U3CSoundCooldownU3Ec__Iterator0_MoveNext_m3232324018_MetadataUsageId;
extern const RuntimeMethod* U3CSoundCooldownU3Ec__Iterator0_Reset_m1882823724_RuntimeMethod_var;
extern const uint32_t U3CSoundCooldownU3Ec__Iterator0_Reset_m1882823724_MetadataUsageId;
extern String_t* _stringLiteral1271059651;
extern const uint32_t tutorialManager_Awake_m2139072952_MetadataUsageId;
extern const uint32_t tutorialManager_Show_m1645464898_MetadataUsageId;
struct GUIStyle_t3956901511_marshaled_pinvoke;
struct GUIStyle_t3956901511_marshaled_com;
struct GUIStyleState_t1397964415_marshaled_pinvoke;
struct GUIStyleState_t1397964415_marshaled_com;
struct RectOffset_t1369453676_marshaled_com;

struct Vector2U5BU5D_t1457185986;
struct TouchU5BU5D_t1849554061;
struct GameObjectU5BU5D_t3328599146;
struct ObjectU5BU5D_t2843939325;
struct RendererU5BU5D_t3210418286;
struct ColliderU5BU5D_t4234922487;
struct CanvasU5BU5D_t682926938;
struct RawImageU5BU5D_t1576731517;
struct characterSlotU5BU5D_t2121073899;
struct SpriteU5BU5D_t2581906349;
struct Nullable_1U5BU5D_t3148003288;
struct CharU5BU5D_t3528271667;


#ifndef U3CMODULEU3E_T692745550_H
#define U3CMODULEU3E_T692745550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745550 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745550_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef YIELDINSTRUCTION_T403091072_H
#define YIELDINSTRUCTION_T403091072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t403091072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T403091072_H
#ifndef U3CSOUNDCOOLDOWNU3EC__ITERATOR0_T604438267_H
#define U3CSOUNDCOOLDOWNU3EC__ITERATOR0_T604438267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// triggerAudio/<SoundCooldown>c__Iterator0
struct  U3CSoundCooldownU3Ec__Iterator0_t604438267  : public RuntimeObject
{
public:
	// System.Single triggerAudio/<SoundCooldown>c__Iterator0::<animationLength>__0
	float ___U3CanimationLengthU3E__0_0;
	// triggerAudio triggerAudio/<SoundCooldown>c__Iterator0::$this
	triggerAudio_t1885395141 * ___U24this_1;
	// System.Object triggerAudio/<SoundCooldown>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean triggerAudio/<SoundCooldown>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 triggerAudio/<SoundCooldown>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CanimationLengthU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSoundCooldownU3Ec__Iterator0_t604438267, ___U3CanimationLengthU3E__0_0)); }
	inline float get_U3CanimationLengthU3E__0_0() const { return ___U3CanimationLengthU3E__0_0; }
	inline float* get_address_of_U3CanimationLengthU3E__0_0() { return &___U3CanimationLengthU3E__0_0; }
	inline void set_U3CanimationLengthU3E__0_0(float value)
	{
		___U3CanimationLengthU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CSoundCooldownU3Ec__Iterator0_t604438267, ___U24this_1)); }
	inline triggerAudio_t1885395141 * get_U24this_1() const { return ___U24this_1; }
	inline triggerAudio_t1885395141 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(triggerAudio_t1885395141 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CSoundCooldownU3Ec__Iterator0_t604438267, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CSoundCooldownU3Ec__Iterator0_t604438267, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CSoundCooldownU3Ec__Iterator0_t604438267, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSOUNDCOOLDOWNU3EC__ITERATOR0_T604438267_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CANIMATIONCOOLDOWNU3EC__ITERATOR0_T1141888348_H
#define U3CANIMATIONCOOLDOWNU3EC__ITERATOR0_T1141888348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// buttonTriggerAnimation/<AnimationCooldown>c__Iterator0
struct  U3CAnimationCooldownU3Ec__Iterator0_t1141888348  : public RuntimeObject
{
public:
	// System.Single buttonTriggerAnimation/<AnimationCooldown>c__Iterator0::<animationLength>__0
	float ___U3CanimationLengthU3E__0_0;
	// buttonTriggerAnimation buttonTriggerAnimation/<AnimationCooldown>c__Iterator0::$this
	buttonTriggerAnimation_t4121295644 * ___U24this_1;
	// System.Object buttonTriggerAnimation/<AnimationCooldown>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean buttonTriggerAnimation/<AnimationCooldown>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 buttonTriggerAnimation/<AnimationCooldown>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CanimationLengthU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimationCooldownU3Ec__Iterator0_t1141888348, ___U3CanimationLengthU3E__0_0)); }
	inline float get_U3CanimationLengthU3E__0_0() const { return ___U3CanimationLengthU3E__0_0; }
	inline float* get_address_of_U3CanimationLengthU3E__0_0() { return &___U3CanimationLengthU3E__0_0; }
	inline void set_U3CanimationLengthU3E__0_0(float value)
	{
		___U3CanimationLengthU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAnimationCooldownU3Ec__Iterator0_t1141888348, ___U24this_1)); }
	inline buttonTriggerAnimation_t4121295644 * get_U24this_1() const { return ___U24this_1; }
	inline buttonTriggerAnimation_t4121295644 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(buttonTriggerAnimation_t4121295644 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CAnimationCooldownU3Ec__Iterator0_t1141888348, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CAnimationCooldownU3Ec__Iterator0_t1141888348, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CAnimationCooldownU3Ec__Iterator0_t1141888348, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATIONCOOLDOWNU3EC__ITERATOR0_T1141888348_H
#ifndef ARCONTROLLER_T116632334_H
#define ARCONTROLLER_T116632334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ARController
struct  ARController_t116632334  : public RuntimeObject
{
public:
	// Vuforia.VuforiaBehaviour Vuforia.ARController::mVuforiaBehaviour
	VuforiaBehaviour_t2151848540 * ___mVuforiaBehaviour_0;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviour_0() { return static_cast<int32_t>(offsetof(ARController_t116632334, ___mVuforiaBehaviour_0)); }
	inline VuforiaBehaviour_t2151848540 * get_mVuforiaBehaviour_0() const { return ___mVuforiaBehaviour_0; }
	inline VuforiaBehaviour_t2151848540 ** get_address_of_mVuforiaBehaviour_0() { return &___mVuforiaBehaviour_0; }
	inline void set_mVuforiaBehaviour_0(VuforiaBehaviour_t2151848540 * value)
	{
		___mVuforiaBehaviour_0 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCONTROLLER_T116632334_H
#ifndef U3CLOADNEXTSCENEAFTERU3EC__ITERATOR0_T994227170_H
#define U3CLOADNEXTSCENEAFTERU3EC__ITERATOR0_T994227170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0
struct  U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170  : public RuntimeObject
{
public:
	// System.Single AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0::seconds
	float ___seconds_0;
	// System.Object AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_seconds_0() { return static_cast<int32_t>(offsetof(U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170, ___seconds_0)); }
	inline float get_seconds_0() const { return ___seconds_0; }
	inline float* get_address_of_seconds_0() { return &___seconds_0; }
	inline void set_seconds_0(float value)
	{
		___seconds_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADNEXTSCENEAFTERU3EC__ITERATOR0_T994227170_H
#ifndef SETTINGSLIST_T1883434577_H
#define SETTINGSLIST_T1883434577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsList
struct  SettingsList_t1883434577  : public RuntimeObject
{
public:
	// System.String SettingsList::SettingName
	String_t* ___SettingName_0;
	// System.Boolean SettingsList::IsActive
	bool ___IsActive_1;

public:
	inline static int32_t get_offset_of_SettingName_0() { return static_cast<int32_t>(offsetof(SettingsList_t1883434577, ___SettingName_0)); }
	inline String_t* get_SettingName_0() const { return ___SettingName_0; }
	inline String_t** get_address_of_SettingName_0() { return &___SettingName_0; }
	inline void set_SettingName_0(String_t* value)
	{
		___SettingName_0 = value;
		Il2CppCodeGenWriteBarrier((&___SettingName_0), value);
	}

	inline static int32_t get_offset_of_IsActive_1() { return static_cast<int32_t>(offsetof(SettingsList_t1883434577, ___IsActive_1)); }
	inline bool get_IsActive_1() const { return ___IsActive_1; }
	inline bool* get_address_of_IsActive_1() { return &___IsActive_1; }
	inline void set_IsActive_1(bool value)
	{
		___IsActive_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSLIST_T1883434577_H
#ifndef LIST_1_T3355509319_H
#define LIST_1_T3355509319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<SettingsList>
struct  List_1_t3355509319  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SettingsListU5BU5D_t4213907788* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3355509319, ____items_1)); }
	inline SettingsListU5BU5D_t4213907788* get__items_1() const { return ____items_1; }
	inline SettingsListU5BU5D_t4213907788** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SettingsListU5BU5D_t4213907788* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3355509319, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3355509319, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3355509319_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	SettingsListU5BU5D_t4213907788* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3355509319_StaticFields, ___EmptyArray_4)); }
	inline SettingsListU5BU5D_t4213907788* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline SettingsListU5BU5D_t4213907788** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(SettingsListU5BU5D_t4213907788* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3355509319_H
#ifndef LIST_1_T463142320_H
#define LIST_1_T463142320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Vuforia.DataSet>
struct  List_1_t463142320  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	DataSetU5BU5D_t4052198943* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t463142320, ____items_1)); }
	inline DataSetU5BU5D_t4052198943* get__items_1() const { return ____items_1; }
	inline DataSetU5BU5D_t4052198943** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(DataSetU5BU5D_t4052198943* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t463142320, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t463142320, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t463142320_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	DataSetU5BU5D_t4052198943* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t463142320_StaticFields, ___EmptyArray_4)); }
	inline DataSetU5BU5D_t4052198943* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline DataSetU5BU5D_t4052198943** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(DataSetU5BU5D_t4052198943* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T463142320_H
#ifndef LIST_1_T2585711361_H
#define LIST_1_T2585711361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct  List_1_t2585711361  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GameObjectU5BU5D_t3328599146* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2585711361, ____items_1)); }
	inline GameObjectU5BU5D_t3328599146* get__items_1() const { return ____items_1; }
	inline GameObjectU5BU5D_t3328599146** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GameObjectU5BU5D_t3328599146* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2585711361, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2585711361, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2585711361_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	GameObjectU5BU5D_t3328599146* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2585711361_StaticFields, ___EmptyArray_4)); }
	inline GameObjectU5BU5D_t3328599146* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(GameObjectU5BU5D_t3328599146* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2585711361_H
#ifndef TRACKER_T2709586299_H
#define TRACKER_T2709586299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Tracker
struct  Tracker_t2709586299  : public RuntimeObject
{
public:
	// System.Boolean Vuforia.Tracker::<IsActive>k__BackingField
	bool ___U3CIsActiveU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIsActiveU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Tracker_t2709586299, ___U3CIsActiveU3Ek__BackingField_0)); }
	inline bool get_U3CIsActiveU3Ek__BackingField_0() const { return ___U3CIsActiveU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsActiveU3Ek__BackingField_0() { return &___U3CIsActiveU3Ek__BackingField_0; }
	inline void set_U3CIsActiveU3Ek__BackingField_0(bool value)
	{
		___U3CIsActiveU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKER_T2709586299_H
#ifndef U3CWAITANDPLAYSHOWTAILU3EC__ITERATOR0_T581090349_H
#define U3CWAITANDPLAYSHOWTAILU3EC__ITERATOR0_T581090349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// onScanController/<WaitAndPlayShowTail>c__Iterator0
struct  U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349  : public RuntimeObject
{
public:
	// onScanController onScanController/<WaitAndPlayShowTail>c__Iterator0::$this
	onScanController_t1472212342 * ___U24this_0;
	// System.Object onScanController/<WaitAndPlayShowTail>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean onScanController/<WaitAndPlayShowTail>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 onScanController/<WaitAndPlayShowTail>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349, ___U24this_0)); }
	inline onScanController_t1472212342 * get_U24this_0() const { return ___U24this_0; }
	inline onScanController_t1472212342 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(onScanController_t1472212342 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITANDPLAYSHOWTAILU3EC__ITERATOR0_T581090349_H
#ifndef CHARACTERSLOT_T990761438_H
#define CHARACTERSLOT_T990761438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// characterSlot
struct  characterSlot_t990761438  : public RuntimeObject
{
public:
	// System.String characterSlot::CharacterID
	String_t* ___CharacterID_0;
	// UnityEngine.GameObject characterSlot::CharacterPrefab
	GameObject_t1113636619 * ___CharacterPrefab_1;

public:
	inline static int32_t get_offset_of_CharacterID_0() { return static_cast<int32_t>(offsetof(characterSlot_t990761438, ___CharacterID_0)); }
	inline String_t* get_CharacterID_0() const { return ___CharacterID_0; }
	inline String_t** get_address_of_CharacterID_0() { return &___CharacterID_0; }
	inline void set_CharacterID_0(String_t* value)
	{
		___CharacterID_0 = value;
		Il2CppCodeGenWriteBarrier((&___CharacterID_0), value);
	}

	inline static int32_t get_offset_of_CharacterPrefab_1() { return static_cast<int32_t>(offsetof(characterSlot_t990761438, ___CharacterPrefab_1)); }
	inline GameObject_t1113636619 * get_CharacterPrefab_1() const { return ___CharacterPrefab_1; }
	inline GameObject_t1113636619 ** get_address_of_CharacterPrefab_1() { return &___CharacterPrefab_1; }
	inline void set_CharacterPrefab_1(GameObject_t1113636619 * value)
	{
		___CharacterPrefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___CharacterPrefab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERSLOT_T990761438_H
#ifndef QUEUE_1_T3568572958_H
#define QUEUE_1_T3568572958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Queue`1<UnityEngine.Vector3>
struct  Queue_1_t3568572958  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Queue`1::_array
	Vector3U5BU5D_t1718750761* ____array_0;
	// System.Int32 System.Collections.Generic.Queue`1::_head
	int32_t ____head_1;
	// System.Int32 System.Collections.Generic.Queue`1::_tail
	int32_t ____tail_2;
	// System.Int32 System.Collections.Generic.Queue`1::_size
	int32_t ____size_3;
	// System.Int32 System.Collections.Generic.Queue`1::_version
	int32_t ____version_4;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Queue_1_t3568572958, ____array_0)); }
	inline Vector3U5BU5D_t1718750761* get__array_0() const { return ____array_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(Vector3U5BU5D_t1718750761* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__head_1() { return static_cast<int32_t>(offsetof(Queue_1_t3568572958, ____head_1)); }
	inline int32_t get__head_1() const { return ____head_1; }
	inline int32_t* get_address_of__head_1() { return &____head_1; }
	inline void set__head_1(int32_t value)
	{
		____head_1 = value;
	}

	inline static int32_t get_offset_of__tail_2() { return static_cast<int32_t>(offsetof(Queue_1_t3568572958, ____tail_2)); }
	inline int32_t get__tail_2() const { return ____tail_2; }
	inline int32_t* get_address_of__tail_2() { return &____tail_2; }
	inline void set__tail_2(int32_t value)
	{
		____tail_2 = value;
	}

	inline static int32_t get_offset_of__size_3() { return static_cast<int32_t>(offsetof(Queue_1_t3568572958, ____size_3)); }
	inline int32_t get__size_3() const { return ____size_3; }
	inline int32_t* get_address_of__size_3() { return &____size_3; }
	inline void set__size_3(int32_t value)
	{
		____size_3 = value;
	}

	inline static int32_t get_offset_of__version_4() { return static_cast<int32_t>(offsetof(Queue_1_t3568572958, ____version_4)); }
	inline int32_t get__version_4() const { return ____version_4; }
	inline int32_t* get_address_of__version_4() { return &____version_4; }
	inline void set__version_4(int32_t value)
	{
		____version_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUEUE_1_T3568572958_H
#ifndef STATEMANAGER_T1982749557_H
#define STATEMANAGER_T1982749557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.StateManager
struct  StateManager_t1982749557  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour> Vuforia.StateManager::mTrackableBehaviours
	Dictionary_2_t2272543 * ___mTrackableBehaviours_0;
	// System.Collections.Generic.List`1<System.Int32> Vuforia.StateManager::mAutomaticallyCreatedBehaviours
	List_1_t128053199 * ___mAutomaticallyCreatedBehaviours_1;
	// System.Collections.Generic.List`1<Vuforia.TrackableBehaviour> Vuforia.StateManager::mBehavioursMarkedForDeletion
	List_1_t2585633954 * ___mBehavioursMarkedForDeletion_2;
	// System.Collections.Generic.List`1<Vuforia.TrackableBehaviour> Vuforia.StateManager::mActiveTrackableBehaviours
	List_1_t2585633954 * ___mActiveTrackableBehaviours_3;
	// Vuforia.VuMarkManager Vuforia.StateManager::mVuMarkManager
	VuMarkManager_t2982459596 * ___mVuMarkManager_4;
	// Vuforia.DeviceTrackingManager Vuforia.StateManager::mDeviceTrackingManager
	DeviceTrackingManager_t3849131975 * ___mDeviceTrackingManager_5;
	// UnityEngine.GameObject Vuforia.StateManager::mCameraPositioningHelper
	GameObject_t1113636619 * ___mCameraPositioningHelper_6;
	// Vuforia.IExtendedTrackingManager Vuforia.StateManager::mExtendedTrackingManager
	RuntimeObject* ___mExtendedTrackingManager_7;
	// Vuforia.IlluminationManager Vuforia.StateManager::mIlluminationManager
	IlluminationManager_t3960931838 * ___mIlluminationManager_8;

public:
	inline static int32_t get_offset_of_mTrackableBehaviours_0() { return static_cast<int32_t>(offsetof(StateManager_t1982749557, ___mTrackableBehaviours_0)); }
	inline Dictionary_2_t2272543 * get_mTrackableBehaviours_0() const { return ___mTrackableBehaviours_0; }
	inline Dictionary_2_t2272543 ** get_address_of_mTrackableBehaviours_0() { return &___mTrackableBehaviours_0; }
	inline void set_mTrackableBehaviours_0(Dictionary_2_t2272543 * value)
	{
		___mTrackableBehaviours_0 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviours_0), value);
	}

	inline static int32_t get_offset_of_mAutomaticallyCreatedBehaviours_1() { return static_cast<int32_t>(offsetof(StateManager_t1982749557, ___mAutomaticallyCreatedBehaviours_1)); }
	inline List_1_t128053199 * get_mAutomaticallyCreatedBehaviours_1() const { return ___mAutomaticallyCreatedBehaviours_1; }
	inline List_1_t128053199 ** get_address_of_mAutomaticallyCreatedBehaviours_1() { return &___mAutomaticallyCreatedBehaviours_1; }
	inline void set_mAutomaticallyCreatedBehaviours_1(List_1_t128053199 * value)
	{
		___mAutomaticallyCreatedBehaviours_1 = value;
		Il2CppCodeGenWriteBarrier((&___mAutomaticallyCreatedBehaviours_1), value);
	}

	inline static int32_t get_offset_of_mBehavioursMarkedForDeletion_2() { return static_cast<int32_t>(offsetof(StateManager_t1982749557, ___mBehavioursMarkedForDeletion_2)); }
	inline List_1_t2585633954 * get_mBehavioursMarkedForDeletion_2() const { return ___mBehavioursMarkedForDeletion_2; }
	inline List_1_t2585633954 ** get_address_of_mBehavioursMarkedForDeletion_2() { return &___mBehavioursMarkedForDeletion_2; }
	inline void set_mBehavioursMarkedForDeletion_2(List_1_t2585633954 * value)
	{
		___mBehavioursMarkedForDeletion_2 = value;
		Il2CppCodeGenWriteBarrier((&___mBehavioursMarkedForDeletion_2), value);
	}

	inline static int32_t get_offset_of_mActiveTrackableBehaviours_3() { return static_cast<int32_t>(offsetof(StateManager_t1982749557, ___mActiveTrackableBehaviours_3)); }
	inline List_1_t2585633954 * get_mActiveTrackableBehaviours_3() const { return ___mActiveTrackableBehaviours_3; }
	inline List_1_t2585633954 ** get_address_of_mActiveTrackableBehaviours_3() { return &___mActiveTrackableBehaviours_3; }
	inline void set_mActiveTrackableBehaviours_3(List_1_t2585633954 * value)
	{
		___mActiveTrackableBehaviours_3 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveTrackableBehaviours_3), value);
	}

	inline static int32_t get_offset_of_mVuMarkManager_4() { return static_cast<int32_t>(offsetof(StateManager_t1982749557, ___mVuMarkManager_4)); }
	inline VuMarkManager_t2982459596 * get_mVuMarkManager_4() const { return ___mVuMarkManager_4; }
	inline VuMarkManager_t2982459596 ** get_address_of_mVuMarkManager_4() { return &___mVuMarkManager_4; }
	inline void set_mVuMarkManager_4(VuMarkManager_t2982459596 * value)
	{
		___mVuMarkManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkManager_4), value);
	}

	inline static int32_t get_offset_of_mDeviceTrackingManager_5() { return static_cast<int32_t>(offsetof(StateManager_t1982749557, ___mDeviceTrackingManager_5)); }
	inline DeviceTrackingManager_t3849131975 * get_mDeviceTrackingManager_5() const { return ___mDeviceTrackingManager_5; }
	inline DeviceTrackingManager_t3849131975 ** get_address_of_mDeviceTrackingManager_5() { return &___mDeviceTrackingManager_5; }
	inline void set_mDeviceTrackingManager_5(DeviceTrackingManager_t3849131975 * value)
	{
		___mDeviceTrackingManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___mDeviceTrackingManager_5), value);
	}

	inline static int32_t get_offset_of_mCameraPositioningHelper_6() { return static_cast<int32_t>(offsetof(StateManager_t1982749557, ___mCameraPositioningHelper_6)); }
	inline GameObject_t1113636619 * get_mCameraPositioningHelper_6() const { return ___mCameraPositioningHelper_6; }
	inline GameObject_t1113636619 ** get_address_of_mCameraPositioningHelper_6() { return &___mCameraPositioningHelper_6; }
	inline void set_mCameraPositioningHelper_6(GameObject_t1113636619 * value)
	{
		___mCameraPositioningHelper_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCameraPositioningHelper_6), value);
	}

	inline static int32_t get_offset_of_mExtendedTrackingManager_7() { return static_cast<int32_t>(offsetof(StateManager_t1982749557, ___mExtendedTrackingManager_7)); }
	inline RuntimeObject* get_mExtendedTrackingManager_7() const { return ___mExtendedTrackingManager_7; }
	inline RuntimeObject** get_address_of_mExtendedTrackingManager_7() { return &___mExtendedTrackingManager_7; }
	inline void set_mExtendedTrackingManager_7(RuntimeObject* value)
	{
		___mExtendedTrackingManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___mExtendedTrackingManager_7), value);
	}

	inline static int32_t get_offset_of_mIlluminationManager_8() { return static_cast<int32_t>(offsetof(StateManager_t1982749557, ___mIlluminationManager_8)); }
	inline IlluminationManager_t3960931838 * get_mIlluminationManager_8() const { return ___mIlluminationManager_8; }
	inline IlluminationManager_t3960931838 ** get_address_of_mIlluminationManager_8() { return &___mIlluminationManager_8; }
	inline void set_mIlluminationManager_8(IlluminationManager_t3960931838 * value)
	{
		___mIlluminationManager_8 = value;
		Il2CppCodeGenWriteBarrier((&___mIlluminationManager_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEMANAGER_T1982749557_H
#ifndef U3CRESTOREORIGINALFOCUSMODEU3EC__ITERATOR0_T2912012229_H
#define U3CRESTOREORIGINALFOCUSMODEU3EC__ITERATOR0_T2912012229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraSettings/<RestoreOriginalFocusMode>c__Iterator0
struct  U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229  : public RuntimeObject
{
public:
	// CameraSettings CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::$this
	CameraSettings_t3152619780 * ___U24this_0;
	// System.Object CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229, ___U24this_0)); }
	inline CameraSettings_t3152619780 * get_U24this_0() const { return ___U24this_0; }
	inline CameraSettings_t3152619780 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(CameraSettings_t3152619780 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESTOREORIGINALFOCUSMODEU3EC__ITERATOR0_T2912012229_H
#ifndef QUEUE_1_T2148187825_H
#define QUEUE_1_T2148187825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Queue`1<UnityEngine.Quaternion>
struct  Queue_1_t2148187825  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Queue`1::_array
	QuaternionU5BU5D_t2571361770* ____array_0;
	// System.Int32 System.Collections.Generic.Queue`1::_head
	int32_t ____head_1;
	// System.Int32 System.Collections.Generic.Queue`1::_tail
	int32_t ____tail_2;
	// System.Int32 System.Collections.Generic.Queue`1::_size
	int32_t ____size_3;
	// System.Int32 System.Collections.Generic.Queue`1::_version
	int32_t ____version_4;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Queue_1_t2148187825, ____array_0)); }
	inline QuaternionU5BU5D_t2571361770* get__array_0() const { return ____array_0; }
	inline QuaternionU5BU5D_t2571361770** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(QuaternionU5BU5D_t2571361770* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__head_1() { return static_cast<int32_t>(offsetof(Queue_1_t2148187825, ____head_1)); }
	inline int32_t get__head_1() const { return ____head_1; }
	inline int32_t* get_address_of__head_1() { return &____head_1; }
	inline void set__head_1(int32_t value)
	{
		____head_1 = value;
	}

	inline static int32_t get_offset_of__tail_2() { return static_cast<int32_t>(offsetof(Queue_1_t2148187825, ____tail_2)); }
	inline int32_t get__tail_2() const { return ____tail_2; }
	inline int32_t* get_address_of__tail_2() { return &____tail_2; }
	inline void set__tail_2(int32_t value)
	{
		____tail_2 = value;
	}

	inline static int32_t get_offset_of__size_3() { return static_cast<int32_t>(offsetof(Queue_1_t2148187825, ____size_3)); }
	inline int32_t get__size_3() const { return ____size_3; }
	inline int32_t* get_address_of__size_3() { return &____size_3; }
	inline void set__size_3(int32_t value)
	{
		____size_3 = value;
	}

	inline static int32_t get_offset_of__version_4() { return static_cast<int32_t>(offsetof(Queue_1_t2148187825, ____version_4)); }
	inline int32_t get__version_4() const { return ____version_4; }
	inline int32_t* get_address_of__version_4() { return &____version_4; }
	inline void set__version_4(int32_t value)
	{
		____version_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUEUE_1_T2148187825_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef LIST_1_T839070149_H
#define LIST_1_T839070149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Texture>
struct  List_1_t839070149  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TextureU5BU5D_t908295702* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t839070149, ____items_1)); }
	inline TextureU5BU5D_t908295702* get__items_1() const { return ____items_1; }
	inline TextureU5BU5D_t908295702** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TextureU5BU5D_t908295702* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t839070149, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t839070149, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t839070149_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	TextureU5BU5D_t908295702* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t839070149_StaticFields, ___EmptyArray_4)); }
	inline TextureU5BU5D_t908295702* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline TextureU5BU5D_t908295702** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(TextureU5BU5D_t908295702* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T839070149_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef CHAR_T3634460470_H
#define CHAR_T3634460470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3634460470 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3634460470, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3634460470_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3634460470_H
#ifndef ENUMERATOR_T2146457487_H
#define ENUMERATOR_T2146457487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2146457487 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t257213610 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___l_0)); }
	inline List_1_t257213610 * get_l_0() const { return ___l_0; }
	inline List_1_t257213610 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t257213610 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2146457487_H
#ifndef ENUMERATOR_T2352386197_H
#define ENUMERATOR_T2352386197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>
struct  Enumerator_t2352386197 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t463142320 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	DataSet_t3286034874 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2352386197, ___l_0)); }
	inline List_1_t463142320 * get_l_0() const { return ___l_0; }
	inline List_1_t463142320 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t463142320 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2352386197, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2352386197, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2352386197, ___current_3)); }
	inline DataSet_t3286034874 * get_current_3() const { return ___current_3; }
	inline DataSet_t3286034874 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(DataSet_t3286034874 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2352386197_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ENUMERATOR_T2810099927_H
#define ENUMERATOR_T2810099927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Queue`1/Enumerator<UnityEngine.Quaternion>
struct  Enumerator_t2810099927 
{
public:
	// System.Collections.Generic.Queue`1<T> System.Collections.Generic.Queue`1/Enumerator::q
	Queue_1_t2148187825 * ___q_0;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator::idx
	int32_t ___idx_1;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator::ver
	int32_t ___ver_2;

public:
	inline static int32_t get_offset_of_q_0() { return static_cast<int32_t>(offsetof(Enumerator_t2810099927, ___q_0)); }
	inline Queue_1_t2148187825 * get_q_0() const { return ___q_0; }
	inline Queue_1_t2148187825 ** get_address_of_q_0() { return &___q_0; }
	inline void set_q_0(Queue_1_t2148187825 * value)
	{
		___q_0 = value;
		Il2CppCodeGenWriteBarrier((&___q_0), value);
	}

	inline static int32_t get_offset_of_idx_1() { return static_cast<int32_t>(offsetof(Enumerator_t2810099927, ___idx_1)); }
	inline int32_t get_idx_1() const { return ___idx_1; }
	inline int32_t* get_address_of_idx_1() { return &___idx_1; }
	inline void set_idx_1(int32_t value)
	{
		___idx_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2810099927, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2810099927_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef VEC2I_T3527036565_H
#define VEC2I_T3527036565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/Vec2I
#pragma pack(push, tp, 1)
struct  Vec2I_t3527036565 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/Vec2I::x
	int32_t ___x_0;
	// System.Int32 Vuforia.VuforiaRenderer/Vec2I::y
	int32_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vec2I_t3527036565, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vec2I_t3527036565, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VEC2I_T3527036565_H
#ifndef ENUMERATOR_T4230485060_H
#define ENUMERATOR_T4230485060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Queue`1/Enumerator<UnityEngine.Vector3>
struct  Enumerator_t4230485060 
{
public:
	// System.Collections.Generic.Queue`1<T> System.Collections.Generic.Queue`1/Enumerator::q
	Queue_1_t3568572958 * ___q_0;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator::idx
	int32_t ___idx_1;
	// System.Int32 System.Collections.Generic.Queue`1/Enumerator::ver
	int32_t ___ver_2;

public:
	inline static int32_t get_offset_of_q_0() { return static_cast<int32_t>(offsetof(Enumerator_t4230485060, ___q_0)); }
	inline Queue_1_t3568572958 * get_q_0() const { return ___q_0; }
	inline Queue_1_t3568572958 ** get_address_of_q_0() { return &___q_0; }
	inline void set_q_0(Queue_1_t3568572958 * value)
	{
		___q_0 = value;
		Il2CppCodeGenWriteBarrier((&___q_0), value);
	}

	inline static int32_t get_offset_of_idx_1() { return static_cast<int32_t>(offsetof(Enumerator_t4230485060, ___idx_1)); }
	inline int32_t get_idx_1() const { return ___idx_1; }
	inline int32_t* get_address_of_idx_1() { return &___idx_1; }
	inline void set_idx_1(int32_t value)
	{
		___idx_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t4230485060, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T4230485060_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef OBJECTTRACKER_T4177997237_H
#define OBJECTTRACKER_T4177997237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ObjectTracker
struct  ObjectTracker_t4177997237  : public Tracker_t2709586299
{
public:
	// System.Collections.Generic.List`1<Vuforia.DataSet> Vuforia.ObjectTracker::mActiveDataSets
	List_1_t463142320 * ___mActiveDataSets_1;
	// System.Collections.Generic.List`1<Vuforia.DataSet> Vuforia.ObjectTracker::mDataSets
	List_1_t463142320 * ___mDataSets_2;
	// Vuforia.ImageTargetBuilder Vuforia.ObjectTracker::mImageTargetBuilder
	ImageTargetBuilder_t2430893908 * ___mImageTargetBuilder_3;
	// Vuforia.TargetFinder Vuforia.ObjectTracker::mTargetFinder
	TargetFinder_t2439332195 * ___mTargetFinder_4;

public:
	inline static int32_t get_offset_of_mActiveDataSets_1() { return static_cast<int32_t>(offsetof(ObjectTracker_t4177997237, ___mActiveDataSets_1)); }
	inline List_1_t463142320 * get_mActiveDataSets_1() const { return ___mActiveDataSets_1; }
	inline List_1_t463142320 ** get_address_of_mActiveDataSets_1() { return &___mActiveDataSets_1; }
	inline void set_mActiveDataSets_1(List_1_t463142320 * value)
	{
		___mActiveDataSets_1 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveDataSets_1), value);
	}

	inline static int32_t get_offset_of_mDataSets_2() { return static_cast<int32_t>(offsetof(ObjectTracker_t4177997237, ___mDataSets_2)); }
	inline List_1_t463142320 * get_mDataSets_2() const { return ___mDataSets_2; }
	inline List_1_t463142320 ** get_address_of_mDataSets_2() { return &___mDataSets_2; }
	inline void set_mDataSets_2(List_1_t463142320 * value)
	{
		___mDataSets_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSets_2), value);
	}

	inline static int32_t get_offset_of_mImageTargetBuilder_3() { return static_cast<int32_t>(offsetof(ObjectTracker_t4177997237, ___mImageTargetBuilder_3)); }
	inline ImageTargetBuilder_t2430893908 * get_mImageTargetBuilder_3() const { return ___mImageTargetBuilder_3; }
	inline ImageTargetBuilder_t2430893908 ** get_address_of_mImageTargetBuilder_3() { return &___mImageTargetBuilder_3; }
	inline void set_mImageTargetBuilder_3(ImageTargetBuilder_t2430893908 * value)
	{
		___mImageTargetBuilder_3 = value;
		Il2CppCodeGenWriteBarrier((&___mImageTargetBuilder_3), value);
	}

	inline static int32_t get_offset_of_mTargetFinder_4() { return static_cast<int32_t>(offsetof(ObjectTracker_t4177997237, ___mTargetFinder_4)); }
	inline TargetFinder_t2439332195 * get_mTargetFinder_4() const { return ___mTargetFinder_4; }
	inline TargetFinder_t2439332195 ** get_address_of_mTargetFinder_4() { return &___mTargetFinder_4; }
	inline void set_mTargetFinder_4(TargetFinder_t2439332195 * value)
	{
		___mTargetFinder_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTargetFinder_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTRACKER_T4177997237_H
#ifndef VIDEOMODEDATA_T2066817255_H
#define VIDEOMODEDATA_T2066817255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/VideoModeData
#pragma pack(push, tp, 1)
struct  VideoModeData_t2066817255 
{
public:
	// System.Int32 Vuforia.CameraDevice/VideoModeData::width
	int32_t ___width_0;
	// System.Int32 Vuforia.CameraDevice/VideoModeData::height
	int32_t ___height_1;
	// System.Single Vuforia.CameraDevice/VideoModeData::frameRate
	float ___frameRate_2;
	// System.Int32 Vuforia.CameraDevice/VideoModeData::unused
	int32_t ___unused_3;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(VideoModeData_t2066817255, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(VideoModeData_t2066817255, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_frameRate_2() { return static_cast<int32_t>(offsetof(VideoModeData_t2066817255, ___frameRate_2)); }
	inline float get_frameRate_2() const { return ___frameRate_2; }
	inline float* get_address_of_frameRate_2() { return &___frameRate_2; }
	inline void set_frameRate_2(float value)
	{
		___frameRate_2 = value;
	}

	inline static int32_t get_offset_of_unused_3() { return static_cast<int32_t>(offsetof(VideoModeData_t2066817255, ___unused_3)); }
	inline int32_t get_unused_3() const { return ___unused_3; }
	inline int32_t* get_address_of_unused_3() { return &___unused_3; }
	inline void set_unused_3(int32_t value)
	{
		___unused_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOMODEDATA_T2066817255_H
#ifndef WAITFORSECONDS_T1699091251_H
#define WAITFORSECONDS_T1699091251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t1699091251  : public YieldInstruction_t403091072
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t1699091251, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	float ___m_Seconds_0;
};
#endif // WAITFORSECONDS_T1699091251_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef SCENE_T2348375561_H
#define SCENE_T2348375561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t2348375561 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t2348375561, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T2348375561_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef TOGGLETRANSITION_T3587297765_H
#define TOGGLETRANSITION_T3587297765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Toggle/ToggleTransition
struct  ToggleTransition_t3587297765 
{
public:
	// System.Int32 UnityEngine.UI.Toggle/ToggleTransition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ToggleTransition_t3587297765, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLETRANSITION_T3587297765_H
#ifndef HIDEFLAGS_T4250555765_H
#define HIDEFLAGS_T4250555765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t4250555765 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HideFlags_t4250555765, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T4250555765_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef INITSTATE_T3374939742_H
#define INITSTATE_T3374939742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntime/InitState
struct  InitState_t3374939742 
{
public:
	// System.Int32 Vuforia.VuforiaRuntime/InitState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InitState_t3374939742, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITSTATE_T3374939742_H
#ifndef SPACE_T654135784_H
#define SPACE_T654135784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t654135784 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Space_t654135784, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T654135784_H
#ifndef VIDEOBGCFGDATA_T994527297_H
#define VIDEOBGCFGDATA_T994527297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/VideoBGCfgData
#pragma pack(push, tp, 1)
struct  VideoBGCfgData_t994527297 
{
public:
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoBGCfgData::position
	Vec2I_t3527036565  ___position_0;
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoBGCfgData::size
	Vec2I_t3527036565  ___size_1;
	// System.Int32 Vuforia.VuforiaRenderer/VideoBGCfgData::reflectionInteger
	int32_t ___reflectionInteger_2;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(VideoBGCfgData_t994527297, ___position_0)); }
	inline Vec2I_t3527036565  get_position_0() const { return ___position_0; }
	inline Vec2I_t3527036565 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vec2I_t3527036565  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(VideoBGCfgData_t994527297, ___size_1)); }
	inline Vec2I_t3527036565  get_size_1() const { return ___size_1; }
	inline Vec2I_t3527036565 * get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(Vec2I_t3527036565  value)
	{
		___size_1 = value;
	}

	inline static int32_t get_offset_of_reflectionInteger_2() { return static_cast<int32_t>(offsetof(VideoBGCfgData_t994527297, ___reflectionInteger_2)); }
	inline int32_t get_reflectionInteger_2() const { return ___reflectionInteger_2; }
	inline int32_t* get_address_of_reflectionInteger_2() { return &___reflectionInteger_2; }
	inline void set_reflectionInteger_2(int32_t value)
	{
		___reflectionInteger_2 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBGCFGDATA_T994527297_H
#ifndef STATUS_T1100905814_H
#define STATUS_T1100905814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour/Status
struct  Status_t1100905814 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour/Status::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Status_t1100905814, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T1100905814_H
#ifndef TEXTUREFORMAT_T2701165832_H
#define TEXTUREFORMAT_T2701165832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t2701165832 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureFormat_t2701165832, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T2701165832_H
#ifndef GUISTYLESTATE_T1397964415_H
#define GUISTYLESTATE_T1397964415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIStyleState
struct  GUIStyleState_t1397964415  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.GUIStyleState::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.GUIStyle UnityEngine.GUIStyleState::m_SourceStyle
	GUIStyle_t3956901511 * ___m_SourceStyle_1;
	// UnityEngine.Texture2D UnityEngine.GUIStyleState::m_Background
	Texture2D_t3840446185 * ___m_Background_2;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(GUIStyleState_t1397964415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_SourceStyle_1() { return static_cast<int32_t>(offsetof(GUIStyleState_t1397964415, ___m_SourceStyle_1)); }
	inline GUIStyle_t3956901511 * get_m_SourceStyle_1() const { return ___m_SourceStyle_1; }
	inline GUIStyle_t3956901511 ** get_address_of_m_SourceStyle_1() { return &___m_SourceStyle_1; }
	inline void set_m_SourceStyle_1(GUIStyle_t3956901511 * value)
	{
		___m_SourceStyle_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceStyle_1), value);
	}

	inline static int32_t get_offset_of_m_Background_2() { return static_cast<int32_t>(offsetof(GUIStyleState_t1397964415, ___m_Background_2)); }
	inline Texture2D_t3840446185 * get_m_Background_2() const { return ___m_Background_2; }
	inline Texture2D_t3840446185 ** get_address_of_m_Background_2() { return &___m_Background_2; }
	inline void set_m_Background_2(Texture2D_t3840446185 * value)
	{
		___m_Background_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Background_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.GUIStyleState
struct GUIStyleState_t1397964415_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	GUIStyle_t3956901511_marshaled_pinvoke* ___m_SourceStyle_1;
	Texture2D_t3840446185 * ___m_Background_2;
};
// Native definition for COM marshalling of UnityEngine.GUIStyleState
struct GUIStyleState_t1397964415_marshaled_com
{
	intptr_t ___m_Ptr_0;
	GUIStyle_t3956901511_marshaled_com* ___m_SourceStyle_1;
	Texture2D_t3840446185 * ___m_Background_2;
};
#endif // GUISTYLESTATE_T1397964415_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef ASYNCOPERATION_T1445031843_H
#define ASYNCOPERATION_T1445031843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AsyncOperation
struct  AsyncOperation_t1445031843  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_t1617499438 * ___m_completeCallback_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AsyncOperation_t1445031843, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_completeCallback_1() { return static_cast<int32_t>(offsetof(AsyncOperation_t1445031843, ___m_completeCallback_1)); }
	inline Action_1_t1617499438 * get_m_completeCallback_1() const { return ___m_completeCallback_1; }
	inline Action_1_t1617499438 ** get_address_of_m_completeCallback_1() { return &___m_completeCallback_1; }
	inline void set_m_completeCallback_1(Action_1_t1617499438 * value)
	{
		___m_completeCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_completeCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
#endif // ASYNCOPERATION_T1445031843_H
#ifndef THREADPRIORITY_T1774350854_H
#define THREADPRIORITY_T1774350854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ThreadPriority
struct  ThreadPriority_t1774350854 
{
public:
	// System.Int32 UnityEngine.ThreadPriority::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ThreadPriority_t1774350854, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADPRIORITY_T1774350854_H
#ifndef FPSHINT_T2906034572_H
#define FPSHINT_T2906034572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/FpsHint
struct  FpsHint_t2906034572 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/FpsHint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FpsHint_t2906034572, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSHINT_T2906034572_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef NULLABLE_1_T3878791605_H
#define NULLABLE_1_T3878791605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Vector2>
struct  Nullable_1_t3878791605 
{
public:
	// T System.Nullable`1::value
	Vector2_t2156229523  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3878791605, ___value_0)); }
	inline Vector2_t2156229523  get_value_0() const { return ___value_0; }
	inline Vector2_t2156229523 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector2_t2156229523  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3878791605, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3878791605_H
#ifndef TOUCHTYPE_T2034578258_H
#define TOUCHTYPE_T2034578258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t2034578258 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t2034578258, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T2034578258_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef IMAGETARGETTYPE_T2834081427_H
#define IMAGETARGETTYPE_T2834081427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetType
struct  ImageTargetType_t2834081427 
{
public:
	// System.Int32 Vuforia.ImageTargetType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ImageTargetType_t2834081427, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETTYPE_T2834081427_H
#ifndef CAMERADEVICEMODE_T2478715656_H
#define CAMERADEVICEMODE_T2478715656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/CameraDeviceMode
struct  CameraDeviceMode_t2478715656 
{
public:
	// System.Int32 Vuforia.CameraDevice/CameraDeviceMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraDeviceMode_t2478715656, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADEVICEMODE_T2478715656_H
#ifndef VIDEOBACKGROUNDREFLECTION_T736962841_H
#define VIDEOBACKGROUNDREFLECTION_T736962841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/VideoBackgroundReflection
struct  VideoBackgroundReflection_t736962841 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/VideoBackgroundReflection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VideoBackgroundReflection_t736962841, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDREFLECTION_T736962841_H
#ifndef INSTANCEIDTYPE_T420283664_H
#define INSTANCEIDTYPE_T420283664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.InstanceIdType
struct  InstanceIdType_t420283664 
{
public:
	// System.Int32 Vuforia.InstanceIdType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InstanceIdType_t420283664, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEIDTYPE_T420283664_H
#ifndef TYPE_T1152881528_H
#define TYPE_T1152881528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t1152881528 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t1152881528, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1152881528_H
#ifndef STORAGETYPE_T857810839_H
#define STORAGETYPE_T857810839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnity/StorageType
struct  StorageType_t857810839 
{
public:
	// System.Int32 Vuforia.VuforiaUnity/StorageType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StorageType_t857810839, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORAGETYPE_T857810839_H
#ifndef FILLMETHOD_T1167457570_H
#define FILLMETHOD_T1167457570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t1167457570 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillMethod_t1167457570, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T1167457570_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef WORLDCENTERMODE_T3672819471_H
#define WORLDCENTERMODE_T3672819471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaARController/WorldCenterMode
struct  WorldCenterMode_t3672819471 
{
public:
	// System.Int32 Vuforia.VuforiaARController/WorldCenterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WorldCenterMode_t3672819471, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDCENTERMODE_T3672819471_H
#ifndef TOUCHPHASE_T72348083_H
#define TOUCHPHASE_T72348083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t72348083 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t72348083, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T72348083_H
#ifndef RECTOFFSET_T1369453676_H
#define RECTOFFSET_T1369453676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectOffset
struct  RectOffset_t1369453676  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.RectOffset::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Object UnityEngine.RectOffset::m_SourceStyle
	RuntimeObject * ___m_SourceStyle_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(RectOffset_t1369453676, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_SourceStyle_1() { return static_cast<int32_t>(offsetof(RectOffset_t1369453676, ___m_SourceStyle_1)); }
	inline RuntimeObject * get_m_SourceStyle_1() const { return ___m_SourceStyle_1; }
	inline RuntimeObject ** get_address_of_m_SourceStyle_1() { return &___m_SourceStyle_1; }
	inline void set_m_SourceStyle_1(RuntimeObject * value)
	{
		___m_SourceStyle_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceStyle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RectOffset
struct RectOffset_t1369453676_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
// Native definition for COM marshalling of UnityEngine.RectOffset
struct RectOffset_t1369453676_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
#endif // RECTOFFSET_T1369453676_H
#ifndef INITERROR_T3420749710_H
#define INITERROR_T3420749710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnity/InitError
struct  InitError_t3420749710 
{
public:
	// System.Int32 Vuforia.VuforiaUnity/InitError::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InitError_t3420749710, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITERROR_T3420749710_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef FOCUSMODE_T3379290461_H
#define FOCUSMODE_T3379290461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/FocusMode
struct  FocusMode_t3379290461 
{
public:
	// System.Int32 Vuforia.CameraDevice/FocusMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FocusMode_t3379290461, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOCUSMODE_T3379290461_H
#ifndef CAMERADIRECTION_T637748435_H
#define CAMERADIRECTION_T637748435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/CameraDirection
struct  CameraDirection_t637748435 
{
public:
	// System.Int32 Vuforia.CameraDevice/CameraDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraDirection_t637748435, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADIRECTION_T637748435_H
#ifndef TEXTANCHOR_T2035777396_H
#define TEXTANCHOR_T2035777396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_t2035777396 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAnchor_t2035777396, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_T2035777396_H
#ifndef COROUTINE_T3829159415_H
#define COROUTINE_T3829159415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t3829159415  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t3829159415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T3829159415_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef MOTION_T1110556653_H
#define MOTION_T1110556653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Motion
struct  Motion_t1110556653  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTION_T1110556653_H
#ifndef GUISTYLE_T3956901511_H
#define GUISTYLE_T3956901511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIStyle
struct  GUIStyle_t3956901511  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.GUIStyle::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Normal
	GUIStyleState_t1397964415 * ___m_Normal_1;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Hover
	GUIStyleState_t1397964415 * ___m_Hover_2;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Active
	GUIStyleState_t1397964415 * ___m_Active_3;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Focused
	GUIStyleState_t1397964415 * ___m_Focused_4;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnNormal
	GUIStyleState_t1397964415 * ___m_OnNormal_5;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnHover
	GUIStyleState_t1397964415 * ___m_OnHover_6;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnActive
	GUIStyleState_t1397964415 * ___m_OnActive_7;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnFocused
	GUIStyleState_t1397964415 * ___m_OnFocused_8;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Border
	RectOffset_t1369453676 * ___m_Border_9;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Padding
	RectOffset_t1369453676 * ___m_Padding_10;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Margin
	RectOffset_t1369453676 * ___m_Margin_11;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Overflow
	RectOffset_t1369453676 * ___m_Overflow_12;
	// UnityEngine.Font UnityEngine.GUIStyle::m_FontInternal
	Font_t1956802104 * ___m_FontInternal_13;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Normal_1)); }
	inline GUIStyleState_t1397964415 * get_m_Normal_1() const { return ___m_Normal_1; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(GUIStyleState_t1397964415 * value)
	{
		___m_Normal_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normal_1), value);
	}

	inline static int32_t get_offset_of_m_Hover_2() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Hover_2)); }
	inline GUIStyleState_t1397964415 * get_m_Hover_2() const { return ___m_Hover_2; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Hover_2() { return &___m_Hover_2; }
	inline void set_m_Hover_2(GUIStyleState_t1397964415 * value)
	{
		___m_Hover_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Hover_2), value);
	}

	inline static int32_t get_offset_of_m_Active_3() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Active_3)); }
	inline GUIStyleState_t1397964415 * get_m_Active_3() const { return ___m_Active_3; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Active_3() { return &___m_Active_3; }
	inline void set_m_Active_3(GUIStyleState_t1397964415 * value)
	{
		___m_Active_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Active_3), value);
	}

	inline static int32_t get_offset_of_m_Focused_4() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Focused_4)); }
	inline GUIStyleState_t1397964415 * get_m_Focused_4() const { return ___m_Focused_4; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Focused_4() { return &___m_Focused_4; }
	inline void set_m_Focused_4(GUIStyleState_t1397964415 * value)
	{
		___m_Focused_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Focused_4), value);
	}

	inline static int32_t get_offset_of_m_OnNormal_5() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnNormal_5)); }
	inline GUIStyleState_t1397964415 * get_m_OnNormal_5() const { return ___m_OnNormal_5; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnNormal_5() { return &___m_OnNormal_5; }
	inline void set_m_OnNormal_5(GUIStyleState_t1397964415 * value)
	{
		___m_OnNormal_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnNormal_5), value);
	}

	inline static int32_t get_offset_of_m_OnHover_6() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnHover_6)); }
	inline GUIStyleState_t1397964415 * get_m_OnHover_6() const { return ___m_OnHover_6; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnHover_6() { return &___m_OnHover_6; }
	inline void set_m_OnHover_6(GUIStyleState_t1397964415 * value)
	{
		___m_OnHover_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnHover_6), value);
	}

	inline static int32_t get_offset_of_m_OnActive_7() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnActive_7)); }
	inline GUIStyleState_t1397964415 * get_m_OnActive_7() const { return ___m_OnActive_7; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnActive_7() { return &___m_OnActive_7; }
	inline void set_m_OnActive_7(GUIStyleState_t1397964415 * value)
	{
		___m_OnActive_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnActive_7), value);
	}

	inline static int32_t get_offset_of_m_OnFocused_8() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnFocused_8)); }
	inline GUIStyleState_t1397964415 * get_m_OnFocused_8() const { return ___m_OnFocused_8; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnFocused_8() { return &___m_OnFocused_8; }
	inline void set_m_OnFocused_8(GUIStyleState_t1397964415 * value)
	{
		___m_OnFocused_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnFocused_8), value);
	}

	inline static int32_t get_offset_of_m_Border_9() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Border_9)); }
	inline RectOffset_t1369453676 * get_m_Border_9() const { return ___m_Border_9; }
	inline RectOffset_t1369453676 ** get_address_of_m_Border_9() { return &___m_Border_9; }
	inline void set_m_Border_9(RectOffset_t1369453676 * value)
	{
		___m_Border_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Border_9), value);
	}

	inline static int32_t get_offset_of_m_Padding_10() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Padding_10)); }
	inline RectOffset_t1369453676 * get_m_Padding_10() const { return ___m_Padding_10; }
	inline RectOffset_t1369453676 ** get_address_of_m_Padding_10() { return &___m_Padding_10; }
	inline void set_m_Padding_10(RectOffset_t1369453676 * value)
	{
		___m_Padding_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_10), value);
	}

	inline static int32_t get_offset_of_m_Margin_11() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Margin_11)); }
	inline RectOffset_t1369453676 * get_m_Margin_11() const { return ___m_Margin_11; }
	inline RectOffset_t1369453676 ** get_address_of_m_Margin_11() { return &___m_Margin_11; }
	inline void set_m_Margin_11(RectOffset_t1369453676 * value)
	{
		___m_Margin_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Margin_11), value);
	}

	inline static int32_t get_offset_of_m_Overflow_12() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Overflow_12)); }
	inline RectOffset_t1369453676 * get_m_Overflow_12() const { return ___m_Overflow_12; }
	inline RectOffset_t1369453676 ** get_address_of_m_Overflow_12() { return &___m_Overflow_12; }
	inline void set_m_Overflow_12(RectOffset_t1369453676 * value)
	{
		___m_Overflow_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Overflow_12), value);
	}

	inline static int32_t get_offset_of_m_FontInternal_13() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_FontInternal_13)); }
	inline Font_t1956802104 * get_m_FontInternal_13() const { return ___m_FontInternal_13; }
	inline Font_t1956802104 ** get_address_of_m_FontInternal_13() { return &___m_FontInternal_13; }
	inline void set_m_FontInternal_13(Font_t1956802104 * value)
	{
		___m_FontInternal_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontInternal_13), value);
	}
};

struct GUIStyle_t3956901511_StaticFields
{
public:
	// System.Boolean UnityEngine.GUIStyle::showKeyboardFocus
	bool ___showKeyboardFocus_14;
	// UnityEngine.GUIStyle UnityEngine.GUIStyle::s_None
	GUIStyle_t3956901511 * ___s_None_15;

public:
	inline static int32_t get_offset_of_showKeyboardFocus_14() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511_StaticFields, ___showKeyboardFocus_14)); }
	inline bool get_showKeyboardFocus_14() const { return ___showKeyboardFocus_14; }
	inline bool* get_address_of_showKeyboardFocus_14() { return &___showKeyboardFocus_14; }
	inline void set_showKeyboardFocus_14(bool value)
	{
		___showKeyboardFocus_14 = value;
	}

	inline static int32_t get_offset_of_s_None_15() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511_StaticFields, ___s_None_15)); }
	inline GUIStyle_t3956901511 * get_s_None_15() const { return ___s_None_15; }
	inline GUIStyle_t3956901511 ** get_address_of_s_None_15() { return &___s_None_15; }
	inline void set_s_None_15(GUIStyle_t3956901511 * value)
	{
		___s_None_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_None_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.GUIStyle
struct GUIStyle_t3956901511_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Normal_1;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Hover_2;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Active_3;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Focused_4;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnNormal_5;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnHover_6;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnActive_7;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnFocused_8;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Border_9;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Padding_10;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Margin_11;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Overflow_12;
	Font_t1956802104 * ___m_FontInternal_13;
};
// Native definition for COM marshalling of UnityEngine.GUIStyle
struct GUIStyle_t3956901511_marshaled_com
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t1397964415_marshaled_com* ___m_Normal_1;
	GUIStyleState_t1397964415_marshaled_com* ___m_Hover_2;
	GUIStyleState_t1397964415_marshaled_com* ___m_Active_3;
	GUIStyleState_t1397964415_marshaled_com* ___m_Focused_4;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnNormal_5;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnHover_6;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnActive_7;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnFocused_8;
	RectOffset_t1369453676_marshaled_com* ___m_Border_9;
	RectOffset_t1369453676_marshaled_com* ___m_Padding_10;
	RectOffset_t1369453676_marshaled_com* ___m_Margin_11;
	RectOffset_t1369453676_marshaled_com* ___m_Overflow_12;
	Font_t1956802104 * ___m_FontInternal_13;
};
#endif // GUISTYLE_T3956901511_H
#ifndef MATERIAL_T340375123_H
#define MATERIAL_T340375123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t340375123  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T340375123_H
#ifndef AUDIOMIXER_T3521020193_H
#define AUDIOMIXER_T3521020193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Audio.AudioMixer
struct  AudioMixer_t3521020193  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOMIXER_T3521020193_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef SPRITE_T280657092_H
#define SPRITE_T280657092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_t280657092  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T280657092_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef TEXTURE_T3661962703_H
#define TEXTURE_T3661962703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t3661962703  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T3661962703_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef VUFORIARUNTIME_T1949122020_H
#define VUFORIARUNTIME_T1949122020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntime
struct  VuforiaRuntime_t1949122020  : public RuntimeObject
{
public:
	// System.Action`1<Vuforia.VuforiaUnity/InitError> Vuforia.VuforiaRuntime::mOnVuforiaInitError
	Action_1_t3593217305 * ___mOnVuforiaInitError_1;
	// System.Boolean Vuforia.VuforiaRuntime::mFailedToInitialize
	bool ___mFailedToInitialize_2;
	// Vuforia.VuforiaUnity/InitError Vuforia.VuforiaRuntime::mInitError
	int32_t ___mInitError_3;
	// Vuforia.VuforiaRuntime/InitState Vuforia.VuforiaRuntime::mInitState
	int32_t ___mInitState_4;
	// System.Boolean Vuforia.VuforiaRuntime::mInitThreadReturned
	bool ___mInitThreadReturned_5;
	// Vuforia.VuforiaUnity/InitError Vuforia.VuforiaRuntime::mReturnedError
	int32_t ___mReturnedError_6;
	// System.Boolean Vuforia.VuforiaRuntime::mAppIsQuitting
	bool ___mAppIsQuitting_9;

public:
	inline static int32_t get_offset_of_mOnVuforiaInitError_1() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t1949122020, ___mOnVuforiaInitError_1)); }
	inline Action_1_t3593217305 * get_mOnVuforiaInitError_1() const { return ___mOnVuforiaInitError_1; }
	inline Action_1_t3593217305 ** get_address_of_mOnVuforiaInitError_1() { return &___mOnVuforiaInitError_1; }
	inline void set_mOnVuforiaInitError_1(Action_1_t3593217305 * value)
	{
		___mOnVuforiaInitError_1 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaInitError_1), value);
	}

	inline static int32_t get_offset_of_mFailedToInitialize_2() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t1949122020, ___mFailedToInitialize_2)); }
	inline bool get_mFailedToInitialize_2() const { return ___mFailedToInitialize_2; }
	inline bool* get_address_of_mFailedToInitialize_2() { return &___mFailedToInitialize_2; }
	inline void set_mFailedToInitialize_2(bool value)
	{
		___mFailedToInitialize_2 = value;
	}

	inline static int32_t get_offset_of_mInitError_3() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t1949122020, ___mInitError_3)); }
	inline int32_t get_mInitError_3() const { return ___mInitError_3; }
	inline int32_t* get_address_of_mInitError_3() { return &___mInitError_3; }
	inline void set_mInitError_3(int32_t value)
	{
		___mInitError_3 = value;
	}

	inline static int32_t get_offset_of_mInitState_4() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t1949122020, ___mInitState_4)); }
	inline int32_t get_mInitState_4() const { return ___mInitState_4; }
	inline int32_t* get_address_of_mInitState_4() { return &___mInitState_4; }
	inline void set_mInitState_4(int32_t value)
	{
		___mInitState_4 = value;
	}

	inline static int32_t get_offset_of_mInitThreadReturned_5() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t1949122020, ___mInitThreadReturned_5)); }
	inline bool get_mInitThreadReturned_5() const { return ___mInitThreadReturned_5; }
	inline bool* get_address_of_mInitThreadReturned_5() { return &___mInitThreadReturned_5; }
	inline void set_mInitThreadReturned_5(bool value)
	{
		___mInitThreadReturned_5 = value;
	}

	inline static int32_t get_offset_of_mReturnedError_6() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t1949122020, ___mReturnedError_6)); }
	inline int32_t get_mReturnedError_6() const { return ___mReturnedError_6; }
	inline int32_t* get_address_of_mReturnedError_6() { return &___mReturnedError_6; }
	inline void set_mReturnedError_6(int32_t value)
	{
		___mReturnedError_6 = value;
	}

	inline static int32_t get_offset_of_mAppIsQuitting_9() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t1949122020, ___mAppIsQuitting_9)); }
	inline bool get_mAppIsQuitting_9() const { return ___mAppIsQuitting_9; }
	inline bool* get_address_of_mAppIsQuitting_9() { return &___mAppIsQuitting_9; }
	inline void set_mAppIsQuitting_9(bool value)
	{
		___mAppIsQuitting_9 = value;
	}
};

struct VuforiaRuntime_t1949122020_StaticFields
{
public:
	// Vuforia.VuforiaRuntime Vuforia.VuforiaRuntime::mInstance
	VuforiaRuntime_t1949122020 * ___mInstance_7;
	// System.Object Vuforia.VuforiaRuntime::mPadlock
	RuntimeObject * ___mPadlock_8;

public:
	inline static int32_t get_offset_of_mInstance_7() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t1949122020_StaticFields, ___mInstance_7)); }
	inline VuforiaRuntime_t1949122020 * get_mInstance_7() const { return ___mInstance_7; }
	inline VuforiaRuntime_t1949122020 ** get_address_of_mInstance_7() { return &___mInstance_7; }
	inline void set_mInstance_7(VuforiaRuntime_t1949122020 * value)
	{
		___mInstance_7 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_7), value);
	}

	inline static int32_t get_offset_of_mPadlock_8() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t1949122020_StaticFields, ___mPadlock_8)); }
	inline RuntimeObject * get_mPadlock_8() const { return ___mPadlock_8; }
	inline RuntimeObject ** get_address_of_mPadlock_8() { return &___mPadlock_8; }
	inline void set_mPadlock_8(RuntimeObject * value)
	{
		___mPadlock_8 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIARUNTIME_T1949122020_H
#ifndef TOUCH_T1921856868_H
#define TOUCH_T1921856868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t1921856868 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t2156229523  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t2156229523  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t2156229523  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Position_1)); }
	inline Vector2_t2156229523  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t2156229523 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t2156229523  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RawPosition_2)); }
	inline Vector2_t2156229523  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t2156229523 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t2156229523  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_PositionDelta_3)); }
	inline Vector2_t2156229523  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t2156229523 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t2156229523  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T1921856868_H
#ifndef FONT_T1956802104_H
#define FONT_T1956802104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Font
struct  Font_t1956802104  : public Object_t631007953
{
public:
	// UnityEngine.Font/FontTextureRebuildCallback UnityEngine.Font::m_FontTextureRebuildCallback
	FontTextureRebuildCallback_t2467502454 * ___m_FontTextureRebuildCallback_3;

public:
	inline static int32_t get_offset_of_m_FontTextureRebuildCallback_3() { return static_cast<int32_t>(offsetof(Font_t1956802104, ___m_FontTextureRebuildCallback_3)); }
	inline FontTextureRebuildCallback_t2467502454 * get_m_FontTextureRebuildCallback_3() const { return ___m_FontTextureRebuildCallback_3; }
	inline FontTextureRebuildCallback_t2467502454 ** get_address_of_m_FontTextureRebuildCallback_3() { return &___m_FontTextureRebuildCallback_3; }
	inline void set_m_FontTextureRebuildCallback_3(FontTextureRebuildCallback_t2467502454 * value)
	{
		___m_FontTextureRebuildCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontTextureRebuildCallback_3), value);
	}
};

struct Font_t1956802104_StaticFields
{
public:
	// System.Action`1<UnityEngine.Font> UnityEngine.Font::textureRebuilt
	Action_1_t2129269699 * ___textureRebuilt_2;

public:
	inline static int32_t get_offset_of_textureRebuilt_2() { return static_cast<int32_t>(offsetof(Font_t1956802104_StaticFields, ___textureRebuilt_2)); }
	inline Action_1_t2129269699 * get_textureRebuilt_2() const { return ___textureRebuilt_2; }
	inline Action_1_t2129269699 ** get_address_of_textureRebuilt_2() { return &___textureRebuilt_2; }
	inline void set_textureRebuilt_2(Action_1_t2129269699 * value)
	{
		___textureRebuilt_2 = value;
		Il2CppCodeGenWriteBarrier((&___textureRebuilt_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONT_T1956802104_H
#ifndef DATASET_T3286034874_H
#define DATASET_T3286034874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DataSet
struct  DataSet_t3286034874  : public RuntimeObject
{
public:
	// System.IntPtr Vuforia.DataSet::mDataSetPtr
	intptr_t ___mDataSetPtr_0;
	// System.String Vuforia.DataSet::mPath
	String_t* ___mPath_1;
	// Vuforia.VuforiaUnity/StorageType Vuforia.DataSet::mStorageType
	int32_t ___mStorageType_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable> Vuforia.DataSet::mTrackablesDict
	Dictionary_2_t1340713322 * ___mTrackablesDict_3;

public:
	inline static int32_t get_offset_of_mDataSetPtr_0() { return static_cast<int32_t>(offsetof(DataSet_t3286034874, ___mDataSetPtr_0)); }
	inline intptr_t get_mDataSetPtr_0() const { return ___mDataSetPtr_0; }
	inline intptr_t* get_address_of_mDataSetPtr_0() { return &___mDataSetPtr_0; }
	inline void set_mDataSetPtr_0(intptr_t value)
	{
		___mDataSetPtr_0 = value;
	}

	inline static int32_t get_offset_of_mPath_1() { return static_cast<int32_t>(offsetof(DataSet_t3286034874, ___mPath_1)); }
	inline String_t* get_mPath_1() const { return ___mPath_1; }
	inline String_t** get_address_of_mPath_1() { return &___mPath_1; }
	inline void set_mPath_1(String_t* value)
	{
		___mPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___mPath_1), value);
	}

	inline static int32_t get_offset_of_mStorageType_2() { return static_cast<int32_t>(offsetof(DataSet_t3286034874, ___mStorageType_2)); }
	inline int32_t get_mStorageType_2() const { return ___mStorageType_2; }
	inline int32_t* get_address_of_mStorageType_2() { return &___mStorageType_2; }
	inline void set_mStorageType_2(int32_t value)
	{
		___mStorageType_2 = value;
	}

	inline static int32_t get_offset_of_mTrackablesDict_3() { return static_cast<int32_t>(offsetof(DataSet_t3286034874, ___mTrackablesDict_3)); }
	inline Dictionary_2_t1340713322 * get_mTrackablesDict_3() const { return ___mTrackablesDict_3; }
	inline Dictionary_2_t1340713322 ** get_address_of_mTrackablesDict_3() { return &___mTrackablesDict_3; }
	inline void set_mTrackablesDict_3(Dictionary_2_t1340713322 * value)
	{
		___mTrackablesDict_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackablesDict_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASET_T3286034874_H
#ifndef VUFORIAARCONTROLLER_T1876945237_H
#define VUFORIAARCONTROLLER_T1876945237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaARController
struct  VuforiaARController_t1876945237  : public ARController_t116632334
{
public:
	// Vuforia.CameraDevice/CameraDeviceMode Vuforia.VuforiaARController::CameraDeviceModeSetting
	int32_t ___CameraDeviceModeSetting_1;
	// System.Int32 Vuforia.VuforiaARController::MaxSimultaneousImageTargets
	int32_t ___MaxSimultaneousImageTargets_2;
	// System.Int32 Vuforia.VuforiaARController::MaxSimultaneousObjectTargets
	int32_t ___MaxSimultaneousObjectTargets_3;
	// System.Boolean Vuforia.VuforiaARController::UseDelayedLoadingObjectTargets
	bool ___UseDelayedLoadingObjectTargets_4;
	// Vuforia.CameraDevice/CameraDirection Vuforia.VuforiaARController::CameraDirection
	int32_t ___CameraDirection_5;
	// Vuforia.VuforiaRenderer/VideoBackgroundReflection Vuforia.VuforiaARController::MirrorVideoBackground
	int32_t ___MirrorVideoBackground_6;
	// Vuforia.VuforiaARController/WorldCenterMode Vuforia.VuforiaARController::mWorldCenterMode
	int32_t ___mWorldCenterMode_7;
	// Vuforia.TrackableBehaviour Vuforia.VuforiaARController::mWorldCenter
	TrackableBehaviour_t1113559212 * ___mWorldCenter_8;
	// System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler> Vuforia.VuforiaARController::mVideoBgEventHandlers
	List_1_t905170877 * ___mVideoBgEventHandlers_9;
	// System.Action Vuforia.VuforiaARController::mOnBeforeVuforiaTrackersInitialized
	Action_t1264377477 * ___mOnBeforeVuforiaTrackersInitialized_10;
	// System.Action Vuforia.VuforiaARController::mOnVuforiaInitialized
	Action_t1264377477 * ___mOnVuforiaInitialized_11;
	// System.Action Vuforia.VuforiaARController::mOnVuforiaStarted
	Action_t1264377477 * ___mOnVuforiaStarted_12;
	// System.Action Vuforia.VuforiaARController::mOnVuforiaDeinitialized
	Action_t1264377477 * ___mOnVuforiaDeinitialized_13;
	// System.Action Vuforia.VuforiaARController::mOnTrackablesUpdated
	Action_t1264377477 * ___mOnTrackablesUpdated_14;
	// System.Action Vuforia.VuforiaARController::mRenderOnUpdate
	Action_t1264377477 * ___mRenderOnUpdate_15;
	// System.Action`1<System.Boolean> Vuforia.VuforiaARController::mOnPause
	Action_1_t269755560 * ___mOnPause_16;
	// System.Boolean Vuforia.VuforiaARController::mPaused
	bool ___mPaused_17;
	// System.Action Vuforia.VuforiaARController::mOnBackgroundTextureChanged
	Action_t1264377477 * ___mOnBackgroundTextureChanged_18;
	// System.Boolean Vuforia.VuforiaARController::mStartHasBeenInvoked
	bool ___mStartHasBeenInvoked_19;
	// System.Boolean Vuforia.VuforiaARController::mHasStarted
	bool ___mHasStarted_20;
	// Vuforia.ICameraConfiguration Vuforia.VuforiaARController::mCameraConfiguration
	RuntimeObject* ___mCameraConfiguration_21;
	// Vuforia.DigitalEyewearARController Vuforia.VuforiaARController::mEyewearBehaviour
	DigitalEyewearARController_t1054226036 * ___mEyewearBehaviour_22;
	// Vuforia.VideoBackgroundManager Vuforia.VuforiaARController::mVideoBackgroundMgr
	VideoBackgroundManager_t2198727358 * ___mVideoBackgroundMgr_23;
	// System.Boolean Vuforia.VuforiaARController::mCheckStopCamera
	bool ___mCheckStopCamera_24;
	// UnityEngine.Material Vuforia.VuforiaARController::mClearMaterial
	Material_t340375123 * ___mClearMaterial_25;
	// System.Boolean Vuforia.VuforiaARController::mMetalRendering
	bool ___mMetalRendering_26;
	// System.Boolean Vuforia.VuforiaARController::mHasStartedOnce
	bool ___mHasStartedOnce_27;
	// System.Boolean Vuforia.VuforiaARController::mWasEnabledBeforePause
	bool ___mWasEnabledBeforePause_28;
	// System.Boolean Vuforia.VuforiaARController::mObjectTrackerWasActiveBeforePause
	bool ___mObjectTrackerWasActiveBeforePause_29;
	// System.Boolean Vuforia.VuforiaARController::mObjectTrackerWasActiveBeforeDisabling
	bool ___mObjectTrackerWasActiveBeforeDisabling_30;
	// System.Int32 Vuforia.VuforiaARController::mLastUpdatedFrame
	int32_t ___mLastUpdatedFrame_31;

public:
	inline static int32_t get_offset_of_CameraDeviceModeSetting_1() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___CameraDeviceModeSetting_1)); }
	inline int32_t get_CameraDeviceModeSetting_1() const { return ___CameraDeviceModeSetting_1; }
	inline int32_t* get_address_of_CameraDeviceModeSetting_1() { return &___CameraDeviceModeSetting_1; }
	inline void set_CameraDeviceModeSetting_1(int32_t value)
	{
		___CameraDeviceModeSetting_1 = value;
	}

	inline static int32_t get_offset_of_MaxSimultaneousImageTargets_2() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___MaxSimultaneousImageTargets_2)); }
	inline int32_t get_MaxSimultaneousImageTargets_2() const { return ___MaxSimultaneousImageTargets_2; }
	inline int32_t* get_address_of_MaxSimultaneousImageTargets_2() { return &___MaxSimultaneousImageTargets_2; }
	inline void set_MaxSimultaneousImageTargets_2(int32_t value)
	{
		___MaxSimultaneousImageTargets_2 = value;
	}

	inline static int32_t get_offset_of_MaxSimultaneousObjectTargets_3() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___MaxSimultaneousObjectTargets_3)); }
	inline int32_t get_MaxSimultaneousObjectTargets_3() const { return ___MaxSimultaneousObjectTargets_3; }
	inline int32_t* get_address_of_MaxSimultaneousObjectTargets_3() { return &___MaxSimultaneousObjectTargets_3; }
	inline void set_MaxSimultaneousObjectTargets_3(int32_t value)
	{
		___MaxSimultaneousObjectTargets_3 = value;
	}

	inline static int32_t get_offset_of_UseDelayedLoadingObjectTargets_4() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___UseDelayedLoadingObjectTargets_4)); }
	inline bool get_UseDelayedLoadingObjectTargets_4() const { return ___UseDelayedLoadingObjectTargets_4; }
	inline bool* get_address_of_UseDelayedLoadingObjectTargets_4() { return &___UseDelayedLoadingObjectTargets_4; }
	inline void set_UseDelayedLoadingObjectTargets_4(bool value)
	{
		___UseDelayedLoadingObjectTargets_4 = value;
	}

	inline static int32_t get_offset_of_CameraDirection_5() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___CameraDirection_5)); }
	inline int32_t get_CameraDirection_5() const { return ___CameraDirection_5; }
	inline int32_t* get_address_of_CameraDirection_5() { return &___CameraDirection_5; }
	inline void set_CameraDirection_5(int32_t value)
	{
		___CameraDirection_5 = value;
	}

	inline static int32_t get_offset_of_MirrorVideoBackground_6() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___MirrorVideoBackground_6)); }
	inline int32_t get_MirrorVideoBackground_6() const { return ___MirrorVideoBackground_6; }
	inline int32_t* get_address_of_MirrorVideoBackground_6() { return &___MirrorVideoBackground_6; }
	inline void set_MirrorVideoBackground_6(int32_t value)
	{
		___MirrorVideoBackground_6 = value;
	}

	inline static int32_t get_offset_of_mWorldCenterMode_7() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mWorldCenterMode_7)); }
	inline int32_t get_mWorldCenterMode_7() const { return ___mWorldCenterMode_7; }
	inline int32_t* get_address_of_mWorldCenterMode_7() { return &___mWorldCenterMode_7; }
	inline void set_mWorldCenterMode_7(int32_t value)
	{
		___mWorldCenterMode_7 = value;
	}

	inline static int32_t get_offset_of_mWorldCenter_8() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mWorldCenter_8)); }
	inline TrackableBehaviour_t1113559212 * get_mWorldCenter_8() const { return ___mWorldCenter_8; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mWorldCenter_8() { return &___mWorldCenter_8; }
	inline void set_mWorldCenter_8(TrackableBehaviour_t1113559212 * value)
	{
		___mWorldCenter_8 = value;
		Il2CppCodeGenWriteBarrier((&___mWorldCenter_8), value);
	}

	inline static int32_t get_offset_of_mVideoBgEventHandlers_9() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mVideoBgEventHandlers_9)); }
	inline List_1_t905170877 * get_mVideoBgEventHandlers_9() const { return ___mVideoBgEventHandlers_9; }
	inline List_1_t905170877 ** get_address_of_mVideoBgEventHandlers_9() { return &___mVideoBgEventHandlers_9; }
	inline void set_mVideoBgEventHandlers_9(List_1_t905170877 * value)
	{
		___mVideoBgEventHandlers_9 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBgEventHandlers_9), value);
	}

	inline static int32_t get_offset_of_mOnBeforeVuforiaTrackersInitialized_10() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mOnBeforeVuforiaTrackersInitialized_10)); }
	inline Action_t1264377477 * get_mOnBeforeVuforiaTrackersInitialized_10() const { return ___mOnBeforeVuforiaTrackersInitialized_10; }
	inline Action_t1264377477 ** get_address_of_mOnBeforeVuforiaTrackersInitialized_10() { return &___mOnBeforeVuforiaTrackersInitialized_10; }
	inline void set_mOnBeforeVuforiaTrackersInitialized_10(Action_t1264377477 * value)
	{
		___mOnBeforeVuforiaTrackersInitialized_10 = value;
		Il2CppCodeGenWriteBarrier((&___mOnBeforeVuforiaTrackersInitialized_10), value);
	}

	inline static int32_t get_offset_of_mOnVuforiaInitialized_11() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mOnVuforiaInitialized_11)); }
	inline Action_t1264377477 * get_mOnVuforiaInitialized_11() const { return ___mOnVuforiaInitialized_11; }
	inline Action_t1264377477 ** get_address_of_mOnVuforiaInitialized_11() { return &___mOnVuforiaInitialized_11; }
	inline void set_mOnVuforiaInitialized_11(Action_t1264377477 * value)
	{
		___mOnVuforiaInitialized_11 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaInitialized_11), value);
	}

	inline static int32_t get_offset_of_mOnVuforiaStarted_12() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mOnVuforiaStarted_12)); }
	inline Action_t1264377477 * get_mOnVuforiaStarted_12() const { return ___mOnVuforiaStarted_12; }
	inline Action_t1264377477 ** get_address_of_mOnVuforiaStarted_12() { return &___mOnVuforiaStarted_12; }
	inline void set_mOnVuforiaStarted_12(Action_t1264377477 * value)
	{
		___mOnVuforiaStarted_12 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaStarted_12), value);
	}

	inline static int32_t get_offset_of_mOnVuforiaDeinitialized_13() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mOnVuforiaDeinitialized_13)); }
	inline Action_t1264377477 * get_mOnVuforiaDeinitialized_13() const { return ___mOnVuforiaDeinitialized_13; }
	inline Action_t1264377477 ** get_address_of_mOnVuforiaDeinitialized_13() { return &___mOnVuforiaDeinitialized_13; }
	inline void set_mOnVuforiaDeinitialized_13(Action_t1264377477 * value)
	{
		___mOnVuforiaDeinitialized_13 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaDeinitialized_13), value);
	}

	inline static int32_t get_offset_of_mOnTrackablesUpdated_14() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mOnTrackablesUpdated_14)); }
	inline Action_t1264377477 * get_mOnTrackablesUpdated_14() const { return ___mOnTrackablesUpdated_14; }
	inline Action_t1264377477 ** get_address_of_mOnTrackablesUpdated_14() { return &___mOnTrackablesUpdated_14; }
	inline void set_mOnTrackablesUpdated_14(Action_t1264377477 * value)
	{
		___mOnTrackablesUpdated_14 = value;
		Il2CppCodeGenWriteBarrier((&___mOnTrackablesUpdated_14), value);
	}

	inline static int32_t get_offset_of_mRenderOnUpdate_15() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mRenderOnUpdate_15)); }
	inline Action_t1264377477 * get_mRenderOnUpdate_15() const { return ___mRenderOnUpdate_15; }
	inline Action_t1264377477 ** get_address_of_mRenderOnUpdate_15() { return &___mRenderOnUpdate_15; }
	inline void set_mRenderOnUpdate_15(Action_t1264377477 * value)
	{
		___mRenderOnUpdate_15 = value;
		Il2CppCodeGenWriteBarrier((&___mRenderOnUpdate_15), value);
	}

	inline static int32_t get_offset_of_mOnPause_16() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mOnPause_16)); }
	inline Action_1_t269755560 * get_mOnPause_16() const { return ___mOnPause_16; }
	inline Action_1_t269755560 ** get_address_of_mOnPause_16() { return &___mOnPause_16; }
	inline void set_mOnPause_16(Action_1_t269755560 * value)
	{
		___mOnPause_16 = value;
		Il2CppCodeGenWriteBarrier((&___mOnPause_16), value);
	}

	inline static int32_t get_offset_of_mPaused_17() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mPaused_17)); }
	inline bool get_mPaused_17() const { return ___mPaused_17; }
	inline bool* get_address_of_mPaused_17() { return &___mPaused_17; }
	inline void set_mPaused_17(bool value)
	{
		___mPaused_17 = value;
	}

	inline static int32_t get_offset_of_mOnBackgroundTextureChanged_18() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mOnBackgroundTextureChanged_18)); }
	inline Action_t1264377477 * get_mOnBackgroundTextureChanged_18() const { return ___mOnBackgroundTextureChanged_18; }
	inline Action_t1264377477 ** get_address_of_mOnBackgroundTextureChanged_18() { return &___mOnBackgroundTextureChanged_18; }
	inline void set_mOnBackgroundTextureChanged_18(Action_t1264377477 * value)
	{
		___mOnBackgroundTextureChanged_18 = value;
		Il2CppCodeGenWriteBarrier((&___mOnBackgroundTextureChanged_18), value);
	}

	inline static int32_t get_offset_of_mStartHasBeenInvoked_19() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mStartHasBeenInvoked_19)); }
	inline bool get_mStartHasBeenInvoked_19() const { return ___mStartHasBeenInvoked_19; }
	inline bool* get_address_of_mStartHasBeenInvoked_19() { return &___mStartHasBeenInvoked_19; }
	inline void set_mStartHasBeenInvoked_19(bool value)
	{
		___mStartHasBeenInvoked_19 = value;
	}

	inline static int32_t get_offset_of_mHasStarted_20() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mHasStarted_20)); }
	inline bool get_mHasStarted_20() const { return ___mHasStarted_20; }
	inline bool* get_address_of_mHasStarted_20() { return &___mHasStarted_20; }
	inline void set_mHasStarted_20(bool value)
	{
		___mHasStarted_20 = value;
	}

	inline static int32_t get_offset_of_mCameraConfiguration_21() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mCameraConfiguration_21)); }
	inline RuntimeObject* get_mCameraConfiguration_21() const { return ___mCameraConfiguration_21; }
	inline RuntimeObject** get_address_of_mCameraConfiguration_21() { return &___mCameraConfiguration_21; }
	inline void set_mCameraConfiguration_21(RuntimeObject* value)
	{
		___mCameraConfiguration_21 = value;
		Il2CppCodeGenWriteBarrier((&___mCameraConfiguration_21), value);
	}

	inline static int32_t get_offset_of_mEyewearBehaviour_22() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mEyewearBehaviour_22)); }
	inline DigitalEyewearARController_t1054226036 * get_mEyewearBehaviour_22() const { return ___mEyewearBehaviour_22; }
	inline DigitalEyewearARController_t1054226036 ** get_address_of_mEyewearBehaviour_22() { return &___mEyewearBehaviour_22; }
	inline void set_mEyewearBehaviour_22(DigitalEyewearARController_t1054226036 * value)
	{
		___mEyewearBehaviour_22 = value;
		Il2CppCodeGenWriteBarrier((&___mEyewearBehaviour_22), value);
	}

	inline static int32_t get_offset_of_mVideoBackgroundMgr_23() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mVideoBackgroundMgr_23)); }
	inline VideoBackgroundManager_t2198727358 * get_mVideoBackgroundMgr_23() const { return ___mVideoBackgroundMgr_23; }
	inline VideoBackgroundManager_t2198727358 ** get_address_of_mVideoBackgroundMgr_23() { return &___mVideoBackgroundMgr_23; }
	inline void set_mVideoBackgroundMgr_23(VideoBackgroundManager_t2198727358 * value)
	{
		___mVideoBackgroundMgr_23 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBackgroundMgr_23), value);
	}

	inline static int32_t get_offset_of_mCheckStopCamera_24() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mCheckStopCamera_24)); }
	inline bool get_mCheckStopCamera_24() const { return ___mCheckStopCamera_24; }
	inline bool* get_address_of_mCheckStopCamera_24() { return &___mCheckStopCamera_24; }
	inline void set_mCheckStopCamera_24(bool value)
	{
		___mCheckStopCamera_24 = value;
	}

	inline static int32_t get_offset_of_mClearMaterial_25() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mClearMaterial_25)); }
	inline Material_t340375123 * get_mClearMaterial_25() const { return ___mClearMaterial_25; }
	inline Material_t340375123 ** get_address_of_mClearMaterial_25() { return &___mClearMaterial_25; }
	inline void set_mClearMaterial_25(Material_t340375123 * value)
	{
		___mClearMaterial_25 = value;
		Il2CppCodeGenWriteBarrier((&___mClearMaterial_25), value);
	}

	inline static int32_t get_offset_of_mMetalRendering_26() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mMetalRendering_26)); }
	inline bool get_mMetalRendering_26() const { return ___mMetalRendering_26; }
	inline bool* get_address_of_mMetalRendering_26() { return &___mMetalRendering_26; }
	inline void set_mMetalRendering_26(bool value)
	{
		___mMetalRendering_26 = value;
	}

	inline static int32_t get_offset_of_mHasStartedOnce_27() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mHasStartedOnce_27)); }
	inline bool get_mHasStartedOnce_27() const { return ___mHasStartedOnce_27; }
	inline bool* get_address_of_mHasStartedOnce_27() { return &___mHasStartedOnce_27; }
	inline void set_mHasStartedOnce_27(bool value)
	{
		___mHasStartedOnce_27 = value;
	}

	inline static int32_t get_offset_of_mWasEnabledBeforePause_28() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mWasEnabledBeforePause_28)); }
	inline bool get_mWasEnabledBeforePause_28() const { return ___mWasEnabledBeforePause_28; }
	inline bool* get_address_of_mWasEnabledBeforePause_28() { return &___mWasEnabledBeforePause_28; }
	inline void set_mWasEnabledBeforePause_28(bool value)
	{
		___mWasEnabledBeforePause_28 = value;
	}

	inline static int32_t get_offset_of_mObjectTrackerWasActiveBeforePause_29() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mObjectTrackerWasActiveBeforePause_29)); }
	inline bool get_mObjectTrackerWasActiveBeforePause_29() const { return ___mObjectTrackerWasActiveBeforePause_29; }
	inline bool* get_address_of_mObjectTrackerWasActiveBeforePause_29() { return &___mObjectTrackerWasActiveBeforePause_29; }
	inline void set_mObjectTrackerWasActiveBeforePause_29(bool value)
	{
		___mObjectTrackerWasActiveBeforePause_29 = value;
	}

	inline static int32_t get_offset_of_mObjectTrackerWasActiveBeforeDisabling_30() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mObjectTrackerWasActiveBeforeDisabling_30)); }
	inline bool get_mObjectTrackerWasActiveBeforeDisabling_30() const { return ___mObjectTrackerWasActiveBeforeDisabling_30; }
	inline bool* get_address_of_mObjectTrackerWasActiveBeforeDisabling_30() { return &___mObjectTrackerWasActiveBeforeDisabling_30; }
	inline void set_mObjectTrackerWasActiveBeforeDisabling_30(bool value)
	{
		___mObjectTrackerWasActiveBeforeDisabling_30 = value;
	}

	inline static int32_t get_offset_of_mLastUpdatedFrame_31() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237, ___mLastUpdatedFrame_31)); }
	inline int32_t get_mLastUpdatedFrame_31() const { return ___mLastUpdatedFrame_31; }
	inline int32_t* get_address_of_mLastUpdatedFrame_31() { return &___mLastUpdatedFrame_31; }
	inline void set_mLastUpdatedFrame_31(int32_t value)
	{
		___mLastUpdatedFrame_31 = value;
	}
};

struct VuforiaARController_t1876945237_StaticFields
{
public:
	// Vuforia.VuforiaARController Vuforia.VuforiaARController::mInstance
	VuforiaARController_t1876945237 * ___mInstance_32;
	// System.Object Vuforia.VuforiaARController::mPadlock
	RuntimeObject * ___mPadlock_33;

public:
	inline static int32_t get_offset_of_mInstance_32() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237_StaticFields, ___mInstance_32)); }
	inline VuforiaARController_t1876945237 * get_mInstance_32() const { return ___mInstance_32; }
	inline VuforiaARController_t1876945237 ** get_address_of_mInstance_32() { return &___mInstance_32; }
	inline void set_mInstance_32(VuforiaARController_t1876945237 * value)
	{
		___mInstance_32 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_32), value);
	}

	inline static int32_t get_offset_of_mPadlock_33() { return static_cast<int32_t>(offsetof(VuforiaARController_t1876945237_StaticFields, ___mPadlock_33)); }
	inline RuntimeObject * get_mPadlock_33() const { return ___mPadlock_33; }
	inline RuntimeObject ** get_address_of_mPadlock_33() { return &___mPadlock_33; }
	inline void set_mPadlock_33(RuntimeObject * value)
	{
		___mPadlock_33 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAARCONTROLLER_T1876945237_H
#ifndef GENERICVUFORIACONFIGURATION_T3697830469_H
#define GENERICVUFORIACONFIGURATION_T3697830469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaConfiguration/GenericVuforiaConfiguration
struct  GenericVuforiaConfiguration_t3697830469  : public RuntimeObject
{
public:
	// System.String Vuforia.VuforiaConfiguration/GenericVuforiaConfiguration::vuforiaLicenseKey
	String_t* ___vuforiaLicenseKey_1;
	// System.String Vuforia.VuforiaConfiguration/GenericVuforiaConfiguration::ufoLicenseKey
	String_t* ___ufoLicenseKey_2;
	// System.Boolean Vuforia.VuforiaConfiguration/GenericVuforiaConfiguration::delayedInitialization
	bool ___delayedInitialization_3;
	// Vuforia.CameraDevice/CameraDeviceMode Vuforia.VuforiaConfiguration/GenericVuforiaConfiguration::cameraDeviceModeSetting
	int32_t ___cameraDeviceModeSetting_4;
	// System.Int32 Vuforia.VuforiaConfiguration/GenericVuforiaConfiguration::maxSimultaneousImageTargets
	int32_t ___maxSimultaneousImageTargets_5;
	// System.Int32 Vuforia.VuforiaConfiguration/GenericVuforiaConfiguration::maxSimultaneousObjectTargets
	int32_t ___maxSimultaneousObjectTargets_6;
	// System.Boolean Vuforia.VuforiaConfiguration/GenericVuforiaConfiguration::useDelayedLoadingObjectTargets
	bool ___useDelayedLoadingObjectTargets_7;
	// Vuforia.CameraDevice/CameraDirection Vuforia.VuforiaConfiguration/GenericVuforiaConfiguration::cameraDirection
	int32_t ___cameraDirection_8;
	// Vuforia.VuforiaRenderer/VideoBackgroundReflection Vuforia.VuforiaConfiguration/GenericVuforiaConfiguration::mirrorVideoBackground
	int32_t ___mirrorVideoBackground_9;
	// System.String Vuforia.VuforiaConfiguration/GenericVuforiaConfiguration::version
	String_t* ___version_10;

public:
	inline static int32_t get_offset_of_vuforiaLicenseKey_1() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t3697830469, ___vuforiaLicenseKey_1)); }
	inline String_t* get_vuforiaLicenseKey_1() const { return ___vuforiaLicenseKey_1; }
	inline String_t** get_address_of_vuforiaLicenseKey_1() { return &___vuforiaLicenseKey_1; }
	inline void set_vuforiaLicenseKey_1(String_t* value)
	{
		___vuforiaLicenseKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___vuforiaLicenseKey_1), value);
	}

	inline static int32_t get_offset_of_ufoLicenseKey_2() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t3697830469, ___ufoLicenseKey_2)); }
	inline String_t* get_ufoLicenseKey_2() const { return ___ufoLicenseKey_2; }
	inline String_t** get_address_of_ufoLicenseKey_2() { return &___ufoLicenseKey_2; }
	inline void set_ufoLicenseKey_2(String_t* value)
	{
		___ufoLicenseKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___ufoLicenseKey_2), value);
	}

	inline static int32_t get_offset_of_delayedInitialization_3() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t3697830469, ___delayedInitialization_3)); }
	inline bool get_delayedInitialization_3() const { return ___delayedInitialization_3; }
	inline bool* get_address_of_delayedInitialization_3() { return &___delayedInitialization_3; }
	inline void set_delayedInitialization_3(bool value)
	{
		___delayedInitialization_3 = value;
	}

	inline static int32_t get_offset_of_cameraDeviceModeSetting_4() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t3697830469, ___cameraDeviceModeSetting_4)); }
	inline int32_t get_cameraDeviceModeSetting_4() const { return ___cameraDeviceModeSetting_4; }
	inline int32_t* get_address_of_cameraDeviceModeSetting_4() { return &___cameraDeviceModeSetting_4; }
	inline void set_cameraDeviceModeSetting_4(int32_t value)
	{
		___cameraDeviceModeSetting_4 = value;
	}

	inline static int32_t get_offset_of_maxSimultaneousImageTargets_5() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t3697830469, ___maxSimultaneousImageTargets_5)); }
	inline int32_t get_maxSimultaneousImageTargets_5() const { return ___maxSimultaneousImageTargets_5; }
	inline int32_t* get_address_of_maxSimultaneousImageTargets_5() { return &___maxSimultaneousImageTargets_5; }
	inline void set_maxSimultaneousImageTargets_5(int32_t value)
	{
		___maxSimultaneousImageTargets_5 = value;
	}

	inline static int32_t get_offset_of_maxSimultaneousObjectTargets_6() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t3697830469, ___maxSimultaneousObjectTargets_6)); }
	inline int32_t get_maxSimultaneousObjectTargets_6() const { return ___maxSimultaneousObjectTargets_6; }
	inline int32_t* get_address_of_maxSimultaneousObjectTargets_6() { return &___maxSimultaneousObjectTargets_6; }
	inline void set_maxSimultaneousObjectTargets_6(int32_t value)
	{
		___maxSimultaneousObjectTargets_6 = value;
	}

	inline static int32_t get_offset_of_useDelayedLoadingObjectTargets_7() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t3697830469, ___useDelayedLoadingObjectTargets_7)); }
	inline bool get_useDelayedLoadingObjectTargets_7() const { return ___useDelayedLoadingObjectTargets_7; }
	inline bool* get_address_of_useDelayedLoadingObjectTargets_7() { return &___useDelayedLoadingObjectTargets_7; }
	inline void set_useDelayedLoadingObjectTargets_7(bool value)
	{
		___useDelayedLoadingObjectTargets_7 = value;
	}

	inline static int32_t get_offset_of_cameraDirection_8() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t3697830469, ___cameraDirection_8)); }
	inline int32_t get_cameraDirection_8() const { return ___cameraDirection_8; }
	inline int32_t* get_address_of_cameraDirection_8() { return &___cameraDirection_8; }
	inline void set_cameraDirection_8(int32_t value)
	{
		___cameraDirection_8 = value;
	}

	inline static int32_t get_offset_of_mirrorVideoBackground_9() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t3697830469, ___mirrorVideoBackground_9)); }
	inline int32_t get_mirrorVideoBackground_9() const { return ___mirrorVideoBackground_9; }
	inline int32_t* get_address_of_mirrorVideoBackground_9() { return &___mirrorVideoBackground_9; }
	inline void set_mirrorVideoBackground_9(int32_t value)
	{
		___mirrorVideoBackground_9 = value;
	}

	inline static int32_t get_offset_of_version_10() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t3697830469, ___version_10)); }
	inline String_t* get_version_10() const { return ___version_10; }
	inline String_t** get_address_of_version_10() { return &___version_10; }
	inline void set_version_10(String_t* value)
	{
		___version_10 = value;
		Il2CppCodeGenWriteBarrier((&___version_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICVUFORIACONFIGURATION_T3697830469_H
#ifndef VUFORIARENDERER_T3433045970_H
#define VUFORIARENDERER_T3433045970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer
struct  VuforiaRenderer_t3433045970  : public RuntimeObject
{
public:
	// Vuforia.VuforiaRenderer/VideoBGCfgData Vuforia.VuforiaRenderer::mVideoBGConfig
	VideoBGCfgData_t994527297  ___mVideoBGConfig_1;
	// System.Boolean Vuforia.VuforiaRenderer::mVideoBGConfigSet
	bool ___mVideoBGConfigSet_2;
	// UnityEngine.Texture Vuforia.VuforiaRenderer::mVideoBackgroundTexture
	Texture_t3661962703 * ___mVideoBackgroundTexture_3;
	// System.Boolean Vuforia.VuforiaRenderer::mBackgroundTextureHasChanged
	bool ___mBackgroundTextureHasChanged_4;
	// Vuforia.VuforiaRenderer/VideoBackgroundReflection Vuforia.VuforiaRenderer::mLastSetReflection
	int32_t ___mLastSetReflection_5;
	// System.IntPtr Vuforia.VuforiaRenderer::mNativeRenderingCallback
	intptr_t ___mNativeRenderingCallback_6;

public:
	inline static int32_t get_offset_of_mVideoBGConfig_1() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t3433045970, ___mVideoBGConfig_1)); }
	inline VideoBGCfgData_t994527297  get_mVideoBGConfig_1() const { return ___mVideoBGConfig_1; }
	inline VideoBGCfgData_t994527297 * get_address_of_mVideoBGConfig_1() { return &___mVideoBGConfig_1; }
	inline void set_mVideoBGConfig_1(VideoBGCfgData_t994527297  value)
	{
		___mVideoBGConfig_1 = value;
	}

	inline static int32_t get_offset_of_mVideoBGConfigSet_2() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t3433045970, ___mVideoBGConfigSet_2)); }
	inline bool get_mVideoBGConfigSet_2() const { return ___mVideoBGConfigSet_2; }
	inline bool* get_address_of_mVideoBGConfigSet_2() { return &___mVideoBGConfigSet_2; }
	inline void set_mVideoBGConfigSet_2(bool value)
	{
		___mVideoBGConfigSet_2 = value;
	}

	inline static int32_t get_offset_of_mVideoBackgroundTexture_3() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t3433045970, ___mVideoBackgroundTexture_3)); }
	inline Texture_t3661962703 * get_mVideoBackgroundTexture_3() const { return ___mVideoBackgroundTexture_3; }
	inline Texture_t3661962703 ** get_address_of_mVideoBackgroundTexture_3() { return &___mVideoBackgroundTexture_3; }
	inline void set_mVideoBackgroundTexture_3(Texture_t3661962703 * value)
	{
		___mVideoBackgroundTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBackgroundTexture_3), value);
	}

	inline static int32_t get_offset_of_mBackgroundTextureHasChanged_4() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t3433045970, ___mBackgroundTextureHasChanged_4)); }
	inline bool get_mBackgroundTextureHasChanged_4() const { return ___mBackgroundTextureHasChanged_4; }
	inline bool* get_address_of_mBackgroundTextureHasChanged_4() { return &___mBackgroundTextureHasChanged_4; }
	inline void set_mBackgroundTextureHasChanged_4(bool value)
	{
		___mBackgroundTextureHasChanged_4 = value;
	}

	inline static int32_t get_offset_of_mLastSetReflection_5() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t3433045970, ___mLastSetReflection_5)); }
	inline int32_t get_mLastSetReflection_5() const { return ___mLastSetReflection_5; }
	inline int32_t* get_address_of_mLastSetReflection_5() { return &___mLastSetReflection_5; }
	inline void set_mLastSetReflection_5(int32_t value)
	{
		___mLastSetReflection_5 = value;
	}

	inline static int32_t get_offset_of_mNativeRenderingCallback_6() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t3433045970, ___mNativeRenderingCallback_6)); }
	inline intptr_t get_mNativeRenderingCallback_6() const { return ___mNativeRenderingCallback_6; }
	inline intptr_t* get_address_of_mNativeRenderingCallback_6() { return &___mNativeRenderingCallback_6; }
	inline void set_mNativeRenderingCallback_6(intptr_t value)
	{
		___mNativeRenderingCallback_6 = value;
	}
};

struct VuforiaRenderer_t3433045970_StaticFields
{
public:
	// Vuforia.VuforiaRenderer Vuforia.VuforiaRenderer::sInstance
	VuforiaRenderer_t3433045970 * ___sInstance_0;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t3433045970_StaticFields, ___sInstance_0)); }
	inline VuforiaRenderer_t3433045970 * get_sInstance_0() const { return ___sInstance_0; }
	inline VuforiaRenderer_t3433045970 ** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(VuforiaRenderer_t3433045970 * value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIARENDERER_T3433045970_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef CAMERADEVICE_T960297568_H
#define CAMERADEVICE_T960297568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice
struct  CameraDevice_t960297568  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image> Vuforia.CameraDevice::mCameraImages
	Dictionary_2_t3630620928 * ___mCameraImages_1;
	// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT> Vuforia.CameraDevice::mForcedCameraFormats
	List_1_t386988881 * ___mForcedCameraFormats_2;
	// System.Boolean Vuforia.CameraDevice::mCameraReady
	bool ___mCameraReady_4;
	// System.Boolean Vuforia.CameraDevice::mIsDirty
	bool ___mIsDirty_5;
	// Vuforia.CameraDevice/CameraDirection Vuforia.CameraDevice::mActualCameraDirection
	int32_t ___mActualCameraDirection_6;
	// Vuforia.CameraDevice/CameraDirection Vuforia.CameraDevice::mSelectedCameraDirection
	int32_t ___mSelectedCameraDirection_7;
	// Vuforia.CameraDevice/CameraDeviceMode Vuforia.CameraDevice::mCameraDeviceMode
	int32_t ___mCameraDeviceMode_8;
	// Vuforia.CameraDevice/VideoModeData Vuforia.CameraDevice::mVideoModeData
	VideoModeData_t2066817255  ___mVideoModeData_9;
	// System.Boolean Vuforia.CameraDevice::mVideoModeDataNeedsUpdate
	bool ___mVideoModeDataNeedsUpdate_10;
	// System.Boolean Vuforia.CameraDevice::mHasCameraDeviceModeBeenSet
	bool ___mHasCameraDeviceModeBeenSet_11;
	// System.Boolean Vuforia.CameraDevice::mCameraActive
	bool ___mCameraActive_12;
	// System.Boolean Vuforia.CameraDevice::mIsCameraMocked
	bool ___mIsCameraMocked_13;

public:
	inline static int32_t get_offset_of_mCameraImages_1() { return static_cast<int32_t>(offsetof(CameraDevice_t960297568, ___mCameraImages_1)); }
	inline Dictionary_2_t3630620928 * get_mCameraImages_1() const { return ___mCameraImages_1; }
	inline Dictionary_2_t3630620928 ** get_address_of_mCameraImages_1() { return &___mCameraImages_1; }
	inline void set_mCameraImages_1(Dictionary_2_t3630620928 * value)
	{
		___mCameraImages_1 = value;
		Il2CppCodeGenWriteBarrier((&___mCameraImages_1), value);
	}

	inline static int32_t get_offset_of_mForcedCameraFormats_2() { return static_cast<int32_t>(offsetof(CameraDevice_t960297568, ___mForcedCameraFormats_2)); }
	inline List_1_t386988881 * get_mForcedCameraFormats_2() const { return ___mForcedCameraFormats_2; }
	inline List_1_t386988881 ** get_address_of_mForcedCameraFormats_2() { return &___mForcedCameraFormats_2; }
	inline void set_mForcedCameraFormats_2(List_1_t386988881 * value)
	{
		___mForcedCameraFormats_2 = value;
		Il2CppCodeGenWriteBarrier((&___mForcedCameraFormats_2), value);
	}

	inline static int32_t get_offset_of_mCameraReady_4() { return static_cast<int32_t>(offsetof(CameraDevice_t960297568, ___mCameraReady_4)); }
	inline bool get_mCameraReady_4() const { return ___mCameraReady_4; }
	inline bool* get_address_of_mCameraReady_4() { return &___mCameraReady_4; }
	inline void set_mCameraReady_4(bool value)
	{
		___mCameraReady_4 = value;
	}

	inline static int32_t get_offset_of_mIsDirty_5() { return static_cast<int32_t>(offsetof(CameraDevice_t960297568, ___mIsDirty_5)); }
	inline bool get_mIsDirty_5() const { return ___mIsDirty_5; }
	inline bool* get_address_of_mIsDirty_5() { return &___mIsDirty_5; }
	inline void set_mIsDirty_5(bool value)
	{
		___mIsDirty_5 = value;
	}

	inline static int32_t get_offset_of_mActualCameraDirection_6() { return static_cast<int32_t>(offsetof(CameraDevice_t960297568, ___mActualCameraDirection_6)); }
	inline int32_t get_mActualCameraDirection_6() const { return ___mActualCameraDirection_6; }
	inline int32_t* get_address_of_mActualCameraDirection_6() { return &___mActualCameraDirection_6; }
	inline void set_mActualCameraDirection_6(int32_t value)
	{
		___mActualCameraDirection_6 = value;
	}

	inline static int32_t get_offset_of_mSelectedCameraDirection_7() { return static_cast<int32_t>(offsetof(CameraDevice_t960297568, ___mSelectedCameraDirection_7)); }
	inline int32_t get_mSelectedCameraDirection_7() const { return ___mSelectedCameraDirection_7; }
	inline int32_t* get_address_of_mSelectedCameraDirection_7() { return &___mSelectedCameraDirection_7; }
	inline void set_mSelectedCameraDirection_7(int32_t value)
	{
		___mSelectedCameraDirection_7 = value;
	}

	inline static int32_t get_offset_of_mCameraDeviceMode_8() { return static_cast<int32_t>(offsetof(CameraDevice_t960297568, ___mCameraDeviceMode_8)); }
	inline int32_t get_mCameraDeviceMode_8() const { return ___mCameraDeviceMode_8; }
	inline int32_t* get_address_of_mCameraDeviceMode_8() { return &___mCameraDeviceMode_8; }
	inline void set_mCameraDeviceMode_8(int32_t value)
	{
		___mCameraDeviceMode_8 = value;
	}

	inline static int32_t get_offset_of_mVideoModeData_9() { return static_cast<int32_t>(offsetof(CameraDevice_t960297568, ___mVideoModeData_9)); }
	inline VideoModeData_t2066817255  get_mVideoModeData_9() const { return ___mVideoModeData_9; }
	inline VideoModeData_t2066817255 * get_address_of_mVideoModeData_9() { return &___mVideoModeData_9; }
	inline void set_mVideoModeData_9(VideoModeData_t2066817255  value)
	{
		___mVideoModeData_9 = value;
	}

	inline static int32_t get_offset_of_mVideoModeDataNeedsUpdate_10() { return static_cast<int32_t>(offsetof(CameraDevice_t960297568, ___mVideoModeDataNeedsUpdate_10)); }
	inline bool get_mVideoModeDataNeedsUpdate_10() const { return ___mVideoModeDataNeedsUpdate_10; }
	inline bool* get_address_of_mVideoModeDataNeedsUpdate_10() { return &___mVideoModeDataNeedsUpdate_10; }
	inline void set_mVideoModeDataNeedsUpdate_10(bool value)
	{
		___mVideoModeDataNeedsUpdate_10 = value;
	}

	inline static int32_t get_offset_of_mHasCameraDeviceModeBeenSet_11() { return static_cast<int32_t>(offsetof(CameraDevice_t960297568, ___mHasCameraDeviceModeBeenSet_11)); }
	inline bool get_mHasCameraDeviceModeBeenSet_11() const { return ___mHasCameraDeviceModeBeenSet_11; }
	inline bool* get_address_of_mHasCameraDeviceModeBeenSet_11() { return &___mHasCameraDeviceModeBeenSet_11; }
	inline void set_mHasCameraDeviceModeBeenSet_11(bool value)
	{
		___mHasCameraDeviceModeBeenSet_11 = value;
	}

	inline static int32_t get_offset_of_mCameraActive_12() { return static_cast<int32_t>(offsetof(CameraDevice_t960297568, ___mCameraActive_12)); }
	inline bool get_mCameraActive_12() const { return ___mCameraActive_12; }
	inline bool* get_address_of_mCameraActive_12() { return &___mCameraActive_12; }
	inline void set_mCameraActive_12(bool value)
	{
		___mCameraActive_12 = value;
	}

	inline static int32_t get_offset_of_mIsCameraMocked_13() { return static_cast<int32_t>(offsetof(CameraDevice_t960297568, ___mIsCameraMocked_13)); }
	inline bool get_mIsCameraMocked_13() const { return ___mIsCameraMocked_13; }
	inline bool* get_address_of_mIsCameraMocked_13() { return &___mIsCameraMocked_13; }
	inline void set_mIsCameraMocked_13(bool value)
	{
		___mIsCameraMocked_13 = value;
	}
};

struct CameraDevice_t960297568_StaticFields
{
public:
	// Vuforia.CameraDevice Vuforia.CameraDevice::mInstance
	CameraDevice_t960297568 * ___mInstance_0;
	// Vuforia.IWebCam Vuforia.CameraDevice::mWebCam
	RuntimeObject* ___mWebCam_3;

public:
	inline static int32_t get_offset_of_mInstance_0() { return static_cast<int32_t>(offsetof(CameraDevice_t960297568_StaticFields, ___mInstance_0)); }
	inline CameraDevice_t960297568 * get_mInstance_0() const { return ___mInstance_0; }
	inline CameraDevice_t960297568 ** get_address_of_mInstance_0() { return &___mInstance_0; }
	inline void set_mInstance_0(CameraDevice_t960297568 * value)
	{
		___mInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_0), value);
	}

	inline static int32_t get_offset_of_mWebCam_3() { return static_cast<int32_t>(offsetof(CameraDevice_t960297568_StaticFields, ___mWebCam_3)); }
	inline RuntimeObject* get_mWebCam_3() const { return ___mWebCam_3; }
	inline RuntimeObject** get_address_of_mWebCam_3() { return &___mWebCam_3; }
	inline void set_mWebCam_3(RuntimeObject* value)
	{
		___mWebCam_3 = value;
		Il2CppCodeGenWriteBarrier((&___mWebCam_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADEVICE_T960297568_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef ANIMATIONCLIP_T2318505987_H
#define ANIMATIONCLIP_T2318505987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationClip
struct  AnimationClip_t2318505987  : public Motion_t1110556653
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCLIP_T2318505987_H
#ifndef ACTION_1_T269755560_H
#define ACTION_1_T269755560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<System.Boolean>
struct  Action_1_t269755560  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T269755560_H
#ifndef UNITYACTION_2_T1262235195_H
#define UNITYACTION_2_T1262235195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>
struct  UnityAction_2_t1262235195  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_2_T1262235195_H
#ifndef FUNC_2_T528868469_H
#define FUNC_2_T528868469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<UnityEngine.Touch,UnityEngine.Vector2>
struct  Func_2_t528868469  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T528868469_H
#ifndef RENDERER_T2627027031_H
#define RENDERER_T2627027031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t2627027031  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T2627027031_H
#ifndef WINDOWFUNCTION_T3146511083_H
#define WINDOWFUNCTION_T3146511083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUI/WindowFunction
struct  WindowFunction_t3146511083  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWFUNCTION_T3146511083_H
#ifndef ACTION_1_T3593217305_H
#define ACTION_1_T3593217305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<Vuforia.VuforiaUnity/InitError>
struct  Action_1_t3593217305  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T3593217305_H
#ifndef VUFORIACONFIGURATION_T1763229349_H
#define VUFORIACONFIGURATION_T1763229349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaConfiguration
struct  VuforiaConfiguration_t1763229349  : public ScriptableObject_t2528358522
{
public:
	// Vuforia.VuforiaConfiguration/GenericVuforiaConfiguration Vuforia.VuforiaConfiguration::vuforia
	GenericVuforiaConfiguration_t3697830469 * ___vuforia_4;
	// Vuforia.VuforiaConfiguration/DigitalEyewearConfiguration Vuforia.VuforiaConfiguration::digitalEyewear
	DigitalEyewearConfiguration_t546560202 * ___digitalEyewear_5;
	// Vuforia.VuforiaConfiguration/VideoBackgroundConfiguration Vuforia.VuforiaConfiguration::videoBackground
	VideoBackgroundConfiguration_t3392414655 * ___videoBackground_6;
	// Vuforia.VuforiaConfiguration/DeviceTrackerConfiguration Vuforia.VuforiaConfiguration::deviceTracker
	DeviceTrackerConfiguration_t721467671 * ___deviceTracker_7;
	// Vuforia.VuforiaConfiguration/SmartTerrainConfiguration Vuforia.VuforiaConfiguration::smartTerrain
	SmartTerrainConfiguration_t1514074484 * ___smartTerrain_8;
	// Vuforia.VuforiaConfiguration/WebCamConfiguration Vuforia.VuforiaConfiguration::webcam
	WebCamConfiguration_t1101614731 * ___webcam_9;

public:
	inline static int32_t get_offset_of_vuforia_4() { return static_cast<int32_t>(offsetof(VuforiaConfiguration_t1763229349, ___vuforia_4)); }
	inline GenericVuforiaConfiguration_t3697830469 * get_vuforia_4() const { return ___vuforia_4; }
	inline GenericVuforiaConfiguration_t3697830469 ** get_address_of_vuforia_4() { return &___vuforia_4; }
	inline void set_vuforia_4(GenericVuforiaConfiguration_t3697830469 * value)
	{
		___vuforia_4 = value;
		Il2CppCodeGenWriteBarrier((&___vuforia_4), value);
	}

	inline static int32_t get_offset_of_digitalEyewear_5() { return static_cast<int32_t>(offsetof(VuforiaConfiguration_t1763229349, ___digitalEyewear_5)); }
	inline DigitalEyewearConfiguration_t546560202 * get_digitalEyewear_5() const { return ___digitalEyewear_5; }
	inline DigitalEyewearConfiguration_t546560202 ** get_address_of_digitalEyewear_5() { return &___digitalEyewear_5; }
	inline void set_digitalEyewear_5(DigitalEyewearConfiguration_t546560202 * value)
	{
		___digitalEyewear_5 = value;
		Il2CppCodeGenWriteBarrier((&___digitalEyewear_5), value);
	}

	inline static int32_t get_offset_of_videoBackground_6() { return static_cast<int32_t>(offsetof(VuforiaConfiguration_t1763229349, ___videoBackground_6)); }
	inline VideoBackgroundConfiguration_t3392414655 * get_videoBackground_6() const { return ___videoBackground_6; }
	inline VideoBackgroundConfiguration_t3392414655 ** get_address_of_videoBackground_6() { return &___videoBackground_6; }
	inline void set_videoBackground_6(VideoBackgroundConfiguration_t3392414655 * value)
	{
		___videoBackground_6 = value;
		Il2CppCodeGenWriteBarrier((&___videoBackground_6), value);
	}

	inline static int32_t get_offset_of_deviceTracker_7() { return static_cast<int32_t>(offsetof(VuforiaConfiguration_t1763229349, ___deviceTracker_7)); }
	inline DeviceTrackerConfiguration_t721467671 * get_deviceTracker_7() const { return ___deviceTracker_7; }
	inline DeviceTrackerConfiguration_t721467671 ** get_address_of_deviceTracker_7() { return &___deviceTracker_7; }
	inline void set_deviceTracker_7(DeviceTrackerConfiguration_t721467671 * value)
	{
		___deviceTracker_7 = value;
		Il2CppCodeGenWriteBarrier((&___deviceTracker_7), value);
	}

	inline static int32_t get_offset_of_smartTerrain_8() { return static_cast<int32_t>(offsetof(VuforiaConfiguration_t1763229349, ___smartTerrain_8)); }
	inline SmartTerrainConfiguration_t1514074484 * get_smartTerrain_8() const { return ___smartTerrain_8; }
	inline SmartTerrainConfiguration_t1514074484 ** get_address_of_smartTerrain_8() { return &___smartTerrain_8; }
	inline void set_smartTerrain_8(SmartTerrainConfiguration_t1514074484 * value)
	{
		___smartTerrain_8 = value;
		Il2CppCodeGenWriteBarrier((&___smartTerrain_8), value);
	}

	inline static int32_t get_offset_of_webcam_9() { return static_cast<int32_t>(offsetof(VuforiaConfiguration_t1763229349, ___webcam_9)); }
	inline WebCamConfiguration_t1101614731 * get_webcam_9() const { return ___webcam_9; }
	inline WebCamConfiguration_t1101614731 ** get_address_of_webcam_9() { return &___webcam_9; }
	inline void set_webcam_9(WebCamConfiguration_t1101614731 * value)
	{
		___webcam_9 = value;
		Il2CppCodeGenWriteBarrier((&___webcam_9), value);
	}
};

struct VuforiaConfiguration_t1763229349_StaticFields
{
public:
	// Vuforia.VuforiaConfiguration Vuforia.VuforiaConfiguration::mInstance
	VuforiaConfiguration_t1763229349 * ___mInstance_2;
	// System.Object Vuforia.VuforiaConfiguration::mPadlock
	RuntimeObject * ___mPadlock_3;

public:
	inline static int32_t get_offset_of_mInstance_2() { return static_cast<int32_t>(offsetof(VuforiaConfiguration_t1763229349_StaticFields, ___mInstance_2)); }
	inline VuforiaConfiguration_t1763229349 * get_mInstance_2() const { return ___mInstance_2; }
	inline VuforiaConfiguration_t1763229349 ** get_address_of_mInstance_2() { return &___mInstance_2; }
	inline void set_mInstance_2(VuforiaConfiguration_t1763229349 * value)
	{
		___mInstance_2 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_2), value);
	}

	inline static int32_t get_offset_of_mPadlock_3() { return static_cast<int32_t>(offsetof(VuforiaConfiguration_t1763229349_StaticFields, ___mPadlock_3)); }
	inline RuntimeObject * get_mPadlock_3() const { return ___mPadlock_3; }
	inline RuntimeObject ** get_address_of_mPadlock_3() { return &___mPadlock_3; }
	inline void set_mPadlock_3(RuntimeObject * value)
	{
		___mPadlock_3 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIACONFIGURATION_T1763229349_H
#ifndef TEXTURE2D_T3840446185_H
#define TEXTURE2D_T3840446185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t3840446185  : public Texture_t3661962703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T3840446185_H
#ifndef COLLIDER_T1773347010_H
#define COLLIDER_T1773347010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider
struct  Collider_t1773347010  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER_T1773347010_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ACTION_T1264377477_H
#define ACTION_T1264377477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action
struct  Action_t1264377477  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T1264377477_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef ANIMATOR_T434523843_H
#define ANIMATOR_T434523843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animator
struct  Animator_t434523843  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATOR_T434523843_H
#ifndef ANIMATION_T3648466861_H
#define ANIMATION_T3648466861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animation
struct  Animation_t3648466861  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATION_T3648466861_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef CANVAS_T3310196443_H
#define CANVAS_T3310196443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Canvas
struct  Canvas_t3310196443  : public Behaviour_t1437897464
{
public:

public:
};

struct Canvas_t3310196443_StaticFields
{
public:
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t3309123499 * ___willRenderCanvases_2;

public:
	inline static int32_t get_offset_of_willRenderCanvases_2() { return static_cast<int32_t>(offsetof(Canvas_t3310196443_StaticFields, ___willRenderCanvases_2)); }
	inline WillRenderCanvases_t3309123499 * get_willRenderCanvases_2() const { return ___willRenderCanvases_2; }
	inline WillRenderCanvases_t3309123499 ** get_address_of_willRenderCanvases_2() { return &___willRenderCanvases_2; }
	inline void set_willRenderCanvases_2(WillRenderCanvases_t3309123499 * value)
	{
		___willRenderCanvases_2 = value;
		Il2CppCodeGenWriteBarrier((&___willRenderCanvases_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVAS_T3310196443_H
#ifndef CAMERA_T4157153871_H
#define CAMERA_T4157153871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t4157153871  : public Behaviour_t1437897464
{
public:

public:
};

struct Camera_t4157153871_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t190067161 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t190067161 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t190067161 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t190067161 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t190067161 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t190067161 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t190067161 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t190067161 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t190067161 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t190067161 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t190067161 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t190067161 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T4157153871_H
#ifndef RECTTRANSFORM_T3704657025_H
#define RECTTRANSFORM_T3704657025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t3704657025  : public Transform_t3600365921
{
public:

public:
};

struct RectTransform_t3704657025_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1258266594 * ___reapplyDrivenProperties_2;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_2() { return static_cast<int32_t>(offsetof(RectTransform_t3704657025_StaticFields, ___reapplyDrivenProperties_2)); }
	inline ReapplyDrivenProperties_t1258266594 * get_reapplyDrivenProperties_2() const { return ___reapplyDrivenProperties_2; }
	inline ReapplyDrivenProperties_t1258266594 ** get_address_of_reapplyDrivenProperties_2() { return &___reapplyDrivenProperties_2; }
	inline void set_reapplyDrivenProperties_2(ReapplyDrivenProperties_t1258266594 * value)
	{
		___reapplyDrivenProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T3704657025_H
#ifndef AUDIOBEHAVIOUR_T2879336574_H
#define AUDIOBEHAVIOUR_T2879336574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioBehaviour
struct  AudioBehaviour_t2879336574  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOBEHAVIOUR_T2879336574_H
#ifndef CAMERAFOCUSCONTROLLER_T1033776956_H
#define CAMERAFOCUSCONTROLLER_T1033776956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFocusController
struct  CameraFocusController_t1033776956  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAFOCUSCONTROLLER_T1033776956_H
#ifndef TUTORIALMANAGER_T3421782323_H
#define TUTORIALMANAGER_T3421782323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// tutorialManager
struct  tutorialManager_t3421782323  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject tutorialManager::tutorial
	GameObject_t1113636619 * ___tutorial_2;
	// System.Boolean tutorialManager::hasShowed
	bool ___hasShowed_3;

public:
	inline static int32_t get_offset_of_tutorial_2() { return static_cast<int32_t>(offsetof(tutorialManager_t3421782323, ___tutorial_2)); }
	inline GameObject_t1113636619 * get_tutorial_2() const { return ___tutorial_2; }
	inline GameObject_t1113636619 ** get_address_of_tutorial_2() { return &___tutorial_2; }
	inline void set_tutorial_2(GameObject_t1113636619 * value)
	{
		___tutorial_2 = value;
		Il2CppCodeGenWriteBarrier((&___tutorial_2), value);
	}

	inline static int32_t get_offset_of_hasShowed_3() { return static_cast<int32_t>(offsetof(tutorialManager_t3421782323, ___hasShowed_3)); }
	inline bool get_hasShowed_3() const { return ___hasShowed_3; }
	inline bool* get_address_of_hasShowed_3() { return &___hasShowed_3; }
	inline void set_hasShowed_3(bool value)
	{
		___hasShowed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALMANAGER_T3421782323_H
#ifndef FINDTUTORIALMANAGER_T1037408675_H
#define FINDTUTORIALMANAGER_T1037408675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// findTutorialManager
struct  findTutorialManager_t1037408675  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINDTUTORIALMANAGER_T1037408675_H
#ifndef ABOUTSCREEN_T2183797299_H
#define ABOUTSCREEN_T2183797299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AboutScreen
struct  AboutScreen_t2183797299  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABOUTSCREEN_T2183797299_H
#ifndef ASYNCSCENELOADER_T621267272_H
#define ASYNCSCENELOADER_T621267272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AsyncSceneLoader
struct  AsyncSceneLoader_t621267272  : public MonoBehaviour_t3962482529
{
public:
	// System.Single AsyncSceneLoader::loadingDelay
	float ___loadingDelay_2;

public:
	inline static int32_t get_offset_of_loadingDelay_2() { return static_cast<int32_t>(offsetof(AsyncSceneLoader_t621267272, ___loadingDelay_2)); }
	inline float get_loadingDelay_2() const { return ___loadingDelay_2; }
	inline float* get_address_of_loadingDelay_2() { return &___loadingDelay_2; }
	inline void set_loadingDelay_2(float value)
	{
		___loadingDelay_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCSCENELOADER_T621267272_H
#ifndef TRIGGERAUDIO_T1885395141_H
#define TRIGGERAUDIO_T1885395141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// triggerAudio
struct  triggerAudio_t1885395141  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean triggerAudio::soundBusy
	bool ___soundBusy_2;

public:
	inline static int32_t get_offset_of_soundBusy_2() { return static_cast<int32_t>(offsetof(triggerAudio_t1885395141, ___soundBusy_2)); }
	inline bool get_soundBusy_2() const { return ___soundBusy_2; }
	inline bool* get_address_of_soundBusy_2() { return &___soundBusy_2; }
	inline void set_soundBusy_2(bool value)
	{
		___soundBusy_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERAUDIO_T1885395141_H
#ifndef BUTTONCOLORTOGGLE_T2066501000_H
#define BUTTONCOLORTOGGLE_T2066501000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// buttonColorToggle
struct  buttonColorToggle_t2066501000  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean buttonColorToggle::toggleOn
	bool ___toggleOn_2;
	// System.Boolean buttonColorToggle::useSprite
	bool ___useSprite_3;
	// System.String buttonColorToggle::settingName
	String_t* ___settingName_4;
	// UnityEngine.Color buttonColorToggle::OnColor
	Color_t2555686324  ___OnColor_5;
	// UnityEngine.Color buttonColorToggle::OffColor
	Color_t2555686324  ___OffColor_6;
	// UnityEngine.Sprite buttonColorToggle::OnSprite
	Sprite_t280657092 * ___OnSprite_7;
	// UnityEngine.Sprite buttonColorToggle::OffSprite
	Sprite_t280657092 * ___OffSprite_8;
	// System.String buttonColorToggle::OnText
	String_t* ___OnText_9;
	// System.String buttonColorToggle::OffText
	String_t* ___OffText_10;
	// System.String buttonColorToggle::buttonText
	String_t* ___buttonText_11;

public:
	inline static int32_t get_offset_of_toggleOn_2() { return static_cast<int32_t>(offsetof(buttonColorToggle_t2066501000, ___toggleOn_2)); }
	inline bool get_toggleOn_2() const { return ___toggleOn_2; }
	inline bool* get_address_of_toggleOn_2() { return &___toggleOn_2; }
	inline void set_toggleOn_2(bool value)
	{
		___toggleOn_2 = value;
	}

	inline static int32_t get_offset_of_useSprite_3() { return static_cast<int32_t>(offsetof(buttonColorToggle_t2066501000, ___useSprite_3)); }
	inline bool get_useSprite_3() const { return ___useSprite_3; }
	inline bool* get_address_of_useSprite_3() { return &___useSprite_3; }
	inline void set_useSprite_3(bool value)
	{
		___useSprite_3 = value;
	}

	inline static int32_t get_offset_of_settingName_4() { return static_cast<int32_t>(offsetof(buttonColorToggle_t2066501000, ___settingName_4)); }
	inline String_t* get_settingName_4() const { return ___settingName_4; }
	inline String_t** get_address_of_settingName_4() { return &___settingName_4; }
	inline void set_settingName_4(String_t* value)
	{
		___settingName_4 = value;
		Il2CppCodeGenWriteBarrier((&___settingName_4), value);
	}

	inline static int32_t get_offset_of_OnColor_5() { return static_cast<int32_t>(offsetof(buttonColorToggle_t2066501000, ___OnColor_5)); }
	inline Color_t2555686324  get_OnColor_5() const { return ___OnColor_5; }
	inline Color_t2555686324 * get_address_of_OnColor_5() { return &___OnColor_5; }
	inline void set_OnColor_5(Color_t2555686324  value)
	{
		___OnColor_5 = value;
	}

	inline static int32_t get_offset_of_OffColor_6() { return static_cast<int32_t>(offsetof(buttonColorToggle_t2066501000, ___OffColor_6)); }
	inline Color_t2555686324  get_OffColor_6() const { return ___OffColor_6; }
	inline Color_t2555686324 * get_address_of_OffColor_6() { return &___OffColor_6; }
	inline void set_OffColor_6(Color_t2555686324  value)
	{
		___OffColor_6 = value;
	}

	inline static int32_t get_offset_of_OnSprite_7() { return static_cast<int32_t>(offsetof(buttonColorToggle_t2066501000, ___OnSprite_7)); }
	inline Sprite_t280657092 * get_OnSprite_7() const { return ___OnSprite_7; }
	inline Sprite_t280657092 ** get_address_of_OnSprite_7() { return &___OnSprite_7; }
	inline void set_OnSprite_7(Sprite_t280657092 * value)
	{
		___OnSprite_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnSprite_7), value);
	}

	inline static int32_t get_offset_of_OffSprite_8() { return static_cast<int32_t>(offsetof(buttonColorToggle_t2066501000, ___OffSprite_8)); }
	inline Sprite_t280657092 * get_OffSprite_8() const { return ___OffSprite_8; }
	inline Sprite_t280657092 ** get_address_of_OffSprite_8() { return &___OffSprite_8; }
	inline void set_OffSprite_8(Sprite_t280657092 * value)
	{
		___OffSprite_8 = value;
		Il2CppCodeGenWriteBarrier((&___OffSprite_8), value);
	}

	inline static int32_t get_offset_of_OnText_9() { return static_cast<int32_t>(offsetof(buttonColorToggle_t2066501000, ___OnText_9)); }
	inline String_t* get_OnText_9() const { return ___OnText_9; }
	inline String_t** get_address_of_OnText_9() { return &___OnText_9; }
	inline void set_OnText_9(String_t* value)
	{
		___OnText_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnText_9), value);
	}

	inline static int32_t get_offset_of_OffText_10() { return static_cast<int32_t>(offsetof(buttonColorToggle_t2066501000, ___OffText_10)); }
	inline String_t* get_OffText_10() const { return ___OffText_10; }
	inline String_t** get_address_of_OffText_10() { return &___OffText_10; }
	inline void set_OffText_10(String_t* value)
	{
		___OffText_10 = value;
		Il2CppCodeGenWriteBarrier((&___OffText_10), value);
	}

	inline static int32_t get_offset_of_buttonText_11() { return static_cast<int32_t>(offsetof(buttonColorToggle_t2066501000, ___buttonText_11)); }
	inline String_t* get_buttonText_11() const { return ___buttonText_11; }
	inline String_t** get_address_of_buttonText_11() { return &___buttonText_11; }
	inline void set_buttonText_11(String_t* value)
	{
		___buttonText_11 = value;
		Il2CppCodeGenWriteBarrier((&___buttonText_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONCOLORTOGGLE_T2066501000_H
#ifndef BUTTONMOVETOSCENE_T2573241184_H
#define BUTTONMOVETOSCENE_T2573241184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// buttonMoveToScene
struct  buttonMoveToScene_t2573241184  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 buttonMoveToScene::nextLvlBuildIndex
	int32_t ___nextLvlBuildIndex_2;

public:
	inline static int32_t get_offset_of_nextLvlBuildIndex_2() { return static_cast<int32_t>(offsetof(buttonMoveToScene_t2573241184, ___nextLvlBuildIndex_2)); }
	inline int32_t get_nextLvlBuildIndex_2() const { return ___nextLvlBuildIndex_2; }
	inline int32_t* get_address_of_nextLvlBuildIndex_2() { return &___nextLvlBuildIndex_2; }
	inline void set_nextLvlBuildIndex_2(int32_t value)
	{
		___nextLvlBuildIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONMOVETOSCENE_T2573241184_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef SETTINGSMANAGER_T2086358119_H
#define SETTINGSMANAGER_T2086358119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// settingsManager
struct  settingsManager_t2086358119  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<SettingsList> settingsManager::settingslist
	List_1_t3355509319 * ___settingslist_2;
	// UnityEngine.Audio.AudioMixer settingsManager::MainMixer
	AudioMixer_t3521020193 * ___MainMixer_3;
	// System.Single settingsManager::orgMusicVol
	float ___orgMusicVol_4;
	// System.Single settingsManager::orgSoundVol
	float ___orgSoundVol_5;

public:
	inline static int32_t get_offset_of_settingslist_2() { return static_cast<int32_t>(offsetof(settingsManager_t2086358119, ___settingslist_2)); }
	inline List_1_t3355509319 * get_settingslist_2() const { return ___settingslist_2; }
	inline List_1_t3355509319 ** get_address_of_settingslist_2() { return &___settingslist_2; }
	inline void set_settingslist_2(List_1_t3355509319 * value)
	{
		___settingslist_2 = value;
		Il2CppCodeGenWriteBarrier((&___settingslist_2), value);
	}

	inline static int32_t get_offset_of_MainMixer_3() { return static_cast<int32_t>(offsetof(settingsManager_t2086358119, ___MainMixer_3)); }
	inline AudioMixer_t3521020193 * get_MainMixer_3() const { return ___MainMixer_3; }
	inline AudioMixer_t3521020193 ** get_address_of_MainMixer_3() { return &___MainMixer_3; }
	inline void set_MainMixer_3(AudioMixer_t3521020193 * value)
	{
		___MainMixer_3 = value;
		Il2CppCodeGenWriteBarrier((&___MainMixer_3), value);
	}

	inline static int32_t get_offset_of_orgMusicVol_4() { return static_cast<int32_t>(offsetof(settingsManager_t2086358119, ___orgMusicVol_4)); }
	inline float get_orgMusicVol_4() const { return ___orgMusicVol_4; }
	inline float* get_address_of_orgMusicVol_4() { return &___orgMusicVol_4; }
	inline void set_orgMusicVol_4(float value)
	{
		___orgMusicVol_4 = value;
	}

	inline static int32_t get_offset_of_orgSoundVol_5() { return static_cast<int32_t>(offsetof(settingsManager_t2086358119, ___orgSoundVol_5)); }
	inline float get_orgSoundVol_5() const { return ___orgSoundVol_5; }
	inline float* get_address_of_orgSoundVol_5() { return &___orgSoundVol_5; }
	inline void set_orgSoundVol_5(float value)
	{
		___orgSoundVol_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSMANAGER_T2086358119_H
#ifndef ANIMATIONSCALING_T1595398591_H
#define ANIMATIONSCALING_T1595398591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// animationScaling
struct  animationScaling_t1595398591  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 animationScaling::orgSize
	Vector3_t3722313464  ___orgSize_2;
	// UnityEngine.Vector3 animationScaling::animaionSize
	Vector3_t3722313464  ___animaionSize_3;

public:
	inline static int32_t get_offset_of_orgSize_2() { return static_cast<int32_t>(offsetof(animationScaling_t1595398591, ___orgSize_2)); }
	inline Vector3_t3722313464  get_orgSize_2() const { return ___orgSize_2; }
	inline Vector3_t3722313464 * get_address_of_orgSize_2() { return &___orgSize_2; }
	inline void set_orgSize_2(Vector3_t3722313464  value)
	{
		___orgSize_2 = value;
	}

	inline static int32_t get_offset_of_animaionSize_3() { return static_cast<int32_t>(offsetof(animationScaling_t1595398591, ___animaionSize_3)); }
	inline Vector3_t3722313464  get_animaionSize_3() const { return ___animaionSize_3; }
	inline Vector3_t3722313464 * get_address_of_animaionSize_3() { return &___animaionSize_3; }
	inline void set_animaionSize_3(Vector3_t3722313464  value)
	{
		___animaionSize_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSCALING_T1595398591_H
#ifndef CAMERACONTROLLER_T3346819214_H
#define CAMERACONTROLLER_T3346819214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraController
struct  CameraController_t3346819214  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Quaternion CameraController::startRotation
	Quaternion_t2301928331  ___startRotation_2;
	// UnityEngine.Vector3 CameraController::startPosition
	Vector3_t3722313464  ___startPosition_3;
	// UnityEngine.Vector3 CameraController::parentStartPosition
	Vector3_t3722313464  ___parentStartPosition_4;

public:
	inline static int32_t get_offset_of_startRotation_2() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___startRotation_2)); }
	inline Quaternion_t2301928331  get_startRotation_2() const { return ___startRotation_2; }
	inline Quaternion_t2301928331 * get_address_of_startRotation_2() { return &___startRotation_2; }
	inline void set_startRotation_2(Quaternion_t2301928331  value)
	{
		___startRotation_2 = value;
	}

	inline static int32_t get_offset_of_startPosition_3() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___startPosition_3)); }
	inline Vector3_t3722313464  get_startPosition_3() const { return ___startPosition_3; }
	inline Vector3_t3722313464 * get_address_of_startPosition_3() { return &___startPosition_3; }
	inline void set_startPosition_3(Vector3_t3722313464  value)
	{
		___startPosition_3 = value;
	}

	inline static int32_t get_offset_of_parentStartPosition_4() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___parentStartPosition_4)); }
	inline Vector3_t3722313464  get_parentStartPosition_4() const { return ___parentStartPosition_4; }
	inline Vector3_t3722313464 * get_address_of_parentStartPosition_4() { return &___parentStartPosition_4; }
	inline void set_parentStartPosition_4(Vector3_t3722313464  value)
	{
		___parentStartPosition_4 = value;
	}
};

struct CameraController_t3346819214_StaticFields
{
public:
	// System.Func`2<UnityEngine.Touch,UnityEngine.Vector2> CameraController::<>f__am$cache0
	Func_2_t528868469 * ___U3CU3Ef__amU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(CameraController_t3346819214_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Func_2_t528868469 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Func_2_t528868469 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Func_2_t528868469 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONTROLLER_T3346819214_H
#ifndef BUTTONTRIGGERANIMATION_T4121295644_H
#define BUTTONTRIGGERANIMATION_T4121295644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// buttonTriggerAnimation
struct  buttonTriggerAnimation_t4121295644  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean buttonTriggerAnimation::animationBusy
	bool ___animationBusy_2;

public:
	inline static int32_t get_offset_of_animationBusy_2() { return static_cast<int32_t>(offsetof(buttonTriggerAnimation_t4121295644, ___animationBusy_2)); }
	inline bool get_animationBusy_2() const { return ___animationBusy_2; }
	inline bool* get_address_of_animationBusy_2() { return &___animationBusy_2; }
	inline void set_animationBusy_2(bool value)
	{
		___animationBusy_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONTRIGGERANIMATION_T4121295644_H
#ifndef ANIMATETEXTURE_T712563028_H
#define ANIMATETEXTURE_T712563028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// animateTexture
struct  animateTexture_t712563028  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Texture> animateTexture::textureFrames
	List_1_t839070149 * ___textureFrames_2;
	// UnityEngine.Material animateTexture::objectMaterial
	Material_t340375123 * ___objectMaterial_3;
	// System.Int32 animateTexture::currentIndex
	int32_t ___currentIndex_4;

public:
	inline static int32_t get_offset_of_textureFrames_2() { return static_cast<int32_t>(offsetof(animateTexture_t712563028, ___textureFrames_2)); }
	inline List_1_t839070149 * get_textureFrames_2() const { return ___textureFrames_2; }
	inline List_1_t839070149 ** get_address_of_textureFrames_2() { return &___textureFrames_2; }
	inline void set_textureFrames_2(List_1_t839070149 * value)
	{
		___textureFrames_2 = value;
		Il2CppCodeGenWriteBarrier((&___textureFrames_2), value);
	}

	inline static int32_t get_offset_of_objectMaterial_3() { return static_cast<int32_t>(offsetof(animateTexture_t712563028, ___objectMaterial_3)); }
	inline Material_t340375123 * get_objectMaterial_3() const { return ___objectMaterial_3; }
	inline Material_t340375123 ** get_address_of_objectMaterial_3() { return &___objectMaterial_3; }
	inline void set_objectMaterial_3(Material_t340375123 * value)
	{
		___objectMaterial_3 = value;
		Il2CppCodeGenWriteBarrier((&___objectMaterial_3), value);
	}

	inline static int32_t get_offset_of_currentIndex_4() { return static_cast<int32_t>(offsetof(animateTexture_t712563028, ___currentIndex_4)); }
	inline int32_t get_currentIndex_4() const { return ___currentIndex_4; }
	inline int32_t* get_address_of_currentIndex_4() { return &___currentIndex_4; }
	inline void set_currentIndex_4(int32_t value)
	{
		___currentIndex_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATETEXTURE_T712563028_H
#ifndef TRACKABLESETTINGS_T2862243993_H
#define TRACKABLESETTINGS_T2862243993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrackableSettings
struct  TrackableSettings_t2862243993  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean TrackableSettings::mExtTrackingEnabled
	bool ___mExtTrackingEnabled_2;

public:
	inline static int32_t get_offset_of_mExtTrackingEnabled_2() { return static_cast<int32_t>(offsetof(TrackableSettings_t2862243993, ___mExtTrackingEnabled_2)); }
	inline bool get_mExtTrackingEnabled_2() const { return ___mExtTrackingEnabled_2; }
	inline bool* get_address_of_mExtTrackingEnabled_2() { return &___mExtTrackingEnabled_2; }
	inline void set_mExtTrackingEnabled_2(bool value)
	{
		___mExtTrackingEnabled_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLESETTINGS_T2862243993_H
#ifndef MENUOPTIONS_T1951716431_H
#define MENUOPTIONS_T1951716431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuOptions
struct  MenuOptions_t1951716431  : public MonoBehaviour_t3962482529
{
public:
	// CameraSettings MenuOptions::mCamSettings
	CameraSettings_t3152619780 * ___mCamSettings_2;
	// TrackableSettings MenuOptions::mTrackableSettings
	TrackableSettings_t2862243993 * ___mTrackableSettings_3;
	// MenuAnimator MenuOptions::mMenuAnim
	MenuAnimator_t2112910832 * ___mMenuAnim_4;

public:
	inline static int32_t get_offset_of_mCamSettings_2() { return static_cast<int32_t>(offsetof(MenuOptions_t1951716431, ___mCamSettings_2)); }
	inline CameraSettings_t3152619780 * get_mCamSettings_2() const { return ___mCamSettings_2; }
	inline CameraSettings_t3152619780 ** get_address_of_mCamSettings_2() { return &___mCamSettings_2; }
	inline void set_mCamSettings_2(CameraSettings_t3152619780 * value)
	{
		___mCamSettings_2 = value;
		Il2CppCodeGenWriteBarrier((&___mCamSettings_2), value);
	}

	inline static int32_t get_offset_of_mTrackableSettings_3() { return static_cast<int32_t>(offsetof(MenuOptions_t1951716431, ___mTrackableSettings_3)); }
	inline TrackableSettings_t2862243993 * get_mTrackableSettings_3() const { return ___mTrackableSettings_3; }
	inline TrackableSettings_t2862243993 ** get_address_of_mTrackableSettings_3() { return &___mTrackableSettings_3; }
	inline void set_mTrackableSettings_3(TrackableSettings_t2862243993 * value)
	{
		___mTrackableSettings_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableSettings_3), value);
	}

	inline static int32_t get_offset_of_mMenuAnim_4() { return static_cast<int32_t>(offsetof(MenuOptions_t1951716431, ___mMenuAnim_4)); }
	inline MenuAnimator_t2112910832 * get_mMenuAnim_4() const { return ___mMenuAnim_4; }
	inline MenuAnimator_t2112910832 ** get_address_of_mMenuAnim_4() { return &___mMenuAnim_4; }
	inline void set_mMenuAnim_4(MenuAnimator_t2112910832 * value)
	{
		___mMenuAnim_4 = value;
		Il2CppCodeGenWriteBarrier((&___mMenuAnim_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUOPTIONS_T1951716431_H
#ifndef MENUANIMATOR_T2112910832_H
#define MENUANIMATOR_T2112910832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuAnimator
struct  MenuAnimator_t2112910832  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 MenuAnimator::mVisiblePos
	Vector3_t3722313464  ___mVisiblePos_2;
	// UnityEngine.Vector3 MenuAnimator::mInvisiblePos
	Vector3_t3722313464  ___mInvisiblePos_3;
	// System.Single MenuAnimator::mVisibility
	float ___mVisibility_4;
	// System.Boolean MenuAnimator::mVisible
	bool ___mVisible_5;
	// UnityEngine.Canvas MenuAnimator::mCanvas
	Canvas_t3310196443 * ___mCanvas_6;
	// MenuOptions MenuAnimator::mMenuOptions
	MenuOptions_t1951716431 * ___mMenuOptions_7;
	// System.Single MenuAnimator::SlidingTime
	float ___SlidingTime_8;

public:
	inline static int32_t get_offset_of_mVisiblePos_2() { return static_cast<int32_t>(offsetof(MenuAnimator_t2112910832, ___mVisiblePos_2)); }
	inline Vector3_t3722313464  get_mVisiblePos_2() const { return ___mVisiblePos_2; }
	inline Vector3_t3722313464 * get_address_of_mVisiblePos_2() { return &___mVisiblePos_2; }
	inline void set_mVisiblePos_2(Vector3_t3722313464  value)
	{
		___mVisiblePos_2 = value;
	}

	inline static int32_t get_offset_of_mInvisiblePos_3() { return static_cast<int32_t>(offsetof(MenuAnimator_t2112910832, ___mInvisiblePos_3)); }
	inline Vector3_t3722313464  get_mInvisiblePos_3() const { return ___mInvisiblePos_3; }
	inline Vector3_t3722313464 * get_address_of_mInvisiblePos_3() { return &___mInvisiblePos_3; }
	inline void set_mInvisiblePos_3(Vector3_t3722313464  value)
	{
		___mInvisiblePos_3 = value;
	}

	inline static int32_t get_offset_of_mVisibility_4() { return static_cast<int32_t>(offsetof(MenuAnimator_t2112910832, ___mVisibility_4)); }
	inline float get_mVisibility_4() const { return ___mVisibility_4; }
	inline float* get_address_of_mVisibility_4() { return &___mVisibility_4; }
	inline void set_mVisibility_4(float value)
	{
		___mVisibility_4 = value;
	}

	inline static int32_t get_offset_of_mVisible_5() { return static_cast<int32_t>(offsetof(MenuAnimator_t2112910832, ___mVisible_5)); }
	inline bool get_mVisible_5() const { return ___mVisible_5; }
	inline bool* get_address_of_mVisible_5() { return &___mVisible_5; }
	inline void set_mVisible_5(bool value)
	{
		___mVisible_5 = value;
	}

	inline static int32_t get_offset_of_mCanvas_6() { return static_cast<int32_t>(offsetof(MenuAnimator_t2112910832, ___mCanvas_6)); }
	inline Canvas_t3310196443 * get_mCanvas_6() const { return ___mCanvas_6; }
	inline Canvas_t3310196443 ** get_address_of_mCanvas_6() { return &___mCanvas_6; }
	inline void set_mCanvas_6(Canvas_t3310196443 * value)
	{
		___mCanvas_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCanvas_6), value);
	}

	inline static int32_t get_offset_of_mMenuOptions_7() { return static_cast<int32_t>(offsetof(MenuAnimator_t2112910832, ___mMenuOptions_7)); }
	inline MenuOptions_t1951716431 * get_mMenuOptions_7() const { return ___mMenuOptions_7; }
	inline MenuOptions_t1951716431 ** get_address_of_mMenuOptions_7() { return &___mMenuOptions_7; }
	inline void set_mMenuOptions_7(MenuOptions_t1951716431 * value)
	{
		___mMenuOptions_7 = value;
		Il2CppCodeGenWriteBarrier((&___mMenuOptions_7), value);
	}

	inline static int32_t get_offset_of_SlidingTime_8() { return static_cast<int32_t>(offsetof(MenuAnimator_t2112910832, ___SlidingTime_8)); }
	inline float get_SlidingTime_8() const { return ___SlidingTime_8; }
	inline float* get_address_of_SlidingTime_8() { return &___SlidingTime_8; }
	inline void set_SlidingTime_8(float value)
	{
		___SlidingTime_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUANIMATOR_T2112910832_H
#ifndef MODELSWAP_T1632145241_H
#define MODELSWAP_T1632145241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModelSwap
struct  ModelSwap_t1632145241  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ModelSwap::mDefaultModel
	GameObject_t1113636619 * ___mDefaultModel_2;
	// UnityEngine.GameObject ModelSwap::mExtTrackedModel
	GameObject_t1113636619 * ___mExtTrackedModel_3;
	// UnityEngine.GameObject ModelSwap::mActiveModel
	GameObject_t1113636619 * ___mActiveModel_4;
	// TrackableSettings ModelSwap::mTrackableSettings
	TrackableSettings_t2862243993 * ___mTrackableSettings_5;

public:
	inline static int32_t get_offset_of_mDefaultModel_2() { return static_cast<int32_t>(offsetof(ModelSwap_t1632145241, ___mDefaultModel_2)); }
	inline GameObject_t1113636619 * get_mDefaultModel_2() const { return ___mDefaultModel_2; }
	inline GameObject_t1113636619 ** get_address_of_mDefaultModel_2() { return &___mDefaultModel_2; }
	inline void set_mDefaultModel_2(GameObject_t1113636619 * value)
	{
		___mDefaultModel_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultModel_2), value);
	}

	inline static int32_t get_offset_of_mExtTrackedModel_3() { return static_cast<int32_t>(offsetof(ModelSwap_t1632145241, ___mExtTrackedModel_3)); }
	inline GameObject_t1113636619 * get_mExtTrackedModel_3() const { return ___mExtTrackedModel_3; }
	inline GameObject_t1113636619 ** get_address_of_mExtTrackedModel_3() { return &___mExtTrackedModel_3; }
	inline void set_mExtTrackedModel_3(GameObject_t1113636619 * value)
	{
		___mExtTrackedModel_3 = value;
		Il2CppCodeGenWriteBarrier((&___mExtTrackedModel_3), value);
	}

	inline static int32_t get_offset_of_mActiveModel_4() { return static_cast<int32_t>(offsetof(ModelSwap_t1632145241, ___mActiveModel_4)); }
	inline GameObject_t1113636619 * get_mActiveModel_4() const { return ___mActiveModel_4; }
	inline GameObject_t1113636619 ** get_address_of_mActiveModel_4() { return &___mActiveModel_4; }
	inline void set_mActiveModel_4(GameObject_t1113636619 * value)
	{
		___mActiveModel_4 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveModel_4), value);
	}

	inline static int32_t get_offset_of_mTrackableSettings_5() { return static_cast<int32_t>(offsetof(ModelSwap_t1632145241, ___mTrackableSettings_5)); }
	inline TrackableSettings_t2862243993 * get_mTrackableSettings_5() const { return ___mTrackableSettings_5; }
	inline TrackableSettings_t2862243993 ** get_address_of_mTrackableSettings_5() { return &___mTrackableSettings_5; }
	inline void set_mTrackableSettings_5(TrackableSettings_t2862243993 * value)
	{
		___mTrackableSettings_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableSettings_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELSWAP_T1632145241_H
#ifndef PAGETURN_T132420247_H
#define PAGETURN_T132420247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// pageTurn
struct  pageTurn_t132420247  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> pageTurn::Pages
	List_1_t2585711361 * ___Pages_2;
	// System.Int32 pageTurn::currentPage
	int32_t ___currentPage_3;

public:
	inline static int32_t get_offset_of_Pages_2() { return static_cast<int32_t>(offsetof(pageTurn_t132420247, ___Pages_2)); }
	inline List_1_t2585711361 * get_Pages_2() const { return ___Pages_2; }
	inline List_1_t2585711361 ** get_address_of_Pages_2() { return &___Pages_2; }
	inline void set_Pages_2(List_1_t2585711361 * value)
	{
		___Pages_2 = value;
		Il2CppCodeGenWriteBarrier((&___Pages_2), value);
	}

	inline static int32_t get_offset_of_currentPage_3() { return static_cast<int32_t>(offsetof(pageTurn_t132420247, ___currentPage_3)); }
	inline int32_t get_currentPage_3() const { return ___currentPage_3; }
	inline int32_t* get_address_of_currentPage_3() { return &___currentPage_3; }
	inline void set_currentPage_3(int32_t value)
	{
		___currentPage_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAGETURN_T132420247_H
#ifndef DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#define DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultTrackableEventHandler
struct  DefaultTrackableEventHandler_t1588957063  : public MonoBehaviour_t3962482529
{
public:
	// Vuforia.TrackableBehaviour DefaultTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t1113559212 * ___mTrackableBehaviour_2;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_2() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___mTrackableBehaviour_2)); }
	inline TrackableBehaviour_t1113559212 * get_mTrackableBehaviour_2() const { return ___mTrackableBehaviour_2; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mTrackableBehaviour_2() { return &___mTrackableBehaviour_2; }
	inline void set_mTrackableBehaviour_2(TrackableBehaviour_t1113559212 * value)
	{
		___mTrackableBehaviour_2 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#ifndef AUDIOSOURCE_T3935305588_H
#define AUDIOSOURCE_T3935305588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSource
struct  AudioSource_t3935305588  : public AudioBehaviour_t2879336574
{
public:
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::spatializerExtension
	AudioSourceExtension_t3064908834 * ___spatializerExtension_2;
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::ambisonicExtension
	AudioSourceExtension_t3064908834 * ___ambisonicExtension_3;

public:
	inline static int32_t get_offset_of_spatializerExtension_2() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___spatializerExtension_2)); }
	inline AudioSourceExtension_t3064908834 * get_spatializerExtension_2() const { return ___spatializerExtension_2; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_spatializerExtension_2() { return &___spatializerExtension_2; }
	inline void set_spatializerExtension_2(AudioSourceExtension_t3064908834 * value)
	{
		___spatializerExtension_2 = value;
		Il2CppCodeGenWriteBarrier((&___spatializerExtension_2), value);
	}

	inline static int32_t get_offset_of_ambisonicExtension_3() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___ambisonicExtension_3)); }
	inline AudioSourceExtension_t3064908834 * get_ambisonicExtension_3() const { return ___ambisonicExtension_3; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_ambisonicExtension_3() { return &___ambisonicExtension_3; }
	inline void set_ambisonicExtension_3(AudioSourceExtension_t3064908834 * value)
	{
		___ambisonicExtension_3 = value;
		Il2CppCodeGenWriteBarrier((&___ambisonicExtension_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSOURCE_T3935305588_H
#ifndef VUFORIAMONOBEHAVIOUR_T1150221792_H
#define VUFORIAMONOBEHAVIOUR_T1150221792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VuforiaMonoBehaviour
struct  VuforiaMonoBehaviour_t1150221792  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMONOBEHAVIOUR_T1150221792_H
#ifndef ENABLEMOUTHANIM_T2443685136_H
#define ENABLEMOUTHANIM_T2443685136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnableMouthAnim
struct  EnableMouthAnim_t2443685136  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animation EnableMouthAnim::mouthAnim
	Animation_t3648466861 * ___mouthAnim_2;

public:
	inline static int32_t get_offset_of_mouthAnim_2() { return static_cast<int32_t>(offsetof(EnableMouthAnim_t2443685136, ___mouthAnim_2)); }
	inline Animation_t3648466861 * get_mouthAnim_2() const { return ___mouthAnim_2; }
	inline Animation_t3648466861 ** get_address_of_mouthAnim_2() { return &___mouthAnim_2; }
	inline void set_mouthAnim_2(Animation_t3648466861 * value)
	{
		___mouthAnim_2 = value;
		Il2CppCodeGenWriteBarrier((&___mouthAnim_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENABLEMOUTHANIM_T2443685136_H
#ifndef DESTORYONAWAKE_T1274961527_H
#define DESTORYONAWAKE_T1274961527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// destoryOnAwake
struct  destoryOnAwake_t1274961527  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean destoryOnAwake::on
	bool ___on_2;

public:
	inline static int32_t get_offset_of_on_2() { return static_cast<int32_t>(offsetof(destoryOnAwake_t1274961527, ___on_2)); }
	inline bool get_on_2() const { return ___on_2; }
	inline bool* get_address_of_on_2() { return &___on_2; }
	inline void set_on_2(bool value)
	{
		___on_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTORYONAWAKE_T1274961527_H
#ifndef ONSCANCONTROLLER_T1472212342_H
#define ONSCANCONTROLLER_T1472212342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// onScanController
struct  onScanController_t1472212342  : public MonoBehaviour_t3962482529
{
public:
	// characterLoader onScanController::CharacterLoader
	characterLoader_t1326495876 * ___CharacterLoader_2;
	// System.String onScanController::CharacterID
	String_t* ___CharacterID_3;
	// System.String onScanController::scanText
	String_t* ___scanText_4;
	// UnityEngine.Sprite[] onScanController::symbols
	SpriteU5BU5D_t2581906349* ___symbols_5;
	// UnityEngine.GameObject onScanController::currentCharacter
	GameObject_t1113636619 * ___currentCharacter_6;
	// UnityEngine.GameObject onScanController::uiCanvas
	GameObject_t1113636619 * ___uiCanvas_7;
	// UnityEngine.GameObject onScanController::speechText
	GameObject_t1113636619 * ___speechText_8;
	// UnityEngine.GameObject onScanController::speechSymbols
	GameObject_t1113636619 * ___speechSymbols_9;
	// UnityEngine.Animation onScanController::mouthAnim
	Animation_t3648466861 * ___mouthAnim_10;
	// System.Boolean onScanController::imPlaying
	bool ___imPlaying_12;
	// System.Boolean onScanController::ShowTailAnimation
	bool ___ShowTailAnimation_13;

public:
	inline static int32_t get_offset_of_CharacterLoader_2() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___CharacterLoader_2)); }
	inline characterLoader_t1326495876 * get_CharacterLoader_2() const { return ___CharacterLoader_2; }
	inline characterLoader_t1326495876 ** get_address_of_CharacterLoader_2() { return &___CharacterLoader_2; }
	inline void set_CharacterLoader_2(characterLoader_t1326495876 * value)
	{
		___CharacterLoader_2 = value;
		Il2CppCodeGenWriteBarrier((&___CharacterLoader_2), value);
	}

	inline static int32_t get_offset_of_CharacterID_3() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___CharacterID_3)); }
	inline String_t* get_CharacterID_3() const { return ___CharacterID_3; }
	inline String_t** get_address_of_CharacterID_3() { return &___CharacterID_3; }
	inline void set_CharacterID_3(String_t* value)
	{
		___CharacterID_3 = value;
		Il2CppCodeGenWriteBarrier((&___CharacterID_3), value);
	}

	inline static int32_t get_offset_of_scanText_4() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___scanText_4)); }
	inline String_t* get_scanText_4() const { return ___scanText_4; }
	inline String_t** get_address_of_scanText_4() { return &___scanText_4; }
	inline void set_scanText_4(String_t* value)
	{
		___scanText_4 = value;
		Il2CppCodeGenWriteBarrier((&___scanText_4), value);
	}

	inline static int32_t get_offset_of_symbols_5() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___symbols_5)); }
	inline SpriteU5BU5D_t2581906349* get_symbols_5() const { return ___symbols_5; }
	inline SpriteU5BU5D_t2581906349** get_address_of_symbols_5() { return &___symbols_5; }
	inline void set_symbols_5(SpriteU5BU5D_t2581906349* value)
	{
		___symbols_5 = value;
		Il2CppCodeGenWriteBarrier((&___symbols_5), value);
	}

	inline static int32_t get_offset_of_currentCharacter_6() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___currentCharacter_6)); }
	inline GameObject_t1113636619 * get_currentCharacter_6() const { return ___currentCharacter_6; }
	inline GameObject_t1113636619 ** get_address_of_currentCharacter_6() { return &___currentCharacter_6; }
	inline void set_currentCharacter_6(GameObject_t1113636619 * value)
	{
		___currentCharacter_6 = value;
		Il2CppCodeGenWriteBarrier((&___currentCharacter_6), value);
	}

	inline static int32_t get_offset_of_uiCanvas_7() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___uiCanvas_7)); }
	inline GameObject_t1113636619 * get_uiCanvas_7() const { return ___uiCanvas_7; }
	inline GameObject_t1113636619 ** get_address_of_uiCanvas_7() { return &___uiCanvas_7; }
	inline void set_uiCanvas_7(GameObject_t1113636619 * value)
	{
		___uiCanvas_7 = value;
		Il2CppCodeGenWriteBarrier((&___uiCanvas_7), value);
	}

	inline static int32_t get_offset_of_speechText_8() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___speechText_8)); }
	inline GameObject_t1113636619 * get_speechText_8() const { return ___speechText_8; }
	inline GameObject_t1113636619 ** get_address_of_speechText_8() { return &___speechText_8; }
	inline void set_speechText_8(GameObject_t1113636619 * value)
	{
		___speechText_8 = value;
		Il2CppCodeGenWriteBarrier((&___speechText_8), value);
	}

	inline static int32_t get_offset_of_speechSymbols_9() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___speechSymbols_9)); }
	inline GameObject_t1113636619 * get_speechSymbols_9() const { return ___speechSymbols_9; }
	inline GameObject_t1113636619 ** get_address_of_speechSymbols_9() { return &___speechSymbols_9; }
	inline void set_speechSymbols_9(GameObject_t1113636619 * value)
	{
		___speechSymbols_9 = value;
		Il2CppCodeGenWriteBarrier((&___speechSymbols_9), value);
	}

	inline static int32_t get_offset_of_mouthAnim_10() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___mouthAnim_10)); }
	inline Animation_t3648466861 * get_mouthAnim_10() const { return ___mouthAnim_10; }
	inline Animation_t3648466861 ** get_address_of_mouthAnim_10() { return &___mouthAnim_10; }
	inline void set_mouthAnim_10(Animation_t3648466861 * value)
	{
		___mouthAnim_10 = value;
		Il2CppCodeGenWriteBarrier((&___mouthAnim_10), value);
	}

	inline static int32_t get_offset_of_imPlaying_12() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___imPlaying_12)); }
	inline bool get_imPlaying_12() const { return ___imPlaying_12; }
	inline bool* get_address_of_imPlaying_12() { return &___imPlaying_12; }
	inline void set_imPlaying_12(bool value)
	{
		___imPlaying_12 = value;
	}

	inline static int32_t get_offset_of_ShowTailAnimation_13() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___ShowTailAnimation_13)); }
	inline bool get_ShowTailAnimation_13() const { return ___ShowTailAnimation_13; }
	inline bool* get_address_of_ShowTailAnimation_13() { return &___ShowTailAnimation_13; }
	inline void set_ShowTailAnimation_13(bool value)
	{
		___ShowTailAnimation_13 = value;
	}
};

struct onScanController_t1472212342_StaticFields
{
public:
	// System.Boolean onScanController::someoneIsPlaying
	bool ___someoneIsPlaying_11;

public:
	inline static int32_t get_offset_of_someoneIsPlaying_11() { return static_cast<int32_t>(offsetof(onScanController_t1472212342_StaticFields, ___someoneIsPlaying_11)); }
	inline bool get_someoneIsPlaying_11() const { return ___someoneIsPlaying_11; }
	inline bool* get_address_of_someoneIsPlaying_11() { return &___someoneIsPlaying_11; }
	inline void set_someoneIsPlaying_11(bool value)
	{
		___someoneIsPlaying_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSCANCONTROLLER_T1472212342_H
#ifndef FRAMERATESETTINGS_T3598747490_H
#define FRAMERATESETTINGS_T3598747490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FrameRateSettings
struct  FrameRateSettings_t3598747490  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMERATESETTINGS_T3598747490_H
#ifndef MATH3D_T1427253148_H
#define MATH3D_T1427253148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Math3d
struct  Math3d_t1427253148  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Math3d_t1427253148_StaticFields
{
public:
	// UnityEngine.Transform Math3d::tempChild
	Transform_t3600365921 * ___tempChild_2;
	// UnityEngine.Transform Math3d::tempParent
	Transform_t3600365921 * ___tempParent_3;

public:
	inline static int32_t get_offset_of_tempChild_2() { return static_cast<int32_t>(offsetof(Math3d_t1427253148_StaticFields, ___tempChild_2)); }
	inline Transform_t3600365921 * get_tempChild_2() const { return ___tempChild_2; }
	inline Transform_t3600365921 ** get_address_of_tempChild_2() { return &___tempChild_2; }
	inline void set_tempChild_2(Transform_t3600365921 * value)
	{
		___tempChild_2 = value;
		Il2CppCodeGenWriteBarrier((&___tempChild_2), value);
	}

	inline static int32_t get_offset_of_tempParent_3() { return static_cast<int32_t>(offsetof(Math3d_t1427253148_StaticFields, ___tempParent_3)); }
	inline Transform_t3600365921 * get_tempParent_3() const { return ___tempParent_3; }
	inline Transform_t3600365921 ** get_address_of_tempParent_3() { return &___tempParent_3; }
	inline void set_tempParent_3(Transform_t3600365921 * value)
	{
		___tempParent_3 = value;
		Il2CppCodeGenWriteBarrier((&___tempParent_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATH3D_T1427253148_H
#ifndef LOADINGSCREEN_T2154736699_H
#define LOADINGSCREEN_T2154736699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadingScreen
struct  LoadingScreen_t2154736699  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean LoadingScreen::mChangeLevel
	bool ___mChangeLevel_2;
	// UnityEngine.UI.RawImage LoadingScreen::mUISpinner
	RawImage_t3182918964 * ___mUISpinner_3;

public:
	inline static int32_t get_offset_of_mChangeLevel_2() { return static_cast<int32_t>(offsetof(LoadingScreen_t2154736699, ___mChangeLevel_2)); }
	inline bool get_mChangeLevel_2() const { return ___mChangeLevel_2; }
	inline bool* get_address_of_mChangeLevel_2() { return &___mChangeLevel_2; }
	inline void set_mChangeLevel_2(bool value)
	{
		___mChangeLevel_2 = value;
	}

	inline static int32_t get_offset_of_mUISpinner_3() { return static_cast<int32_t>(offsetof(LoadingScreen_t2154736699, ___mUISpinner_3)); }
	inline RawImage_t3182918964 * get_mUISpinner_3() const { return ___mUISpinner_3; }
	inline RawImage_t3182918964 ** get_address_of_mUISpinner_3() { return &___mUISpinner_3; }
	inline void set_mUISpinner_3(RawImage_t3182918964 * value)
	{
		___mUISpinner_3 = value;
		Il2CppCodeGenWriteBarrier((&___mUISpinner_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADINGSCREEN_T2154736699_H
#ifndef INITERRORHANDLER_T2159361531_H
#define INITERRORHANDLER_T2159361531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InitErrorHandler
struct  InitErrorHandler_t2159361531  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text InitErrorHandler::errorText
	Text_t1901882714 * ___errorText_2;
	// UnityEngine.Canvas InitErrorHandler::errorCanvas
	Canvas_t3310196443 * ___errorCanvas_3;
	// System.String InitErrorHandler::key
	String_t* ___key_4;

public:
	inline static int32_t get_offset_of_errorText_2() { return static_cast<int32_t>(offsetof(InitErrorHandler_t2159361531, ___errorText_2)); }
	inline Text_t1901882714 * get_errorText_2() const { return ___errorText_2; }
	inline Text_t1901882714 ** get_address_of_errorText_2() { return &___errorText_2; }
	inline void set_errorText_2(Text_t1901882714 * value)
	{
		___errorText_2 = value;
		Il2CppCodeGenWriteBarrier((&___errorText_2), value);
	}

	inline static int32_t get_offset_of_errorCanvas_3() { return static_cast<int32_t>(offsetof(InitErrorHandler_t2159361531, ___errorCanvas_3)); }
	inline Canvas_t3310196443 * get_errorCanvas_3() const { return ___errorCanvas_3; }
	inline Canvas_t3310196443 ** get_address_of_errorCanvas_3() { return &___errorCanvas_3; }
	inline void set_errorCanvas_3(Canvas_t3310196443 * value)
	{
		___errorCanvas_3 = value;
		Il2CppCodeGenWriteBarrier((&___errorCanvas_3), value);
	}

	inline static int32_t get_offset_of_key_4() { return static_cast<int32_t>(offsetof(InitErrorHandler_t2159361531, ___key_4)); }
	inline String_t* get_key_4() const { return ___key_4; }
	inline String_t** get_address_of_key_4() { return &___key_4; }
	inline void set_key_4(String_t* value)
	{
		___key_4 = value;
		Il2CppCodeGenWriteBarrier((&___key_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITERRORHANDLER_T2159361531_H
#ifndef CHARACTERCYCLER_T3521054542_H
#define CHARACTERCYCLER_T3521054542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterCycler
struct  CharacterCycler_t3521054542  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 CharacterCycler::currentVal
	int32_t ___currentVal_2;
	// UnityEngine.GameObject[] CharacterCycler::charList
	GameObjectU5BU5D_t3328599146* ___charList_3;
	// System.Int32 CharacterCycler::n
	int32_t ___n_4;

public:
	inline static int32_t get_offset_of_currentVal_2() { return static_cast<int32_t>(offsetof(CharacterCycler_t3521054542, ___currentVal_2)); }
	inline int32_t get_currentVal_2() const { return ___currentVal_2; }
	inline int32_t* get_address_of_currentVal_2() { return &___currentVal_2; }
	inline void set_currentVal_2(int32_t value)
	{
		___currentVal_2 = value;
	}

	inline static int32_t get_offset_of_charList_3() { return static_cast<int32_t>(offsetof(CharacterCycler_t3521054542, ___charList_3)); }
	inline GameObjectU5BU5D_t3328599146* get_charList_3() const { return ___charList_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_charList_3() { return &___charList_3; }
	inline void set_charList_3(GameObjectU5BU5D_t3328599146* value)
	{
		___charList_3 = value;
		Il2CppCodeGenWriteBarrier((&___charList_3), value);
	}

	inline static int32_t get_offset_of_n_4() { return static_cast<int32_t>(offsetof(CharacterCycler_t3521054542, ___n_4)); }
	inline int32_t get_n_4() const { return ___n_4; }
	inline int32_t* get_address_of_n_4() { return &___n_4; }
	inline void set_n_4(int32_t value)
	{
		___n_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERCYCLER_T3521054542_H
#ifndef SMOOTHCAMERA_T4222009864_H
#define SMOOTHCAMERA_T4222009864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmoothCamera
struct  SmoothCamera_t4222009864  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 SmoothCamera::smoothingFrames
	int32_t ___smoothingFrames_2;
	// Vuforia.VuforiaBehaviour SmoothCamera::qcarBehavior
	VuforiaBehaviour_t2151848540 * ___qcarBehavior_3;
	// UnityEngine.Quaternion SmoothCamera::smoothedRotation
	Quaternion_t2301928331  ___smoothedRotation_4;
	// UnityEngine.Vector3 SmoothCamera::smoothedPosition
	Vector3_t3722313464  ___smoothedPosition_5;
	// System.Collections.Generic.Queue`1<UnityEngine.Quaternion> SmoothCamera::rotations
	Queue_1_t2148187825 * ___rotations_6;
	// System.Collections.Generic.Queue`1<UnityEngine.Vector3> SmoothCamera::positions
	Queue_1_t3568572958 * ___positions_7;

public:
	inline static int32_t get_offset_of_smoothingFrames_2() { return static_cast<int32_t>(offsetof(SmoothCamera_t4222009864, ___smoothingFrames_2)); }
	inline int32_t get_smoothingFrames_2() const { return ___smoothingFrames_2; }
	inline int32_t* get_address_of_smoothingFrames_2() { return &___smoothingFrames_2; }
	inline void set_smoothingFrames_2(int32_t value)
	{
		___smoothingFrames_2 = value;
	}

	inline static int32_t get_offset_of_qcarBehavior_3() { return static_cast<int32_t>(offsetof(SmoothCamera_t4222009864, ___qcarBehavior_3)); }
	inline VuforiaBehaviour_t2151848540 * get_qcarBehavior_3() const { return ___qcarBehavior_3; }
	inline VuforiaBehaviour_t2151848540 ** get_address_of_qcarBehavior_3() { return &___qcarBehavior_3; }
	inline void set_qcarBehavior_3(VuforiaBehaviour_t2151848540 * value)
	{
		___qcarBehavior_3 = value;
		Il2CppCodeGenWriteBarrier((&___qcarBehavior_3), value);
	}

	inline static int32_t get_offset_of_smoothedRotation_4() { return static_cast<int32_t>(offsetof(SmoothCamera_t4222009864, ___smoothedRotation_4)); }
	inline Quaternion_t2301928331  get_smoothedRotation_4() const { return ___smoothedRotation_4; }
	inline Quaternion_t2301928331 * get_address_of_smoothedRotation_4() { return &___smoothedRotation_4; }
	inline void set_smoothedRotation_4(Quaternion_t2301928331  value)
	{
		___smoothedRotation_4 = value;
	}

	inline static int32_t get_offset_of_smoothedPosition_5() { return static_cast<int32_t>(offsetof(SmoothCamera_t4222009864, ___smoothedPosition_5)); }
	inline Vector3_t3722313464  get_smoothedPosition_5() const { return ___smoothedPosition_5; }
	inline Vector3_t3722313464 * get_address_of_smoothedPosition_5() { return &___smoothedPosition_5; }
	inline void set_smoothedPosition_5(Vector3_t3722313464  value)
	{
		___smoothedPosition_5 = value;
	}

	inline static int32_t get_offset_of_rotations_6() { return static_cast<int32_t>(offsetof(SmoothCamera_t4222009864, ___rotations_6)); }
	inline Queue_1_t2148187825 * get_rotations_6() const { return ___rotations_6; }
	inline Queue_1_t2148187825 ** get_address_of_rotations_6() { return &___rotations_6; }
	inline void set_rotations_6(Queue_1_t2148187825 * value)
	{
		___rotations_6 = value;
		Il2CppCodeGenWriteBarrier((&___rotations_6), value);
	}

	inline static int32_t get_offset_of_positions_7() { return static_cast<int32_t>(offsetof(SmoothCamera_t4222009864, ___positions_7)); }
	inline Queue_1_t3568572958 * get_positions_7() const { return ___positions_7; }
	inline Queue_1_t3568572958 ** get_address_of_positions_7() { return &___positions_7; }
	inline void set_positions_7(Queue_1_t3568572958 * value)
	{
		___positions_7 = value;
		Il2CppCodeGenWriteBarrier((&___positions_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHCAMERA_T4222009864_H
#ifndef CAMERASETTINGS_T3152619780_H
#define CAMERASETTINGS_T3152619780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraSettings
struct  CameraSettings_t3152619780  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CameraSettings::mVuforiaStarted
	bool ___mVuforiaStarted_2;
	// System.Boolean CameraSettings::mAutofocusEnabled
	bool ___mAutofocusEnabled_3;
	// System.Boolean CameraSettings::mFlashTorchEnabled
	bool ___mFlashTorchEnabled_4;
	// Vuforia.CameraDevice/CameraDirection CameraSettings::mActiveDirection
	int32_t ___mActiveDirection_5;

public:
	inline static int32_t get_offset_of_mVuforiaStarted_2() { return static_cast<int32_t>(offsetof(CameraSettings_t3152619780, ___mVuforiaStarted_2)); }
	inline bool get_mVuforiaStarted_2() const { return ___mVuforiaStarted_2; }
	inline bool* get_address_of_mVuforiaStarted_2() { return &___mVuforiaStarted_2; }
	inline void set_mVuforiaStarted_2(bool value)
	{
		___mVuforiaStarted_2 = value;
	}

	inline static int32_t get_offset_of_mAutofocusEnabled_3() { return static_cast<int32_t>(offsetof(CameraSettings_t3152619780, ___mAutofocusEnabled_3)); }
	inline bool get_mAutofocusEnabled_3() const { return ___mAutofocusEnabled_3; }
	inline bool* get_address_of_mAutofocusEnabled_3() { return &___mAutofocusEnabled_3; }
	inline void set_mAutofocusEnabled_3(bool value)
	{
		___mAutofocusEnabled_3 = value;
	}

	inline static int32_t get_offset_of_mFlashTorchEnabled_4() { return static_cast<int32_t>(offsetof(CameraSettings_t3152619780, ___mFlashTorchEnabled_4)); }
	inline bool get_mFlashTorchEnabled_4() const { return ___mFlashTorchEnabled_4; }
	inline bool* get_address_of_mFlashTorchEnabled_4() { return &___mFlashTorchEnabled_4; }
	inline void set_mFlashTorchEnabled_4(bool value)
	{
		___mFlashTorchEnabled_4 = value;
	}

	inline static int32_t get_offset_of_mActiveDirection_5() { return static_cast<int32_t>(offsetof(CameraSettings_t3152619780, ___mActiveDirection_5)); }
	inline int32_t get_mActiveDirection_5() const { return ___mActiveDirection_5; }
	inline int32_t* get_address_of_mActiveDirection_5() { return &___mActiveDirection_5; }
	inline void set_mActiveDirection_5(int32_t value)
	{
		___mActiveDirection_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERASETTINGS_T3152619780_H
#ifndef SYMBOLSSWITCH_T3635132447_H
#define SYMBOLSSWITCH_T3635132447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// symbolsSwitch
struct  symbolsSwitch_t3635132447  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean symbolsSwitch::useSymbols
	bool ___useSymbols_2;

public:
	inline static int32_t get_offset_of_useSymbols_2() { return static_cast<int32_t>(offsetof(symbolsSwitch_t3635132447, ___useSymbols_2)); }
	inline bool get_useSymbols_2() const { return ___useSymbols_2; }
	inline bool* get_address_of_useSymbols_2() { return &___useSymbols_2; }
	inline void set_useSymbols_2(bool value)
	{
		___useSymbols_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMBOLSSWITCH_T3635132447_H
#ifndef TAPHANDLER_T334234343_H
#define TAPHANDLER_T334234343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TapHandler
struct  TapHandler_t334234343  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TapHandler::mTimeSinceLastTap
	float ___mTimeSinceLastTap_3;
	// MenuAnimator TapHandler::mMenuAnim
	MenuAnimator_t2112910832 * ___mMenuAnim_4;
	// System.Int32 TapHandler::mTapCount
	int32_t ___mTapCount_5;

public:
	inline static int32_t get_offset_of_mTimeSinceLastTap_3() { return static_cast<int32_t>(offsetof(TapHandler_t334234343, ___mTimeSinceLastTap_3)); }
	inline float get_mTimeSinceLastTap_3() const { return ___mTimeSinceLastTap_3; }
	inline float* get_address_of_mTimeSinceLastTap_3() { return &___mTimeSinceLastTap_3; }
	inline void set_mTimeSinceLastTap_3(float value)
	{
		___mTimeSinceLastTap_3 = value;
	}

	inline static int32_t get_offset_of_mMenuAnim_4() { return static_cast<int32_t>(offsetof(TapHandler_t334234343, ___mMenuAnim_4)); }
	inline MenuAnimator_t2112910832 * get_mMenuAnim_4() const { return ___mMenuAnim_4; }
	inline MenuAnimator_t2112910832 ** get_address_of_mMenuAnim_4() { return &___mMenuAnim_4; }
	inline void set_mMenuAnim_4(MenuAnimator_t2112910832 * value)
	{
		___mMenuAnim_4 = value;
		Il2CppCodeGenWriteBarrier((&___mMenuAnim_4), value);
	}

	inline static int32_t get_offset_of_mTapCount_5() { return static_cast<int32_t>(offsetof(TapHandler_t334234343, ___mTapCount_5)); }
	inline int32_t get_mTapCount_5() const { return ___mTapCount_5; }
	inline int32_t* get_address_of_mTapCount_5() { return &___mTapCount_5; }
	inline void set_mTapCount_5(int32_t value)
	{
		___mTapCount_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPHANDLER_T334234343_H
#ifndef SETSETTINGS_T4116128020_H
#define SETSETTINGS_T4116128020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// setSettings
struct  setSettings_t4116128020  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject setSettings::settingsManager
	GameObject_t1113636619 * ___settingsManager_2;

public:
	inline static int32_t get_offset_of_settingsManager_2() { return static_cast<int32_t>(offsetof(setSettings_t4116128020, ___settingsManager_2)); }
	inline GameObject_t1113636619 * get_settingsManager_2() const { return ___settingsManager_2; }
	inline GameObject_t1113636619 ** get_address_of_settingsManager_2() { return &___settingsManager_2; }
	inline void set_settingsManager_2(GameObject_t1113636619 * value)
	{
		___settingsManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___settingsManager_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETSETTINGS_T4116128020_H
#ifndef CHARACTERLOADER_T1326495876_H
#define CHARACTERLOADER_T1326495876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// characterLoader
struct  characterLoader_t1326495876  : public MonoBehaviour_t3962482529
{
public:
	// characterSlot[] characterLoader::characterList
	characterSlotU5BU5D_t2121073899* ___characterList_2;

public:
	inline static int32_t get_offset_of_characterList_2() { return static_cast<int32_t>(offsetof(characterLoader_t1326495876, ___characterList_2)); }
	inline characterSlotU5BU5D_t2121073899* get_characterList_2() const { return ___characterList_2; }
	inline characterSlotU5BU5D_t2121073899** get_address_of_characterList_2() { return &___characterList_2; }
	inline void set_characterList_2(characterSlotU5BU5D_t2121073899* value)
	{
		___characterList_2 = value;
		Il2CppCodeGenWriteBarrier((&___characterList_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERLOADER_T1326495876_H
#ifndef SYMBOLPREFABDEFINER_T2798204667_H
#define SYMBOLPREFABDEFINER_T2798204667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// symbolPrefabDefiner
struct  symbolPrefabDefiner_t2798204667  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject symbolPrefabDefiner::symbolPrefab
	GameObject_t1113636619 * ___symbolPrefab_2;

public:
	inline static int32_t get_offset_of_symbolPrefab_2() { return static_cast<int32_t>(offsetof(symbolPrefabDefiner_t2798204667, ___symbolPrefab_2)); }
	inline GameObject_t1113636619 * get_symbolPrefab_2() const { return ___symbolPrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_symbolPrefab_2() { return &___symbolPrefab_2; }
	inline void set_symbolPrefab_2(GameObject_t1113636619 * value)
	{
		___symbolPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___symbolPrefab_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMBOLPREFABDEFINER_T2798204667_H
#ifndef TOUCHCAMERA_T3780128488_H
#define TOUCHCAMERA_T3780128488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchCamera
struct  TouchCamera_t3780128488  : public MonoBehaviour_t3962482529
{
public:
	// System.Nullable`1<UnityEngine.Vector2>[] TouchCamera::oldTouchPositions
	Nullable_1U5BU5D_t3148003288* ___oldTouchPositions_2;
	// UnityEngine.Vector2 TouchCamera::oldTouchVector
	Vector2_t2156229523  ___oldTouchVector_3;
	// System.Single TouchCamera::oldTouchDistance
	float ___oldTouchDistance_4;

public:
	inline static int32_t get_offset_of_oldTouchPositions_2() { return static_cast<int32_t>(offsetof(TouchCamera_t3780128488, ___oldTouchPositions_2)); }
	inline Nullable_1U5BU5D_t3148003288* get_oldTouchPositions_2() const { return ___oldTouchPositions_2; }
	inline Nullable_1U5BU5D_t3148003288** get_address_of_oldTouchPositions_2() { return &___oldTouchPositions_2; }
	inline void set_oldTouchPositions_2(Nullable_1U5BU5D_t3148003288* value)
	{
		___oldTouchPositions_2 = value;
		Il2CppCodeGenWriteBarrier((&___oldTouchPositions_2), value);
	}

	inline static int32_t get_offset_of_oldTouchVector_3() { return static_cast<int32_t>(offsetof(TouchCamera_t3780128488, ___oldTouchVector_3)); }
	inline Vector2_t2156229523  get_oldTouchVector_3() const { return ___oldTouchVector_3; }
	inline Vector2_t2156229523 * get_address_of_oldTouchVector_3() { return &___oldTouchVector_3; }
	inline void set_oldTouchVector_3(Vector2_t2156229523  value)
	{
		___oldTouchVector_3 = value;
	}

	inline static int32_t get_offset_of_oldTouchDistance_4() { return static_cast<int32_t>(offsetof(TouchCamera_t3780128488, ___oldTouchDistance_4)); }
	inline float get_oldTouchDistance_4() const { return ___oldTouchDistance_4; }
	inline float* get_address_of_oldTouchDistance_4() { return &___oldTouchDistance_4; }
	inline void set_oldTouchDistance_4(float value)
	{
		___oldTouchDistance_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHCAMERA_T3780128488_H
#ifndef VUFORIABEHAVIOUR_T2151848540_H
#define VUFORIABEHAVIOUR_T2151848540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaBehaviour
struct  VuforiaBehaviour_t2151848540  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// Vuforia.VuforiaARController/WorldCenterMode Vuforia.VuforiaBehaviour::mWorldCenterMode
	int32_t ___mWorldCenterMode_3;
	// Vuforia.TrackableBehaviour Vuforia.VuforiaBehaviour::mWorldCenter
	TrackableBehaviour_t1113559212 * ___mWorldCenter_4;
	// System.Boolean Vuforia.VuforiaBehaviour::mAppIsQuitting
	bool ___mAppIsQuitting_5;
	// System.Action Vuforia.VuforiaBehaviour::AwakeEvent
	Action_t1264377477 * ___AwakeEvent_8;
	// System.Action Vuforia.VuforiaBehaviour::OnEnableEvent
	Action_t1264377477 * ___OnEnableEvent_9;
	// System.Action Vuforia.VuforiaBehaviour::StartEvent
	Action_t1264377477 * ___StartEvent_10;
	// System.Action Vuforia.VuforiaBehaviour::UpdateEvent
	Action_t1264377477 * ___UpdateEvent_11;
	// System.Action Vuforia.VuforiaBehaviour::OnLevelWasLoadedEvent
	Action_t1264377477 * ___OnLevelWasLoadedEvent_12;
	// System.Action`1<System.Boolean> Vuforia.VuforiaBehaviour::OnApplicationPauseEvent
	Action_1_t269755560 * ___OnApplicationPauseEvent_13;
	// System.Action Vuforia.VuforiaBehaviour::OnDisableEvent
	Action_t1264377477 * ___OnDisableEvent_14;
	// System.Action Vuforia.VuforiaBehaviour::OnDestroyEvent
	Action_t1264377477 * ___OnDestroyEvent_15;

public:
	inline static int32_t get_offset_of_mWorldCenterMode_3() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t2151848540, ___mWorldCenterMode_3)); }
	inline int32_t get_mWorldCenterMode_3() const { return ___mWorldCenterMode_3; }
	inline int32_t* get_address_of_mWorldCenterMode_3() { return &___mWorldCenterMode_3; }
	inline void set_mWorldCenterMode_3(int32_t value)
	{
		___mWorldCenterMode_3 = value;
	}

	inline static int32_t get_offset_of_mWorldCenter_4() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t2151848540, ___mWorldCenter_4)); }
	inline TrackableBehaviour_t1113559212 * get_mWorldCenter_4() const { return ___mWorldCenter_4; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mWorldCenter_4() { return &___mWorldCenter_4; }
	inline void set_mWorldCenter_4(TrackableBehaviour_t1113559212 * value)
	{
		___mWorldCenter_4 = value;
		Il2CppCodeGenWriteBarrier((&___mWorldCenter_4), value);
	}

	inline static int32_t get_offset_of_mAppIsQuitting_5() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t2151848540, ___mAppIsQuitting_5)); }
	inline bool get_mAppIsQuitting_5() const { return ___mAppIsQuitting_5; }
	inline bool* get_address_of_mAppIsQuitting_5() { return &___mAppIsQuitting_5; }
	inline void set_mAppIsQuitting_5(bool value)
	{
		___mAppIsQuitting_5 = value;
	}

	inline static int32_t get_offset_of_AwakeEvent_8() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t2151848540, ___AwakeEvent_8)); }
	inline Action_t1264377477 * get_AwakeEvent_8() const { return ___AwakeEvent_8; }
	inline Action_t1264377477 ** get_address_of_AwakeEvent_8() { return &___AwakeEvent_8; }
	inline void set_AwakeEvent_8(Action_t1264377477 * value)
	{
		___AwakeEvent_8 = value;
		Il2CppCodeGenWriteBarrier((&___AwakeEvent_8), value);
	}

	inline static int32_t get_offset_of_OnEnableEvent_9() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t2151848540, ___OnEnableEvent_9)); }
	inline Action_t1264377477 * get_OnEnableEvent_9() const { return ___OnEnableEvent_9; }
	inline Action_t1264377477 ** get_address_of_OnEnableEvent_9() { return &___OnEnableEvent_9; }
	inline void set_OnEnableEvent_9(Action_t1264377477 * value)
	{
		___OnEnableEvent_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnEnableEvent_9), value);
	}

	inline static int32_t get_offset_of_StartEvent_10() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t2151848540, ___StartEvent_10)); }
	inline Action_t1264377477 * get_StartEvent_10() const { return ___StartEvent_10; }
	inline Action_t1264377477 ** get_address_of_StartEvent_10() { return &___StartEvent_10; }
	inline void set_StartEvent_10(Action_t1264377477 * value)
	{
		___StartEvent_10 = value;
		Il2CppCodeGenWriteBarrier((&___StartEvent_10), value);
	}

	inline static int32_t get_offset_of_UpdateEvent_11() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t2151848540, ___UpdateEvent_11)); }
	inline Action_t1264377477 * get_UpdateEvent_11() const { return ___UpdateEvent_11; }
	inline Action_t1264377477 ** get_address_of_UpdateEvent_11() { return &___UpdateEvent_11; }
	inline void set_UpdateEvent_11(Action_t1264377477 * value)
	{
		___UpdateEvent_11 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateEvent_11), value);
	}

	inline static int32_t get_offset_of_OnLevelWasLoadedEvent_12() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t2151848540, ___OnLevelWasLoadedEvent_12)); }
	inline Action_t1264377477 * get_OnLevelWasLoadedEvent_12() const { return ___OnLevelWasLoadedEvent_12; }
	inline Action_t1264377477 ** get_address_of_OnLevelWasLoadedEvent_12() { return &___OnLevelWasLoadedEvent_12; }
	inline void set_OnLevelWasLoadedEvent_12(Action_t1264377477 * value)
	{
		___OnLevelWasLoadedEvent_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnLevelWasLoadedEvent_12), value);
	}

	inline static int32_t get_offset_of_OnApplicationPauseEvent_13() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t2151848540, ___OnApplicationPauseEvent_13)); }
	inline Action_1_t269755560 * get_OnApplicationPauseEvent_13() const { return ___OnApplicationPauseEvent_13; }
	inline Action_1_t269755560 ** get_address_of_OnApplicationPauseEvent_13() { return &___OnApplicationPauseEvent_13; }
	inline void set_OnApplicationPauseEvent_13(Action_1_t269755560 * value)
	{
		___OnApplicationPauseEvent_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnApplicationPauseEvent_13), value);
	}

	inline static int32_t get_offset_of_OnDisableEvent_14() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t2151848540, ___OnDisableEvent_14)); }
	inline Action_t1264377477 * get_OnDisableEvent_14() const { return ___OnDisableEvent_14; }
	inline Action_t1264377477 ** get_address_of_OnDisableEvent_14() { return &___OnDisableEvent_14; }
	inline void set_OnDisableEvent_14(Action_t1264377477 * value)
	{
		___OnDisableEvent_14 = value;
		Il2CppCodeGenWriteBarrier((&___OnDisableEvent_14), value);
	}

	inline static int32_t get_offset_of_OnDestroyEvent_15() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t2151848540, ___OnDestroyEvent_15)); }
	inline Action_t1264377477 * get_OnDestroyEvent_15() const { return ___OnDestroyEvent_15; }
	inline Action_t1264377477 ** get_address_of_OnDestroyEvent_15() { return &___OnDestroyEvent_15; }
	inline void set_OnDestroyEvent_15(Action_t1264377477 * value)
	{
		___OnDestroyEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___OnDestroyEvent_15), value);
	}
};

struct VuforiaBehaviour_t2151848540_StaticFields
{
public:
	// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::mVuforiaBehaviour
	VuforiaBehaviour_t2151848540 * ___mVuforiaBehaviour_2;
	// System.Action`1<Vuforia.VuforiaBehaviour> Vuforia.VuforiaBehaviour::BehaviourCreated
	Action_1_t2324316135 * ___BehaviourCreated_6;
	// System.Action`1<Vuforia.VuforiaBehaviour> Vuforia.VuforiaBehaviour::BehaviourDestroyed
	Action_1_t2324316135 * ___BehaviourDestroyed_7;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviour_2() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t2151848540_StaticFields, ___mVuforiaBehaviour_2)); }
	inline VuforiaBehaviour_t2151848540 * get_mVuforiaBehaviour_2() const { return ___mVuforiaBehaviour_2; }
	inline VuforiaBehaviour_t2151848540 ** get_address_of_mVuforiaBehaviour_2() { return &___mVuforiaBehaviour_2; }
	inline void set_mVuforiaBehaviour_2(VuforiaBehaviour_t2151848540 * value)
	{
		___mVuforiaBehaviour_2 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_2), value);
	}

	inline static int32_t get_offset_of_BehaviourCreated_6() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t2151848540_StaticFields, ___BehaviourCreated_6)); }
	inline Action_1_t2324316135 * get_BehaviourCreated_6() const { return ___BehaviourCreated_6; }
	inline Action_1_t2324316135 ** get_address_of_BehaviourCreated_6() { return &___BehaviourCreated_6; }
	inline void set_BehaviourCreated_6(Action_1_t2324316135 * value)
	{
		___BehaviourCreated_6 = value;
		Il2CppCodeGenWriteBarrier((&___BehaviourCreated_6), value);
	}

	inline static int32_t get_offset_of_BehaviourDestroyed_7() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t2151848540_StaticFields, ___BehaviourDestroyed_7)); }
	inline Action_1_t2324316135 * get_BehaviourDestroyed_7() const { return ___BehaviourDestroyed_7; }
	inline Action_1_t2324316135 ** get_address_of_BehaviourDestroyed_7() { return &___BehaviourDestroyed_7; }
	inline void set_BehaviourDestroyed_7(Action_1_t2324316135 * value)
	{
		___BehaviourDestroyed_7 = value;
		Il2CppCodeGenWriteBarrier((&___BehaviourDestroyed_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIABEHAVIOUR_T2151848540_H
#ifndef TRACKABLEBEHAVIOUR_T1113559212_H
#define TRACKABLEBEHAVIOUR_T1113559212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour
struct  TrackableBehaviour_t1113559212  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// System.Double Vuforia.TrackableBehaviour::<TimeStamp>k__BackingField
	double ___U3CTimeStampU3Ek__BackingField_2;
	// System.String Vuforia.TrackableBehaviour::mTrackableName
	String_t* ___mTrackableName_3;
	// System.Boolean Vuforia.TrackableBehaviour::mPreserveChildSize
	bool ___mPreserveChildSize_4;
	// System.Boolean Vuforia.TrackableBehaviour::mInitializedInEditor
	bool ___mInitializedInEditor_5;
	// UnityEngine.Vector3 Vuforia.TrackableBehaviour::mPreviousScale
	Vector3_t3722313464  ___mPreviousScale_6;
	// Vuforia.TrackableBehaviour/Status Vuforia.TrackableBehaviour::mStatus
	int32_t ___mStatus_7;
	// Vuforia.Trackable Vuforia.TrackableBehaviour::mTrackable
	RuntimeObject* ___mTrackable_8;
	// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler> Vuforia.TrackableBehaviour::mTrackableEventHandlers
	List_1_t2968050330 * ___mTrackableEventHandlers_9;

public:
	inline static int32_t get_offset_of_U3CTimeStampU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___U3CTimeStampU3Ek__BackingField_2)); }
	inline double get_U3CTimeStampU3Ek__BackingField_2() const { return ___U3CTimeStampU3Ek__BackingField_2; }
	inline double* get_address_of_U3CTimeStampU3Ek__BackingField_2() { return &___U3CTimeStampU3Ek__BackingField_2; }
	inline void set_U3CTimeStampU3Ek__BackingField_2(double value)
	{
		___U3CTimeStampU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_mTrackableName_3() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mTrackableName_3)); }
	inline String_t* get_mTrackableName_3() const { return ___mTrackableName_3; }
	inline String_t** get_address_of_mTrackableName_3() { return &___mTrackableName_3; }
	inline void set_mTrackableName_3(String_t* value)
	{
		___mTrackableName_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableName_3), value);
	}

	inline static int32_t get_offset_of_mPreserveChildSize_4() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mPreserveChildSize_4)); }
	inline bool get_mPreserveChildSize_4() const { return ___mPreserveChildSize_4; }
	inline bool* get_address_of_mPreserveChildSize_4() { return &___mPreserveChildSize_4; }
	inline void set_mPreserveChildSize_4(bool value)
	{
		___mPreserveChildSize_4 = value;
	}

	inline static int32_t get_offset_of_mInitializedInEditor_5() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mInitializedInEditor_5)); }
	inline bool get_mInitializedInEditor_5() const { return ___mInitializedInEditor_5; }
	inline bool* get_address_of_mInitializedInEditor_5() { return &___mInitializedInEditor_5; }
	inline void set_mInitializedInEditor_5(bool value)
	{
		___mInitializedInEditor_5 = value;
	}

	inline static int32_t get_offset_of_mPreviousScale_6() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mPreviousScale_6)); }
	inline Vector3_t3722313464  get_mPreviousScale_6() const { return ___mPreviousScale_6; }
	inline Vector3_t3722313464 * get_address_of_mPreviousScale_6() { return &___mPreviousScale_6; }
	inline void set_mPreviousScale_6(Vector3_t3722313464  value)
	{
		___mPreviousScale_6 = value;
	}

	inline static int32_t get_offset_of_mStatus_7() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mStatus_7)); }
	inline int32_t get_mStatus_7() const { return ___mStatus_7; }
	inline int32_t* get_address_of_mStatus_7() { return &___mStatus_7; }
	inline void set_mStatus_7(int32_t value)
	{
		___mStatus_7 = value;
	}

	inline static int32_t get_offset_of_mTrackable_8() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mTrackable_8)); }
	inline RuntimeObject* get_mTrackable_8() const { return ___mTrackable_8; }
	inline RuntimeObject** get_address_of_mTrackable_8() { return &___mTrackable_8; }
	inline void set_mTrackable_8(RuntimeObject* value)
	{
		___mTrackable_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackable_8), value);
	}

	inline static int32_t get_offset_of_mTrackableEventHandlers_9() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mTrackableEventHandlers_9)); }
	inline List_1_t2968050330 * get_mTrackableEventHandlers_9() const { return ___mTrackableEventHandlers_9; }
	inline List_1_t2968050330 ** get_address_of_mTrackableEventHandlers_9() { return &___mTrackableEventHandlers_9; }
	inline void set_mTrackableEventHandlers_9(List_1_t2968050330 * value)
	{
		___mTrackableEventHandlers_9 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableEventHandlers_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEBEHAVIOUR_T1113559212_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t2598313366 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_3)); }
	inline Navigation_t3049316579  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t3049316579  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_5)); }
	inline ColorBlock_t2139031574  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t2139031574  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_6)); }
	inline SpriteState_t1362986479  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t1362986479  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_9)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_15)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_2)); }
	inline List_1_t427135887 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t427135887 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t427135887 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H
#define DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultInitializationErrorHandler
struct  DefaultInitializationErrorHandler_t3109936861  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// System.String DefaultInitializationErrorHandler::mErrorText
	String_t* ___mErrorText_2;
	// System.Boolean DefaultInitializationErrorHandler::mErrorOccurred
	bool ___mErrorOccurred_3;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::bodyStyle
	GUIStyle_t3956901511 * ___bodyStyle_5;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::headerStyle
	GUIStyle_t3956901511 * ___headerStyle_6;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::footerStyle
	GUIStyle_t3956901511 * ___footerStyle_7;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::bodyTexture
	Texture2D_t3840446185 * ___bodyTexture_8;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::headerTexture
	Texture2D_t3840446185 * ___headerTexture_9;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::footerTexture
	Texture2D_t3840446185 * ___footerTexture_10;

public:
	inline static int32_t get_offset_of_mErrorText_2() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___mErrorText_2)); }
	inline String_t* get_mErrorText_2() const { return ___mErrorText_2; }
	inline String_t** get_address_of_mErrorText_2() { return &___mErrorText_2; }
	inline void set_mErrorText_2(String_t* value)
	{
		___mErrorText_2 = value;
		Il2CppCodeGenWriteBarrier((&___mErrorText_2), value);
	}

	inline static int32_t get_offset_of_mErrorOccurred_3() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___mErrorOccurred_3)); }
	inline bool get_mErrorOccurred_3() const { return ___mErrorOccurred_3; }
	inline bool* get_address_of_mErrorOccurred_3() { return &___mErrorOccurred_3; }
	inline void set_mErrorOccurred_3(bool value)
	{
		___mErrorOccurred_3 = value;
	}

	inline static int32_t get_offset_of_bodyStyle_5() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___bodyStyle_5)); }
	inline GUIStyle_t3956901511 * get_bodyStyle_5() const { return ___bodyStyle_5; }
	inline GUIStyle_t3956901511 ** get_address_of_bodyStyle_5() { return &___bodyStyle_5; }
	inline void set_bodyStyle_5(GUIStyle_t3956901511 * value)
	{
		___bodyStyle_5 = value;
		Il2CppCodeGenWriteBarrier((&___bodyStyle_5), value);
	}

	inline static int32_t get_offset_of_headerStyle_6() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___headerStyle_6)); }
	inline GUIStyle_t3956901511 * get_headerStyle_6() const { return ___headerStyle_6; }
	inline GUIStyle_t3956901511 ** get_address_of_headerStyle_6() { return &___headerStyle_6; }
	inline void set_headerStyle_6(GUIStyle_t3956901511 * value)
	{
		___headerStyle_6 = value;
		Il2CppCodeGenWriteBarrier((&___headerStyle_6), value);
	}

	inline static int32_t get_offset_of_footerStyle_7() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___footerStyle_7)); }
	inline GUIStyle_t3956901511 * get_footerStyle_7() const { return ___footerStyle_7; }
	inline GUIStyle_t3956901511 ** get_address_of_footerStyle_7() { return &___footerStyle_7; }
	inline void set_footerStyle_7(GUIStyle_t3956901511 * value)
	{
		___footerStyle_7 = value;
		Il2CppCodeGenWriteBarrier((&___footerStyle_7), value);
	}

	inline static int32_t get_offset_of_bodyTexture_8() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___bodyTexture_8)); }
	inline Texture2D_t3840446185 * get_bodyTexture_8() const { return ___bodyTexture_8; }
	inline Texture2D_t3840446185 ** get_address_of_bodyTexture_8() { return &___bodyTexture_8; }
	inline void set_bodyTexture_8(Texture2D_t3840446185 * value)
	{
		___bodyTexture_8 = value;
		Il2CppCodeGenWriteBarrier((&___bodyTexture_8), value);
	}

	inline static int32_t get_offset_of_headerTexture_9() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___headerTexture_9)); }
	inline Texture2D_t3840446185 * get_headerTexture_9() const { return ___headerTexture_9; }
	inline Texture2D_t3840446185 ** get_address_of_headerTexture_9() { return &___headerTexture_9; }
	inline void set_headerTexture_9(Texture2D_t3840446185 * value)
	{
		___headerTexture_9 = value;
		Il2CppCodeGenWriteBarrier((&___headerTexture_9), value);
	}

	inline static int32_t get_offset_of_footerTexture_10() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___footerTexture_10)); }
	inline Texture2D_t3840446185 * get_footerTexture_10() const { return ___footerTexture_10; }
	inline Texture2D_t3840446185 ** get_address_of_footerTexture_10() { return &___footerTexture_10; }
	inline void set_footerTexture_10(Texture2D_t3840446185 * value)
	{
		___footerTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___footerTexture_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H
#ifndef TOGGLE_T2735377061_H
#define TOGGLE_T2735377061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Toggle
struct  Toggle_t2735377061  : public Selectable_t3250028441
{
public:
	// UnityEngine.UI.Toggle/ToggleTransition UnityEngine.UI.Toggle::toggleTransition
	int32_t ___toggleTransition_16;
	// UnityEngine.UI.Graphic UnityEngine.UI.Toggle::graphic
	Graphic_t1660335611 * ___graphic_17;
	// UnityEngine.UI.ToggleGroup UnityEngine.UI.Toggle::m_Group
	ToggleGroup_t123837990 * ___m_Group_18;
	// UnityEngine.UI.Toggle/ToggleEvent UnityEngine.UI.Toggle::onValueChanged
	ToggleEvent_t1873685584 * ___onValueChanged_19;
	// System.Boolean UnityEngine.UI.Toggle::m_IsOn
	bool ___m_IsOn_20;

public:
	inline static int32_t get_offset_of_toggleTransition_16() { return static_cast<int32_t>(offsetof(Toggle_t2735377061, ___toggleTransition_16)); }
	inline int32_t get_toggleTransition_16() const { return ___toggleTransition_16; }
	inline int32_t* get_address_of_toggleTransition_16() { return &___toggleTransition_16; }
	inline void set_toggleTransition_16(int32_t value)
	{
		___toggleTransition_16 = value;
	}

	inline static int32_t get_offset_of_graphic_17() { return static_cast<int32_t>(offsetof(Toggle_t2735377061, ___graphic_17)); }
	inline Graphic_t1660335611 * get_graphic_17() const { return ___graphic_17; }
	inline Graphic_t1660335611 ** get_address_of_graphic_17() { return &___graphic_17; }
	inline void set_graphic_17(Graphic_t1660335611 * value)
	{
		___graphic_17 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_17), value);
	}

	inline static int32_t get_offset_of_m_Group_18() { return static_cast<int32_t>(offsetof(Toggle_t2735377061, ___m_Group_18)); }
	inline ToggleGroup_t123837990 * get_m_Group_18() const { return ___m_Group_18; }
	inline ToggleGroup_t123837990 ** get_address_of_m_Group_18() { return &___m_Group_18; }
	inline void set_m_Group_18(ToggleGroup_t123837990 * value)
	{
		___m_Group_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Group_18), value);
	}

	inline static int32_t get_offset_of_onValueChanged_19() { return static_cast<int32_t>(offsetof(Toggle_t2735377061, ___onValueChanged_19)); }
	inline ToggleEvent_t1873685584 * get_onValueChanged_19() const { return ___onValueChanged_19; }
	inline ToggleEvent_t1873685584 ** get_address_of_onValueChanged_19() { return &___onValueChanged_19; }
	inline void set_onValueChanged_19(ToggleEvent_t1873685584 * value)
	{
		___onValueChanged_19 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_19), value);
	}

	inline static int32_t get_offset_of_m_IsOn_20() { return static_cast<int32_t>(offsetof(Toggle_t2735377061, ___m_IsOn_20)); }
	inline bool get_m_IsOn_20() const { return ___m_IsOn_20; }
	inline bool* get_address_of_m_IsOn_20() { return &___m_IsOn_20; }
	inline void set_m_IsOn_20(bool value)
	{
		___m_IsOn_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLE_T2735377061_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef DATASETTRACKABLEBEHAVIOUR_T3430730379_H
#define DATASETTRACKABLEBEHAVIOUR_T3430730379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DataSetTrackableBehaviour
struct  DataSetTrackableBehaviour_t3430730379  : public TrackableBehaviour_t1113559212
{
public:
	// System.String Vuforia.DataSetTrackableBehaviour::mDataSetPath
	String_t* ___mDataSetPath_10;

public:
	inline static int32_t get_offset_of_mDataSetPath_10() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3430730379, ___mDataSetPath_10)); }
	inline String_t* get_mDataSetPath_10() const { return ___mDataSetPath_10; }
	inline String_t** get_address_of_mDataSetPath_10() { return &___mDataSetPath_10; }
	inline void set_mDataSetPath_10(String_t* value)
	{
		___mDataSetPath_10 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSetPath_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETTRACKABLEBEHAVIOUR_T3430730379_H
#ifndef OBJECTTARGETBEHAVIOUR_T728125005_H
#define OBJECTTARGETBEHAVIOUR_T728125005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ObjectTargetBehaviour
struct  ObjectTargetBehaviour_t728125005  : public DataSetTrackableBehaviour_t3430730379
{
public:
	// Vuforia.ObjectTarget Vuforia.ObjectTargetBehaviour::mObjectTarget
	RuntimeObject* ___mObjectTarget_11;
	// System.Single Vuforia.ObjectTargetBehaviour::mAspectRatioXY
	float ___mAspectRatioXY_12;
	// System.Single Vuforia.ObjectTargetBehaviour::mAspectRatioXZ
	float ___mAspectRatioXZ_13;
	// System.Boolean Vuforia.ObjectTargetBehaviour::mShowBoundingBox
	bool ___mShowBoundingBox_14;
	// UnityEngine.Vector3 Vuforia.ObjectTargetBehaviour::mBBoxMin
	Vector3_t3722313464  ___mBBoxMin_15;
	// UnityEngine.Vector3 Vuforia.ObjectTargetBehaviour::mBBoxMax
	Vector3_t3722313464  ___mBBoxMax_16;
	// UnityEngine.Texture2D Vuforia.ObjectTargetBehaviour::mPreviewImage
	Texture2D_t3840446185 * ___mPreviewImage_17;
	// System.Single Vuforia.ObjectTargetBehaviour::mLength
	float ___mLength_18;
	// System.Single Vuforia.ObjectTargetBehaviour::mWidth
	float ___mWidth_19;
	// System.Single Vuforia.ObjectTargetBehaviour::mHeight
	float ___mHeight_20;
	// System.Single Vuforia.ObjectTargetBehaviour::mLastTransformScale
	float ___mLastTransformScale_21;
	// UnityEngine.Vector3 Vuforia.ObjectTargetBehaviour::mLastSize
	Vector3_t3722313464  ___mLastSize_22;

public:
	inline static int32_t get_offset_of_mObjectTarget_11() { return static_cast<int32_t>(offsetof(ObjectTargetBehaviour_t728125005, ___mObjectTarget_11)); }
	inline RuntimeObject* get_mObjectTarget_11() const { return ___mObjectTarget_11; }
	inline RuntimeObject** get_address_of_mObjectTarget_11() { return &___mObjectTarget_11; }
	inline void set_mObjectTarget_11(RuntimeObject* value)
	{
		___mObjectTarget_11 = value;
		Il2CppCodeGenWriteBarrier((&___mObjectTarget_11), value);
	}

	inline static int32_t get_offset_of_mAspectRatioXY_12() { return static_cast<int32_t>(offsetof(ObjectTargetBehaviour_t728125005, ___mAspectRatioXY_12)); }
	inline float get_mAspectRatioXY_12() const { return ___mAspectRatioXY_12; }
	inline float* get_address_of_mAspectRatioXY_12() { return &___mAspectRatioXY_12; }
	inline void set_mAspectRatioXY_12(float value)
	{
		___mAspectRatioXY_12 = value;
	}

	inline static int32_t get_offset_of_mAspectRatioXZ_13() { return static_cast<int32_t>(offsetof(ObjectTargetBehaviour_t728125005, ___mAspectRatioXZ_13)); }
	inline float get_mAspectRatioXZ_13() const { return ___mAspectRatioXZ_13; }
	inline float* get_address_of_mAspectRatioXZ_13() { return &___mAspectRatioXZ_13; }
	inline void set_mAspectRatioXZ_13(float value)
	{
		___mAspectRatioXZ_13 = value;
	}

	inline static int32_t get_offset_of_mShowBoundingBox_14() { return static_cast<int32_t>(offsetof(ObjectTargetBehaviour_t728125005, ___mShowBoundingBox_14)); }
	inline bool get_mShowBoundingBox_14() const { return ___mShowBoundingBox_14; }
	inline bool* get_address_of_mShowBoundingBox_14() { return &___mShowBoundingBox_14; }
	inline void set_mShowBoundingBox_14(bool value)
	{
		___mShowBoundingBox_14 = value;
	}

	inline static int32_t get_offset_of_mBBoxMin_15() { return static_cast<int32_t>(offsetof(ObjectTargetBehaviour_t728125005, ___mBBoxMin_15)); }
	inline Vector3_t3722313464  get_mBBoxMin_15() const { return ___mBBoxMin_15; }
	inline Vector3_t3722313464 * get_address_of_mBBoxMin_15() { return &___mBBoxMin_15; }
	inline void set_mBBoxMin_15(Vector3_t3722313464  value)
	{
		___mBBoxMin_15 = value;
	}

	inline static int32_t get_offset_of_mBBoxMax_16() { return static_cast<int32_t>(offsetof(ObjectTargetBehaviour_t728125005, ___mBBoxMax_16)); }
	inline Vector3_t3722313464  get_mBBoxMax_16() const { return ___mBBoxMax_16; }
	inline Vector3_t3722313464 * get_address_of_mBBoxMax_16() { return &___mBBoxMax_16; }
	inline void set_mBBoxMax_16(Vector3_t3722313464  value)
	{
		___mBBoxMax_16 = value;
	}

	inline static int32_t get_offset_of_mPreviewImage_17() { return static_cast<int32_t>(offsetof(ObjectTargetBehaviour_t728125005, ___mPreviewImage_17)); }
	inline Texture2D_t3840446185 * get_mPreviewImage_17() const { return ___mPreviewImage_17; }
	inline Texture2D_t3840446185 ** get_address_of_mPreviewImage_17() { return &___mPreviewImage_17; }
	inline void set_mPreviewImage_17(Texture2D_t3840446185 * value)
	{
		___mPreviewImage_17 = value;
		Il2CppCodeGenWriteBarrier((&___mPreviewImage_17), value);
	}

	inline static int32_t get_offset_of_mLength_18() { return static_cast<int32_t>(offsetof(ObjectTargetBehaviour_t728125005, ___mLength_18)); }
	inline float get_mLength_18() const { return ___mLength_18; }
	inline float* get_address_of_mLength_18() { return &___mLength_18; }
	inline void set_mLength_18(float value)
	{
		___mLength_18 = value;
	}

	inline static int32_t get_offset_of_mWidth_19() { return static_cast<int32_t>(offsetof(ObjectTargetBehaviour_t728125005, ___mWidth_19)); }
	inline float get_mWidth_19() const { return ___mWidth_19; }
	inline float* get_address_of_mWidth_19() { return &___mWidth_19; }
	inline void set_mWidth_19(float value)
	{
		___mWidth_19 = value;
	}

	inline static int32_t get_offset_of_mHeight_20() { return static_cast<int32_t>(offsetof(ObjectTargetBehaviour_t728125005, ___mHeight_20)); }
	inline float get_mHeight_20() const { return ___mHeight_20; }
	inline float* get_address_of_mHeight_20() { return &___mHeight_20; }
	inline void set_mHeight_20(float value)
	{
		___mHeight_20 = value;
	}

	inline static int32_t get_offset_of_mLastTransformScale_21() { return static_cast<int32_t>(offsetof(ObjectTargetBehaviour_t728125005, ___mLastTransformScale_21)); }
	inline float get_mLastTransformScale_21() const { return ___mLastTransformScale_21; }
	inline float* get_address_of_mLastTransformScale_21() { return &___mLastTransformScale_21; }
	inline void set_mLastTransformScale_21(float value)
	{
		___mLastTransformScale_21 = value;
	}

	inline static int32_t get_offset_of_mLastSize_22() { return static_cast<int32_t>(offsetof(ObjectTargetBehaviour_t728125005, ___mLastSize_22)); }
	inline Vector3_t3722313464  get_mLastSize_22() const { return ___mLastSize_22; }
	inline Vector3_t3722313464 * get_address_of_mLastSize_22() { return &___mLastSize_22; }
	inline void set_mLastSize_22(Vector3_t3722313464  value)
	{
		___mLastSize_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTARGETBEHAVIOUR_T728125005_H
#ifndef CYLINDERTARGETBEHAVIOUR_T822809409_H
#define CYLINDERTARGETBEHAVIOUR_T822809409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CylinderTargetBehaviour
struct  CylinderTargetBehaviour_t822809409  : public DataSetTrackableBehaviour_t3430730379
{
public:
	// Vuforia.CylinderTarget Vuforia.CylinderTargetBehaviour::mCylinderTarget
	RuntimeObject* ___mCylinderTarget_11;
	// System.Single Vuforia.CylinderTargetBehaviour::mTopDiameterRatio
	float ___mTopDiameterRatio_12;
	// System.Single Vuforia.CylinderTargetBehaviour::mBottomDiameterRatio
	float ___mBottomDiameterRatio_13;
	// System.Single Vuforia.CylinderTargetBehaviour::mSideLength
	float ___mSideLength_14;
	// System.Single Vuforia.CylinderTargetBehaviour::mTopDiameter
	float ___mTopDiameter_15;
	// System.Single Vuforia.CylinderTargetBehaviour::mBottomDiameter
	float ___mBottomDiameter_16;
	// System.Int32 Vuforia.CylinderTargetBehaviour::mFrameIndex
	int32_t ___mFrameIndex_17;
	// System.Int32 Vuforia.CylinderTargetBehaviour::mUpdateFrameIndex
	int32_t ___mUpdateFrameIndex_18;
	// System.Single Vuforia.CylinderTargetBehaviour::mFutureScale
	float ___mFutureScale_19;
	// System.Single Vuforia.CylinderTargetBehaviour::mLastTransformScale
	float ___mLastTransformScale_20;

public:
	inline static int32_t get_offset_of_mCylinderTarget_11() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_t822809409, ___mCylinderTarget_11)); }
	inline RuntimeObject* get_mCylinderTarget_11() const { return ___mCylinderTarget_11; }
	inline RuntimeObject** get_address_of_mCylinderTarget_11() { return &___mCylinderTarget_11; }
	inline void set_mCylinderTarget_11(RuntimeObject* value)
	{
		___mCylinderTarget_11 = value;
		Il2CppCodeGenWriteBarrier((&___mCylinderTarget_11), value);
	}

	inline static int32_t get_offset_of_mTopDiameterRatio_12() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_t822809409, ___mTopDiameterRatio_12)); }
	inline float get_mTopDiameterRatio_12() const { return ___mTopDiameterRatio_12; }
	inline float* get_address_of_mTopDiameterRatio_12() { return &___mTopDiameterRatio_12; }
	inline void set_mTopDiameterRatio_12(float value)
	{
		___mTopDiameterRatio_12 = value;
	}

	inline static int32_t get_offset_of_mBottomDiameterRatio_13() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_t822809409, ___mBottomDiameterRatio_13)); }
	inline float get_mBottomDiameterRatio_13() const { return ___mBottomDiameterRatio_13; }
	inline float* get_address_of_mBottomDiameterRatio_13() { return &___mBottomDiameterRatio_13; }
	inline void set_mBottomDiameterRatio_13(float value)
	{
		___mBottomDiameterRatio_13 = value;
	}

	inline static int32_t get_offset_of_mSideLength_14() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_t822809409, ___mSideLength_14)); }
	inline float get_mSideLength_14() const { return ___mSideLength_14; }
	inline float* get_address_of_mSideLength_14() { return &___mSideLength_14; }
	inline void set_mSideLength_14(float value)
	{
		___mSideLength_14 = value;
	}

	inline static int32_t get_offset_of_mTopDiameter_15() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_t822809409, ___mTopDiameter_15)); }
	inline float get_mTopDiameter_15() const { return ___mTopDiameter_15; }
	inline float* get_address_of_mTopDiameter_15() { return &___mTopDiameter_15; }
	inline void set_mTopDiameter_15(float value)
	{
		___mTopDiameter_15 = value;
	}

	inline static int32_t get_offset_of_mBottomDiameter_16() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_t822809409, ___mBottomDiameter_16)); }
	inline float get_mBottomDiameter_16() const { return ___mBottomDiameter_16; }
	inline float* get_address_of_mBottomDiameter_16() { return &___mBottomDiameter_16; }
	inline void set_mBottomDiameter_16(float value)
	{
		___mBottomDiameter_16 = value;
	}

	inline static int32_t get_offset_of_mFrameIndex_17() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_t822809409, ___mFrameIndex_17)); }
	inline int32_t get_mFrameIndex_17() const { return ___mFrameIndex_17; }
	inline int32_t* get_address_of_mFrameIndex_17() { return &___mFrameIndex_17; }
	inline void set_mFrameIndex_17(int32_t value)
	{
		___mFrameIndex_17 = value;
	}

	inline static int32_t get_offset_of_mUpdateFrameIndex_18() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_t822809409, ___mUpdateFrameIndex_18)); }
	inline int32_t get_mUpdateFrameIndex_18() const { return ___mUpdateFrameIndex_18; }
	inline int32_t* get_address_of_mUpdateFrameIndex_18() { return &___mUpdateFrameIndex_18; }
	inline void set_mUpdateFrameIndex_18(int32_t value)
	{
		___mUpdateFrameIndex_18 = value;
	}

	inline static int32_t get_offset_of_mFutureScale_19() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_t822809409, ___mFutureScale_19)); }
	inline float get_mFutureScale_19() const { return ___mFutureScale_19; }
	inline float* get_address_of_mFutureScale_19() { return &___mFutureScale_19; }
	inline void set_mFutureScale_19(float value)
	{
		___mFutureScale_19 = value;
	}

	inline static int32_t get_offset_of_mLastTransformScale_20() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_t822809409, ___mLastTransformScale_20)); }
	inline float get_mLastTransformScale_20() const { return ___mLastTransformScale_20; }
	inline float* get_address_of_mLastTransformScale_20() { return &___mLastTransformScale_20; }
	inline void set_mLastTransformScale_20(float value)
	{
		___mLastTransformScale_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYLINDERTARGETBEHAVIOUR_T822809409_H
#ifndef VUMARKBEHAVIOUR_T1178230459_H
#define VUMARKBEHAVIOUR_T1178230459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuMarkBehaviour
struct  VuMarkBehaviour_t1178230459  : public DataSetTrackableBehaviour_t3430730379
{
public:
	// System.Single Vuforia.VuMarkBehaviour::mAspectRatio
	float ___mAspectRatio_11;
	// System.Single Vuforia.VuMarkBehaviour::mWidth
	float ___mWidth_12;
	// System.Single Vuforia.VuMarkBehaviour::mHeight
	float ___mHeight_13;
	// System.String Vuforia.VuMarkBehaviour::mPreviewImage
	String_t* ___mPreviewImage_14;
	// Vuforia.InstanceIdType Vuforia.VuMarkBehaviour::mIdType
	int32_t ___mIdType_15;
	// System.Int32 Vuforia.VuMarkBehaviour::mIdLength
	int32_t ___mIdLength_16;
	// UnityEngine.Rect Vuforia.VuMarkBehaviour::mBoundingBox
	Rect_t2360479859  ___mBoundingBox_17;
	// UnityEngine.Vector2 Vuforia.VuMarkBehaviour::mOrigin
	Vector2_t2156229523  ___mOrigin_18;
	// System.Boolean Vuforia.VuMarkBehaviour::mTrackingFromRuntimeAppearance
	bool ___mTrackingFromRuntimeAppearance_19;
	// Vuforia.VuMarkTemplate Vuforia.VuMarkBehaviour::mVuMarkTemplate
	RuntimeObject* ___mVuMarkTemplate_20;
	// Vuforia.VuMarkTarget Vuforia.VuMarkBehaviour::mVuMarkTarget
	RuntimeObject* ___mVuMarkTarget_21;
	// System.Int32 Vuforia.VuMarkBehaviour::mVuMarkResultId
	int32_t ___mVuMarkResultId_22;
	// System.Action Vuforia.VuMarkBehaviour::mOnTargetAssigned
	Action_t1264377477 * ___mOnTargetAssigned_23;
	// System.Action Vuforia.VuMarkBehaviour::mOnTargetLost
	Action_t1264377477 * ___mOnTargetLost_24;
	// System.Single Vuforia.VuMarkBehaviour::mLastTransformScale
	float ___mLastTransformScale_25;
	// UnityEngine.Vector2 Vuforia.VuMarkBehaviour::mLastSize
	Vector2_t2156229523  ___mLastSize_26;

public:
	inline static int32_t get_offset_of_mAspectRatio_11() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t1178230459, ___mAspectRatio_11)); }
	inline float get_mAspectRatio_11() const { return ___mAspectRatio_11; }
	inline float* get_address_of_mAspectRatio_11() { return &___mAspectRatio_11; }
	inline void set_mAspectRatio_11(float value)
	{
		___mAspectRatio_11 = value;
	}

	inline static int32_t get_offset_of_mWidth_12() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t1178230459, ___mWidth_12)); }
	inline float get_mWidth_12() const { return ___mWidth_12; }
	inline float* get_address_of_mWidth_12() { return &___mWidth_12; }
	inline void set_mWidth_12(float value)
	{
		___mWidth_12 = value;
	}

	inline static int32_t get_offset_of_mHeight_13() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t1178230459, ___mHeight_13)); }
	inline float get_mHeight_13() const { return ___mHeight_13; }
	inline float* get_address_of_mHeight_13() { return &___mHeight_13; }
	inline void set_mHeight_13(float value)
	{
		___mHeight_13 = value;
	}

	inline static int32_t get_offset_of_mPreviewImage_14() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t1178230459, ___mPreviewImage_14)); }
	inline String_t* get_mPreviewImage_14() const { return ___mPreviewImage_14; }
	inline String_t** get_address_of_mPreviewImage_14() { return &___mPreviewImage_14; }
	inline void set_mPreviewImage_14(String_t* value)
	{
		___mPreviewImage_14 = value;
		Il2CppCodeGenWriteBarrier((&___mPreviewImage_14), value);
	}

	inline static int32_t get_offset_of_mIdType_15() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t1178230459, ___mIdType_15)); }
	inline int32_t get_mIdType_15() const { return ___mIdType_15; }
	inline int32_t* get_address_of_mIdType_15() { return &___mIdType_15; }
	inline void set_mIdType_15(int32_t value)
	{
		___mIdType_15 = value;
	}

	inline static int32_t get_offset_of_mIdLength_16() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t1178230459, ___mIdLength_16)); }
	inline int32_t get_mIdLength_16() const { return ___mIdLength_16; }
	inline int32_t* get_address_of_mIdLength_16() { return &___mIdLength_16; }
	inline void set_mIdLength_16(int32_t value)
	{
		___mIdLength_16 = value;
	}

	inline static int32_t get_offset_of_mBoundingBox_17() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t1178230459, ___mBoundingBox_17)); }
	inline Rect_t2360479859  get_mBoundingBox_17() const { return ___mBoundingBox_17; }
	inline Rect_t2360479859 * get_address_of_mBoundingBox_17() { return &___mBoundingBox_17; }
	inline void set_mBoundingBox_17(Rect_t2360479859  value)
	{
		___mBoundingBox_17 = value;
	}

	inline static int32_t get_offset_of_mOrigin_18() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t1178230459, ___mOrigin_18)); }
	inline Vector2_t2156229523  get_mOrigin_18() const { return ___mOrigin_18; }
	inline Vector2_t2156229523 * get_address_of_mOrigin_18() { return &___mOrigin_18; }
	inline void set_mOrigin_18(Vector2_t2156229523  value)
	{
		___mOrigin_18 = value;
	}

	inline static int32_t get_offset_of_mTrackingFromRuntimeAppearance_19() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t1178230459, ___mTrackingFromRuntimeAppearance_19)); }
	inline bool get_mTrackingFromRuntimeAppearance_19() const { return ___mTrackingFromRuntimeAppearance_19; }
	inline bool* get_address_of_mTrackingFromRuntimeAppearance_19() { return &___mTrackingFromRuntimeAppearance_19; }
	inline void set_mTrackingFromRuntimeAppearance_19(bool value)
	{
		___mTrackingFromRuntimeAppearance_19 = value;
	}

	inline static int32_t get_offset_of_mVuMarkTemplate_20() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t1178230459, ___mVuMarkTemplate_20)); }
	inline RuntimeObject* get_mVuMarkTemplate_20() const { return ___mVuMarkTemplate_20; }
	inline RuntimeObject** get_address_of_mVuMarkTemplate_20() { return &___mVuMarkTemplate_20; }
	inline void set_mVuMarkTemplate_20(RuntimeObject* value)
	{
		___mVuMarkTemplate_20 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkTemplate_20), value);
	}

	inline static int32_t get_offset_of_mVuMarkTarget_21() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t1178230459, ___mVuMarkTarget_21)); }
	inline RuntimeObject* get_mVuMarkTarget_21() const { return ___mVuMarkTarget_21; }
	inline RuntimeObject** get_address_of_mVuMarkTarget_21() { return &___mVuMarkTarget_21; }
	inline void set_mVuMarkTarget_21(RuntimeObject* value)
	{
		___mVuMarkTarget_21 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkTarget_21), value);
	}

	inline static int32_t get_offset_of_mVuMarkResultId_22() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t1178230459, ___mVuMarkResultId_22)); }
	inline int32_t get_mVuMarkResultId_22() const { return ___mVuMarkResultId_22; }
	inline int32_t* get_address_of_mVuMarkResultId_22() { return &___mVuMarkResultId_22; }
	inline void set_mVuMarkResultId_22(int32_t value)
	{
		___mVuMarkResultId_22 = value;
	}

	inline static int32_t get_offset_of_mOnTargetAssigned_23() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t1178230459, ___mOnTargetAssigned_23)); }
	inline Action_t1264377477 * get_mOnTargetAssigned_23() const { return ___mOnTargetAssigned_23; }
	inline Action_t1264377477 ** get_address_of_mOnTargetAssigned_23() { return &___mOnTargetAssigned_23; }
	inline void set_mOnTargetAssigned_23(Action_t1264377477 * value)
	{
		___mOnTargetAssigned_23 = value;
		Il2CppCodeGenWriteBarrier((&___mOnTargetAssigned_23), value);
	}

	inline static int32_t get_offset_of_mOnTargetLost_24() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t1178230459, ___mOnTargetLost_24)); }
	inline Action_t1264377477 * get_mOnTargetLost_24() const { return ___mOnTargetLost_24; }
	inline Action_t1264377477 ** get_address_of_mOnTargetLost_24() { return &___mOnTargetLost_24; }
	inline void set_mOnTargetLost_24(Action_t1264377477 * value)
	{
		___mOnTargetLost_24 = value;
		Il2CppCodeGenWriteBarrier((&___mOnTargetLost_24), value);
	}

	inline static int32_t get_offset_of_mLastTransformScale_25() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t1178230459, ___mLastTransformScale_25)); }
	inline float get_mLastTransformScale_25() const { return ___mLastTransformScale_25; }
	inline float* get_address_of_mLastTransformScale_25() { return &___mLastTransformScale_25; }
	inline void set_mLastTransformScale_25(float value)
	{
		___mLastTransformScale_25 = value;
	}

	inline static int32_t get_offset_of_mLastSize_26() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t1178230459, ___mLastSize_26)); }
	inline Vector2_t2156229523  get_mLastSize_26() const { return ___mLastSize_26; }
	inline Vector2_t2156229523 * get_address_of_mLastSize_26() { return &___mLastSize_26; }
	inline void set_mLastSize_26(Vector2_t2156229523  value)
	{
		___mLastSize_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKBEHAVIOUR_T1178230459_H
#ifndef IMAGE_T2670269651_H
#define IMAGE_T2670269651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t2670269651  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t280657092 * ___m_Sprite_29;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t280657092 * ___m_OverrideSprite_30;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_31;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_32;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_33;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_34;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_35;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_36;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_37;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_38;

public:
	inline static int32_t get_offset_of_m_Sprite_29() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Sprite_29)); }
	inline Sprite_t280657092 * get_m_Sprite_29() const { return ___m_Sprite_29; }
	inline Sprite_t280657092 ** get_address_of_m_Sprite_29() { return &___m_Sprite_29; }
	inline void set_m_Sprite_29(Sprite_t280657092 * value)
	{
		___m_Sprite_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_29), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_30() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_OverrideSprite_30)); }
	inline Sprite_t280657092 * get_m_OverrideSprite_30() const { return ___m_OverrideSprite_30; }
	inline Sprite_t280657092 ** get_address_of_m_OverrideSprite_30() { return &___m_OverrideSprite_30; }
	inline void set_m_OverrideSprite_30(Sprite_t280657092 * value)
	{
		___m_OverrideSprite_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_30), value);
	}

	inline static int32_t get_offset_of_m_Type_31() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Type_31)); }
	inline int32_t get_m_Type_31() const { return ___m_Type_31; }
	inline int32_t* get_address_of_m_Type_31() { return &___m_Type_31; }
	inline void set_m_Type_31(int32_t value)
	{
		___m_Type_31 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_32() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_PreserveAspect_32)); }
	inline bool get_m_PreserveAspect_32() const { return ___m_PreserveAspect_32; }
	inline bool* get_address_of_m_PreserveAspect_32() { return &___m_PreserveAspect_32; }
	inline void set_m_PreserveAspect_32(bool value)
	{
		___m_PreserveAspect_32 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_33() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillCenter_33)); }
	inline bool get_m_FillCenter_33() const { return ___m_FillCenter_33; }
	inline bool* get_address_of_m_FillCenter_33() { return &___m_FillCenter_33; }
	inline void set_m_FillCenter_33(bool value)
	{
		___m_FillCenter_33 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_34() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillMethod_34)); }
	inline int32_t get_m_FillMethod_34() const { return ___m_FillMethod_34; }
	inline int32_t* get_address_of_m_FillMethod_34() { return &___m_FillMethod_34; }
	inline void set_m_FillMethod_34(int32_t value)
	{
		___m_FillMethod_34 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_35() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillAmount_35)); }
	inline float get_m_FillAmount_35() const { return ___m_FillAmount_35; }
	inline float* get_address_of_m_FillAmount_35() { return &___m_FillAmount_35; }
	inline void set_m_FillAmount_35(float value)
	{
		___m_FillAmount_35 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_36() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillClockwise_36)); }
	inline bool get_m_FillClockwise_36() const { return ___m_FillClockwise_36; }
	inline bool* get_address_of_m_FillClockwise_36() { return &___m_FillClockwise_36; }
	inline void set_m_FillClockwise_36(bool value)
	{
		___m_FillClockwise_36 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_37() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillOrigin_37)); }
	inline int32_t get_m_FillOrigin_37() const { return ___m_FillOrigin_37; }
	inline int32_t* get_address_of_m_FillOrigin_37() { return &___m_FillOrigin_37; }
	inline void set_m_FillOrigin_37(int32_t value)
	{
		___m_FillOrigin_37 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_38() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_AlphaHitTestMinimumThreshold_38)); }
	inline float get_m_AlphaHitTestMinimumThreshold_38() const { return ___m_AlphaHitTestMinimumThreshold_38; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_38() { return &___m_AlphaHitTestMinimumThreshold_38; }
	inline void set_m_AlphaHitTestMinimumThreshold_38(float value)
	{
		___m_AlphaHitTestMinimumThreshold_38 = value;
	}
};

struct Image_t2670269651_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t340375123 * ___s_ETC1DefaultUI_28;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_t1457185986* ___s_VertScratch_39;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_t1457185986* ___s_UVScratch_40;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t1718750761* ___s_Xy_41;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t1718750761* ___s_Uv_42;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_28() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_ETC1DefaultUI_28)); }
	inline Material_t340375123 * get_s_ETC1DefaultUI_28() const { return ___s_ETC1DefaultUI_28; }
	inline Material_t340375123 ** get_address_of_s_ETC1DefaultUI_28() { return &___s_ETC1DefaultUI_28; }
	inline void set_s_ETC1DefaultUI_28(Material_t340375123 * value)
	{
		___s_ETC1DefaultUI_28 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_28), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_39() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_VertScratch_39)); }
	inline Vector2U5BU5D_t1457185986* get_s_VertScratch_39() const { return ___s_VertScratch_39; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_VertScratch_39() { return &___s_VertScratch_39; }
	inline void set_s_VertScratch_39(Vector2U5BU5D_t1457185986* value)
	{
		___s_VertScratch_39 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_39), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_40() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_UVScratch_40)); }
	inline Vector2U5BU5D_t1457185986* get_s_UVScratch_40() const { return ___s_UVScratch_40; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_UVScratch_40() { return &___s_UVScratch_40; }
	inline void set_s_UVScratch_40(Vector2U5BU5D_t1457185986* value)
	{
		___s_UVScratch_40 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_40), value);
	}

	inline static int32_t get_offset_of_s_Xy_41() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Xy_41)); }
	inline Vector3U5BU5D_t1718750761* get_s_Xy_41() const { return ___s_Xy_41; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Xy_41() { return &___s_Xy_41; }
	inline void set_s_Xy_41(Vector3U5BU5D_t1718750761* value)
	{
		___s_Xy_41 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_41), value);
	}

	inline static int32_t get_offset_of_s_Uv_42() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Uv_42)); }
	inline Vector3U5BU5D_t1718750761* get_s_Uv_42() const { return ___s_Uv_42; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Uv_42() { return &___s_Uv_42; }
	inline void set_s_Uv_42(Vector3U5BU5D_t1718750761* value)
	{
		___s_Uv_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_42), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T2670269651_H
#ifndef TEXT_T1901882714_H
#define TEXT_T1901882714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t1901882714  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t746620069 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t3211863866 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t3211863866 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t1981460040* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_FontData_28)); }
	inline FontData_t746620069 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t746620069 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t746620069 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCache_30)); }
	inline TextGenerator_t3211863866 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t3211863866 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t3211863866 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t3211863866 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t1981460040* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t1981460040* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t1901882714_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t340375123 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t1901882714_StaticFields, ___s_DefaultText_32)); }
	inline Material_t340375123 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t340375123 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t340375123 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T1901882714_H
#ifndef RAWIMAGE_T3182918964_H
#define RAWIMAGE_T3182918964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.RawImage
struct  RawImage_t3182918964  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t3661962703 * ___m_Texture_28;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_t2360479859  ___m_UVRect_29;

public:
	inline static int32_t get_offset_of_m_Texture_28() { return static_cast<int32_t>(offsetof(RawImage_t3182918964, ___m_Texture_28)); }
	inline Texture_t3661962703 * get_m_Texture_28() const { return ___m_Texture_28; }
	inline Texture_t3661962703 ** get_address_of_m_Texture_28() { return &___m_Texture_28; }
	inline void set_m_Texture_28(Texture_t3661962703 * value)
	{
		___m_Texture_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_Texture_28), value);
	}

	inline static int32_t get_offset_of_m_UVRect_29() { return static_cast<int32_t>(offsetof(RawImage_t3182918964, ___m_UVRect_29)); }
	inline Rect_t2360479859  get_m_UVRect_29() const { return ___m_UVRect_29; }
	inline Rect_t2360479859 * get_address_of_m_UVRect_29() { return &___m_UVRect_29; }
	inline void set_m_UVRect_29(Rect_t2360479859  value)
	{
		___m_UVRect_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWIMAGE_T3182918964_H
#ifndef IMAGETARGETBEHAVIOUR_T2200418350_H
#define IMAGETARGETBEHAVIOUR_T2200418350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetBehaviour
struct  ImageTargetBehaviour_t2200418350  : public DataSetTrackableBehaviour_t3430730379
{
public:
	// System.Single Vuforia.ImageTargetBehaviour::mAspectRatio
	float ___mAspectRatio_11;
	// Vuforia.ImageTargetType Vuforia.ImageTargetBehaviour::mImageTargetType
	int32_t ___mImageTargetType_12;
	// System.Single Vuforia.ImageTargetBehaviour::mWidth
	float ___mWidth_13;
	// System.Single Vuforia.ImageTargetBehaviour::mHeight
	float ___mHeight_14;
	// Vuforia.ImageTarget Vuforia.ImageTargetBehaviour::mImageTarget
	RuntimeObject* ___mImageTarget_15;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButtonBehaviour> Vuforia.ImageTargetBehaviour::mVirtualButtonBehaviours
	Dictionary_2_t325039782 * ___mVirtualButtonBehaviours_16;
	// System.Single Vuforia.ImageTargetBehaviour::mLastTransformScale
	float ___mLastTransformScale_17;
	// UnityEngine.Vector2 Vuforia.ImageTargetBehaviour::mLastSize
	Vector2_t2156229523  ___mLastSize_18;

public:
	inline static int32_t get_offset_of_mAspectRatio_11() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2200418350, ___mAspectRatio_11)); }
	inline float get_mAspectRatio_11() const { return ___mAspectRatio_11; }
	inline float* get_address_of_mAspectRatio_11() { return &___mAspectRatio_11; }
	inline void set_mAspectRatio_11(float value)
	{
		___mAspectRatio_11 = value;
	}

	inline static int32_t get_offset_of_mImageTargetType_12() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2200418350, ___mImageTargetType_12)); }
	inline int32_t get_mImageTargetType_12() const { return ___mImageTargetType_12; }
	inline int32_t* get_address_of_mImageTargetType_12() { return &___mImageTargetType_12; }
	inline void set_mImageTargetType_12(int32_t value)
	{
		___mImageTargetType_12 = value;
	}

	inline static int32_t get_offset_of_mWidth_13() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2200418350, ___mWidth_13)); }
	inline float get_mWidth_13() const { return ___mWidth_13; }
	inline float* get_address_of_mWidth_13() { return &___mWidth_13; }
	inline void set_mWidth_13(float value)
	{
		___mWidth_13 = value;
	}

	inline static int32_t get_offset_of_mHeight_14() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2200418350, ___mHeight_14)); }
	inline float get_mHeight_14() const { return ___mHeight_14; }
	inline float* get_address_of_mHeight_14() { return &___mHeight_14; }
	inline void set_mHeight_14(float value)
	{
		___mHeight_14 = value;
	}

	inline static int32_t get_offset_of_mImageTarget_15() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2200418350, ___mImageTarget_15)); }
	inline RuntimeObject* get_mImageTarget_15() const { return ___mImageTarget_15; }
	inline RuntimeObject** get_address_of_mImageTarget_15() { return &___mImageTarget_15; }
	inline void set_mImageTarget_15(RuntimeObject* value)
	{
		___mImageTarget_15 = value;
		Il2CppCodeGenWriteBarrier((&___mImageTarget_15), value);
	}

	inline static int32_t get_offset_of_mVirtualButtonBehaviours_16() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2200418350, ___mVirtualButtonBehaviours_16)); }
	inline Dictionary_2_t325039782 * get_mVirtualButtonBehaviours_16() const { return ___mVirtualButtonBehaviours_16; }
	inline Dictionary_2_t325039782 ** get_address_of_mVirtualButtonBehaviours_16() { return &___mVirtualButtonBehaviours_16; }
	inline void set_mVirtualButtonBehaviours_16(Dictionary_2_t325039782 * value)
	{
		___mVirtualButtonBehaviours_16 = value;
		Il2CppCodeGenWriteBarrier((&___mVirtualButtonBehaviours_16), value);
	}

	inline static int32_t get_offset_of_mLastTransformScale_17() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2200418350, ___mLastTransformScale_17)); }
	inline float get_mLastTransformScale_17() const { return ___mLastTransformScale_17; }
	inline float* get_address_of_mLastTransformScale_17() { return &___mLastTransformScale_17; }
	inline void set_mLastTransformScale_17(float value)
	{
		___mLastTransformScale_17 = value;
	}

	inline static int32_t get_offset_of_mLastSize_18() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2200418350, ___mLastSize_18)); }
	inline Vector2_t2156229523  get_mLastSize_18() const { return ___mLastSize_18; }
	inline Vector2_t2156229523 * get_address_of_mLastSize_18() { return &___mLastSize_18; }
	inline void set_mLastSize_18(Vector2_t2156229523  value)
	{
		___mLastSize_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETBEHAVIOUR_T2200418350_H
#ifndef MULTITARGETBEHAVIOUR_T2061511750_H
#define MULTITARGETBEHAVIOUR_T2061511750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MultiTargetBehaviour
struct  MultiTargetBehaviour_t2061511750  : public DataSetTrackableBehaviour_t3430730379
{
public:
	// Vuforia.MultiTarget Vuforia.MultiTargetBehaviour::mMultiTarget
	RuntimeObject* ___mMultiTarget_11;

public:
	inline static int32_t get_offset_of_mMultiTarget_11() { return static_cast<int32_t>(offsetof(MultiTargetBehaviour_t2061511750, ___mMultiTarget_11)); }
	inline RuntimeObject* get_mMultiTarget_11() const { return ___mMultiTarget_11; }
	inline RuntimeObject** get_address_of_mMultiTarget_11() { return &___mMultiTarget_11; }
	inline void set_mMultiTarget_11(RuntimeObject* value)
	{
		___mMultiTarget_11 = value;
		Il2CppCodeGenWriteBarrier((&___mMultiTarget_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTITARGETBEHAVIOUR_T2061511750_H
#ifndef FPSCOUNT_T2935661334_H
#define FPSCOUNT_T2935661334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FpsCount
struct  FpsCount_t2935661334  : public Text_t1901882714
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNT_T2935661334_H
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2_t2156229523  m_Items[1];

public:
	inline Vector2_t2156229523  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_t2156229523 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_t2156229523  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_t2156229523  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_t2156229523 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_t2156229523  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Touch[]
struct TouchU5BU5D_t1849554061  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Touch_t1921856868  m_Items[1];

public:
	inline Touch_t1921856868  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Touch_t1921856868 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Touch_t1921856868  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Touch_t1921856868  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Touch_t1921856868 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Touch_t1921856868  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_t1113636619 * m_Items[1];

public:
	inline GameObject_t1113636619 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_t1113636619 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Renderer[]
struct RendererU5BU5D_t3210418286  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Renderer_t2627027031 * m_Items[1];

public:
	inline Renderer_t2627027031 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Renderer_t2627027031 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Renderer_t2627027031 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Renderer_t2627027031 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Renderer_t2627027031 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Renderer_t2627027031 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Collider[]
struct ColliderU5BU5D_t4234922487  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Collider_t1773347010 * m_Items[1];

public:
	inline Collider_t1773347010 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Collider_t1773347010 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Collider_t1773347010 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Collider_t1773347010 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Collider_t1773347010 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Collider_t1773347010 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Canvas[]
struct CanvasU5BU5D_t682926938  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Canvas_t3310196443 * m_Items[1];

public:
	inline Canvas_t3310196443 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Canvas_t3310196443 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Canvas_t3310196443 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Canvas_t3310196443 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Canvas_t3310196443 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Canvas_t3310196443 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.UI.RawImage[]
struct RawImageU5BU5D_t1576731517  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RawImage_t3182918964 * m_Items[1];

public:
	inline RawImage_t3182918964 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RawImage_t3182918964 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RawImage_t3182918964 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RawImage_t3182918964 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RawImage_t3182918964 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RawImage_t3182918964 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// characterSlot[]
struct characterSlotU5BU5D_t2121073899  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) characterSlot_t990761438 * m_Items[1];

public:
	inline characterSlot_t990761438 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline characterSlot_t990761438 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, characterSlot_t990761438 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline characterSlot_t990761438 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline characterSlot_t990761438 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, characterSlot_t990761438 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Sprite_t280657092 * m_Items[1];

public:
	inline Sprite_t280657092 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Sprite_t280657092 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Sprite_t280657092 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Sprite_t280657092 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Sprite_t280657092 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Sprite_t280657092 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Nullable`1<UnityEngine.Vector2>[]
struct Nullable_1U5BU5D_t3148003288  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Nullable_1_t3878791605  m_Items[1];

public:
	inline Nullable_1_t3878791605  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Nullable_1_t3878791605 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Nullable_1_t3878791605  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Nullable_1_t3878791605  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Nullable_1_t3878791605 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Nullable_1_t3878791605  value)
	{
		m_Items[index] = value;
	}
};
// System.Char[]
struct CharU5BU5D_t3528271667  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m2287542950_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  RuntimeObject * Component_GetComponentInChildren_TisRuntimeObject_m1033527003_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void System.Func`2<UnityEngine.Touch,UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m3243656238_gshared (Func_2_t528868469 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<UnityEngine.Touch,UnityEngine.Vector2>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  RuntimeObject* Enumerable_Select_TisTouch_t1921856868_TisVector2_t2156229523_m1617180933_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, Func_2_t528868469 * p1, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<UnityEngine.Vector2>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Vector2U5BU5D_t1457185986* Enumerable_ToArray_TisVector2_t2156229523_m3384610930_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2079027267_gshared (Action_1_t269755560 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void System.Action`1<Vuforia.VuforiaUnity/InitError>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2713332384_gshared (Action_1_t3593217305 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::GetBuiltinResource<System.Object>(System.String)
extern "C"  RuntimeObject * Resources_GetBuiltinResource_TisRuntimeObject_m3352626831_gshared (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t2843939325* Component_GetComponentsInChildren_TisRuntimeObject_m2748495586_gshared (Component_t1923634451 * __this, bool p0, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInParent<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t2843939325* Component_GetComponentsInParent_TisRuntimeObject_m492379918_gshared (Component_t1923634451 * __this, bool p0, const RuntimeMethod* method);
// !!0[] UnityEngine.Object::FindObjectsOfType<System.Object>()
extern "C"  ObjectU5BU5D_t2843939325* Object_FindObjectsOfType_TisRuntimeObject_m2647183545_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  RuntimeObject * Object_FindObjectOfType_TisRuntimeObject_m1542987838_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// T MenuOptions::FindUISelectableWithText<System.Object>(System.String)
extern "C"  RuntimeObject * MenuOptions_FindUISelectableWithText_TisRuntimeObject_m1777269550_gshared (MenuOptions_t1951716431 * __this, String_t* ___text0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Transform,System.Boolean)
extern "C"  RuntimeObject * Object_Instantiate_TisRuntimeObject_m1858812370_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, Transform_t3600365921 * p1, bool p2, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponentInChildren_TisRuntimeObject_m1513755678_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Transform)
extern "C"  RuntimeObject * Object_Instantiate_TisRuntimeObject_m1061214600_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, Transform_t3600365921 * p1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Queue`1<UnityEngine.Quaternion>::get_Count()
extern "C"  int32_t Queue_1_get_Count_m566549244_gshared (Queue_1_t2148187825 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.Queue`1<UnityEngine.Quaternion>::Dequeue()
extern "C"  Quaternion_t2301928331  Queue_1_Dequeue_m127687118_gshared (Queue_1_t2148187825 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.Queue`1<UnityEngine.Vector3>::Dequeue()
extern "C"  Vector3_t3722313464  Queue_1_Dequeue_m3534639416_gshared (Queue_1_t3568572958 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Quaternion>::Enqueue(!0)
extern "C"  void Queue_1_Enqueue_m309039112_gshared (Queue_1_t2148187825 * __this, Quaternion_t2301928331  p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Vector3>::Enqueue(!0)
extern "C"  void Queue_1_Enqueue_m2554143986_gshared (Queue_1_t3568572958 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// System.Collections.Generic.Queue`1/Enumerator<!0> System.Collections.Generic.Queue`1<UnityEngine.Quaternion>::GetEnumerator()
extern "C"  Enumerator_t2810099927  Queue_1_GetEnumerator_m3198989551_gshared (Queue_1_t2148187825 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.Queue`1/Enumerator<UnityEngine.Quaternion>::get_Current()
extern "C"  Quaternion_t2301928331  Enumerator_get_Current_m2950039238_gshared (Enumerator_t2810099927 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.Queue`1<UnityEngine.Quaternion>::Peek()
extern "C"  Quaternion_t2301928331  Queue_1_Peek_m1284364542_gshared (Queue_1_t2148187825 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<UnityEngine.Quaternion>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m459395817_gshared (Enumerator_t2810099927 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1/Enumerator<UnityEngine.Quaternion>::Dispose()
extern "C"  void Enumerator_Dispose_m557667343_gshared (Enumerator_t2810099927 * __this, const RuntimeMethod* method);
// System.Collections.Generic.Queue`1/Enumerator<!0> System.Collections.Generic.Queue`1<UnityEngine.Vector3>::GetEnumerator()
extern "C"  Enumerator_t4230485060  Queue_1_GetEnumerator_m676935694_gshared (Queue_1_t3568572958 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.Queue`1/Enumerator<UnityEngine.Vector3>::get_Current()
extern "C"  Vector3_t3722313464  Enumerator_get_Current_m4117045769_gshared (Enumerator_t4230485060 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<UnityEngine.Vector3>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2202395141_gshared (Enumerator_t4230485060 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1/Enumerator<UnityEngine.Vector3>::Dispose()
extern "C"  void Enumerator_Dispose_m1218804216_gshared (Enumerator_t4230485060 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Queue`1<UnityEngine.Vector3>::get_Count()
extern "C"  int32_t Queue_1_get_Count_m2376062550_gshared (Queue_1_t3568572958 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Quaternion>::.ctor(System.Int32)
extern "C"  void Queue_1__ctor_m1117100023_gshared (Queue_1_t2148187825 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Vector3>::.ctor(System.Int32)
extern "C"  void Queue_1__ctor_m3566777400_gshared (Queue_1_t3568572958 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m608474170_gshared (UnityAction_2_t1262235195 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<UnityEngine.Vector2>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1317965445_gshared (Nullable_1_t3878791605 * __this, const RuntimeMethod* method);
// System.Void System.Nullable`1<UnityEngine.Vector2>::.ctor(!0)
extern "C"  void Nullable_1__ctor_m680077965_gshared (Nullable_1_t3878791605 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method);
// !0 System.Nullable`1<UnityEngine.Vector2>::GetValueOrDefault()
extern "C"  Vector2_t2156229523  Nullable_1_GetValueOrDefault_m3722452406_gshared (Nullable_1_t3878791605 * __this, const RuntimeMethod* method);
// !0 System.Nullable`1<UnityEngine.Vector2>::get_Value()
extern "C"  Vector2_t2156229523  Nullable_1_get_Value_m963759207_gshared (Nullable_1_t3878791605 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t257213610 * Enumerable_ToList_TisRuntimeObject_m1551499789_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// !!0 System.Linq.Enumerable::ElementAt<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
extern "C"  RuntimeObject * Enumerable_ElementAt_TisRuntimeObject_m2579126540_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, int32_t p1, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t2146457487  List_1_GetEnumerator_m2930774921_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m470245444_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3007748546_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m4051431634 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C"  void SceneManager_LoadScene_m1758133949 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKeyUp(UnityEngine.KeyCode)
extern "C"  bool Input_GetKeyUp_m2808015270 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void AboutScreen::OnStartAR()
extern "C"  void AboutScreen_OnStartAR_m2896384759 (AboutScreen_t2183797299 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t2627027031_m2222376672(__this, method) ((  Renderer_t2627027031 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t340375123 * Renderer_get_material_m4171603682 (Renderer_t2627027031 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<UnityEngine.Texture>::get_Item(System.Int32)
#define List_1_get_Item_m1945629867(__this, p0, method) ((  Texture_t3661962703 * (*) (List_1_t839070149 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Void UnityEngine.Material::set_mainTexture(UnityEngine.Texture)
extern "C"  void Material_set_mainTexture_m544811714 (Material_t340375123 * __this, Texture_t3661962703 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Texture>::get_Count()
#define List_1_get_Count_m314570365(__this, method) ((  int32_t (*) (List_1_t839070149 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C"  Vector3_t3722313464  Transform_get_localScale_m129152068 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m3053443106 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AsyncSceneLoader::LoadNextSceneAfter(System.Single)
extern "C"  RuntimeObject* AsyncSceneLoader_LoadNextSceneAfter_m1751933459 (AsyncSceneLoader_t621267272 * __this, float ___seconds0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m3411253000 (MonoBehaviour_t3962482529 * __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0::.ctor()
extern "C"  void U3CLoadNextSceneAfterU3Ec__Iterator0__ctor_m1226795792 (U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m2199082655 (WaitForSeconds_t1699091251 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C"  Scene_t2348375561  SceneManager_GetActiveScene_m1825203488 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SceneManagement.Scene::get_buildIndex()
extern "C"  int32_t Scene_get_buildIndex_m270272723 (Scene_t2348375561 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
extern "C"  void SceneManager_LoadScene_m3463216446 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m2498754347 (Vector4_t3319028937 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::op_Implicit(UnityEngine.Vector4)
extern "C"  Color_t2555686324  Color_op_Implicit_m2665280078 (RuntimeObject * __this /* static, unused */, Vector4_t3319028937  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean buttonColorToggle::checkIfOn(System.String)
extern "C"  bool buttonColorToggle_checkIfOn_m1439767701 (buttonColorToggle_t2066501000 * __this, String_t* ___calledSetting0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.UI.Text>()
#define Component_GetComponentInChildren_TisText_t1901882714_m396351542(__this, method) ((  Text_t1901882714 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponentInChildren_TisRuntimeObject_m1033527003_gshared)(__this, method)
// System.Void buttonColorToggle::toggleColor(System.Boolean)
extern "C"  void buttonColorToggle_toggleColor_m646326456 (buttonColorToggle_t2066501000 * __this, bool ___turnOfOtherSettingCall0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t2670269651_m980647750(__this, method) ((  Image_t2670269651 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
extern "C"  void Image_set_sprite_m2369174689 (Image_t2670269651 * __this, Sprite_t280657092 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m3755062657 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t1113636619 * GameObject_Find_m2032535176 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<settingsManager>()
#define GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329(__this, method) ((  settingsManager_t2086358119 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Int32 settingsManager::findSetting(System.String)
extern "C"  int32_t settingsManager_findSetting_m2352407313 (settingsManager_t2086358119 * __this, String_t* ___calledSetting0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<SettingsList>::get_Item(System.Int32)
#define List_1_get_Item_m3797380788(__this, p0, method) ((  SettingsList_t1883434577 * (*) (List_1_t3355509319 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animation>()
#define GameObject_GetComponent_TisAnimation_t3648466861_m3766528019(__this, method) ((  Animation_t3648466861 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Boolean UnityEngine.Animation::Play()
extern "C"  bool Animation_Play_m2798573761 (Animation_t3648466861 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator buttonTriggerAnimation::AnimationCooldown()
extern "C"  RuntimeObject* buttonTriggerAnimation_AnimationCooldown_m4119330121 (buttonTriggerAnimation_t4121295644 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void buttonTriggerAnimation/<AnimationCooldown>c__Iterator0::.ctor()
extern "C"  void U3CAnimationCooldownU3Ec__Iterator0__ctor_m346444143 (U3CAnimationCooldownU3Ec__Iterator0_t1141888348 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationClip UnityEngine.Animation::get_clip()
extern "C"  AnimationClip_t2318505987 * Animation_get_clip_m1019546398 (Animation_t3648466861 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimationClip::get_length()
extern "C"  float AnimationClip_get_length_m3296085482 (AnimationClip_t2318505987 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3600365921 * Transform_get_parent_m835071599 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t2301928331  Transform_get_rotation_m3502953881 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Touch[] UnityEngine.Input::get_touches()
extern "C"  TouchU5BU5D_t1849554061* Input_get_touches_m1702694043 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Func`2<UnityEngine.Touch,UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3243656238(__this, p0, p1, method) ((  void (*) (Func_2_t528868469 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_m3243656238_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<UnityEngine.Touch,UnityEngine.Vector2>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisTouch_t1921856868_TisVector2_t2156229523_m1617180933(__this /* static, unused */, p0, p1, method) ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Func_2_t528868469 *, const RuntimeMethod*))Enumerable_Select_TisTouch_t1921856868_TisVector2_t2156229523_m1617180933_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<UnityEngine.Vector2>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisVector2_t2156229523_m3384610930(__this /* static, unused */, p0, method) ((  Vector2U5BU5D_t1457185986* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisVector2_t2156229523_m3384610930_gshared)(__this /* static, unused */, p0, method)
// System.Void CameraController::UpdatePanning(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void CameraController_UpdatePanning_m509325418 (CameraController_t3346819214 * __this, Vector2_t2156229523  ___touch00, Vector2_t2156229523  ___touch11, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CameraController::UpdateRotate(UnityEngine.Vector2)
extern "C"  void CameraController_UpdateRotate_m1805720568 (CameraController_t3346819214 * __this, Vector2_t2156229523  ___touch00, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C"  Vector3_t3722313464  Vector3_get_up_m3584168373 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::RotateAround(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  void Transform_RotateAround_m2651195670 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, Vector3_t3722313464  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_right()
extern "C"  Vector3_t3722313464  Transform_get_right_m2535262102 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_op_Multiply_m3376773913 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
extern "C"  Vector3_t3722313464  Transform_get_up_m3972993886 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Addition_m779775034 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m3387557959 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m3524318132 (Transform_t3600365921 * __this, Quaternion_t2301928331  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_deltaPosition()
extern "C"  Vector2_t2156229523  Touch_get_deltaPosition_m2389653382 (Touch_t1921856868 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaARController Vuforia.VuforiaARController::get_Instance()
extern "C"  VuforiaARController_t1876945237 * VuforiaARController_get_Instance_m1100644214 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
extern "C"  void Action__ctor_m2994342681 (Action_t1264377477 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaARController::RegisterVuforiaStartedCallback(System.Action)
extern "C"  void VuforiaARController_RegisterVuforiaStartedCallback_m3258152770 (VuforiaARController_t1876945237 * __this, Action_t1264377477 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m2079027267(__this, p0, p1, method) ((  void (*) (Action_1_t269755560 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_m2079027267_gshared)(__this, p0, p1, method)
// System.Void Vuforia.VuforiaARController::RegisterOnPauseCallback(System.Action`1<System.Boolean>)
extern "C"  void VuforiaARController_RegisterOnPauseCallback_m1833237148 (VuforiaARController_t1876945237 * __this, Action_1_t269755560 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.CameraDevice Vuforia.CameraDevice::get_Instance()
extern "C"  CameraDevice_t960297568 * CameraDevice_get_Instance_m2002290155 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDevice::SetFocusMode(Vuforia.CameraDevice/FocusMode)
extern "C"  bool CameraDevice_SetFocusMode_m2424803588 (CameraDevice_t960297568 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDevice::SetFlashTorchMode(System.Boolean)
extern "C"  bool CameraDevice_SetFlashTorchMode_m1499936922 (CameraDevice_t960297568 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CameraSettings::RestoreOriginalFocusMode()
extern "C"  RuntimeObject* CameraSettings_RestoreOriginalFocusMode_m3433876528 (CameraSettings_t3152619780 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraSettings::RestartCamera(Vuforia.CameraDevice/CameraDirection)
extern "C"  bool CameraSettings_RestartCamera_m1627822698 (CameraSettings_t3152619780 * __this, int32_t ___direction0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSettings::SwitchAutofocus(System.Boolean)
extern "C"  void CameraSettings_SwitchAutofocus_m1104105893 (CameraSettings_t3152619780 * __this, bool ___ON0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::.ctor()
extern "C"  void U3CRestoreOriginalFocusModeU3Ec__Iterator0__ctor_m1994535627 (U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.ITrackerManager Vuforia.TrackerManager::get_Instance()
extern "C"  RuntimeObject* TrackerManager_get_Instance_m777262631 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDevice::Stop()
extern "C"  bool CameraDevice_Stop_m1782631459 (CameraDevice_t960297568 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDevice::Deinit()
extern "C"  bool CameraDevice_Deinit_m726427492 (CameraDevice_t960297568 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDevice::Init(Vuforia.CameraDevice/CameraDirection)
extern "C"  bool CameraDevice_Init_m2722714107 (CameraDevice_t960297568 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDevice::Start()
extern "C"  bool CameraDevice_Start_m2947498176 (CameraDevice_t960297568 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m796801857 (GameObject_t1113636619 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void VuforiaMonoBehaviour::.ctor()
extern "C"  void VuforiaMonoBehaviour__ctor_m348562674 (VuforiaMonoBehaviour_t1150221792 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DefaultInitializationErrorHandler::SetErrorCode(Vuforia.VuforiaUnity/InitError)
extern "C"  void DefaultInitializationErrorHandler_SetErrorCode_m599033302 (DefaultInitializationErrorHandler_t3109936861 * __this, int32_t ___errorCode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DefaultInitializationErrorHandler::SetErrorOccurred(System.Boolean)
extern "C"  void DefaultInitializationErrorHandler_SetErrorOccurred_m1940230672 (DefaultInitializationErrorHandler_t3109936861 * __this, bool ___errorOccurred0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaRuntime Vuforia.VuforiaRuntime::get_Instance()
extern "C"  VuforiaRuntime_t1949122020 * VuforiaRuntime_get_Instance_m1058251676 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<Vuforia.VuforiaUnity/InitError>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m2713332384(__this, p0, p1, method) ((  void (*) (Action_1_t3593217305 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_m2713332384_gshared)(__this, p0, p1, method)
// System.Void Vuforia.VuforiaRuntime::RegisterVuforiaInitErrorCallback(System.Action`1<Vuforia.VuforiaUnity/InitError>)
extern "C"  void VuforiaRuntime_RegisterVuforiaInitErrorCallback_m2728879505 (VuforiaRuntime_t1949122020 * __this, Action_1_t3593217305 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DefaultInitializationErrorHandler::SetupGUIStyles()
extern "C"  void DefaultInitializationErrorHandler_SetupGUIStyles_m3863535424 (DefaultInitializationErrorHandler_t3109936861 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m345039817 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1623532518 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m2614021312 (Rect_t2360479859 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI/WindowFunction::.ctor(System.Object,System.IntPtr)
extern "C"  void WindowFunction__ctor_m2544237635 (WindowFunction_t3146511083 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUI::Window(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,System.String)
extern "C"  Rect_t2360479859  GUI_Window_m1088326791 (RuntimeObject * __this /* static, unused */, int32_t p0, Rect_t2360479859  p1, WindowFunction_t3146511083 * p2, String_t* p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaRuntime::UnregisterVuforiaInitErrorCallback(System.Action`1<Vuforia.VuforiaUnity/InitError>)
extern "C"  void VuforiaRuntime_UnregisterVuforiaInitErrorCallback_m1304340042 (VuforiaRuntime_t1949122020 * __this, Action_1_t3593217305 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String,UnityEngine.GUIStyle)
extern "C"  void GUI_Label_m2420537077 (RuntimeObject * __this /* static, unused */, Rect_t2360479859  p0, String_t* p1, GUIStyle_t3956901511 * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,System.String,UnityEngine.GUIStyle)
extern "C"  bool GUI_Button_m2223708732 (RuntimeObject * __this /* static, unused */, Rect_t2360479859  p0, String_t* p1, GUIStyle_t3956901511 * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::Quit()
extern "C"  void Application_Quit_m470877999 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String DefaultInitializationErrorHandler::getKeyInfo()
extern "C"  String_t* DefaultInitializationErrorHandler_getKeyInfo_m1864640064 (DefaultInitializationErrorHandler_t3109936861 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Application::get_productName()
extern "C"  String_t* Application_get_productName_m2401755738 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Replace(System.String,System.String)
extern "C"  String_t* String_Replace_m1273907647 (String_t* __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m2163913788 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, String_t* p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object[])
extern "C"  String_t* String_Concat_m2971454694 (RuntimeObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m2850623458 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaConfiguration Vuforia.VuforiaConfiguration::get_Instance()
extern "C"  VuforiaConfiguration_t1763229349 * VuforiaConfiguration_get_Instance_m3335903280 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaConfiguration/GenericVuforiaConfiguration Vuforia.VuforiaConfiguration::get_Vuforia()
extern "C"  GenericVuforiaConfiguration_t3697830469 * VuforiaConfiguration_get_Vuforia_m1588208597 (VuforiaConfiguration_t1763229349 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.VuforiaConfiguration/GenericVuforiaConfiguration::get_LicenseKey()
extern "C"  String_t* GenericVuforiaConfiguration_get_LicenseKey_m2270076687 (GenericVuforiaConfiguration_t3697830469 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::get_Length()
extern "C"  int32_t String_get_Length_m3847582255 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C"  String_t* String_Substring_m1610150815 (String_t* __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Screen::get_dpi()
extern "C"  float Screen_get_dpi_m495672463 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t2555686324  Color_get_white_m332174077 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D DefaultInitializationErrorHandler::CreateSinglePixelTexture(UnityEngine.Color)
extern "C"  Texture2D_t3840446185 * DefaultInitializationErrorHandler_CreateSinglePixelTexture_m424000749 (DefaultInitializationErrorHandler_t3109936861 * __this, Color_t2555686324  ___color0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::InverseLerp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_InverseLerp_m4155825980 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m286683560 (Color_t2555686324 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyle::.ctor()
extern "C"  void GUIStyle__ctor_m4038363858 (GUIStyle_t3956901511 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIStyleState UnityEngine.GUIStyle::get_normal()
extern "C"  GUIStyleState_t1397964415 * GUIStyle_get_normal_m729441812 (GUIStyle_t3956901511 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyleState::set_background(UnityEngine.Texture2D)
extern "C"  void GUIStyleState_set_background_m369476077 (GUIStyleState_t1397964415 * __this, Texture2D_t3840446185 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Resources::GetBuiltinResource<UnityEngine.Font>(System.String)
#define Resources_GetBuiltinResource_TisFont_t1956802104_m2738776830(__this /* static, unused */, p0, method) ((  Font_t1956802104 * (*) (RuntimeObject * /* static, unused */, String_t*, const RuntimeMethod*))Resources_GetBuiltinResource_TisRuntimeObject_m3352626831_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.GUIStyle::set_font(UnityEngine.Font)
extern "C"  void GUIStyle_set_font_m2490449107 (GUIStyle_t3956901511 * __this, Font_t1956802104 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyle::set_fontSize(System.Int32)
extern "C"  void GUIStyle_set_fontSize_m1566850023 (GUIStyle_t3956901511 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C"  Color_t2555686324  Color_get_black_m719512684 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyleState::set_textColor(UnityEngine.Color)
extern "C"  void GUIStyleState_set_textColor_m1105876047 (GUIStyleState_t1397964415 * __this, Color_t2555686324  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyle::set_wordWrap(System.Boolean)
extern "C"  void GUIStyle_set_wordWrap_m1419501823 (GUIStyle_t3956901511 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyle::set_alignment(UnityEngine.TextAnchor)
extern "C"  void GUIStyle_set_alignment_m3944619660 (GUIStyle_t3956901511 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void RectOffset__ctor_m732140021 (RectOffset_t1369453676 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyle::set_padding(UnityEngine.RectOffset)
extern "C"  void GUIStyle_set_padding_m3302456044 (GUIStyle_t3956901511 * __this, RectOffset_t1369453676 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyle::.ctor(UnityEngine.GUIStyle)
extern "C"  void GUIStyle__ctor_m2912682974 (GUIStyle_t3956901511 * __this, GUIStyle_t3956901511 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern "C"  void Texture2D__ctor_m2862217990 (Texture2D_t3840446185 * __this, int32_t p0, int32_t p1, int32_t p2, bool p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetPixel(System.Int32,System.Int32,UnityEngine.Color)
extern "C"  void Texture2D_SetPixel_m2984741184 (Texture2D_t3840446185 * __this, int32_t p0, int32_t p1, Color_t2555686324  p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::Apply()
extern "C"  void Texture2D_Apply_m2271746283 (Texture2D_t3840446185 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<Vuforia.TrackableBehaviour>()
#define Component_GetComponent_TisTrackableBehaviour_t1113559212_m1736119408(__this, method) ((  TrackableBehaviour_t1113559212 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m3574996620 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableBehaviour::RegisterTrackableEventHandler(Vuforia.ITrackableEventHandler)
extern "C"  void TrackableBehaviour_RegisterTrackableEventHandler_m2462783619 (TrackableBehaviour_t1113559212 * __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackableBehaviour::UnregisterTrackableEventHandler(Vuforia.ITrackableEventHandler)
extern "C"  bool TrackableBehaviour_UnregisterTrackableEventHandler_m1201569614 (TrackableBehaviour_t1113559212 * __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.TrackableBehaviour::get_TrackableName()
extern "C"  String_t* TrackableBehaviour_get_TrackableName_m3644057705 (TrackableBehaviour_t1113559212 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<onScanController>()
#define Component_GetComponent_TisonScanController_t1472212342_m2197168753(__this, method) ((  onScanController_t1472212342 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m20417929 (Behaviour_t1437897464 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>(System.Boolean)
#define Component_GetComponentsInChildren_TisRenderer_t2627027031_m2673895911(__this, p0, method) ((  RendererU5BU5D_t3210418286* (*) (Component_t1923634451 *, bool, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m2748495586_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Collider>(System.Boolean)
#define Component_GetComponentsInChildren_TisCollider_t1773347010_m2667952426(__this, p0, method) ((  ColliderU5BU5D_t4234922487* (*) (Component_t1923634451 *, bool, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m2748495586_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Canvas>(System.Boolean)
#define Component_GetComponentsInChildren_TisCanvas_t3310196443_m1457345007(__this, p0, method) ((  CanvasU5BU5D_t682926938* (*) (Component_t1923634451 *, bool, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m2748495586_gshared)(__this, p0, method)
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C"  void Renderer_set_enabled_m1727253150 (Renderer_t2627027031 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider::set_enabled(System.Boolean)
extern "C"  void Collider_set_enabled_m1517463283 (Collider_t1773347010 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m565254235 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.Animation>()
#define Component_GetComponentInChildren_TisAnimation_t3648466861_m4170904281(__this, method) ((  Animation_t3648466861 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponentInChildren_TisRuntimeObject_m1033527003_gshared)(__this, method)
// System.Void UnityEngine.UI.Text::.ctor()
extern "C"  void Text__ctor_m1150387577 (Text_t1901882714 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m372706562 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuforiaRenderer Vuforia.VuforiaRenderer::get_Instance()
extern "C"  VuforiaRenderer_t3433045970 * VuforiaRenderer_get_Instance_m1013623488 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaRenderer::GetRecommendedFps(Vuforia.VuforiaRenderer/FpsHint)
extern "C"  int32_t VuforiaRenderer_GetRecommendedFps_m561576480 (VuforiaRenderer_t3433045970 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Application::get_targetFrameRate()
extern "C"  int32_t Application_get_targetFrameRate_m1077623221 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C"  String_t* String_Concat_m1715369213 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::set_targetFrameRate(System.Int32)
extern "C"  void Application_set_targetFrameRate_m3682352535 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.Component::GetComponentsInParent<UnityEngine.Canvas>(System.Boolean)
#define Component_GetComponentsInParent_TisCanvas_t3310196443_m1039379662(__this, p0, method) ((  CanvasU5BU5D_t682926938* (*) (Component_t1923634451 *, bool, const RuntimeMethod*))Component_GetComponentsInParent_TisRuntimeObject_m492379918_gshared)(__this, p0, method)
// System.Void InitErrorHandler::ShowErrorMessage(Vuforia.VuforiaUnity/InitError)
extern "C"  void InitErrorHandler_ShowErrorMessage_m1141628225 (InitErrorHandler_t2159361531 * __this, int32_t ___errorCode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t3722313464  Vector3_get_zero_m1409827619 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.RawImage LoadingScreen::FindSpinnerImage()
extern "C"  RawImage_t3182918964 * LoadingScreen_FindSpinnerImage_m2988402308 (LoadingScreen_t2154736699 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::set_backgroundLoadingPriority(UnityEngine.ThreadPriority)
extern "C"  void Application_set_backgroundLoadingPriority_m1116453950 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform UnityEngine.UI.Graphic::get_rectTransform()
extern "C"  RectTransform_t3704657025 * Graphic_get_rectTransform_m1167152468 (Graphic_t1660335611 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t3722313464  Vector3_get_forward_m3100859705 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,System.Single)
extern "C"  void Transform_Rotate_m1749346957 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingScreen::LoadNextSceneAsync()
extern "C"  void LoadingScreen_LoadNextSceneAsync_m3801488710 (LoadingScreen_t2154736699 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsync(System.Int32)
extern "C"  AsyncOperation_t1445031843 * SceneManager_LoadSceneAsync_m3240398665 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.Object::FindObjectsOfType<UnityEngine.UI.RawImage>()
#define Object_FindObjectsOfType_TisRawImage_t3182918964_m2400832436(__this /* static, unused */, method) ((  RawImageU5BU5D_t1576731517* (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectsOfType_TisRuntimeObject_m2647183545_gshared)(__this /* static, unused */, method)
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m4211327027 (Object_t631007953 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::Contains(System.String)
extern "C"  bool String_Contains_m1147431944 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C"  void GameObject__ctor_m2093116449 (GameObject_t1113636619 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3600365921 * GameObject_get_transform_m1369836730 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m1648752846 (Object_t631007953 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m166252750 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern "C"  void Transform_set_parent_m786917804 (Transform_t3600365921 * __this, Transform_t3600365921 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Magnitude(UnityEngine.Vector3)
extern "C"  float Vector3_Magnitude_m3447562852 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_Normalize_m2941713846 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Scale(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_Scale_m4006687392 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C"  Quaternion_t2301928331  Quaternion_Inverse_m1311579081 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t2301928331  Quaternion_op_Multiply_m1294064023 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, Quaternion_t2301928331  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Cross(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_Cross_m418170344 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Dot_m606404487 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Subtraction_m3073674971 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Multiply_m2104357790 (RuntimeObject * __this /* static, unused */, float p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Math3d::SetVectorLength(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Math3d_SetVectorLength_m3782886289 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___vector0, float ___size1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
extern "C"  float Vector3_get_sqrMagnitude_m1474274574 (Vector3_t3722313464 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Quaternion__ctor_m435141806 (Quaternion_t2301928331 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  float Quaternion_Dot_m1392781996 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, Quaternion_t2301928331  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t3722313464  Vector3_get_normalized_m2454957984 (Vector3_t3722313464 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Math3d::ProjectPointOnLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Math3d_ProjectPointOnLine_m4192705973 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___linePoint0, Vector3_t3722313464  ___lineVec1, Vector3_t3722313464  ___point2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Math3d::PointOnWhichSideOfLineSegment(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  int32_t Math3d_PointOnWhichSideOfLineSegment_m3215589412 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___linePoint10, Vector3_t3722313464  ___linePoint21, Vector3_t3722313464  ___point2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Math3d::SignedDistancePlanePoint(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Math3d_SignedDistancePlanePoint_m2566831789 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___planeNormal0, Vector3_t3722313464  ___planePoint1, Vector3_t3722313464  ___point2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_op_Division_m510815599 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Math3d::ClosestPointsOnTwoLines(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Math3d_ClosestPointsOnTwoLines_m2440915867 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464 * ___closestPointLine10, Vector3_t3722313464 * ___closestPointLine21, Vector3_t3722313464  ___linePoint12, Vector3_t3722313464  ___lineVec13, Vector3_t3722313464  ___linePoint24, Vector3_t3722313464  ___lineVec25, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Quaternion_op_Multiply_m2607404835 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern "C"  Vector3_t3722313464  Vector3_get_right_m1913784872 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
extern "C"  Vector4_t3319028937  Matrix4x4_GetColumn_m461504848 (Matrix4x4_t1817901843 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector4)
extern "C"  Vector3_t3722313464  Vector4_op_Implicit_m1158564884 (RuntimeObject * __this /* static, unused */, Vector4_t3319028937  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t2301928331  Quaternion_LookRotation_m3197602968 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t2301928331  Quaternion_get_identity_m3722672781 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern "C"  Vector3_t3722313464  Vector3_get_one_m1629952498 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Math3d::LookRotationExtended(UnityEngine.GameObject&,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Math3d_LookRotationExtended_m2189285815 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 ** ___gameObjectInOut0, Vector3_t3722313464  ___alignWithVector1, Vector3_t3722313464  ___alignWithNormal2, Vector3_t3722313464  ___customForward3, Vector3_t3722313464  ___customUp4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Transform_TransformPoint_m226827784 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3,UnityEngine.Space)
extern "C"  void Transform_Translate_m1990195114 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::get_magnitude()
extern "C"  float Vector3_get_magnitude_m27958459 (Vector3_t3722313464 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t4157153871 * Camera_get_main_m3643453163 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t3722313464  Input_get_mousePosition_m1616496925 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Camera_WorldToScreenPoint_m3726311023 (Camera_t4157153871 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Math3d::ProjectPointOnLineSegment(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Math3d_ProjectPointOnLineSegment_m2179139403 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___linePoint10, Vector3_t3722313464  ___linePoint21, Vector3_t3722313464  ___point2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Math3d::IsPointInRectangle(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Math3d_IsPointInRectangle_m50583159 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___point0, Vector3_t3722313464  ___rectA1, Vector3_t3722313464  ___rectC2, Vector3_t3722313464  ___rectB3, Vector3_t3722313464  ___rectD4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Math3d::AreLineSegmentsCrossing(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Math3d_AreLineSegmentsCrossing_m616452860 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___pointA10, Vector3_t3722313464  ___pointA21, Vector3_t3722313464  ___pointB12, Vector3_t3722313464  ___pointB23, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Math3d::AddVectorLength(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Math3d_AddVectorLength_m3019376001 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___vector0, float ___size1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Math3d::AreQuaternionsClose(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Math3d_AreQuaternionsClose_m3116464126 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  ___q10, Quaternion_t2301928331  ___q21, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Math3d::InverseSignQuaternion(UnityEngine.Quaternion)
extern "C"  Quaternion_t2301928331  Math3d_InverseSignQuaternion_m7038823 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  ___q0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_UnaryNegation_m1951478815 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::FindObjectOfType<MenuOptions>()
#define Object_FindObjectOfType_TisMenuOptions_t1951716431_m1050906446(__this /* static, unused */, method) ((  MenuOptions_t1951716431 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m1542987838_gshared)(__this /* static, unused */, method)
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C"  bool Behaviour_get_enabled_m753527255 (Behaviour_t1437897464 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C"  float Mathf_Clamp01_m56433566 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Slerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_Slerp_m802114822 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void MenuOptions::UpdateUI()
extern "C"  void MenuOptions_UpdateUI_m2695051465 (MenuOptions_t1951716431 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::FindObjectOfType<CameraSettings>()
#define Object_FindObjectOfType_TisCameraSettings_t3152619780_m4203198741(__this /* static, unused */, method) ((  CameraSettings_t3152619780 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m1542987838_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Object::FindObjectOfType<TrackableSettings>()
#define Object_FindObjectOfType_TisTrackableSettings_t2862243993_m1852430619(__this /* static, unused */, method) ((  TrackableSettings_t2862243993 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m1542987838_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Object::FindObjectOfType<MenuAnimator>()
#define Object_FindObjectOfType_TisMenuAnimator_t2112910832_m1830680649(__this /* static, unused */, method) ((  MenuAnimator_t2112910832 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m1542987838_gshared)(__this /* static, unused */, method)
// T MenuOptions::FindUISelectableWithText<UnityEngine.UI.Toggle>(System.String)
#define MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125(__this, ___text0, method) ((  Toggle_t2735377061 * (*) (MenuOptions_t1951716431 *, String_t*, const RuntimeMethod*))MenuOptions_FindUISelectableWithText_TisRuntimeObject_m1777269550_gshared)(__this, ___text0, method)
// System.Boolean UnityEngine.UI.Toggle::get_isOn()
extern "C"  bool Toggle_get_isOn_m1428293607 (Toggle_t2735377061 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSettings::SwitchFlashTorch(System.Boolean)
extern "C"  void CameraSettings_SwitchFlashTorch_m1540789541 (CameraSettings_t3152619780 * __this, bool ___ON0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraSettings::IsFlashTorchEnabled()
extern "C"  bool CameraSettings_IsFlashTorchEnabled_m3875252701 (CameraSettings_t3152619780 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Toggle::set_isOn(System.Boolean)
extern "C"  void Toggle_set_isOn_m3548357404 (Toggle_t2735377061 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void CameraSettings::SelectCamera(Vuforia.CameraDevice/CameraDirection)
extern "C"  void CameraSettings_SelectCamera_m387205834 (CameraSettings_t3152619780 * __this, int32_t ___camDir0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void MenuOptions::ToggleTorch()
extern "C"  void MenuOptions_ToggleTorch_m1503051540 (MenuOptions_t1951716431 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TrackableSettings::ActivateDataSet(System.String)
extern "C"  void TrackableSettings_ActivateDataSet_m4118878993 (TrackableSettings_t2862243993 * __this, String_t* ___datasetName0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean TrackableSettings::IsExtendedTrackingEnabled()
extern "C"  bool TrackableSettings_IsExtendedTrackingEnabled_m1319587722 (TrackableSettings_t2862243993 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraSettings::IsAutofocusEnabled()
extern "C"  bool CameraSettings_IsAutofocusEnabled_m386305083 (CameraSettings_t3152619780 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraSettings::IsFrontCameraActive()
extern "C"  bool CameraSettings_IsFrontCameraActive_m282456036 (CameraSettings_t3152619780 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern "C"  bool GameObject_get_activeInHierarchy_m2006396688 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String TrackableSettings::GetActiveDatasetName()
extern "C"  String_t* TrackableSettings_GetActiveDatasetName_m1771968640 (TrackableSettings_t2862243993 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void MenuAnimator::Hide()
extern "C"  void MenuAnimator_Hide_m1323916210 (MenuAnimator_t2112910832 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern "C"  Transform_t3600365921 * Transform_Find_m1729760951 (Transform_t3600365921 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
extern "C"  GameObject_t1113636619 * GameObject_FindGameObjectWithTag_m2129039296 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<characterLoader>()
#define GameObject_GetComponent_TischaracterLoader_t1326495876_m1035246574(__this, method) ((  characterLoader_t1326495876 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Transform,System.Boolean)
#define Object_Instantiate_TisGameObject_t1113636619_m4130575780(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1113636619 * (*) (RuntimeObject * /* static, unused */, GameObject_t1113636619 *, Transform_t3600365921 *, bool, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m1858812370_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<UnityEngine.Animation>()
#define GameObject_GetComponentInChildren_TisAnimation_t3648466861_m235625869(__this, method) ((  Animation_t3648466861 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponentInChildren_TisRuntimeObject_m1513755678_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.AudioSource>()
#define GameObject_GetComponent_TisAudioSource_t3935305588_m625814604(__this, method) ((  AudioSource_t3935305588 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Boolean UnityEngine.AudioSource::get_isPlaying()
extern "C"  bool AudioSource_get_isPlaying_m1896551654 (AudioSource_t3935305588 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
#define GameObject_GetComponent_TisAnimator_t434523843_m440019408(__this, method) ((  Animator_t434523843 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void UnityEngine.Animator::SetBool(System.String,System.Boolean)
extern "C"  void Animator_SetBool_m234840832 (Animator_t434523843 * __this, String_t* p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Canvas>()
#define GameObject_GetComponent_TisCanvas_t3310196443_m1808013672(__this, method) ((  Canvas_t3310196443 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t1901882714_m2114913816(__this, method) ((  Text_t1901882714 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern "C"  RuntimeObject* Transform_GetEnumerator_m2717073726 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<symbolPrefabDefiner>()
#define GameObject_GetComponent_TissymbolPrefabDefiner_t2798204667_m4248518593(__this, method) ((  symbolPrefabDefiner_t2798204667 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Transform)
#define Object_Instantiate_TisGameObject_t1113636619_m3215236302(__this /* static, unused */, p0, p1, method) ((  GameObject_t1113636619 * (*) (RuntimeObject * /* static, unused */, GameObject_t1113636619 *, Transform_t3600365921 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m1061214600_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.RectTransform>()
#define GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398(__this, method) ((  RectTransform_t3704657025 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C"  Rect_t2360479859  RectTransform_get_rect_m574169965 (RectTransform_t3704657025 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m1358425599 (Rect_t2360479859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m4126691837 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t2670269651_m2486712510(__this, method) ((  Image_t2670269651 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void UnityEngine.AudioSource::Play()
extern "C"  void AudioSource_Play_m48294159 (AudioSource_t3935305588 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::Play(System.String)
extern "C"  void Animator_Play_m1697843332 (Animator_t434523843 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator onScanController::WaitAndPlayShowTail()
extern "C"  RuntimeObject* onScanController_WaitAndPlayShowTail_m1087383620 (onScanController_t1472212342 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void onScanController/<WaitAndPlayShowTail>c__Iterator0::.ctor()
extern "C"  void U3CWaitAndPlayShowTailU3Ec__Iterator0__ctor_m1252499421 (U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void pageTurn::turnToPage(System.Int32)
extern "C"  void pageTurn_turnToPage_m4215582371 (pageTurn_t132420247 * __this, int32_t ___page0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Item(System.Int32)
#define List_1_get_Item_m3743125852(__this, p0, method) ((  GameObject_t1113636619 * (*) (List_1_t2585711361 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Count()
#define List_1_get_Count_m2812834599(__this, method) ((  int32_t (*) (List_1_t2585711361 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern "C"  void MonoBehaviour_print_m330341231 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void settingsManager::toggleSetting(System.String)
extern "C"  void settingsManager_toggleSetting_m1197959296 (settingsManager_t2086358119 * __this, String_t* ___calledSetting0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean setSettings::checkIfOn(System.String)
extern "C"  bool setSettings_checkIfOn_m1363969882 (setSettings_t4116128020 * __this, String_t* ___calledSetting0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
extern "C"  GameObjectU5BU5D_t3328599146* GameObject_FindGameObjectsWithTag_m2585173894 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::set_tag(System.String)
extern "C"  void GameObject_set_tag_m2353670106 (GameObject_t1113636619 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Audio.AudioMixer::GetFloat(System.String,System.Single&)
extern "C"  bool AudioMixer_GetFloat_m3026076910 (AudioMixer_t3521020193 * __this, String_t* p0, float* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void settingsManager::mute(System.String,System.Boolean)
extern "C"  void settingsManager_mute_m3259797164 (settingsManager_t2086358119 * __this, String_t* ___calledSetting0, bool ___isMuted1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<SettingsList>::get_Count()
#define List_1_get_Count_m3659879520(__this, method) ((  int32_t (*) (List_1_t3355509319 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Boolean UnityEngine.Audio.AudioMixer::SetFloat(System.String,System.Single)
extern "C"  bool AudioMixer_SetFloat_m3036056706 (AudioMixer_t3521020193 * __this, String_t* p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Queue`1<UnityEngine.Quaternion>::get_Count()
#define Queue_1_get_Count_m566549244(__this, method) ((  int32_t (*) (Queue_1_t2148187825 *, const RuntimeMethod*))Queue_1_get_Count_m566549244_gshared)(__this, method)
// !0 System.Collections.Generic.Queue`1<UnityEngine.Quaternion>::Dequeue()
#define Queue_1_Dequeue_m127687118(__this, method) ((  Quaternion_t2301928331  (*) (Queue_1_t2148187825 *, const RuntimeMethod*))Queue_1_Dequeue_m127687118_gshared)(__this, method)
// !0 System.Collections.Generic.Queue`1<UnityEngine.Vector3>::Dequeue()
#define Queue_1_Dequeue_m3534639416(__this, method) ((  Vector3_t3722313464  (*) (Queue_1_t3568572958 *, const RuntimeMethod*))Queue_1_Dequeue_m3534639416_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Quaternion>::Enqueue(!0)
#define Queue_1_Enqueue_m309039112(__this, p0, method) ((  void (*) (Queue_1_t2148187825 *, Quaternion_t2301928331 , const RuntimeMethod*))Queue_1_Enqueue_m309039112_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Vector3>::Enqueue(!0)
#define Queue_1_Enqueue_m2554143986(__this, p0, method) ((  void (*) (Queue_1_t3568572958 *, Vector3_t3722313464 , const RuntimeMethod*))Queue_1_Enqueue_m2554143986_gshared)(__this, p0, method)
// UnityEngine.Vector4 UnityEngine.Vector4::get_zero()
extern "C"  Vector4_t3319028937  Vector4_get_zero_m1422399515 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Queue`1/Enumerator<!0> System.Collections.Generic.Queue`1<UnityEngine.Quaternion>::GetEnumerator()
#define Queue_1_GetEnumerator_m3198989551(__this, method) ((  Enumerator_t2810099927  (*) (Queue_1_t2148187825 *, const RuntimeMethod*))Queue_1_GetEnumerator_m3198989551_gshared)(__this, method)
// !0 System.Collections.Generic.Queue`1/Enumerator<UnityEngine.Quaternion>::get_Current()
#define Enumerator_get_Current_m2950039238(__this, method) ((  Quaternion_t2301928331  (*) (Enumerator_t2810099927 *, const RuntimeMethod*))Enumerator_get_Current_m2950039238_gshared)(__this, method)
// !0 System.Collections.Generic.Queue`1<UnityEngine.Quaternion>::Peek()
#define Queue_1_Peek_m1284364542(__this, method) ((  Quaternion_t2301928331  (*) (Queue_1_t2148187825 *, const RuntimeMethod*))Queue_1_Peek_m1284364542_gshared)(__this, method)
// System.Void Math3d::AverageQuaternion(UnityEngine.Vector4&,UnityEngine.Quaternion,UnityEngine.Quaternion,System.Int32)
extern "C"  void Math3d_AverageQuaternion_m2118685977 (RuntimeObject * __this /* static, unused */, Vector4_t3319028937 * ___cumulative0, Quaternion_t2301928331  ___newRotation1, Quaternion_t2301928331  ___firstRotation2, int32_t ___addAmount3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<UnityEngine.Quaternion>::MoveNext()
#define Enumerator_MoveNext_m459395817(__this, method) ((  bool (*) (Enumerator_t2810099927 *, const RuntimeMethod*))Enumerator_MoveNext_m459395817_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<UnityEngine.Quaternion>::Dispose()
#define Enumerator_Dispose_m557667343(__this, method) ((  void (*) (Enumerator_t2810099927 *, const RuntimeMethod*))Enumerator_Dispose_m557667343_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<!0> System.Collections.Generic.Queue`1<UnityEngine.Vector3>::GetEnumerator()
#define Queue_1_GetEnumerator_m676935694(__this, method) ((  Enumerator_t4230485060  (*) (Queue_1_t3568572958 *, const RuntimeMethod*))Queue_1_GetEnumerator_m676935694_gshared)(__this, method)
// !0 System.Collections.Generic.Queue`1/Enumerator<UnityEngine.Vector3>::get_Current()
#define Enumerator_get_Current_m4117045769(__this, method) ((  Vector3_t3722313464  (*) (Enumerator_t4230485060 *, const RuntimeMethod*))Enumerator_get_Current_m4117045769_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<UnityEngine.Vector3>::MoveNext()
#define Enumerator_MoveNext_m2202395141(__this, method) ((  bool (*) (Enumerator_t4230485060 *, const RuntimeMethod*))Enumerator_MoveNext_m2202395141_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<UnityEngine.Vector3>::Dispose()
#define Enumerator_Dispose_m1218804216(__this, method) ((  void (*) (Enumerator_t4230485060 *, const RuntimeMethod*))Enumerator_Dispose_m1218804216_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Queue`1<UnityEngine.Vector3>::get_Count()
#define Queue_1_get_Count_m2376062550(__this, method) ((  int32_t (*) (Queue_1_t3568572958 *, const RuntimeMethod*))Queue_1_get_Count_m2376062550_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Quaternion>::.ctor(System.Int32)
#define Queue_1__ctor_m1117100023(__this, p0, method) ((  void (*) (Queue_1_t2148187825 *, int32_t, const RuntimeMethod*))Queue_1__ctor_m1117100023_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Vector3>::.ctor(System.Int32)
#define Queue_1__ctor_m3566777400(__this, p0, method) ((  void (*) (Queue_1_t3568572958 *, int32_t, const RuntimeMethod*))Queue_1__ctor_m3566777400_gshared)(__this, p0, method)
// !!0 UnityEngine.Component::GetComponent<Vuforia.VuforiaBehaviour>()
#define Component_GetComponent_TisVuforiaBehaviour_t2151848540_m3060796760(__this, method) ((  VuforiaBehaviour_t2151848540 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void Vuforia.VuforiaARController::RegisterTrackablesUpdatedCallback(System.Action)
extern "C"  void VuforiaARController_RegisterTrackablesUpdatedCallback_m2341042762 (VuforiaARController_t1876945237 * __this, Action_t1264377477 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::.ctor(System.Object,System.IntPtr)
#define UnityAction_2__ctor_m608474170(__this, p0, p1, method) ((  void (*) (UnityAction_2_t1262235195 *, RuntimeObject *, intptr_t, const RuntimeMethod*))UnityAction_2__ctor_m608474170_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.SceneManagement.SceneManager::add_activeSceneChanged(UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>)
extern "C"  void SceneManager_add_activeSceneChanged_m1118944293 (RuntimeObject * __this /* static, unused */, UnityAction_2_t1262235195 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SceneManagement.Scene::get_name()
extern "C"  String_t* Scene_get_name_m622963475 (Scene_t2348375561 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaARController::UnregisterVuforiaStartedCallback(System.Action)
extern "C"  void VuforiaARController_UnregisterVuforiaStartedCallback_m3486105406 (VuforiaARController_t1876945237 * __this, Action_t1264377477 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaARController::UnregisterTrackablesUpdatedCallback(System.Action)
extern "C"  void VuforiaARController_UnregisterTrackablesUpdatedCallback_m3199567651 (VuforiaARController_t1876945237 * __this, Action_t1264377477 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean MenuAnimator::IsVisible()
extern "C"  bool MenuAnimator_IsVisible_m928022866 (MenuAnimator_t2112910832 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void TapHandler::HandleTap()
extern "C"  void TapHandler_HandleTap_m1322678531 (TapHandler_t334234343 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButtonUp(System.Int32)
extern "C"  bool Input_GetMouseButtonUp_m2924350851 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponentInChildren<CameraSettings>()
#define Component_GetComponentInChildren_TisCameraSettings_t3152619780_m2209036194(__this, method) ((  CameraSettings_t3152619780 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponentInChildren_TisRuntimeObject_m1033527003_gshared)(__this, method)
// System.Void CameraSettings::TriggerAutofocusEvent()
extern "C"  void CameraSettings_TriggerAutofocusEvent_m898525935 (CameraSettings_t3152619780 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void MenuAnimator::Show()
extern "C"  void MenuAnimator_Show_m982174260 (MenuAnimator_t2112910832 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C"  int32_t Input_get_touchCount_m3403849067 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Nullable`1<UnityEngine.Vector2>::get_HasValue()
#define Nullable_1_get_HasValue_m1317965445(__this, method) ((  bool (*) (Nullable_1_t3878791605 *, const RuntimeMethod*))Nullable_1_get_HasValue_m1317965445_gshared)(__this, method)
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C"  Touch_t1921856868  Input_GetTouch_m2192712756 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C"  Vector2_t2156229523  Touch_get_position_m3109777936 (Touch_t1921856868 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Nullable`1<UnityEngine.Vector2>::.ctor(!0)
#define Nullable_1__ctor_m680077965(__this, p0, method) ((  void (*) (Nullable_1_t3878791605 *, Vector2_t2156229523 , const RuntimeMethod*))Nullable_1__ctor_m680077965_gshared)(__this, p0, method)
// !0 System.Nullable`1<UnityEngine.Vector2>::GetValueOrDefault()
#define Nullable_1_GetValueOrDefault_m3722452406(__this, method) ((  Vector2_t2156229523  (*) (Nullable_1_t3878791605 *, const RuntimeMethod*))Nullable_1_GetValueOrDefault_m3722452406_gshared)(__this, method)
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2156229523  Vector2_op_Subtraction_m73004381 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, method) ((  Camera_t4157153871 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Single UnityEngine.Camera::get_orthographicSize()
extern "C"  float Camera_get_orthographicSize_m3903216845 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2156229523  Vector2_op_Multiply_m2347887432 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_pixelHeight()
extern "C"  int32_t Camera_get_pixelHeight_m722276884 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2156229523  Vector2_op_Division_m132623573 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t3722313464  Vector2_op_Implicit_m1860157806 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Transform_TransformDirection_m3784028109 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Nullable`1<UnityEngine.Vector2>::get_Value()
#define Nullable_1_get_Value_m963759207(__this, method) ((  Vector2_t2156229523  (*) (Nullable_1_t3878791605 *, const RuntimeMethod*))Nullable_1_get_Value_m963759207_gshared)(__this, method)
// System.Single UnityEngine.Vector2::get_magnitude()
extern "C"  float Vector2_get_magnitude_m2752892833 (Vector2_t2156229523 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Camera::get_pixelWidth()
extern "C"  int32_t Camera_get_pixelWidth_m1110053668 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3970636864 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2156229523  Vector2_op_Addition_m800700293 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C"  Quaternion_t2301928331  Transform_get_localRotation_m3487911431 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m3350697880 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
extern "C"  Quaternion_t2301928331  Quaternion_Euler_m1803555822 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_localRotation_m19445462 (Transform_t3600365921 * __this, Quaternion_t2301928331  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
extern "C"  void Camera_set_orthographicSize_m76971700 (Camera_t4157153871 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour> Vuforia.StateManager::GetTrackableBehaviours()
extern "C"  RuntimeObject* StateManager_GetTrackableBehaviours_m3331997292 (StateManager_t1982749557 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTarget Vuforia.ImageTargetBehaviour::get_ImageTarget()
extern "C"  RuntimeObject* ImageTargetBehaviour_get_ImageTarget_m2707173779 (ImageTargetBehaviour_t2200418350 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.MultiTarget Vuforia.MultiTargetBehaviour::get_MultiTarget()
extern "C"  RuntimeObject* MultiTargetBehaviour_get_MultiTarget_m1306973800 (MultiTargetBehaviour_t2061511750 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.CylinderTarget Vuforia.CylinderTargetBehaviour::get_CylinderTarget()
extern "C"  RuntimeObject* CylinderTargetBehaviour_get_CylinderTarget_m2779698828 (CylinderTargetBehaviour_t822809409 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.ObjectTarget Vuforia.ObjectTargetBehaviour::get_ObjectTarget()
extern "C"  RuntimeObject* ObjectTargetBehaviour_get_ObjectTarget_m1153386099 (ObjectTargetBehaviour_t728125005 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vuforia.VuMarkTemplate Vuforia.VuMarkBehaviour::get_VuMarkTemplate()
extern "C"  RuntimeObject* VuMarkBehaviour_get_VuMarkTemplate_m3771121130 (VuMarkBehaviour_t1178230459 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet> Vuforia.ObjectTracker::GetActiveDataSets()
extern "C"  RuntimeObject* ObjectTracker_GetActiveDataSets_m254019146 (ObjectTracker_t4177997237 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<Vuforia.DataSet>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisDataSet_t3286034874_m1984750843(__this /* static, unused */, p0, method) ((  List_1_t463142320 * (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_ToList_TisRuntimeObject_m1551499789_gshared)(__this /* static, unused */, p0, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSet>::get_Count()
#define List_1_get_Count_m3978133574(__this, method) ((  int32_t (*) (List_1_t463142320 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// !!0 System.Linq.Enumerable::ElementAt<Vuforia.DataSet>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
#define Enumerable_ElementAt_TisDataSet_t3286034874_m1304043092(__this /* static, unused */, p0, p1, method) ((  DataSet_t3286034874 * (*) (RuntimeObject * /* static, unused */, RuntimeObject*, int32_t, const RuntimeMethod*))Enumerable_ElementAt_TisRuntimeObject_m2579126540_gshared)(__this /* static, unused */, p0, p1, method)
// System.String Vuforia.DataSet::get_Path()
extern "C"  String_t* DataSet_get_Path_m176743276 (DataSet_t3286034874 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::LastIndexOf(System.String)
extern "C"  int32_t String_LastIndexOf_m2676535141 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32)
extern "C"  String_t* String_Substring_m2848979100 (String_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Char[] System.String::ToCharArray()
extern "C"  CharU5BU5D_t3528271667* String_ToCharArray_m1492846834 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::TrimEnd(System.Char[])
extern "C"  String_t* String_TrimEnd_m3824727301 (String_t* __this, CharU5BU5D_t3528271667* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet> Vuforia.ObjectTracker::GetDataSets()
extern "C"  RuntimeObject* ObjectTracker_GetDataSets_m1245718448 (ObjectTracker_t4177997237 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Vuforia.DataSet>::GetEnumerator()
#define List_1_GetEnumerator_m6144716(__this, method) ((  Enumerator_t2352386197  (*) (List_1_t463142320 *, const RuntimeMethod*))List_1_GetEnumerator_m2930774921_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>::get_Current()
#define Enumerator_get_Current_m1064371760(__this, method) ((  DataSet_t3286034874 * (*) (Enumerator_t2352386197 *, const RuntimeMethod*))Enumerator_get_Current_m470245444_gshared)(__this, method)
// System.Boolean Vuforia.ObjectTracker::DeactivateDataSet(Vuforia.DataSet)
extern "C"  bool ObjectTracker_DeactivateDataSet_m2094146088 (ObjectTracker_t4177997237 * __this, DataSet_t3286034874 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>::MoveNext()
#define Enumerator_MoveNext_m1234730920(__this, method) ((  bool (*) (Enumerator_t2352386197 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>::Dispose()
#define Enumerator_Dispose_m1247034833(__this, method) ((  void (*) (Enumerator_t2352386197 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method)
// System.Boolean Vuforia.ObjectTracker::ActivateDataSet(Vuforia.DataSet)
extern "C"  bool ObjectTracker_ActivateDataSet_m3009523340 (ObjectTracker_t4177997237 * __this, DataSet_t3286034874 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator triggerAudio::SoundCooldown()
extern "C"  RuntimeObject* triggerAudio_SoundCooldown_m3107703534 (triggerAudio_t1885395141 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void triggerAudio/<SoundCooldown>c__Iterator0::.ctor()
extern "C"  void U3CSoundCooldownU3Ec__Iterator0__ctor_m968837215 (U3CSoundCooldownU3Ec__Iterator0_t604438267 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void tutorialManager::Show()
extern "C"  void tutorialManager_Show_m1645464898 (tutorialManager_t3421782323 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AboutScreen::.ctor()
extern "C"  void AboutScreen__ctor_m346556219 (AboutScreen_t2183797299 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AboutScreen::OnStartAR()
extern "C"  void AboutScreen_OnStartAR_m2896384759 (AboutScreen_t2183797299 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AboutScreen_OnStartAR_m2896384759_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3525588294, /*hidden argument*/NULL);
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, _stringLiteral4188196043, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AboutScreen::Update()
extern "C"  void AboutScreen_Update_m1006106731 (AboutScreen_t2183797299 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AboutScreen_Update_m1006106731_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyUp_m2808015270(NULL /*static, unused*/, ((int32_t)13), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		AboutScreen_OnStartAR_m2896384759(__this, /*hidden argument*/NULL);
		goto IL_003d;
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyUp_m2808015270(NULL /*static, unused*/, ((int32_t)330), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		AboutScreen_OnStartAR_m2896384759(__this, /*hidden argument*/NULL);
		goto IL_003d;
	}

IL_0031:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKeyUp_m2808015270(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003d;
		}
	}

IL_003d:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void animateTexture::.ctor()
extern "C"  void animateTexture__ctor_m2542088831 (animateTexture_t712563028 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void animateTexture::Start()
extern "C"  void animateTexture_Start_m3979085971 (animateTexture_t712563028 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (animateTexture_Start_m3979085971_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Renderer_t2627027031 * L_0 = Component_GetComponent_TisRenderer_t2627027031_m2222376672(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2222376672_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Renderer_t2627027031 * L_2 = Component_GetComponent_TisRenderer_t2627027031_m2222376672(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2222376672_RuntimeMethod_var);
		NullCheck(L_2);
		Material_t340375123 * L_3 = Renderer_get_material_m4171603682(L_2, /*hidden argument*/NULL);
		__this->set_objectMaterial_3(L_3);
	}

IL_0022:
	{
		return;
	}
}
// System.Void animateTexture::changeToTexture(System.Int32)
extern "C"  void animateTexture_changeToTexture_m3561338433 (animateTexture_t712563028 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (animateTexture_changeToTexture_m3561338433_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t340375123 * L_0 = __this->get_objectMaterial_3();
		List_1_t839070149 * L_1 = __this->get_textureFrames_2();
		int32_t L_2 = ___index0;
		NullCheck(L_1);
		Texture_t3661962703 * L_3 = List_1_get_Item_m1945629867(L_1, L_2, /*hidden argument*/List_1_get_Item_m1945629867_RuntimeMethod_var);
		NullCheck(L_0);
		Material_set_mainTexture_m544811714(L_0, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___index0;
		__this->set_currentIndex_4(L_4);
		return;
	}
}
// System.Void animateTexture::changeToNextTexture()
extern "C"  void animateTexture_changeToNextTexture_m2676168445 (animateTexture_t712563028 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (animateTexture_changeToNextTexture_m2676168445_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_currentIndex_4();
		List_1_t839070149 * L_1 = __this->get_textureFrames_2();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m314570365(L_1, /*hidden argument*/List_1_get_Count_m314570365_RuntimeMethod_var);
		if ((((int32_t)L_0) >= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1)))))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = __this->get_currentIndex_4();
		__this->set_currentIndex_4(((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1)));
		goto IL_0032;
	}

IL_002b:
	{
		__this->set_currentIndex_4(0);
	}

IL_0032:
	{
		Material_t340375123 * L_4 = __this->get_objectMaterial_3();
		List_1_t839070149 * L_5 = __this->get_textureFrames_2();
		int32_t L_6 = __this->get_currentIndex_4();
		NullCheck(L_5);
		Texture_t3661962703 * L_7 = List_1_get_Item_m1945629867(L_5, L_6, /*hidden argument*/List_1_get_Item_m1945629867_RuntimeMethod_var);
		NullCheck(L_4);
		Material_set_mainTexture_m544811714(L_4, L_7, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void animationScaling::.ctor()
extern "C"  void animationScaling__ctor_m2713918027 (animationScaling_t1595398591 * __this, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m3353183577((&L_0), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_animaionSize_3(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void animationScaling::Start()
extern "C"  void animationScaling_Start_m289985233 (animationScaling_t1595398591 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_localScale_m129152068(L_0, /*hidden argument*/NULL);
		__this->set_orgSize_2(L_1);
		return;
	}
}
// System.Void animationScaling::Update()
extern "C"  void animationScaling_Update_m2204874217 (animationScaling_t1595398591 * __this, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464 * L_0 = __this->get_address_of_orgSize_2();
		float L_1 = L_0->get_x_1();
		Vector3_t3722313464 * L_2 = __this->get_address_of_animaionSize_3();
		float L_3 = L_2->get_x_1();
		(&V_0)->set_x_1(((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)));
		Vector3_t3722313464 * L_4 = __this->get_address_of_orgSize_2();
		float L_5 = L_4->get_y_2();
		Vector3_t3722313464 * L_6 = __this->get_address_of_animaionSize_3();
		float L_7 = L_6->get_y_2();
		(&V_0)->set_y_2(((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)));
		Vector3_t3722313464 * L_8 = __this->get_address_of_orgSize_2();
		float L_9 = L_8->get_z_3();
		Vector3_t3722313464 * L_10 = __this->get_address_of_animaionSize_3();
		float L_11 = L_10->get_z_3();
		(&V_0)->set_z_3(((float)il2cpp_codegen_multiply((float)L_9, (float)L_11)));
		Transform_t3600365921 * L_12 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_13 = V_0;
		NullCheck(L_12);
		Transform_set_localScale_m3053443106(L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AsyncSceneLoader::.ctor()
extern "C"  void AsyncSceneLoader__ctor_m4061046102 (AsyncSceneLoader_t621267272 * __this, const RuntimeMethod* method)
{
	{
		__this->set_loadingDelay_2((5.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AsyncSceneLoader::Start()
extern "C"  void AsyncSceneLoader_Start_m3052968032 (AsyncSceneLoader_t621267272 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_loadingDelay_2();
		RuntimeObject* L_1 = AsyncSceneLoader_LoadNextSceneAfter_m1751933459(__this, L_0, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator AsyncSceneLoader::LoadNextSceneAfter(System.Single)
extern "C"  RuntimeObject* AsyncSceneLoader_LoadNextSceneAfter_m1751933459 (AsyncSceneLoader_t621267272 * __this, float ___seconds0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AsyncSceneLoader_LoadNextSceneAfter_m1751933459_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170 * V_0 = NULL;
	{
		U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170 * L_0 = (U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170 *)il2cpp_codegen_object_new(U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170_il2cpp_TypeInfo_var);
		U3CLoadNextSceneAfterU3Ec__Iterator0__ctor_m1226795792(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170 * L_1 = V_0;
		float L_2 = ___seconds0;
		NullCheck(L_1);
		L_1->set_seconds_0(L_2);
		U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170 * L_3 = V_0;
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0::.ctor()
extern "C"  void U3CLoadNextSceneAfterU3Ec__Iterator0__ctor_m1226795792 (U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0::MoveNext()
extern "C"  bool U3CLoadNextSceneAfterU3Ec__Iterator0_MoveNext_m3855908786 (U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadNextSceneAfterU3Ec__Iterator0_MoveNext_m3855908786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Scene_t2348375561  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0046;
			}
		}
	}
	{
		goto IL_0061;
	}

IL_0021:
	{
		float L_2 = __this->get_seconds_0();
		WaitForSeconds_t1699091251 * L_3 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U24current_1(L_3);
		bool L_4 = __this->get_U24disposing_2();
		if (L_4)
		{
			goto IL_0041;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0041:
	{
		goto IL_0063;
	}

IL_0046:
	{
		Scene_t2348375561  L_5 = SceneManager_GetActiveScene_m1825203488(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = Scene_get_buildIndex_m270272723((Scene_t2348375561 *)(&V_1), /*hidden argument*/NULL);
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1)), /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_0061:
	{
		return (bool)0;
	}

IL_0063:
	{
		return (bool)1;
	}
}
// System.Object AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CLoadNextSceneAfterU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1590379521 (U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CLoadNextSceneAfterU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1516849507 (U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0::Dispose()
extern "C"  void U3CLoadNextSceneAfterU3Ec__Iterator0_Dispose_m2915084317 (U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0::Reset()
extern "C"  void U3CLoadNextSceneAfterU3Ec__Iterator0_Reset_m3376656304 (U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadNextSceneAfterU3Ec__Iterator0_Reset_m3376656304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CLoadNextSceneAfterU3Ec__Iterator0_Reset_m3376656304_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void buttonColorToggle::.ctor()
extern "C"  void buttonColorToggle__ctor_m3550269698 (buttonColorToggle_t2066501000 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (buttonColorToggle__ctor_m3550269698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector4_t3319028937  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector4__ctor_m2498754347((&L_0), (0.0f), (0.7f), (0.0f), (1.0f), /*hidden argument*/NULL);
		Color_t2555686324  L_1 = Color_op_Implicit_m2665280078(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_OnColor_5(L_1);
		Vector4_t3319028937  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector4__ctor_m2498754347((&L_2), (0.7f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		Color_t2555686324  L_3 = Color_op_Implicit_m2665280078(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_OffColor_6(L_3);
		__this->set_OnText_9(_stringLiteral3454842849);
		__this->set_OffText_10(_stringLiteral133445133);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void buttonColorToggle::Start()
extern "C"  void buttonColorToggle_Start_m2208318078 (buttonColorToggle_t2066501000 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (buttonColorToggle_Start_m2208318078_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = __this->get_settingName_4();
		bool L_1 = buttonColorToggle_checkIfOn_m1439767701(__this, L_0, /*hidden argument*/NULL);
		__this->set_toggleOn_2((bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0));
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Text_t1901882714 * L_3 = Component_GetComponentInChildren_TisText_t1901882714_m396351542(L_2, /*hidden argument*/Component_GetComponentInChildren_TisText_t1901882714_m396351542_RuntimeMethod_var);
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_3);
		__this->set_buttonText_11(L_4);
		buttonColorToggle_toggleColor_m646326456(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void buttonColorToggle::toggleColor(System.Boolean)
extern "C"  void buttonColorToggle_toggleColor_m646326456 (buttonColorToggle_t2066501000 * __this, bool ___turnOfOtherSettingCall0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (buttonColorToggle_toggleColor_m646326456_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___turnOfOtherSettingCall0;
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		bool L_1 = __this->get_toggleOn_2();
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		bool L_2 = __this->get_toggleOn_2();
		if (!L_2)
		{
			goto IL_0081;
		}
	}
	{
		bool L_3 = __this->get_useSprite_3();
		if (L_3)
		{
			goto IL_003e;
		}
	}
	{
		Image_t2670269651 * L_4 = Component_GetComponent_TisImage_t2670269651_m980647750(__this, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		Color_t2555686324  L_5 = __this->get_OffColor_6();
		NullCheck(L_4);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_4, L_5);
		goto IL_004f;
	}

IL_003e:
	{
		Image_t2670269651 * L_6 = Component_GetComponent_TisImage_t2670269651_m980647750(__this, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		Sprite_t280657092 * L_7 = __this->get_OffSprite_8();
		NullCheck(L_6);
		Image_set_sprite_m2369174689(L_6, L_7, /*hidden argument*/NULL);
	}

IL_004f:
	{
		Transform_t3600365921 * L_8 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Text_t1901882714 * L_9 = Component_GetComponentInChildren_TisText_t1901882714_m396351542(L_8, /*hidden argument*/Component_GetComponentInChildren_TisText_t1901882714_m396351542_RuntimeMethod_var);
		String_t* L_10 = __this->get_buttonText_11();
		String_t* L_11 = __this->get_OffText_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m3755062657(NULL /*static, unused*/, L_10, _stringLiteral3452614528, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_9, L_12);
		__this->set_toggleOn_2((bool)0);
		goto IL_00e0;
	}

IL_0081:
	{
		bool L_13 = __this->get_useSprite_3();
		if (L_13)
		{
			goto IL_00a2;
		}
	}
	{
		Image_t2670269651 * L_14 = Component_GetComponent_TisImage_t2670269651_m980647750(__this, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		Color_t2555686324  L_15 = __this->get_OnColor_5();
		NullCheck(L_14);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_14, L_15);
		goto IL_00b3;
	}

IL_00a2:
	{
		Image_t2670269651 * L_16 = Component_GetComponent_TisImage_t2670269651_m980647750(__this, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		Sprite_t280657092 * L_17 = __this->get_OnSprite_7();
		NullCheck(L_16);
		Image_set_sprite_m2369174689(L_16, L_17, /*hidden argument*/NULL);
	}

IL_00b3:
	{
		Transform_t3600365921 * L_18 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Text_t1901882714 * L_19 = Component_GetComponentInChildren_TisText_t1901882714_m396351542(L_18, /*hidden argument*/Component_GetComponentInChildren_TisText_t1901882714_m396351542_RuntimeMethod_var);
		String_t* L_20 = __this->get_buttonText_11();
		String_t* L_21 = __this->get_OnText_9();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m3755062657(NULL /*static, unused*/, L_20, _stringLiteral3452614528, L_21, /*hidden argument*/NULL);
		NullCheck(L_19);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_19, L_22);
		__this->set_toggleOn_2((bool)1);
	}

IL_00e0:
	{
		return;
	}
}
// System.Boolean buttonColorToggle::checkIfOn(System.String)
extern "C"  bool buttonColorToggle_checkIfOn_m1439767701 (buttonColorToggle_t2066501000 * __this, String_t* ___calledSetting0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (buttonColorToggle_checkIfOn_m1439767701_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral537141514, /*hidden argument*/NULL);
		NullCheck(L_0);
		settingsManager_t2086358119 * L_1 = GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329(L_0, /*hidden argument*/GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329_RuntimeMethod_var);
		NullCheck(L_1);
		List_1_t3355509319 * L_2 = L_1->get_settingslist_2();
		GameObject_t1113636619 * L_3 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral537141514, /*hidden argument*/NULL);
		NullCheck(L_3);
		settingsManager_t2086358119 * L_4 = GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329(L_3, /*hidden argument*/GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329_RuntimeMethod_var);
		String_t* L_5 = ___calledSetting0;
		NullCheck(L_4);
		int32_t L_6 = settingsManager_findSetting_m2352407313(L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		SettingsList_t1883434577 * L_7 = List_1_get_Item_m3797380788(L_2, L_6, /*hidden argument*/List_1_get_Item_m3797380788_RuntimeMethod_var);
		NullCheck(L_7);
		bool L_8 = L_7->get_IsActive_1();
		if (!L_8)
		{
			goto IL_003a;
		}
	}
	{
		return (bool)1;
	}

IL_003a:
	{
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void buttonMoveToScene::.ctor()
extern "C"  void buttonMoveToScene__ctor_m3189783950 (buttonMoveToScene_t2573241184 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void buttonMoveToScene::LoadLevel()
extern "C"  void buttonMoveToScene_LoadLevel_m418220376 (buttonMoveToScene_t2573241184 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_nextLvlBuildIndex_2();
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void buttonTriggerAnimation::.ctor()
extern "C"  void buttonTriggerAnimation__ctor_m3884684461 (buttonTriggerAnimation_t4121295644 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void buttonTriggerAnimation::PlayAnimation()
extern "C"  void buttonTriggerAnimation_PlayAnimation_m1140260127 (buttonTriggerAnimation_t4121295644 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (buttonTriggerAnimation_PlayAnimation_m1140260127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_animationBusy_2();
		if (L_0)
		{
			goto IL_0029;
		}
	}
	{
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Animation_t3648466861 * L_2 = GameObject_GetComponent_TisAnimation_t3648466861_m3766528019(L_1, /*hidden argument*/GameObject_GetComponent_TisAnimation_t3648466861_m3766528019_RuntimeMethod_var);
		NullCheck(L_2);
		Animation_Play_m2798573761(L_2, /*hidden argument*/NULL);
		RuntimeObject* L_3 = buttonTriggerAnimation_AnimationCooldown_m4119330121(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Collections.IEnumerator buttonTriggerAnimation::AnimationCooldown()
extern "C"  RuntimeObject* buttonTriggerAnimation_AnimationCooldown_m4119330121 (buttonTriggerAnimation_t4121295644 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (buttonTriggerAnimation_AnimationCooldown_m4119330121_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CAnimationCooldownU3Ec__Iterator0_t1141888348 * V_0 = NULL;
	{
		U3CAnimationCooldownU3Ec__Iterator0_t1141888348 * L_0 = (U3CAnimationCooldownU3Ec__Iterator0_t1141888348 *)il2cpp_codegen_object_new(U3CAnimationCooldownU3Ec__Iterator0_t1141888348_il2cpp_TypeInfo_var);
		U3CAnimationCooldownU3Ec__Iterator0__ctor_m346444143(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAnimationCooldownU3Ec__Iterator0_t1141888348 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		U3CAnimationCooldownU3Ec__Iterator0_t1141888348 * L_2 = V_0;
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void buttonTriggerAnimation/<AnimationCooldown>c__Iterator0::.ctor()
extern "C"  void U3CAnimationCooldownU3Ec__Iterator0__ctor_m346444143 (U3CAnimationCooldownU3Ec__Iterator0_t1141888348 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean buttonTriggerAnimation/<AnimationCooldown>c__Iterator0::MoveNext()
extern "C"  bool U3CAnimationCooldownU3Ec__Iterator0_MoveNext_m786066803 (U3CAnimationCooldownU3Ec__Iterator0_t1141888348 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CAnimationCooldownU3Ec__Iterator0_MoveNext_m786066803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0072;
			}
		}
	}
	{
		goto IL_0085;
	}

IL_0021:
	{
		buttonTriggerAnimation_t4121295644 * L_2 = __this->get_U24this_1();
		NullCheck(L_2);
		L_2->set_animationBusy_2((bool)1);
		buttonTriggerAnimation_t4121295644 * L_3 = __this->get_U24this_1();
		NullCheck(L_3);
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Animation_t3648466861 * L_5 = GameObject_GetComponent_TisAnimation_t3648466861_m3766528019(L_4, /*hidden argument*/GameObject_GetComponent_TisAnimation_t3648466861_m3766528019_RuntimeMethod_var);
		NullCheck(L_5);
		AnimationClip_t2318505987 * L_6 = Animation_get_clip_m1019546398(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		float L_7 = AnimationClip_get_length_m3296085482(L_6, /*hidden argument*/NULL);
		__this->set_U3CanimationLengthU3E__0_0(L_7);
		float L_8 = __this->get_U3CanimationLengthU3E__0_0();
		WaitForSeconds_t1699091251 * L_9 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_9, L_8, /*hidden argument*/NULL);
		__this->set_U24current_2(L_9);
		bool L_10 = __this->get_U24disposing_3();
		if (L_10)
		{
			goto IL_006d;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_006d:
	{
		goto IL_0087;
	}

IL_0072:
	{
		buttonTriggerAnimation_t4121295644 * L_11 = __this->get_U24this_1();
		NullCheck(L_11);
		L_11->set_animationBusy_2((bool)0);
		__this->set_U24PC_4((-1));
	}

IL_0085:
	{
		return (bool)0;
	}

IL_0087:
	{
		return (bool)1;
	}
}
// System.Object buttonTriggerAnimation/<AnimationCooldown>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CAnimationCooldownU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m867172114 (U3CAnimationCooldownU3Ec__Iterator0_t1141888348 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object buttonTriggerAnimation/<AnimationCooldown>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CAnimationCooldownU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2436317299 (U3CAnimationCooldownU3Ec__Iterator0_t1141888348 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void buttonTriggerAnimation/<AnimationCooldown>c__Iterator0::Dispose()
extern "C"  void U3CAnimationCooldownU3Ec__Iterator0_Dispose_m1096724639 (U3CAnimationCooldownU3Ec__Iterator0_t1141888348 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void buttonTriggerAnimation/<AnimationCooldown>c__Iterator0::Reset()
extern "C"  void U3CAnimationCooldownU3Ec__Iterator0_Reset_m4271818490 (U3CAnimationCooldownU3Ec__Iterator0_t1141888348 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CAnimationCooldownU3Ec__Iterator0_Reset_m4271818490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CAnimationCooldownU3Ec__Iterator0_Reset_m4271818490_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CameraController::.ctor()
extern "C"  void CameraController__ctor_m3292347237 (CameraController_t3346819214 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraController::Start()
extern "C"  void CameraController_Start_m1416469467 (CameraController_t3346819214 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = Transform_get_parent_m835071599(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t3722313464  L_2 = Transform_get_position_m36019626(L_1, /*hidden argument*/NULL);
		__this->set_parentStartPosition_4(L_2);
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Quaternion_t2301928331  L_4 = Transform_get_rotation_m3502953881(L_3, /*hidden argument*/NULL);
		__this->set_startRotation_2(L_4);
		Transform_t3600365921 * L_5 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t3722313464  L_6 = Transform_get_position_m36019626(L_5, /*hidden argument*/NULL);
		__this->set_startPosition_3(L_6);
		return;
	}
}
// System.Void CameraController::Update()
extern "C"  void CameraController_Update_m2075811523 (CameraController_t3346819214 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraController_Update_m2075811523_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2U5BU5D_t1457185986* V_0 = NULL;
	int32_t V_1 = 0;
	TouchU5BU5D_t1849554061* G_B2_0 = NULL;
	TouchU5BU5D_t1849554061* G_B1_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		TouchU5BU5D_t1849554061* L_0 = Input_get_touches_m1702694043(NULL /*static, unused*/, /*hidden argument*/NULL);
		Func_2_t528868469 * L_1 = ((CameraController_t3346819214_StaticFields*)il2cpp_codegen_static_fields_for(CameraController_t3346819214_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_5();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001d;
		}
	}
	{
		intptr_t L_2 = (intptr_t)CameraController_U3CUpdateU3Em__0_m3793165627_RuntimeMethod_var;
		Func_2_t528868469 * L_3 = (Func_2_t528868469 *)il2cpp_codegen_object_new(Func_2_t528868469_il2cpp_TypeInfo_var);
		Func_2__ctor_m3243656238(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m3243656238_RuntimeMethod_var);
		((CameraController_t3346819214_StaticFields*)il2cpp_codegen_static_fields_for(CameraController_t3346819214_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache0_5(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001d:
	{
		Func_2_t528868469 * L_4 = ((CameraController_t3346819214_StaticFields*)il2cpp_codegen_static_fields_for(CameraController_t3346819214_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_5();
		RuntimeObject* L_5 = Enumerable_Select_TisTouch_t1921856868_TisVector2_t2156229523_m1617180933(NULL /*static, unused*/, (RuntimeObject*)(RuntimeObject*)G_B2_0, L_4, /*hidden argument*/Enumerable_Select_TisTouch_t1921856868_TisVector2_t2156229523_m1617180933_RuntimeMethod_var);
		Vector2U5BU5D_t1457185986* L_6 = Enumerable_ToArray_TisVector2_t2156229523_m3384610930(NULL /*static, unused*/, L_5, /*hidden argument*/Enumerable_ToArray_TisVector2_t2156229523_m3384610930_RuntimeMethod_var);
		V_0 = L_6;
		Vector2U5BU5D_t1457185986* L_7 = V_0;
		NullCheck(L_7);
		V_1 = (((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length))));
		int32_t L_8 = V_1;
		if ((((int32_t)L_8) == ((int32_t)2)))
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_9 = V_1;
		if ((((int32_t)L_9) == ((int32_t)1)))
		{
			goto IL_0067;
		}
	}
	{
		goto IL_007e;
	}

IL_0044:
	{
		Vector2U5BU5D_t1457185986* L_10 = V_0;
		NullCheck(L_10);
		Vector2U5BU5D_t1457185986* L_11 = V_0;
		NullCheck(L_11);
		CameraController_UpdatePanning_m509325418(__this, (*(Vector2_t2156229523 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), (*(Vector2_t2156229523 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))), /*hidden argument*/NULL);
		goto IL_0083;
	}

IL_0067:
	{
		Vector2U5BU5D_t1457185986* L_12 = V_0;
		NullCheck(L_12);
		CameraController_UpdateRotate_m1805720568(__this, (*(Vector2_t2156229523 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/NULL);
		goto IL_0083;
	}

IL_007e:
	{
		goto IL_0083;
	}

IL_0083:
	{
		return;
	}
}
// System.Void CameraController::UpdateRotate(UnityEngine.Vector2)
extern "C"  void CameraController_UpdateRotate_m1805720568 (CameraController_t3346819214 * __this, Vector2_t2156229523  ___touch00, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraController_UpdateRotate_m1805720568_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t3600365921 * L_2 = Transform_get_parent_m835071599(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t3722313464  L_3 = Transform_get_position_m36019626(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_4 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = (&___touch00)->get_x_0();
		NullCheck(L_0);
		Transform_RotateAround_m2651195670(L_0, L_3, L_4, L_5, /*hidden argument*/NULL);
		Transform_t3600365921 * L_6 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_7 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t3600365921 * L_8 = Transform_get_parent_m835071599(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t3722313464  L_9 = Transform_get_position_m36019626(L_8, /*hidden argument*/NULL);
		Transform_t3600365921 * L_10 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t3722313464  L_11 = Transform_get_right_m2535262102(L_10, /*hidden argument*/NULL);
		Vector3_t3722313464  L_12 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_11, (-1.0f), /*hidden argument*/NULL);
		float L_13 = (&___touch00)->get_y_1();
		NullCheck(L_6);
		Transform_RotateAround_m2651195670(L_6, L_9, L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraController::UpdatePanning(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void CameraController_UpdatePanning_m509325418 (CameraController_t3346819214 * __this, Vector2_t2156229523  ___touch00, Vector2_t2156229523  ___touch11, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraController_UpdatePanning_m509325418_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = Transform_get_parent_m835071599(L_0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_2 = L_1;
		NullCheck(L_2);
		Vector3_t3722313464  L_3 = Transform_get_position_m36019626(L_2, /*hidden argument*/NULL);
		Transform_t3600365921 * L_4 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t3722313464  L_5 = Transform_get_up_m3972993886(L_4, /*hidden argument*/NULL);
		float L_6 = (&___touch11)->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_7 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Vector3_t3722313464  L_8 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_7, (-0.01f), /*hidden argument*/NULL);
		Transform_t3600365921 * L_9 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t3722313464  L_10 = Transform_get_right_m2535262102(L_9, /*hidden argument*/NULL);
		float L_11 = (&___touch11)->get_x_0();
		Vector3_t3722313464  L_12 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		Vector3_t3722313464  L_13 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_12, (-0.01f), /*hidden argument*/NULL);
		Vector3_t3722313464  L_14 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_8, L_13, /*hidden argument*/NULL);
		Vector3_t3722313464  L_15 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_3, L_14, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m3387557959(L_2, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraController::ResetToStart()
extern "C"  void CameraController_ResetToStart_m1020900234 (CameraController_t3346819214 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = Transform_get_parent_m835071599(L_0, /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = __this->get_parentStartPosition_4();
		NullCheck(L_1);
		Transform_set_position_m3387557959(L_1, L_2, /*hidden argument*/NULL);
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_4 = __this->get_startRotation_2();
		NullCheck(L_3);
		Transform_set_rotation_m3524318132(L_3, L_4, /*hidden argument*/NULL);
		Transform_t3600365921 * L_5 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = __this->get_startPosition_3();
		NullCheck(L_5);
		Transform_set_position_m3387557959(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 CameraController::<Update>m__0(UnityEngine.Touch)
extern "C"  Vector2_t2156229523  CameraController_U3CUpdateU3Em__0_m3793165627 (RuntimeObject * __this /* static, unused */, Touch_t1921856868  ___t0, const RuntimeMethod* method)
{
	{
		Vector2_t2156229523  L_0 = Touch_get_deltaPosition_m2389653382((Touch_t1921856868 *)(&___t0), /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CameraFocusController::.ctor()
extern "C"  void CameraFocusController__ctor_m777056272 (CameraFocusController_t1033776956 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraFocusController::Start()
extern "C"  void CameraFocusController_Start_m4173619726 (CameraFocusController_t1033776956 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraFocusController_Start_m4173619726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VuforiaARController_t1876945237 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaARController_t1876945237_il2cpp_TypeInfo_var);
		VuforiaARController_t1876945237 * L_0 = VuforiaARController_get_Instance_m1100644214(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		VuforiaARController_t1876945237 * L_1 = V_0;
		intptr_t L_2 = (intptr_t)CameraFocusController_OnVuforiaStarted_m1082443761_RuntimeMethod_var;
		Action_t1264377477 * L_3 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_3, __this, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		VuforiaARController_RegisterVuforiaStartedCallback_m3258152770(L_1, L_3, /*hidden argument*/NULL);
		VuforiaARController_t1876945237 * L_4 = V_0;
		intptr_t L_5 = (intptr_t)CameraFocusController_OnPaused_m3022290520_RuntimeMethod_var;
		Action_1_t269755560 * L_6 = (Action_1_t269755560 *)il2cpp_codegen_object_new(Action_1_t269755560_il2cpp_TypeInfo_var);
		Action_1__ctor_m2079027267(L_6, __this, L_5, /*hidden argument*/Action_1__ctor_m2079027267_RuntimeMethod_var);
		NullCheck(L_4);
		VuforiaARController_RegisterOnPauseCallback_m1833237148(L_4, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraFocusController::OnVuforiaStarted()
extern "C"  void CameraFocusController_OnVuforiaStarted_m1082443761 (CameraFocusController_t1033776956 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraFocusController_OnVuforiaStarted_m1082443761_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t960297568_il2cpp_TypeInfo_var);
		CameraDevice_t960297568 * L_0 = CameraDevice_get_Instance_m2002290155(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		CameraDevice_SetFocusMode_m2424803588(L_0, 2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraFocusController::OnPaused(System.Boolean)
extern "C"  void CameraFocusController_OnPaused_m3022290520 (CameraFocusController_t1033776956 * __this, bool ___paused0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraFocusController_OnPaused_m3022290520_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___paused0;
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t960297568_il2cpp_TypeInfo_var);
		CameraDevice_t960297568 * L_1 = CameraDevice_get_Instance_m2002290155(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		CameraDevice_SetFocusMode_m2424803588(L_1, 2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CameraSettings::.ctor()
extern "C"  void CameraSettings__ctor_m3533038972 (CameraSettings_t3152619780 * __this, const RuntimeMethod* method)
{
	{
		__this->set_mAutofocusEnabled_3((bool)1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraSettings::Start()
extern "C"  void CameraSettings_Start_m1913056950 (CameraSettings_t3152619780 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraSettings_Start_m1913056950_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VuforiaARController_t1876945237 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaARController_t1876945237_il2cpp_TypeInfo_var);
		VuforiaARController_t1876945237 * L_0 = VuforiaARController_get_Instance_m1100644214(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		VuforiaARController_t1876945237 * L_1 = V_0;
		intptr_t L_2 = (intptr_t)CameraSettings_OnVuforiaStarted_m924645186_RuntimeMethod_var;
		Action_t1264377477 * L_3 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_3, __this, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		VuforiaARController_RegisterVuforiaStartedCallback_m3258152770(L_1, L_3, /*hidden argument*/NULL);
		VuforiaARController_t1876945237 * L_4 = V_0;
		intptr_t L_5 = (intptr_t)CameraSettings_OnPaused_m1743728020_RuntimeMethod_var;
		Action_1_t269755560 * L_6 = (Action_1_t269755560 *)il2cpp_codegen_object_new(Action_1_t269755560_il2cpp_TypeInfo_var);
		Action_1__ctor_m2079027267(L_6, __this, L_5, /*hidden argument*/Action_1__ctor_m2079027267_RuntimeMethod_var);
		NullCheck(L_4);
		VuforiaARController_RegisterOnPauseCallback_m1833237148(L_4, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean CameraSettings::IsFlashTorchEnabled()
extern "C"  bool CameraSettings_IsFlashTorchEnabled_m3875252701 (CameraSettings_t3152619780 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_mFlashTorchEnabled_4();
		return L_0;
	}
}
// System.Void CameraSettings::SwitchFlashTorch(System.Boolean)
extern "C"  void CameraSettings_SwitchFlashTorch_m1540789541 (CameraSettings_t3152619780 * __this, bool ___ON0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraSettings_SwitchFlashTorch_m1540789541_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t960297568_il2cpp_TypeInfo_var);
		CameraDevice_t960297568 * L_0 = CameraDevice_get_Instance_m2002290155(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = ___ON0;
		NullCheck(L_0);
		bool L_2 = CameraDevice_SetFlashTorchMode_m1499936922(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		bool L_3 = ___ON0;
		bool L_4 = L_3;
		RuntimeObject * L_5 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral328110920, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		bool L_7 = ___ON0;
		__this->set_mFlashTorchEnabled_4(L_7);
		goto IL_004d;
	}

IL_0031:
	{
		bool L_8 = ___ON0;
		bool L_9 = L_8;
		RuntimeObject * L_10 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral3558393413, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		__this->set_mFlashTorchEnabled_4((bool)0);
	}

IL_004d:
	{
		return;
	}
}
// System.Boolean CameraSettings::IsAutofocusEnabled()
extern "C"  bool CameraSettings_IsAutofocusEnabled_m386305083 (CameraSettings_t3152619780 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_mAutofocusEnabled_3();
		return L_0;
	}
}
// System.Void CameraSettings::SwitchAutofocus(System.Boolean)
extern "C"  void CameraSettings_SwitchAutofocus_m1104105893 (CameraSettings_t3152619780 * __this, bool ___ON0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraSettings_SwitchAutofocus_m1104105893_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___ON0;
		if (!L_0)
		{
			goto IL_004e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t960297568_il2cpp_TypeInfo_var);
		CameraDevice_t960297568 * L_1 = CameraDevice_get_Instance_m2002290155(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = CameraDevice_SetFocusMode_m2424803588(L_1, 2, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral1601691866, /*hidden argument*/NULL);
		__this->set_mAutofocusEnabled_3((bool)1);
		goto IL_0049;
	}

IL_002c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral1647848890, /*hidden argument*/NULL);
		__this->set_mAutofocusEnabled_3((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t960297568_il2cpp_TypeInfo_var);
		CameraDevice_t960297568 * L_3 = CameraDevice_get_Instance_m2002290155(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		CameraDevice_SetFocusMode_m2424803588(L_3, 0, /*hidden argument*/NULL);
	}

IL_0049:
	{
		goto IL_006b;
	}

IL_004e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3666098040, /*hidden argument*/NULL);
		__this->set_mAutofocusEnabled_3((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t960297568_il2cpp_TypeInfo_var);
		CameraDevice_t960297568 * L_4 = CameraDevice_get_Instance_m2002290155(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		CameraDevice_SetFocusMode_m2424803588(L_4, 0, /*hidden argument*/NULL);
	}

IL_006b:
	{
		return;
	}
}
// System.Void CameraSettings::TriggerAutofocusEvent()
extern "C"  void CameraSettings_TriggerAutofocusEvent_m898525935 (CameraSettings_t3152619780 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraSettings_TriggerAutofocusEvent_m898525935_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t960297568_il2cpp_TypeInfo_var);
		CameraDevice_t960297568 * L_0 = CameraDevice_get_Instance_m2002290155(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		CameraDevice_SetFocusMode_m2424803588(L_0, 1, /*hidden argument*/NULL);
		RuntimeObject* L_1 = CameraSettings_RestoreOriginalFocusMode_m3433876528(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraSettings::SelectCamera(Vuforia.CameraDevice/CameraDirection)
extern "C"  void CameraSettings_SelectCamera_m387205834 (CameraSettings_t3152619780 * __this, int32_t ___camDir0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___camDir0;
		bool L_1 = CameraSettings_RestartCamera_m1627822698(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_2 = ___camDir0;
		__this->set_mActiveDirection_5(L_2);
		__this->set_mFlashTorchEnabled_4((bool)0);
	}

IL_001a:
	{
		return;
	}
}
// System.Boolean CameraSettings::IsFrontCameraActive()
extern "C"  bool CameraSettings_IsFrontCameraActive_m282456036 (CameraSettings_t3152619780 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_mActiveDirection_5();
		return (bool)((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
	}
}
// System.Void CameraSettings::OnVuforiaStarted()
extern "C"  void CameraSettings_OnVuforiaStarted_m924645186 (CameraSettings_t3152619780 * __this, const RuntimeMethod* method)
{
	{
		__this->set_mVuforiaStarted_2((bool)1);
		CameraSettings_SwitchAutofocus_m1104105893(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraSettings::OnPaused(System.Boolean)
extern "C"  void CameraSettings_OnPaused_m1743728020 (CameraSettings_t3152619780 * __this, bool ___paused0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraSettings_OnPaused_m1743728020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		bool L_0 = ___paused0;
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		bool L_2 = __this->get_mVuforiaStarted_2();
		if (!L_2)
		{
			goto IL_0043;
		}
	}
	{
		bool L_3 = __this->get_mAutofocusEnabled_3();
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t960297568_il2cpp_TypeInfo_var);
		CameraDevice_t960297568 * L_4 = CameraDevice_get_Instance_m2002290155(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		CameraDevice_SetFocusMode_m2424803588(L_4, 2, /*hidden argument*/NULL);
		goto IL_003e;
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t960297568_il2cpp_TypeInfo_var);
		CameraDevice_t960297568 * L_5 = CameraDevice_get_Instance_m2002290155(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		CameraDevice_SetFocusMode_m2424803588(L_5, 0, /*hidden argument*/NULL);
	}

IL_003e:
	{
		goto IL_004a;
	}

IL_0043:
	{
		__this->set_mFlashTorchEnabled_4((bool)0);
	}

IL_004a:
	{
		return;
	}
}
// System.Collections.IEnumerator CameraSettings::RestoreOriginalFocusMode()
extern "C"  RuntimeObject* CameraSettings_RestoreOriginalFocusMode_m3433876528 (CameraSettings_t3152619780 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraSettings_RestoreOriginalFocusMode_m3433876528_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229 * V_0 = NULL;
	{
		U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229 * L_0 = (U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229 *)il2cpp_codegen_object_new(U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229_il2cpp_TypeInfo_var);
		U3CRestoreOriginalFocusModeU3Ec__Iterator0__ctor_m1994535627(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229 * L_2 = V_0;
		return L_2;
	}
}
// System.Boolean CameraSettings::RestartCamera(Vuforia.CameraDevice/CameraDirection)
extern "C"  bool CameraSettings_RestartCamera_m1627822698 (CameraSettings_t3152619780 * __this, int32_t ___direction0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraSettings_RestartCamera_m1627822698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectTracker_t4177997237 * V_0 = NULL;
	{
		RuntimeObject* L_0 = TrackerManager_get_Instance_m777262631(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectTracker_t4177997237 * L_1 = GenericInterfaceFuncInvoker0< ObjectTracker_t4177997237 * >::Invoke(ITrackerManager_GetTracker_TisObjectTracker_t4177997237_m1548597255_RuntimeMethod_var, L_0);
		V_0 = L_1;
		ObjectTracker_t4177997237 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		ObjectTracker_t4177997237 * L_3 = V_0;
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(5 /* System.Void Vuforia.Tracker::Stop() */, L_3);
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t960297568_il2cpp_TypeInfo_var);
		CameraDevice_t960297568 * L_4 = CameraDevice_get_Instance_m2002290155(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		CameraDevice_Stop_m1782631459(L_4, /*hidden argument*/NULL);
		CameraDevice_t960297568 * L_5 = CameraDevice_get_Instance_m2002290155(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		CameraDevice_Deinit_m726427492(L_5, /*hidden argument*/NULL);
		CameraDevice_t960297568 * L_6 = CameraDevice_get_Instance_m2002290155(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_7 = ___direction0;
		NullCheck(L_6);
		bool L_8 = CameraDevice_Init_m2722714107(L_6, L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_005b;
		}
	}
	{
		RuntimeObject * L_9 = Box(CameraDirection_t637748435_il2cpp_TypeInfo_var, (&___direction0));
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		___direction0 = *(int32_t*)UnBox(L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral4178167562, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_005b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t960297568_il2cpp_TypeInfo_var);
		CameraDevice_t960297568 * L_12 = CameraDevice_get_Instance_m2002290155(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		bool L_13 = CameraDevice_Start_m2947498176(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0088;
		}
	}
	{
		RuntimeObject * L_14 = Box(CameraDirection_t637748435_il2cpp_TypeInfo_var, (&___direction0));
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
		___direction0 = *(int32_t*)UnBox(L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3976947749, L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0088:
	{
		ObjectTracker_t4177997237 * L_17 = V_0;
		if (!L_17)
		{
			goto IL_00a5;
		}
	}
	{
		ObjectTracker_t4177997237 * L_18 = V_0;
		NullCheck(L_18);
		bool L_19 = VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean Vuforia.Tracker::Start() */, L_18);
		if (L_19)
		{
			goto IL_00a5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral1322537578, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_00a5:
	{
		return (bool)1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::.ctor()
extern "C"  void U3CRestoreOriginalFocusModeU3Ec__Iterator0__ctor_m1994535627 (U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::MoveNext()
extern "C"  bool U3CRestoreOriginalFocusModeU3Ec__Iterator0_MoveNext_m3953872261 (U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CRestoreOriginalFocusModeU3Ec__Iterator0_MoveNext_m3953872261_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0045;
			}
		}
	}
	{
		goto IL_0079;
	}

IL_0021:
	{
		WaitForSeconds_t1699091251 * L_2 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_2, (1.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_0040;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0040:
	{
		goto IL_007b;
	}

IL_0045:
	{
		CameraSettings_t3152619780 * L_4 = __this->get_U24this_0();
		NullCheck(L_4);
		bool L_5 = L_4->get_mAutofocusEnabled_3();
		if (!L_5)
		{
			goto IL_0066;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t960297568_il2cpp_TypeInfo_var);
		CameraDevice_t960297568 * L_6 = CameraDevice_get_Instance_m2002290155(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		CameraDevice_SetFocusMode_m2424803588(L_6, 2, /*hidden argument*/NULL);
		goto IL_0072;
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t960297568_il2cpp_TypeInfo_var);
		CameraDevice_t960297568 * L_7 = CameraDevice_get_Instance_m2002290155(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		CameraDevice_SetFocusMode_m2424803588(L_7, 0, /*hidden argument*/NULL);
	}

IL_0072:
	{
		__this->set_U24PC_3((-1));
	}

IL_0079:
	{
		return (bool)0;
	}

IL_007b:
	{
		return (bool)1;
	}
}
// System.Object CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CRestoreOriginalFocusModeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1094649693 (U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CRestoreOriginalFocusModeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1550947433 (U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::Dispose()
extern "C"  void U3CRestoreOriginalFocusModeU3Ec__Iterator0_Dispose_m2241939766 (U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::Reset()
extern "C"  void U3CRestoreOriginalFocusModeU3Ec__Iterator0_Reset_m884766291 (U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CRestoreOriginalFocusModeU3Ec__Iterator0_Reset_m884766291_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CRestoreOriginalFocusModeU3Ec__Iterator0_Reset_m884766291_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CharacterCycler::.ctor()
extern "C"  void CharacterCycler__ctor_m191614308 (CharacterCycler_t3521054542 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CharacterCycler::Start()
extern "C"  void CharacterCycler_Start_m401767514 (CharacterCycler_t3521054542 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CharacterCycler_Start_m401767514_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_n_4(0);
		goto IL_002d;
	}

IL_000c:
	{
		GameObjectU5BU5D_t3328599146* L_0 = __this->get_charList_3();
		int32_t L_1 = __this->get_n_4();
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_t1113636619 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)0, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_n_4();
		__this->set_n_4(((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1)));
	}

IL_002d:
	{
		int32_t L_5 = __this->get_n_4();
		GameObjectU5BU5D_t3328599146* L_6 = __this->get_charList_3();
		NullCheck(L_6);
		if ((((int32_t)L_5) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_6)->max_length)))))))
		{
			goto IL_000c;
		}
	}
	{
		__this->set_n_4(0);
		GameObjectU5BU5D_t3328599146* L_7 = __this->get_charList_3();
		NullCheck(L_7);
		int32_t L_8 = 0;
		GameObject_t1113636619 * L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_9);
		GameObject_SetActive_m796801857(L_9, (bool)1, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_10 = __this->get_charList_3();
		NullCheck(L_10);
		int32_t L_11 = (((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length))));
		RuntimeObject * L_12 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_11);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		int32_t L_13 = __this->get_n_4();
		int32_t L_14 = L_13;
		RuntimeObject * L_15 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_14);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CharacterCycler::cycle()
extern "C"  void CharacterCycler_cycle_m206077335 (CharacterCycler_t3521054542 * __this, const RuntimeMethod* method)
{
	{
		GameObjectU5BU5D_t3328599146* L_0 = __this->get_charList_3();
		int32_t L_1 = __this->get_n_4();
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_t1113636619 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)0, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_n_4();
		__this->set_n_4(((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1)));
		int32_t L_5 = __this->get_n_4();
		GameObjectU5BU5D_t3328599146* L_6 = __this->get_charList_3();
		NullCheck(L_6);
		if ((!(((uint32_t)L_5) == ((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_6)->max_length))))))))
		{
			goto IL_003b;
		}
	}
	{
		__this->set_n_4(0);
	}

IL_003b:
	{
		GameObjectU5BU5D_t3328599146* L_7 = __this->get_charList_3();
		int32_t L_8 = __this->get_n_4();
		NullCheck(L_7);
		int32_t L_9 = L_8;
		GameObject_t1113636619 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		GameObject_SetActive_m796801857(L_10, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CharacterCycler::Update()
extern "C"  void CharacterCycler_Update_m2032368594 (CharacterCycler_t3521054542 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void characterLoader::.ctor()
extern "C"  void characterLoader__ctor_m3029715901 (characterLoader_t1326495876 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void characterSlot::.ctor(System.String,UnityEngine.GameObject)
extern "C"  void characterSlot__ctor_m3808146015 (characterSlot_t990761438 * __this, String_t* ___newCharacterID0, GameObject_t1113636619 * ___newCharacterPrefab1, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___newCharacterID0;
		__this->set_CharacterID_0(L_0);
		GameObject_t1113636619 * L_1 = ___newCharacterPrefab1;
		__this->set_CharacterPrefab_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DefaultInitializationErrorHandler::.ctor()
extern "C"  void DefaultInitializationErrorHandler__ctor_m2145257936 (DefaultInitializationErrorHandler_t3109936861 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler__ctor_m2145257936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		__this->set_mErrorText_2(L_0);
		VuforiaMonoBehaviour__ctor_m348562674(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DefaultInitializationErrorHandler::OnVuforiaInitializationError(Vuforia.VuforiaUnity/InitError)
extern "C"  void DefaultInitializationErrorHandler_OnVuforiaInitializationError_m512807497 (DefaultInitializationErrorHandler_t3109936861 * __this, int32_t ___initError0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___initError0;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_1 = ___initError0;
		DefaultInitializationErrorHandler_SetErrorCode_m599033302(__this, L_1, /*hidden argument*/NULL);
		DefaultInitializationErrorHandler_SetErrorOccurred_m1940230672(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void DefaultInitializationErrorHandler::Awake()
extern "C"  void DefaultInitializationErrorHandler_Awake_m1713298888 (DefaultInitializationErrorHandler_t3109936861 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_Awake_m1713298888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntime_t1949122020_il2cpp_TypeInfo_var);
		VuforiaRuntime_t1949122020 * L_0 = VuforiaRuntime_get_Instance_m1058251676(NULL /*static, unused*/, /*hidden argument*/NULL);
		intptr_t L_1 = (intptr_t)DefaultInitializationErrorHandler_OnVuforiaInitializationError_m512807497_RuntimeMethod_var;
		Action_1_t3593217305 * L_2 = (Action_1_t3593217305 *)il2cpp_codegen_object_new(Action_1_t3593217305_il2cpp_TypeInfo_var);
		Action_1__ctor_m2713332384(L_2, __this, L_1, /*hidden argument*/Action_1__ctor_m2713332384_RuntimeMethod_var);
		NullCheck(L_0);
		VuforiaRuntime_RegisterVuforiaInitErrorCallback_m2728879505(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DefaultInitializationErrorHandler::Start()
extern "C"  void DefaultInitializationErrorHandler_Start_m2498604264 (DefaultInitializationErrorHandler_t3109936861 * __this, const RuntimeMethod* method)
{
	{
		DefaultInitializationErrorHandler_SetupGUIStyles_m3863535424(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DefaultInitializationErrorHandler::OnGUI()
extern "C"  void DefaultInitializationErrorHandler_OnGUI_m2338842741 (DefaultInitializationErrorHandler_t3109936861 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_OnGUI_m2338842741_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_mErrorOccurred_3();
		if (!L_0)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t2360479859  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Rect__ctor_m2614021312((&L_3), (0.0f), (0.0f), (((float)((float)L_1))), (((float)((float)L_2))), /*hidden argument*/NULL);
		intptr_t L_4 = (intptr_t)DefaultInitializationErrorHandler_DrawWindowContent_m2208378571_RuntimeMethod_var;
		WindowFunction_t3146511083 * L_5 = (WindowFunction_t3146511083 *)il2cpp_codegen_object_new(WindowFunction_t3146511083_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m2544237635(L_5, __this, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_Window_m1088326791(NULL /*static, unused*/, 0, L_3, L_5, L_6, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void DefaultInitializationErrorHandler::OnDestroy()
extern "C"  void DefaultInitializationErrorHandler_OnDestroy_m3668093536 (DefaultInitializationErrorHandler_t3109936861 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_OnDestroy_m3668093536_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntime_t1949122020_il2cpp_TypeInfo_var);
		VuforiaRuntime_t1949122020 * L_0 = VuforiaRuntime_get_Instance_m1058251676(NULL /*static, unused*/, /*hidden argument*/NULL);
		intptr_t L_1 = (intptr_t)DefaultInitializationErrorHandler_OnVuforiaInitializationError_m512807497_RuntimeMethod_var;
		Action_1_t3593217305 * L_2 = (Action_1_t3593217305 *)il2cpp_codegen_object_new(Action_1_t3593217305_il2cpp_TypeInfo_var);
		Action_1__ctor_m2713332384(L_2, __this, L_1, /*hidden argument*/Action_1__ctor_m2713332384_RuntimeMethod_var);
		NullCheck(L_0);
		VuforiaRuntime_UnregisterVuforiaInitErrorCallback_m1304340042(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DefaultInitializationErrorHandler::DrawWindowContent(System.Int32)
extern "C"  void DefaultInitializationErrorHandler_DrawWindowContent_m2208378571 (DefaultInitializationErrorHandler_t3109936861 * __this, int32_t ___id0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_DrawWindowContent_m2208378571_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t2360479859  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t2360479859  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t2360479859  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		int32_t L_0 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect__ctor_m2614021312((Rect_t2360479859 *)(&V_0), (0.0f), (0.0f), (((float)((float)L_0))), (((float)((float)((int32_t)((int32_t)L_1/(int32_t)8))))), /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_3 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect__ctor_m2614021312((Rect_t2360479859 *)(&V_1), (0.0f), (((float)((float)((int32_t)((int32_t)L_2/(int32_t)8))))), (((float)((float)L_3))), (((float)((float)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)((int32_t)L_4/(int32_t)8)), (int32_t)6))))), /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_6 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_7 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect__ctor_m2614021312((Rect_t2360479859 *)(&V_2), (0.0f), (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_5, (int32_t)((int32_t)((int32_t)L_6/(int32_t)8))))))), (((float)((float)L_7))), (((float)((float)((int32_t)((int32_t)L_8/(int32_t)8))))), /*hidden argument*/NULL);
		Rect_t2360479859  L_9 = V_0;
		GUIStyle_t3956901511 * L_10 = __this->get_headerStyle_6();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_Label_m2420537077(NULL /*static, unused*/, L_9, _stringLiteral2016908147, L_10, /*hidden argument*/NULL);
		Rect_t2360479859  L_11 = V_1;
		String_t* L_12 = __this->get_mErrorText_2();
		GUIStyle_t3956901511 * L_13 = __this->get_bodyStyle_5();
		GUI_Label_m2420537077(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		Rect_t2360479859  L_14 = V_2;
		GUIStyle_t3956901511 * L_15 = __this->get_footerStyle_7();
		bool L_16 = GUI_Button_m2223708732(NULL /*static, unused*/, L_14, _stringLiteral3483484711, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00a9;
		}
	}
	{
		Application_Quit_m470877999(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_00a9:
	{
		return;
	}
}
// System.Void DefaultInitializationErrorHandler::SetErrorCode(Vuforia.VuforiaUnity/InitError)
extern "C"  void DefaultInitializationErrorHandler_SetErrorCode_m599033302 (DefaultInitializationErrorHandler_t3109936861 * __this, int32_t ___errorCode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_SetErrorCode_m599033302_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		int32_t L_0 = ___errorCode0;
		switch (((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)((int32_t)10))))
		{
			case 0:
			{
				goto IL_0036;
			}
			case 1:
			{
				goto IL_00ac;
			}
			case 2:
			{
				goto IL_0091;
			}
			case 3:
			{
				goto IL_0071;
			}
			case 4:
			{
				goto IL_0081;
			}
			case 5:
			{
				goto IL_0056;
			}
			case 6:
			{
				goto IL_0046;
			}
			case 7:
			{
				goto IL_00cc;
			}
			case 8:
			{
				goto IL_00eb;
			}
			case 9:
			{
				goto IL_00fb;
			}
		}
	}
	{
		goto IL_010b;
	}

IL_0036:
	{
		__this->set_mErrorText_2(_stringLiteral3279329212);
		goto IL_010b;
	}

IL_0046:
	{
		__this->set_mErrorText_2(_stringLiteral3325583105);
		goto IL_010b;
	}

IL_0056:
	{
		String_t* L_1 = DefaultInitializationErrorHandler_getKeyInfo_m1864640064(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1253325676, L_1, /*hidden argument*/NULL);
		__this->set_mErrorText_2(L_2);
		goto IL_010b;
	}

IL_0071:
	{
		__this->set_mErrorText_2(_stringLiteral2959890895);
		goto IL_010b;
	}

IL_0081:
	{
		__this->set_mErrorText_2(_stringLiteral2293327149);
		goto IL_010b;
	}

IL_0091:
	{
		String_t* L_3 = DefaultInitializationErrorHandler_getKeyInfo_m1864640064(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2746077084, L_3, /*hidden argument*/NULL);
		__this->set_mErrorText_2(L_4);
		goto IL_010b;
	}

IL_00ac:
	{
		String_t* L_5 = DefaultInitializationErrorHandler_getKeyInfo_m1864640064(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral2746058527, L_5, _stringLiteral491174246, /*hidden argument*/NULL);
		__this->set_mErrorText_2(L_6);
		goto IL_010b;
	}

IL_00cc:
	{
		String_t* L_7 = Application_get_productName_m2401755738(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral3183081100, L_7, _stringLiteral868600955, /*hidden argument*/NULL);
		__this->set_mErrorText_2(L_8);
		goto IL_010b;
	}

IL_00eb:
	{
		__this->set_mErrorText_2(_stringLiteral3122929577);
		goto IL_010b;
	}

IL_00fb:
	{
		__this->set_mErrorText_2(_stringLiteral3567432369);
		goto IL_010b;
	}

IL_010b:
	{
		RuntimeObject * L_9 = Box(InitError_t3420749710_il2cpp_TypeInfo_var, (&___errorCode0));
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		___errorCode0 = *(int32_t*)UnBox(L_9);
		NullCheck(L_10);
		String_t* L_11 = String_Replace_m1273907647(L_10, _stringLiteral3452614641, _stringLiteral3452614528, /*hidden argument*/NULL);
		String_t* L_12 = __this->get_mErrorText_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m2163913788(NULL /*static, unused*/, _stringLiteral229317972, L_11, _stringLiteral2072581803, L_12, /*hidden argument*/NULL);
		__this->set_mErrorText_2(L_13);
		String_t* L_14 = __this->get_mErrorText_2();
		String_t* L_15 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		NullCheck(L_14);
		String_t* L_16 = String_Replace_m1273907647(L_14, _stringLiteral229317972, L_15, /*hidden argument*/NULL);
		String_t* L_17 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		NullCheck(L_16);
		String_t* L_18 = String_Replace_m1273907647(L_16, _stringLiteral2642543365, L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		ObjectU5BU5D_t2843939325* L_19 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral3752705136);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral3752705136);
		ObjectU5BU5D_t2843939325* L_20 = L_19;
		int32_t L_21 = ___errorCode0;
		int32_t L_22 = L_21;
		RuntimeObject * L_23 = Box(InitError_t3420749710_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_23);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_23);
		ObjectU5BU5D_t2843939325* L_24 = L_20;
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, _stringLiteral3453007782);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral3453007782);
		ObjectU5BU5D_t2843939325* L_25 = L_24;
		String_t* L_26 = V_0;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, L_26);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_26);
		String_t* L_27 = String_Concat_m2971454694(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DefaultInitializationErrorHandler::SetErrorOccurred(System.Boolean)
extern "C"  void DefaultInitializationErrorHandler_SetErrorOccurred_m1940230672 (DefaultInitializationErrorHandler_t3109936861 * __this, bool ___errorOccurred0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___errorOccurred0;
		__this->set_mErrorOccurred_3(L_0);
		return;
	}
}
// System.String DefaultInitializationErrorHandler::getKeyInfo()
extern "C"  String_t* DefaultInitializationErrorHandler_getKeyInfo_m1864640064 (DefaultInitializationErrorHandler_t3109936861 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_getKeyInfo_m1864640064_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaConfiguration_t1763229349_il2cpp_TypeInfo_var);
		VuforiaConfiguration_t1763229349 * L_0 = VuforiaConfiguration_get_Instance_m3335903280(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		GenericVuforiaConfiguration_t3697830469 * L_1 = VuforiaConfiguration_get_Vuforia_m1588208597(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = GenericVuforiaConfiguration_get_LicenseKey_m2270076687(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m3847582255(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)((int32_t)10))))
		{
			goto IL_0079;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_5 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral1431967569);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral1431967569);
		ObjectU5BU5D_t2843939325* L_6 = L_5;
		String_t* L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m3847582255(L_7, /*hidden argument*/NULL);
		int32_t L_9 = L_8;
		RuntimeObject * L_10 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		ObjectU5BU5D_t2843939325* L_11 = L_6;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteral3797279721);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral3797279721);
		ObjectU5BU5D_t2843939325* L_12 = L_11;
		String_t* L_13 = V_0;
		NullCheck(L_13);
		String_t* L_14 = String_Substring_m1610150815(L_13, 0, 5, /*hidden argument*/NULL);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_14);
		ObjectU5BU5D_t2843939325* L_15 = L_12;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral1108443480);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral1108443480);
		ObjectU5BU5D_t2843939325* L_16 = L_15;
		String_t* L_17 = V_0;
		String_t* L_18 = V_0;
		NullCheck(L_18);
		int32_t L_19 = String_get_Length_m3847582255(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		String_t* L_20 = String_Substring_m1610150815(L_17, ((int32_t)il2cpp_codegen_subtract((int32_t)L_19, (int32_t)5)), 5, /*hidden argument*/NULL);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_20);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_20);
		ObjectU5BU5D_t2843939325* L_21 = L_16;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteral2072975055);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)_stringLiteral2072975055);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m2971454694(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		V_1 = L_22;
		goto IL_00af;
	}

IL_0079:
	{
		ObjectU5BU5D_t2843939325* L_23 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral1431967569);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral1431967569);
		ObjectU5BU5D_t2843939325* L_24 = L_23;
		String_t* L_25 = V_0;
		NullCheck(L_25);
		int32_t L_26 = String_get_Length_m3847582255(L_25, /*hidden argument*/NULL);
		int32_t L_27 = L_26;
		RuntimeObject * L_28 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_27);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_28);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_28);
		ObjectU5BU5D_t2843939325* L_29 = L_24;
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, _stringLiteral1498400317);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral1498400317);
		ObjectU5BU5D_t2843939325* L_30 = L_29;
		String_t* L_31 = V_0;
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, L_31);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_31);
		ObjectU5BU5D_t2843939325* L_32 = L_30;
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, _stringLiteral2072975055);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral2072975055);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m2971454694(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		V_1 = L_33;
	}

IL_00af:
	{
		String_t* L_34 = V_1;
		return L_34;
	}
}
// System.Void DefaultInitializationErrorHandler::SetupGUIStyles()
extern "C"  void DefaultInitializationErrorHandler_SetupGUIStyles_m3863535424 (DefaultInitializationErrorHandler_t3109936861 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_SetupGUIStyles_m3863535424_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int32_t G_B3_0 = 0;
	int32_t G_B6_0 = 0;
	{
		int32_t L_0 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_2 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_001e;
	}

IL_0019:
	{
		int32_t L_3 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_001e:
	{
		V_0 = G_B3_0;
		int32_t L_4 = V_0;
		float L_5 = Screen_get_dpi_m495672463(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)(((float)((float)L_4)))/(float)L_5));
		float L_6 = V_1;
		if ((!(((float)L_6) > ((float)(4.0f)))))
		{
			goto IL_0039;
		}
	}
	{
		G_B6_0 = 2;
		goto IL_003a;
	}

IL_0039:
	{
		G_B6_0 = 1;
	}

IL_003a:
	{
		V_2 = G_B6_0;
		Color_t2555686324  L_7 = Color_get_white_m332174077(NULL /*static, unused*/, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_8 = DefaultInitializationErrorHandler_CreateSinglePixelTexture_m424000749(__this, L_7, /*hidden argument*/NULL);
		__this->set_bodyTexture_8(L_8);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_9 = Mathf_InverseLerp_m4155825980(NULL /*static, unused*/, (0.0f), (255.0f), (220.0f), /*hidden argument*/NULL);
		float L_10 = Mathf_InverseLerp_m4155825980(NULL /*static, unused*/, (0.0f), (255.0f), (220.0f), /*hidden argument*/NULL);
		float L_11 = Mathf_InverseLerp_m4155825980(NULL /*static, unused*/, (0.0f), (255.0f), (220.0f), /*hidden argument*/NULL);
		Color_t2555686324  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Color__ctor_m286683560((&L_12), L_9, L_10, L_11, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_13 = DefaultInitializationErrorHandler_CreateSinglePixelTexture_m424000749(__this, L_12, /*hidden argument*/NULL);
		__this->set_headerTexture_9(L_13);
		float L_14 = Mathf_InverseLerp_m4155825980(NULL /*static, unused*/, (0.0f), (255.0f), (35.0f), /*hidden argument*/NULL);
		float L_15 = Mathf_InverseLerp_m4155825980(NULL /*static, unused*/, (0.0f), (255.0f), (178.0f), /*hidden argument*/NULL);
		float L_16 = Mathf_InverseLerp_m4155825980(NULL /*static, unused*/, (0.0f), (255.0f), (0.0f), /*hidden argument*/NULL);
		Color_t2555686324  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Color__ctor_m286683560((&L_17), L_14, L_15, L_16, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_18 = DefaultInitializationErrorHandler_CreateSinglePixelTexture_m424000749(__this, L_17, /*hidden argument*/NULL);
		__this->set_footerTexture_10(L_18);
		GUIStyle_t3956901511 * L_19 = (GUIStyle_t3956901511 *)il2cpp_codegen_object_new(GUIStyle_t3956901511_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m4038363858(L_19, /*hidden argument*/NULL);
		__this->set_bodyStyle_5(L_19);
		GUIStyle_t3956901511 * L_20 = __this->get_bodyStyle_5();
		NullCheck(L_20);
		GUIStyleState_t1397964415 * L_21 = GUIStyle_get_normal_m729441812(L_20, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_22 = __this->get_bodyTexture_8();
		NullCheck(L_21);
		GUIStyleState_set_background_m369476077(L_21, L_22, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_23 = __this->get_bodyStyle_5();
		Font_t1956802104 * L_24 = Resources_GetBuiltinResource_TisFont_t1956802104_m2738776830(NULL /*static, unused*/, _stringLiteral2974894664, /*hidden argument*/Resources_GetBuiltinResource_TisFont_t1956802104_m2738776830_RuntimeMethod_var);
		NullCheck(L_23);
		GUIStyle_set_font_m2490449107(L_23, L_24, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_25 = __this->get_bodyStyle_5();
		int32_t L_26 = V_2;
		float L_27 = Screen_get_dpi_m495672463(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_25);
		GUIStyle_set_fontSize_m1566850023(L_25, (((int32_t)((int32_t)((float)((float)((float)il2cpp_codegen_multiply((float)(((float)((float)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)18), (int32_t)L_26))))), (float)L_27))/(float)(160.0f)))))), /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_28 = __this->get_bodyStyle_5();
		NullCheck(L_28);
		GUIStyleState_t1397964415 * L_29 = GUIStyle_get_normal_m729441812(L_28, /*hidden argument*/NULL);
		Color_t2555686324  L_30 = Color_get_black_m719512684(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_29);
		GUIStyleState_set_textColor_m1105876047(L_29, L_30, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_31 = __this->get_bodyStyle_5();
		NullCheck(L_31);
		GUIStyle_set_wordWrap_m1419501823(L_31, (bool)1, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_32 = __this->get_bodyStyle_5();
		NullCheck(L_32);
		GUIStyle_set_alignment_m3944619660(L_32, 4, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_33 = __this->get_bodyStyle_5();
		RectOffset_t1369453676 * L_34 = (RectOffset_t1369453676 *)il2cpp_codegen_object_new(RectOffset_t1369453676_il2cpp_TypeInfo_var);
		RectOffset__ctor_m732140021(L_34, ((int32_t)40), ((int32_t)40), 0, 0, /*hidden argument*/NULL);
		NullCheck(L_33);
		GUIStyle_set_padding_m3302456044(L_33, L_34, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_35 = __this->get_bodyStyle_5();
		GUIStyle_t3956901511 * L_36 = (GUIStyle_t3956901511 *)il2cpp_codegen_object_new(GUIStyle_t3956901511_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2912682974(L_36, L_35, /*hidden argument*/NULL);
		__this->set_headerStyle_6(L_36);
		GUIStyle_t3956901511 * L_37 = __this->get_headerStyle_6();
		NullCheck(L_37);
		GUIStyleState_t1397964415 * L_38 = GUIStyle_get_normal_m729441812(L_37, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_39 = __this->get_headerTexture_9();
		NullCheck(L_38);
		GUIStyleState_set_background_m369476077(L_38, L_39, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_40 = __this->get_headerStyle_6();
		int32_t L_41 = V_2;
		float L_42 = Screen_get_dpi_m495672463(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_40);
		GUIStyle_set_fontSize_m1566850023(L_40, (((int32_t)((int32_t)((float)((float)((float)il2cpp_codegen_multiply((float)(((float)((float)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)24), (int32_t)L_41))))), (float)L_42))/(float)(160.0f)))))), /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_43 = __this->get_bodyStyle_5();
		GUIStyle_t3956901511 * L_44 = (GUIStyle_t3956901511 *)il2cpp_codegen_object_new(GUIStyle_t3956901511_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2912682974(L_44, L_43, /*hidden argument*/NULL);
		__this->set_footerStyle_7(L_44);
		GUIStyle_t3956901511 * L_45 = __this->get_footerStyle_7();
		NullCheck(L_45);
		GUIStyleState_t1397964415 * L_46 = GUIStyle_get_normal_m729441812(L_45, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_47 = __this->get_footerTexture_10();
		NullCheck(L_46);
		GUIStyleState_set_background_m369476077(L_46, L_47, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_48 = __this->get_footerStyle_7();
		NullCheck(L_48);
		GUIStyleState_t1397964415 * L_49 = GUIStyle_get_normal_m729441812(L_48, /*hidden argument*/NULL);
		Color_t2555686324  L_50 = Color_get_white_m332174077(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_49);
		GUIStyleState_set_textColor_m1105876047(L_49, L_50, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_51 = __this->get_footerStyle_7();
		int32_t L_52 = V_2;
		float L_53 = Screen_get_dpi_m495672463(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_51);
		GUIStyle_set_fontSize_m1566850023(L_51, (((int32_t)((int32_t)((float)((float)((float)il2cpp_codegen_multiply((float)(((float)((float)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)28), (int32_t)L_52))))), (float)L_53))/(float)(160.0f)))))), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Texture2D DefaultInitializationErrorHandler::CreateSinglePixelTexture(UnityEngine.Color)
extern "C"  Texture2D_t3840446185 * DefaultInitializationErrorHandler_CreateSinglePixelTexture_m424000749 (DefaultInitializationErrorHandler_t3109936861 * __this, Color_t2555686324  ___color0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultInitializationErrorHandler_CreateSinglePixelTexture_m424000749_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Texture2D_t3840446185 * V_0 = NULL;
	{
		Texture2D_t3840446185 * L_0 = (Texture2D_t3840446185 *)il2cpp_codegen_object_new(Texture2D_t3840446185_il2cpp_TypeInfo_var);
		Texture2D__ctor_m2862217990(L_0, 1, 1, 5, (bool)0, /*hidden argument*/NULL);
		V_0 = L_0;
		Texture2D_t3840446185 * L_1 = V_0;
		Color_t2555686324  L_2 = ___color0;
		NullCheck(L_1);
		Texture2D_SetPixel_m2984741184(L_1, 0, 0, L_2, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_3 = V_0;
		NullCheck(L_3);
		Texture2D_Apply_m2271746283(L_3, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_4 = V_0;
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DefaultTrackableEventHandler::.ctor()
extern "C"  void DefaultTrackableEventHandler__ctor_m2856359002 (DefaultTrackableEventHandler_t1588957063 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DefaultTrackableEventHandler::Start()
extern "C"  void DefaultTrackableEventHandler_Start_m796446126 (DefaultTrackableEventHandler_t1588957063 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultTrackableEventHandler_Start_m796446126_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t1113559212 * L_0 = Component_GetComponent_TisTrackableBehaviour_t1113559212_m1736119408(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t1113559212_m1736119408_RuntimeMethod_var);
		__this->set_mTrackableBehaviour_2(L_0);
		TrackableBehaviour_t1113559212 * L_1 = __this->get_mTrackableBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t1113559212 * L_3 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m2462783619(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void DefaultTrackableEventHandler::OnDestroy()
extern "C"  void DefaultTrackableEventHandler_OnDestroy_m1080906236 (DefaultTrackableEventHandler_t1588957063 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultTrackableEventHandler_OnDestroy_m1080906236_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableBehaviour_t1113559212 * L_0 = __this->get_mTrackableBehaviour_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		TrackableBehaviour_t1113559212 * L_2 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_2);
		TrackableBehaviour_UnregisterTrackableEventHandler_m1201569614(L_2, __this, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void DefaultTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C"  void DefaultTrackableEventHandler_OnTrackableStateChanged_m77027111 (DefaultTrackableEventHandler_t1588957063 * __this, int32_t ___previousStatus0, int32_t ___newStatus1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultTrackableEventHandler_OnTrackableStateChanged_m77027111_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___newStatus1;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = ___newStatus1;
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_2 = ___newStatus1;
		if ((!(((uint32_t)L_2) == ((uint32_t)4))))
		{
			goto IL_003f;
		}
	}

IL_0015:
	{
		TrackableBehaviour_t1113559212 * L_3 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_3);
		String_t* L_4 = TrackableBehaviour_get_TrackableName_m3644057705(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral3820270571, L_4, _stringLiteral3073488411, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(7 /* System.Void DefaultTrackableEventHandler::OnTrackingFound() */, __this);
		goto IL_007c;
	}

IL_003f:
	{
		int32_t L_6 = ___previousStatus0;
		if ((!(((uint32_t)L_6) == ((uint32_t)3))))
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_7 = ___newStatus1;
		if (L_7)
		{
			goto IL_0076;
		}
	}
	{
		TrackableBehaviour_t1113559212 * L_8 = __this->get_mTrackableBehaviour_2();
		NullCheck(L_8);
		String_t* L_9 = TrackableBehaviour_get_TrackableName_m3644057705(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral3820270571, L_9, _stringLiteral3483481617, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(8 /* System.Void DefaultTrackableEventHandler::OnTrackingLost() */, __this);
		goto IL_007c;
	}

IL_0076:
	{
		VirtActionInvoker0::Invoke(8 /* System.Void DefaultTrackableEventHandler::OnTrackingLost() */, __this);
	}

IL_007c:
	{
		return;
	}
}
// System.Void DefaultTrackableEventHandler::OnTrackingFound()
extern "C"  void DefaultTrackableEventHandler_OnTrackingFound_m4202593607 (DefaultTrackableEventHandler_t1588957063 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultTrackableEventHandler_OnTrackingFound_m4202593607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t3210418286* V_0 = NULL;
	ColliderU5BU5D_t4234922487* V_1 = NULL;
	CanvasU5BU5D_t682926938* V_2 = NULL;
	Renderer_t2627027031 * V_3 = NULL;
	RendererU5BU5D_t3210418286* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t1773347010 * V_6 = NULL;
	ColliderU5BU5D_t4234922487* V_7 = NULL;
	int32_t V_8 = 0;
	Canvas_t3310196443 * V_9 = NULL;
	CanvasU5BU5D_t682926938* V_10 = NULL;
	int32_t V_11 = 0;
	{
		onScanController_t1472212342 * L_0 = Component_GetComponent_TisonScanController_t1472212342_m2197168753(__this, /*hidden argument*/Component_GetComponent_TisonScanController_t1472212342_m2197168753_RuntimeMethod_var);
		NullCheck(L_0);
		Behaviour_set_enabled_m20417929(L_0, (bool)1, /*hidden argument*/NULL);
		RendererU5BU5D_t3210418286* L_1 = Component_GetComponentsInChildren_TisRenderer_t2627027031_m2673895911(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t2627027031_m2673895911_RuntimeMethod_var);
		V_0 = L_1;
		ColliderU5BU5D_t4234922487* L_2 = Component_GetComponentsInChildren_TisCollider_t1773347010_m2667952426(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t1773347010_m2667952426_RuntimeMethod_var);
		V_1 = L_2;
		CanvasU5BU5D_t682926938* L_3 = Component_GetComponentsInChildren_TisCanvas_t3310196443_m1457345007(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCanvas_t3310196443_m1457345007_RuntimeMethod_var);
		V_2 = L_3;
		RendererU5BU5D_t3210418286* L_4 = V_0;
		V_4 = L_4;
		V_5 = 0;
		goto IL_0042;
	}

IL_002f:
	{
		RendererU5BU5D_t3210418286* L_5 = V_4;
		int32_t L_6 = V_5;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		Renderer_t2627027031 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_3 = L_8;
		Renderer_t2627027031 * L_9 = V_3;
		NullCheck(L_9);
		Renderer_set_enabled_m1727253150(L_9, (bool)1, /*hidden argument*/NULL);
		int32_t L_10 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0042:
	{
		int32_t L_11 = V_5;
		RendererU5BU5D_t3210418286* L_12 = V_4;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_12)->max_length)))))))
		{
			goto IL_002f;
		}
	}
	{
		ColliderU5BU5D_t4234922487* L_13 = V_1;
		V_7 = L_13;
		V_8 = 0;
		goto IL_006d;
	}

IL_0058:
	{
		ColliderU5BU5D_t4234922487* L_14 = V_7;
		int32_t L_15 = V_8;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Collider_t1773347010 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		V_6 = L_17;
		Collider_t1773347010 * L_18 = V_6;
		NullCheck(L_18);
		Collider_set_enabled_m1517463283(L_18, (bool)1, /*hidden argument*/NULL);
		int32_t L_19 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
	}

IL_006d:
	{
		int32_t L_20 = V_8;
		ColliderU5BU5D_t4234922487* L_21 = V_7;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_21)->max_length)))))))
		{
			goto IL_0058;
		}
	}
	{
		CanvasU5BU5D_t682926938* L_22 = V_2;
		V_10 = L_22;
		V_11 = 0;
		goto IL_0098;
	}

IL_0083:
	{
		CanvasU5BU5D_t682926938* L_23 = V_10;
		int32_t L_24 = V_11;
		NullCheck(L_23);
		int32_t L_25 = L_24;
		Canvas_t3310196443 * L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		V_9 = L_26;
		Canvas_t3310196443 * L_27 = V_9;
		NullCheck(L_27);
		Behaviour_set_enabled_m20417929(L_27, (bool)1, /*hidden argument*/NULL);
		int32_t L_28 = V_11;
		V_11 = ((int32_t)il2cpp_codegen_add((int32_t)L_28, (int32_t)1));
	}

IL_0098:
	{
		int32_t L_29 = V_11;
		CanvasU5BU5D_t682926938* L_30 = V_10;
		NullCheck(L_30);
		if ((((int32_t)L_29) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_30)->max_length)))))))
		{
			goto IL_0083;
		}
	}
	{
		return;
	}
}
// System.Void DefaultTrackableEventHandler::OnTrackingLost()
extern "C"  void DefaultTrackableEventHandler_OnTrackingLost_m424172778 (DefaultTrackableEventHandler_t1588957063 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DefaultTrackableEventHandler_OnTrackingLost_m424172778_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t3210418286* V_0 = NULL;
	ColliderU5BU5D_t4234922487* V_1 = NULL;
	CanvasU5BU5D_t682926938* V_2 = NULL;
	Renderer_t2627027031 * V_3 = NULL;
	RendererU5BU5D_t3210418286* V_4 = NULL;
	int32_t V_5 = 0;
	Collider_t1773347010 * V_6 = NULL;
	ColliderU5BU5D_t4234922487* V_7 = NULL;
	int32_t V_8 = 0;
	Canvas_t3310196443 * V_9 = NULL;
	CanvasU5BU5D_t682926938* V_10 = NULL;
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t3210418286* L_0 = Component_GetComponentsInChildren_TisRenderer_t2627027031_m2673895911(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t2627027031_m2673895911_RuntimeMethod_var);
		V_0 = L_0;
		ColliderU5BU5D_t4234922487* L_1 = Component_GetComponentsInChildren_TisCollider_t1773347010_m2667952426(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t1773347010_m2667952426_RuntimeMethod_var);
		V_1 = L_1;
		CanvasU5BU5D_t682926938* L_2 = Component_GetComponentsInChildren_TisCanvas_t3310196443_m1457345007(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCanvas_t3310196443_m1457345007_RuntimeMethod_var);
		V_2 = L_2;
		RendererU5BU5D_t3210418286* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t3210418286* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Renderer_t2627027031 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		Renderer_t2627027031 * L_8 = V_3;
		NullCheck(L_8);
		Renderer_set_enabled_m1727253150(L_8, (bool)0, /*hidden argument*/NULL);
		int32_t L_9 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0036:
	{
		int32_t L_10 = V_5;
		RendererU5BU5D_t3210418286* L_11 = V_4;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length)))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t4234922487* L_12 = V_1;
		V_7 = L_12;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t4234922487* L_13 = V_7;
		int32_t L_14 = V_8;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Collider_t1773347010 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		V_6 = L_16;
		Collider_t1773347010 * L_17 = V_6;
		NullCheck(L_17);
		Collider_set_enabled_m1517463283(L_17, (bool)0, /*hidden argument*/NULL);
		int32_t L_18 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_0061:
	{
		int32_t L_19 = V_8;
		ColliderU5BU5D_t4234922487* L_20 = V_7;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_20)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		CanvasU5BU5D_t682926938* L_21 = V_2;
		V_10 = L_21;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		CanvasU5BU5D_t682926938* L_22 = V_10;
		int32_t L_23 = V_11;
		NullCheck(L_22);
		int32_t L_24 = L_23;
		Canvas_t3310196443 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_9 = L_25;
		Canvas_t3310196443 * L_26 = V_9;
		NullCheck(L_26);
		Behaviour_set_enabled_m20417929(L_26, (bool)0, /*hidden argument*/NULL);
		int32_t L_27 = V_11;
		V_11 = ((int32_t)il2cpp_codegen_add((int32_t)L_27, (int32_t)1));
	}

IL_008c:
	{
		int32_t L_28 = V_11;
		CanvasU5BU5D_t682926938* L_29 = V_10;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_29)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void destoryOnAwake::.ctor()
extern "C"  void destoryOnAwake__ctor_m3566069992 (destoryOnAwake_t1274961527 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void destoryOnAwake::Awake()
extern "C"  void destoryOnAwake_Awake_m2265884017 (destoryOnAwake_t1274961527 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (destoryOnAwake_Awake_m2265884017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_on_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EnableMouthAnim::.ctor()
extern "C"  void EnableMouthAnim__ctor_m320856416 (EnableMouthAnim_t2443685136 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnableMouthAnim::Start()
extern "C"  void EnableMouthAnim_Start_m2930014436 (EnableMouthAnim_t2443685136 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnableMouthAnim_Start_m2930014436_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Animation_t3648466861 * L_0 = Component_GetComponentInChildren_TisAnimation_t3648466861_m4170904281(__this, /*hidden argument*/Component_GetComponentInChildren_TisAnimation_t3648466861_m4170904281_RuntimeMethod_var);
		__this->set_mouthAnim_2(L_0);
		return;
	}
}
// System.Void EnableMouthAnim::Update()
extern "C"  void EnableMouthAnim_Update_m2801338086 (EnableMouthAnim_t2443685136 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void findTutorialManager::.ctor()
extern "C"  void findTutorialManager__ctor_m3194460748 (findTutorialManager_t1037408675 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void findTutorialManager::Awake()
extern "C"  void findTutorialManager_Awake_m3194035460 (findTutorialManager_t1037408675 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FpsCount::.ctor()
extern "C"  void FpsCount__ctor_m3739041898 (FpsCount_t2935661334 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FpsCount__ctor_m3739041898_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Text_t1901882714_il2cpp_TypeInfo_var);
		Text__ctor_m1150387577(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FpsCount::Update()
extern "C"  void FpsCount_Update_m3597445316 (FpsCount_t2935661334 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FpsCount_Update_m3597445316_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = ((float)((float)(1.0f)/(float)L_0));
		RuntimeObject * L_2 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral3990009627, L_2, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, __this, L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FrameRateSettings::.ctor()
extern "C"  void FrameRateSettings__ctor_m863056381 (FrameRateSettings_t3598747490 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FrameRateSettings::Start()
extern "C"  void FrameRateSettings_Start_m580618101 (FrameRateSettings_t3598747490 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FrameRateSettings_Start_m580618101_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaARController_t1876945237_il2cpp_TypeInfo_var);
		VuforiaARController_t1876945237 * L_0 = VuforiaARController_get_Instance_m1100644214(NULL /*static, unused*/, /*hidden argument*/NULL);
		intptr_t L_1 = (intptr_t)FrameRateSettings_OnVuforiaStarted_m2069567078_RuntimeMethod_var;
		Action_t1264377477 * L_2 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_2, __this, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VuforiaARController_RegisterVuforiaStartedCallback_m3258152770(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FrameRateSettings::OnVuforiaStarted()
extern "C"  void FrameRateSettings_OnVuforiaStarted_m2069567078 (FrameRateSettings_t3598747490 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FrameRateSettings_OnVuforiaStarted_m2069567078_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRenderer_t3433045970_il2cpp_TypeInfo_var);
		VuforiaRenderer_t3433045970 * L_0 = VuforiaRenderer_get_Instance_m1013623488(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = VuforiaRenderer_GetRecommendedFps_m561576480(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = Application_get_targetFrameRate_m1077623221(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_4 = V_0;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1715369213(NULL /*static, unused*/, _stringLiteral3233180876, L_6, _stringLiteral4120677989, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		Application_set_targetFrameRate_m3682352535(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0037:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InitErrorHandler::.ctor()
extern "C"  void InitErrorHandler__ctor_m3192923495 (InitErrorHandler_t2159361531 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InitErrorHandler::Awake()
extern "C"  void InitErrorHandler_Awake_m3752167262 (InitErrorHandler_t2159361531 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InitErrorHandler_Awake_m3752167262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t1901882714 * L_0 = __this->get_errorText_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		Text_t1901882714 * L_2 = __this->get_errorText_2();
		NullCheck(L_2);
		CanvasU5BU5D_t682926938* L_3 = Component_GetComponentsInParent_TisCanvas_t3310196443_m1039379662(L_2, (bool)1, /*hidden argument*/Component_GetComponentsInParent_TisCanvas_t3310196443_m1039379662_RuntimeMethod_var);
		NullCheck(L_3);
		int32_t L_4 = 0;
		Canvas_t3310196443 * L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		__this->set_errorCanvas_3(L_5);
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntime_t1949122020_il2cpp_TypeInfo_var);
		VuforiaRuntime_t1949122020 * L_6 = VuforiaRuntime_get_Instance_m1058251676(NULL /*static, unused*/, /*hidden argument*/NULL);
		intptr_t L_7 = (intptr_t)InitErrorHandler_OnInitError_m2884619828_RuntimeMethod_var;
		Action_1_t3593217305 * L_8 = (Action_1_t3593217305 *)il2cpp_codegen_object_new(Action_1_t3593217305_il2cpp_TypeInfo_var);
		Action_1__ctor_m2713332384(L_8, __this, L_7, /*hidden argument*/Action_1__ctor_m2713332384_RuntimeMethod_var);
		NullCheck(L_6);
		VuforiaRuntime_RegisterVuforiaInitErrorCallback_m2728879505(L_6, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InitErrorHandler::OnInitError(Vuforia.VuforiaUnity/InitError)
extern "C"  void InitErrorHandler_OnInitError_m2884619828 (InitErrorHandler_t2159361531 * __this, int32_t ___error0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___error0;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___error0;
		InitErrorHandler_ShowErrorMessage_m1141628225(__this, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	{
		return;
	}
}
// System.Void InitErrorHandler::ShowErrorMessage(Vuforia.VuforiaUnity/InitError)
extern "C"  void InitErrorHandler_ShowErrorMessage_m1141628225 (InitErrorHandler_t2159361531 * __this, int32_t ___errorCode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InitErrorHandler_ShowErrorMessage_m1141628225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___errorCode0;
		switch (((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)((int32_t)10))))
		{
			case 0:
			{
				goto IL_0036;
			}
			case 1:
			{
				goto IL_01f1;
			}
			case 2:
			{
				goto IL_016e;
			}
			case 3:
			{
				goto IL_0144;
			}
			case 4:
			{
				goto IL_0159;
			}
			case 5:
			{
				goto IL_0060;
			}
			case 6:
			{
				goto IL_004b;
			}
			case 7:
			{
				goto IL_0274;
			}
			case 8:
			{
				goto IL_0289;
			}
			case 9:
			{
				goto IL_029e;
			}
		}
	}
	{
		goto IL_02b3;
	}

IL_0036:
	{
		Text_t1901882714 * L_1 = __this->get_errorText_2();
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, _stringLiteral3279329212);
		goto IL_02b3;
	}

IL_004b:
	{
		Text_t1901882714 * L_2 = __this->get_errorText_2();
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, _stringLiteral3258121531);
		goto IL_02b3;
	}

IL_0060:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaConfiguration_t1763229349_il2cpp_TypeInfo_var);
		VuforiaConfiguration_t1763229349 * L_3 = VuforiaConfiguration_get_Instance_m3335903280(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		GenericVuforiaConfiguration_t3697830469 * L_4 = VuforiaConfiguration_get_Vuforia_m1588208597(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = GenericVuforiaConfiguration_get_LicenseKey_m2270076687(L_4, /*hidden argument*/NULL);
		__this->set_key_4(L_5);
		String_t* L_6 = __this->get_key_4();
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m3847582255(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) <= ((int32_t)((int32_t)10))))
		{
			goto IL_00f5;
		}
	}
	{
		Text_t1901882714 * L_8 = __this->get_errorText_2();
		ObjectU5BU5D_t2843939325* L_9 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral3820000162);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral3820000162);
		ObjectU5BU5D_t2843939325* L_10 = L_9;
		String_t* L_11 = __this->get_key_4();
		NullCheck(L_11);
		int32_t L_12 = String_get_Length_m3847582255(L_11, /*hidden argument*/NULL);
		int32_t L_13 = L_12;
		RuntimeObject * L_14 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_14);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_14);
		ObjectU5BU5D_t2843939325* L_15 = L_10;
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral3797279721);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral3797279721);
		ObjectU5BU5D_t2843939325* L_16 = L_15;
		String_t* L_17 = __this->get_key_4();
		NullCheck(L_17);
		String_t* L_18 = String_Substring_m1610150815(L_17, 0, 5, /*hidden argument*/NULL);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_18);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_18);
		ObjectU5BU5D_t2843939325* L_19 = L_16;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral1108443480);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral1108443480);
		ObjectU5BU5D_t2843939325* L_20 = L_19;
		String_t* L_21 = __this->get_key_4();
		NullCheck(L_21);
		String_t* L_22 = String_Substring_m1610150815(L_21, 5, 5, /*hidden argument*/NULL);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_22);
		ObjectU5BU5D_t2843939325* L_23 = L_20;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral2072975055);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)_stringLiteral2072975055);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = String_Concat_m2971454694(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, L_24);
		goto IL_013f;
	}

IL_00f5:
	{
		Text_t1901882714 * L_25 = __this->get_errorText_2();
		ObjectU5BU5D_t2843939325* L_26 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral3820000162);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral3820000162);
		ObjectU5BU5D_t2843939325* L_27 = L_26;
		String_t* L_28 = __this->get_key_4();
		NullCheck(L_28);
		int32_t L_29 = String_get_Length_m3847582255(L_28, /*hidden argument*/NULL);
		int32_t L_30 = L_29;
		RuntimeObject * L_31 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_30);
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, L_31);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_31);
		ObjectU5BU5D_t2843939325* L_32 = L_27;
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, _stringLiteral1498400317);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral1498400317);
		ObjectU5BU5D_t2843939325* L_33 = L_32;
		String_t* L_34 = __this->get_key_4();
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, L_34);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_34);
		ObjectU5BU5D_t2843939325* L_35 = L_33;
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, _stringLiteral2072975055);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral2072975055);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_36 = String_Concat_m2971454694(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		NullCheck(L_25);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_25, L_36);
	}

IL_013f:
	{
		goto IL_02b3;
	}

IL_0144:
	{
		Text_t1901882714 * L_37 = __this->get_errorText_2();
		NullCheck(L_37);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_37, _stringLiteral2959890895);
		goto IL_02b3;
	}

IL_0159:
	{
		Text_t1901882714 * L_38 = __this->get_errorText_2();
		NullCheck(L_38);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_38, _stringLiteral2293294349);
		goto IL_02b3;
	}

IL_016e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaConfiguration_t1763229349_il2cpp_TypeInfo_var);
		VuforiaConfiguration_t1763229349 * L_39 = VuforiaConfiguration_get_Instance_m3335903280(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_39);
		GenericVuforiaConfiguration_t3697830469 * L_40 = VuforiaConfiguration_get_Vuforia_m1588208597(L_39, /*hidden argument*/NULL);
		NullCheck(L_40);
		String_t* L_41 = GenericVuforiaConfiguration_get_LicenseKey_m2270076687(L_40, /*hidden argument*/NULL);
		__this->set_key_4(L_41);
		Text_t1901882714 * L_42 = __this->get_errorText_2();
		ObjectU5BU5D_t2843939325* L_43 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_43);
		ArrayElementTypeCheck (L_43, _stringLiteral2177475950);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral2177475950);
		ObjectU5BU5D_t2843939325* L_44 = L_43;
		String_t* L_45 = __this->get_key_4();
		NullCheck(L_45);
		int32_t L_46 = String_get_Length_m3847582255(L_45, /*hidden argument*/NULL);
		int32_t L_47 = L_46;
		RuntimeObject * L_48 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_47);
		NullCheck(L_44);
		ArrayElementTypeCheck (L_44, L_48);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_48);
		ObjectU5BU5D_t2843939325* L_49 = L_44;
		NullCheck(L_49);
		ArrayElementTypeCheck (L_49, _stringLiteral3797279721);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral3797279721);
		ObjectU5BU5D_t2843939325* L_50 = L_49;
		String_t* L_51 = __this->get_key_4();
		NullCheck(L_51);
		String_t* L_52 = String_Substring_m1610150815(L_51, 0, 5, /*hidden argument*/NULL);
		NullCheck(L_50);
		ArrayElementTypeCheck (L_50, L_52);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_52);
		ObjectU5BU5D_t2843939325* L_53 = L_50;
		NullCheck(L_53);
		ArrayElementTypeCheck (L_53, _stringLiteral1108443480);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral1108443480);
		ObjectU5BU5D_t2843939325* L_54 = L_53;
		String_t* L_55 = __this->get_key_4();
		NullCheck(L_55);
		String_t* L_56 = String_Substring_m1610150815(L_55, 5, 5, /*hidden argument*/NULL);
		NullCheck(L_54);
		ArrayElementTypeCheck (L_54, L_56);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_56);
		ObjectU5BU5D_t2843939325* L_57 = L_54;
		NullCheck(L_57);
		ArrayElementTypeCheck (L_57, _stringLiteral2072975055);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)_stringLiteral2072975055);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_58 = String_Concat_m2971454694(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		NullCheck(L_42);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_42, L_58);
		goto IL_02b3;
	}

IL_01f1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaConfiguration_t1763229349_il2cpp_TypeInfo_var);
		VuforiaConfiguration_t1763229349 * L_59 = VuforiaConfiguration_get_Instance_m3335903280(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_59);
		GenericVuforiaConfiguration_t3697830469 * L_60 = VuforiaConfiguration_get_Vuforia_m1588208597(L_59, /*hidden argument*/NULL);
		NullCheck(L_60);
		String_t* L_61 = GenericVuforiaConfiguration_get_LicenseKey_m2270076687(L_60, /*hidden argument*/NULL);
		__this->set_key_4(L_61);
		Text_t1901882714 * L_62 = __this->get_errorText_2();
		ObjectU5BU5D_t2843939325* L_63 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_63);
		ArrayElementTypeCheck (L_63, _stringLiteral3262298861);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral3262298861);
		ObjectU5BU5D_t2843939325* L_64 = L_63;
		String_t* L_65 = __this->get_key_4();
		NullCheck(L_65);
		int32_t L_66 = String_get_Length_m3847582255(L_65, /*hidden argument*/NULL);
		int32_t L_67 = L_66;
		RuntimeObject * L_68 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_67);
		NullCheck(L_64);
		ArrayElementTypeCheck (L_64, L_68);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_68);
		ObjectU5BU5D_t2843939325* L_69 = L_64;
		NullCheck(L_69);
		ArrayElementTypeCheck (L_69, _stringLiteral3797279721);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral3797279721);
		ObjectU5BU5D_t2843939325* L_70 = L_69;
		String_t* L_71 = __this->get_key_4();
		NullCheck(L_71);
		String_t* L_72 = String_Substring_m1610150815(L_71, 0, 5, /*hidden argument*/NULL);
		NullCheck(L_70);
		ArrayElementTypeCheck (L_70, L_72);
		(L_70)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_72);
		ObjectU5BU5D_t2843939325* L_73 = L_70;
		NullCheck(L_73);
		ArrayElementTypeCheck (L_73, _stringLiteral1108443480);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteral1108443480);
		ObjectU5BU5D_t2843939325* L_74 = L_73;
		String_t* L_75 = __this->get_key_4();
		NullCheck(L_75);
		String_t* L_76 = String_Substring_m1610150815(L_75, 5, 5, /*hidden argument*/NULL);
		NullCheck(L_74);
		ArrayElementTypeCheck (L_74, L_76);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_76);
		ObjectU5BU5D_t2843939325* L_77 = L_74;
		NullCheck(L_77);
		ArrayElementTypeCheck (L_77, _stringLiteral3078096941);
		(L_77)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)_stringLiteral3078096941);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_78 = String_Concat_m2971454694(NULL /*static, unused*/, L_77, /*hidden argument*/NULL);
		NullCheck(L_62);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_62, L_78);
		goto IL_02b3;
	}

IL_0274:
	{
		Text_t1901882714 * L_79 = __this->get_errorText_2();
		NullCheck(L_79);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_79, _stringLiteral3034338515);
		goto IL_02b3;
	}

IL_0289:
	{
		Text_t1901882714 * L_80 = __this->get_errorText_2();
		NullCheck(L_80);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_80, _stringLiteral3122929577);
		goto IL_02b3;
	}

IL_029e:
	{
		Text_t1901882714 * L_81 = __this->get_errorText_2();
		NullCheck(L_81);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_81, _stringLiteral3567432369);
		goto IL_02b3;
	}

IL_02b3:
	{
		int32_t L_82 = ___errorCode0;
		int32_t L_83 = L_82;
		RuntimeObject * L_84 = Box(InitError_t3420749710_il2cpp_TypeInfo_var, &L_83);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_84, /*hidden argument*/NULL);
		Text_t1901882714 * L_85 = __this->get_errorText_2();
		NullCheck(L_85);
		String_t* L_86 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_85);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_86, /*hidden argument*/NULL);
		Canvas_t3310196443 * L_87 = __this->get_errorCanvas_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_88 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_87, /*hidden argument*/NULL);
		if (!L_88)
		{
			goto IL_0315;
		}
	}
	{
		Canvas_t3310196443 * L_89 = __this->get_errorCanvas_3();
		NullCheck(L_89);
		Transform_t3600365921 * L_90 = Component_get_transform_m3162698980(L_89, /*hidden argument*/NULL);
		NullCheck(L_90);
		Transform_t3600365921 * L_91 = Transform_get_parent_m835071599(L_90, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_92 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_91);
		Transform_set_position_m3387557959(L_91, L_92, /*hidden argument*/NULL);
		Canvas_t3310196443 * L_93 = __this->get_errorCanvas_3();
		NullCheck(L_93);
		GameObject_t1113636619 * L_94 = Component_get_gameObject_m442555142(L_93, /*hidden argument*/NULL);
		NullCheck(L_94);
		GameObject_SetActive_m796801857(L_94, (bool)1, /*hidden argument*/NULL);
		Canvas_t3310196443 * L_95 = __this->get_errorCanvas_3();
		NullCheck(L_95);
		Behaviour_set_enabled_m20417929(L_95, (bool)1, /*hidden argument*/NULL);
	}

IL_0315:
	{
		return;
	}
}
// System.Void InitErrorHandler::OnErrorDialogClose()
extern "C"  void InitErrorHandler_OnErrorDialogClose_m1431281891 (InitErrorHandler_t2159361531 * __this, const RuntimeMethod* method)
{
	{
		Application_Quit_m470877999(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LoadingScreen::.ctor()
extern "C"  void LoadingScreen__ctor_m3828008876 (LoadingScreen_t2154736699 * __this, const RuntimeMethod* method)
{
	{
		__this->set_mChangeLevel_2((bool)1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LoadingScreen::Start()
extern "C"  void LoadingScreen_Start_m3016827352 (LoadingScreen_t2154736699 * __this, const RuntimeMethod* method)
{
	{
		RawImage_t3182918964 * L_0 = LoadingScreen_FindSpinnerImage_m2988402308(__this, /*hidden argument*/NULL);
		__this->set_mUISpinner_3(L_0);
		Application_set_backgroundLoadingPriority_m1116453950(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->set_mChangeLevel_2((bool)1);
		return;
	}
}
// System.Void LoadingScreen::Update()
extern "C"  void LoadingScreen_Update_m69320718 (LoadingScreen_t2154736699 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LoadingScreen_Update_m69320718_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RawImage_t3182918964 * L_0 = __this->get_mUISpinner_3();
		NullCheck(L_0);
		RectTransform_t3704657025 * L_1 = Graphic_get_rectTransform_m1167152468(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Vector3_get_forward_m3100859705(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_Rotate_m1749346957(L_1, L_2, ((float)il2cpp_codegen_multiply((float)(90.0f), (float)L_3)), /*hidden argument*/NULL);
		bool L_4 = __this->get_mChangeLevel_2();
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		LoadingScreen_LoadNextSceneAsync_m3801488710(__this, /*hidden argument*/NULL);
		__this->set_mChangeLevel_2((bool)0);
	}

IL_0038:
	{
		return;
	}
}
// System.Void LoadingScreen::LoadNextSceneAsync()
extern "C"  void LoadingScreen_LoadNextSceneAsync_m3801488710 (LoadingScreen_t2154736699 * __this, const RuntimeMethod* method)
{
	Scene_t2348375561  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Scene_t2348375561  L_0 = SceneManager_GetActiveScene_m1825203488(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Scene_get_buildIndex_m270272723((Scene_t2348375561 *)(&V_0), /*hidden argument*/NULL);
		SceneManager_LoadSceneAsync_m3240398665(NULL /*static, unused*/, ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.RawImage LoadingScreen::FindSpinnerImage()
extern "C"  RawImage_t3182918964 * LoadingScreen_FindSpinnerImage_m2988402308 (LoadingScreen_t2154736699 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LoadingScreen_FindSpinnerImage_m2988402308_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RawImageU5BU5D_t1576731517* V_0 = NULL;
	RawImage_t3182918964 * V_1 = NULL;
	RawImageU5BU5D_t1576731517* V_2 = NULL;
	int32_t V_3 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		RawImageU5BU5D_t1576731517* L_0 = Object_FindObjectsOfType_TisRawImage_t3182918964_m2400832436(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectsOfType_TisRawImage_t3182918964_m2400832436_RuntimeMethod_var);
		V_0 = L_0;
		RawImageU5BU5D_t1576731517* L_1 = V_0;
		V_2 = L_1;
		V_3 = 0;
		goto IL_002e;
	}

IL_000f:
	{
		RawImageU5BU5D_t1576731517* L_2 = V_2;
		int32_t L_3 = V_3;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		RawImage_t3182918964 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_1 = L_5;
		RawImage_t3182918964 * L_6 = V_1;
		NullCheck(L_6);
		String_t* L_7 = Object_get_name_m4211327027(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = String_Contains_m1147431944(L_7, _stringLiteral1395798546, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002a;
		}
	}
	{
		RawImage_t3182918964 * L_9 = V_1;
		return L_9;
	}

IL_002a:
	{
		int32_t L_10 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_002e:
	{
		int32_t L_11 = V_3;
		RawImageU5BU5D_t1576731517* L_12 = V_2;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_12)->max_length)))))))
		{
			goto IL_000f;
		}
	}
	{
		return (RawImage_t3182918964 *)NULL;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Math3d::.ctor()
extern "C"  void Math3d__ctor_m4211531773 (Math3d_t1427253148 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Math3d::Init()
extern "C"  void Math3d_Init_m2456345004 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_Init_m2456345004_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m2093116449(L_0, _stringLiteral2613755077, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = GameObject_get_transform_m1369836730(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Math3d_t1427253148_il2cpp_TypeInfo_var);
		((Math3d_t1427253148_StaticFields*)il2cpp_codegen_static_fields_for(Math3d_t1427253148_il2cpp_TypeInfo_var))->set_tempChild_2(L_1);
		GameObject_t1113636619 * L_2 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m2093116449(L_2, _stringLiteral2078730823, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = GameObject_get_transform_m1369836730(L_2, /*hidden argument*/NULL);
		((Math3d_t1427253148_StaticFields*)il2cpp_codegen_static_fields_for(Math3d_t1427253148_il2cpp_TypeInfo_var))->set_tempParent_3(L_3);
		Transform_t3600365921 * L_4 = ((Math3d_t1427253148_StaticFields*)il2cpp_codegen_static_fields_for(Math3d_t1427253148_il2cpp_TypeInfo_var))->get_tempChild_2();
		NullCheck(L_4);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Object_set_hideFlags_m1648752846(L_5, ((int32_t)61), /*hidden argument*/NULL);
		Transform_t3600365921 * L_6 = ((Math3d_t1427253148_StaticFields*)il2cpp_codegen_static_fields_for(Math3d_t1427253148_il2cpp_TypeInfo_var))->get_tempChild_2();
		NullCheck(L_6);
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m166252750(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Transform_t3600365921 * L_8 = ((Math3d_t1427253148_StaticFields*)il2cpp_codegen_static_fields_for(Math3d_t1427253148_il2cpp_TypeInfo_var))->get_tempParent_3();
		NullCheck(L_8);
		GameObject_t1113636619 * L_9 = Component_get_gameObject_m442555142(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Object_set_hideFlags_m1648752846(L_9, ((int32_t)61), /*hidden argument*/NULL);
		Transform_t3600365921 * L_10 = ((Math3d_t1427253148_StaticFields*)il2cpp_codegen_static_fields_for(Math3d_t1427253148_il2cpp_TypeInfo_var))->get_tempParent_3();
		NullCheck(L_10);
		GameObject_t1113636619 * L_11 = Component_get_gameObject_m442555142(L_10, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m166252750(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		Transform_t3600365921 * L_12 = ((Math3d_t1427253148_StaticFields*)il2cpp_codegen_static_fields_for(Math3d_t1427253148_il2cpp_TypeInfo_var))->get_tempChild_2();
		Transform_t3600365921 * L_13 = ((Math3d_t1427253148_StaticFields*)il2cpp_codegen_static_fields_for(Math3d_t1427253148_il2cpp_TypeInfo_var))->get_tempParent_3();
		NullCheck(L_12);
		Transform_set_parent_m786917804(L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 Math3d::AddVectorLength(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Math3d_AddVectorLength_m3019376001 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___vector0, float ___size1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_AddVectorLength_m3019376001_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector3_t3722313464  L_0 = ___vector0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_1 = Vector3_Magnitude_m3447562852(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = V_0;
		float L_3 = ___size1;
		V_0 = ((float)il2cpp_codegen_add((float)L_2, (float)L_3));
		Vector3_t3722313464  L_4 = ___vector0;
		Vector3_t3722313464  L_5 = Vector3_Normalize_m2941713846(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector3_t3722313464  L_6 = V_1;
		float L_7 = V_0;
		float L_8 = V_0;
		float L_9 = V_0;
		Vector3_t3722313464  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m3353183577((&L_10), L_7, L_8, L_9, /*hidden argument*/NULL);
		Vector3_t3722313464  L_11 = Vector3_Scale_m4006687392(NULL /*static, unused*/, L_6, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// UnityEngine.Vector3 Math3d::SetVectorLength(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Math3d_SetVectorLength_m3782886289 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___vector0, float ___size1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_SetVectorLength_m3782886289_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = ___vector0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Vector3_Normalize_m2941713846(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector3_t3722313464  L_2 = V_0;
		float L_3 = ___size1;
		Vector3_t3722313464  L_4 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Vector3_t3722313464  L_5 = L_4;
		V_0 = L_5;
		return L_5;
	}
}
// UnityEngine.Quaternion Math3d::SubtractRotation(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t2301928331  Math3d_SubtractRotation_m1474400458 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  ___B0, Quaternion_t2301928331  ___A1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_SubtractRotation_m1474400458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_t2301928331  L_0 = ___A1;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_1 = Quaternion_Inverse_m1311579081(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_2 = ___B0;
		Quaternion_t2301928331  L_3 = Quaternion_op_Multiply_m1294064023(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Quaternion_t2301928331  L_4 = V_0;
		return L_4;
	}
}
// System.Boolean Math3d::PlanePlaneIntersection(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Math3d_PlanePlaneIntersection_m1345259878 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464 * ___linePoint0, Vector3_t3722313464 * ___lineVec1, Vector3_t3722313464  ___plane1Normal2, Vector3_t3722313464  ___plane1Position3, Vector3_t3722313464  ___plane2Normal4, Vector3_t3722313464  ___plane2Position5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_PlanePlaneIntersection_m1345259878_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	{
		Vector3_t3722313464 * L_0 = ___linePoint0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_0 = L_1;
		Vector3_t3722313464 * L_2 = ___lineVec1;
		Vector3_t3722313464  L_3 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_2 = L_3;
		Vector3_t3722313464 * L_4 = ___lineVec1;
		Vector3_t3722313464  L_5 = ___plane1Normal2;
		Vector3_t3722313464  L_6 = ___plane2Normal4;
		Vector3_t3722313464  L_7 = Vector3_Cross_m418170344(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_4 = L_7;
		Vector3_t3722313464  L_8 = ___plane2Normal4;
		Vector3_t3722313464 * L_9 = ___lineVec1;
		Vector3_t3722313464  L_10 = Vector3_Cross_m418170344(NULL /*static, unused*/, L_8, (*(Vector3_t3722313464 *)L_9), /*hidden argument*/NULL);
		V_0 = L_10;
		Vector3_t3722313464  L_11 = ___plane1Normal2;
		Vector3_t3722313464  L_12 = V_0;
		float L_13 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		float L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_15 = fabsf(L_14);
		if ((!(((float)L_15) > ((float)(0.006f)))))
		{
			goto IL_0073;
		}
	}
	{
		Vector3_t3722313464  L_16 = ___plane1Position3;
		Vector3_t3722313464  L_17 = ___plane2Position5;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_18 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		Vector3_t3722313464  L_19 = ___plane1Normal2;
		Vector3_t3722313464  L_20 = V_2;
		float L_21 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		float L_22 = V_1;
		V_3 = ((float)((float)L_21/(float)L_22));
		Vector3_t3722313464 * L_23 = ___linePoint0;
		Vector3_t3722313464  L_24 = ___plane2Position5;
		float L_25 = V_3;
		Vector3_t3722313464  L_26 = V_0;
		Vector3_t3722313464  L_27 = Vector3_op_Multiply_m2104357790(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		Vector3_t3722313464  L_28 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_24, L_27, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_23 = L_28;
		return (bool)1;
	}

IL_0073:
	{
		return (bool)0;
	}
}
// System.Boolean Math3d::LinePlaneIntersection(UnityEngine.Vector3&,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Math3d_LinePlaneIntersection_m869610868 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464 * ___intersection0, Vector3_t3722313464  ___linePoint1, Vector3_t3722313464  ___lineVec2, Vector3_t3722313464  ___planeNormal3, Vector3_t3722313464  ___planePoint4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_LinePlaneIntersection_m869610868_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Vector3_t3722313464 * L_0 = ___intersection0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_0 = L_1;
		Vector3_t3722313464  L_2 = ___planePoint4;
		Vector3_t3722313464  L_3 = ___linePoint1;
		Vector3_t3722313464  L_4 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Vector3_t3722313464  L_5 = ___planeNormal3;
		float L_6 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Vector3_t3722313464  L_7 = ___lineVec2;
		Vector3_t3722313464  L_8 = ___planeNormal3;
		float L_9 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = V_2;
		if ((((float)L_10) == ((float)(0.0f))))
		{
			goto IL_0048;
		}
	}
	{
		float L_11 = V_1;
		float L_12 = V_2;
		V_0 = ((float)((float)L_11/(float)L_12));
		Vector3_t3722313464  L_13 = ___lineVec2;
		float L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Math3d_t1427253148_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_15 = Math3d_SetVectorLength_m3782886289(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		Vector3_t3722313464 * L_16 = ___intersection0;
		Vector3_t3722313464  L_17 = ___linePoint1;
		Vector3_t3722313464  L_18 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_19 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_16 = L_19;
		return (bool)1;
	}

IL_0048:
	{
		return (bool)0;
	}
}
// System.Boolean Math3d::LineLineIntersection(UnityEngine.Vector3&,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Math3d_LineLineIntersection_m2101611845 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464 * ___intersection0, Vector3_t3722313464  ___linePoint11, Vector3_t3722313464  ___lineVec12, Vector3_t3722313464  ___linePoint23, Vector3_t3722313464  ___lineVec24, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_LineLineIntersection_m2101611845_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Vector3_t3722313464 * L_0 = ___intersection0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_0 = L_1;
		Vector3_t3722313464  L_2 = ___linePoint23;
		Vector3_t3722313464  L_3 = ___linePoint11;
		Vector3_t3722313464  L_4 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Vector3_t3722313464  L_5 = ___lineVec12;
		Vector3_t3722313464  L_6 = ___lineVec24;
		Vector3_t3722313464  L_7 = Vector3_Cross_m418170344(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Vector3_t3722313464  L_8 = V_0;
		Vector3_t3722313464  L_9 = ___lineVec24;
		Vector3_t3722313464  L_10 = Vector3_Cross_m418170344(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		Vector3_t3722313464  L_11 = V_0;
		Vector3_t3722313464  L_12 = V_1;
		float L_13 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		float L_14 = V_3;
		if ((((float)L_14) >= ((float)(1.0E-05f))))
		{
			goto IL_0043;
		}
	}
	{
		float L_15 = V_3;
		if ((!(((float)L_15) <= ((float)(-1.0E-05f)))))
		{
			goto IL_0045;
		}
	}

IL_0043:
	{
		return (bool)0;
	}

IL_0045:
	{
		Vector3_t3722313464  L_16 = V_2;
		Vector3_t3722313464  L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_18 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		float L_19 = Vector3_get_sqrMagnitude_m1474274574((Vector3_t3722313464 *)(&V_1), /*hidden argument*/NULL);
		V_4 = ((float)((float)L_18/(float)L_19));
		float L_20 = V_4;
		if ((!(((float)L_20) >= ((float)(0.0f)))))
		{
			goto IL_0084;
		}
	}
	{
		float L_21 = V_4;
		if ((!(((float)L_21) <= ((float)(1.0f)))))
		{
			goto IL_0084;
		}
	}
	{
		Vector3_t3722313464 * L_22 = ___intersection0;
		Vector3_t3722313464  L_23 = ___linePoint11;
		Vector3_t3722313464  L_24 = ___lineVec12;
		float L_25 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_26 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		Vector3_t3722313464  L_27 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_23, L_26, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_22 = L_27;
		return (bool)1;
	}

IL_0084:
	{
		return (bool)0;
	}
}
// System.Boolean Math3d::ClosestPointsOnTwoLines(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Math3d_ClosestPointsOnTwoLines_m2440915867 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464 * ___closestPointLine10, Vector3_t3722313464 * ___closestPointLine21, Vector3_t3722313464  ___linePoint12, Vector3_t3722313464  ___lineVec13, Vector3_t3722313464  ___linePoint24, Vector3_t3722313464  ___lineVec25, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_ClosestPointsOnTwoLines_m2440915867_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	{
		Vector3_t3722313464 * L_0 = ___closestPointLine10;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_0 = L_1;
		Vector3_t3722313464 * L_2 = ___closestPointLine21;
		Vector3_t3722313464  L_3 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_2 = L_3;
		Vector3_t3722313464  L_4 = ___lineVec13;
		Vector3_t3722313464  L_5 = ___lineVec13;
		float L_6 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector3_t3722313464  L_7 = ___lineVec13;
		Vector3_t3722313464  L_8 = ___lineVec25;
		float L_9 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		Vector3_t3722313464  L_10 = ___lineVec25;
		Vector3_t3722313464  L_11 = ___lineVec25;
		float L_12 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		float L_13 = V_0;
		float L_14 = V_2;
		float L_15 = V_1;
		float L_16 = V_1;
		V_3 = ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_13, (float)L_14)), (float)((float)il2cpp_codegen_multiply((float)L_15, (float)L_16))));
		float L_17 = V_3;
		if ((((float)L_17) == ((float)(0.0f))))
		{
			goto IL_00a9;
		}
	}
	{
		Vector3_t3722313464  L_18 = ___linePoint12;
		Vector3_t3722313464  L_19 = ___linePoint24;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_20 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_4 = L_20;
		Vector3_t3722313464  L_21 = ___lineVec13;
		Vector3_t3722313464  L_22 = V_4;
		float L_23 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		V_5 = L_23;
		Vector3_t3722313464  L_24 = ___lineVec25;
		Vector3_t3722313464  L_25 = V_4;
		float L_26 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		V_6 = L_26;
		float L_27 = V_1;
		float L_28 = V_6;
		float L_29 = V_5;
		float L_30 = V_2;
		float L_31 = V_3;
		V_7 = ((float)((float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_27, (float)L_28)), (float)((float)il2cpp_codegen_multiply((float)L_29, (float)L_30))))/(float)L_31));
		float L_32 = V_0;
		float L_33 = V_6;
		float L_34 = V_5;
		float L_35 = V_1;
		float L_36 = V_3;
		V_8 = ((float)((float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_32, (float)L_33)), (float)((float)il2cpp_codegen_multiply((float)L_34, (float)L_35))))/(float)L_36));
		Vector3_t3722313464 * L_37 = ___closestPointLine10;
		Vector3_t3722313464  L_38 = ___linePoint12;
		Vector3_t3722313464  L_39 = ___lineVec13;
		float L_40 = V_7;
		Vector3_t3722313464  L_41 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		Vector3_t3722313464  L_42 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_38, L_41, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_37 = L_42;
		Vector3_t3722313464 * L_43 = ___closestPointLine21;
		Vector3_t3722313464  L_44 = ___linePoint24;
		Vector3_t3722313464  L_45 = ___lineVec25;
		float L_46 = V_8;
		Vector3_t3722313464  L_47 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
		Vector3_t3722313464  L_48 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_44, L_47, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_43 = L_48;
		return (bool)1;
	}

IL_00a9:
	{
		return (bool)0;
	}
}
// UnityEngine.Quaternion Math3d::InverseSignQuaternion(UnityEngine.Quaternion)
extern "C"  Quaternion_t2301928331  Math3d_InverseSignQuaternion_m7038823 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  ___q0, const RuntimeMethod* method)
{
	{
		float L_0 = (&___q0)->get_x_0();
		float L_1 = (&___q0)->get_y_1();
		float L_2 = (&___q0)->get_z_2();
		float L_3 = (&___q0)->get_w_3();
		Quaternion_t2301928331  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Quaternion__ctor_m435141806((&L_4), ((-L_0)), ((-L_1)), ((-L_2)), ((-L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean Math3d::AreQuaternionsClose(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Math3d_AreQuaternionsClose_m3116464126 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  ___q10, Quaternion_t2301928331  ___q21, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_AreQuaternionsClose_m3116464126_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		Quaternion_t2301928331  L_0 = ___q10;
		Quaternion_t2301928331  L_1 = ___q21;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		float L_2 = Quaternion_Dot_m1392781996(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		if ((!(((float)L_3) < ((float)(0.0f)))))
		{
			goto IL_0015;
		}
	}
	{
		return (bool)0;
	}

IL_0015:
	{
		return (bool)1;
	}
}
// UnityEngine.Vector3 Math3d::ProjectPointOnLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Math3d_ProjectPointOnLine_m4192705973 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___linePoint0, Vector3_t3722313464  ___lineVec1, Vector3_t3722313464  ___point2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_ProjectPointOnLine_m4192705973_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		Vector3_t3722313464  L_0 = ___point2;
		Vector3_t3722313464  L_1 = ___linePoint0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t3722313464  L_3 = V_0;
		Vector3_t3722313464  L_4 = ___lineVec1;
		float L_5 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector3_t3722313464  L_6 = ___linePoint0;
		Vector3_t3722313464  L_7 = ___lineVec1;
		float L_8 = V_1;
		Vector3_t3722313464  L_9 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		Vector3_t3722313464  L_10 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_6, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// UnityEngine.Vector3 Math3d::ProjectPointOnLineSegment(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Math3d_ProjectPointOnLineSegment_m2179139403 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___linePoint10, Vector3_t3722313464  ___linePoint21, Vector3_t3722313464  ___point2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_ProjectPointOnLineSegment_m2179139403_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		Vector3_t3722313464  L_0 = ___linePoint21;
		Vector3_t3722313464  L_1 = ___linePoint10;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t3722313464  L_3 = ___linePoint10;
		Vector3_t3722313464  L_4 = Vector3_get_normalized_m2454957984((Vector3_t3722313464 *)(&V_0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_5 = ___point2;
		IL2CPP_RUNTIME_CLASS_INIT(Math3d_t1427253148_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_6 = Math3d_ProjectPointOnLine_m4192705973(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Vector3_t3722313464  L_7 = ___linePoint10;
		Vector3_t3722313464  L_8 = ___linePoint21;
		Vector3_t3722313464  L_9 = V_1;
		int32_t L_10 = Math3d_PointOnWhichSideOfLineSegment_m3215589412(NULL /*static, unused*/, L_7, L_8, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		int32_t L_11 = V_2;
		if (L_11)
		{
			goto IL_0028;
		}
	}
	{
		Vector3_t3722313464  L_12 = V_1;
		return L_12;
	}

IL_0028:
	{
		int32_t L_13 = V_2;
		if ((!(((uint32_t)L_13) == ((uint32_t)1))))
		{
			goto IL_0031;
		}
	}
	{
		Vector3_t3722313464  L_14 = ___linePoint10;
		return L_14;
	}

IL_0031:
	{
		int32_t L_15 = V_2;
		if ((!(((uint32_t)L_15) == ((uint32_t)2))))
		{
			goto IL_003a;
		}
	}
	{
		Vector3_t3722313464  L_16 = ___linePoint21;
		return L_16;
	}

IL_003a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_17 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_17;
	}
}
// UnityEngine.Vector3 Math3d::ProjectPointOnPlane(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Math3d_ProjectPointOnPlane_m447207244 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___planeNormal0, Vector3_t3722313464  ___planePoint1, Vector3_t3722313464  ___point2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_ProjectPointOnPlane_m447207244_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector3_t3722313464  L_0 = ___planeNormal0;
		Vector3_t3722313464  L_1 = ___planePoint1;
		Vector3_t3722313464  L_2 = ___point2;
		IL2CPP_RUNTIME_CLASS_INIT(Math3d_t1427253148_il2cpp_TypeInfo_var);
		float L_3 = Math3d_SignedDistancePlanePoint_m2566831789(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = V_0;
		V_0 = ((float)il2cpp_codegen_multiply((float)L_4, (float)(-1.0f)));
		Vector3_t3722313464  L_5 = ___planeNormal0;
		float L_6 = V_0;
		Vector3_t3722313464  L_7 = Math3d_SetVectorLength_m3782886289(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Vector3_t3722313464  L_8 = ___point2;
		Vector3_t3722313464  L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_10 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// UnityEngine.Vector3 Math3d::ProjectVectorOnPlane(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Math3d_ProjectVectorOnPlane_m3889860530 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___planeNormal0, Vector3_t3722313464  ___vector1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_ProjectVectorOnPlane_m3889860530_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t3722313464  L_0 = ___vector1;
		Vector3_t3722313464  L_1 = ___vector1;
		Vector3_t3722313464  L_2 = ___planeNormal0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_3 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = ___planeNormal0;
		Vector3_t3722313464  L_5 = Vector3_op_Multiply_m2104357790(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Single Math3d::SignedDistancePlanePoint(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Math3d_SignedDistancePlanePoint_m2566831789 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___planeNormal0, Vector3_t3722313464  ___planePoint1, Vector3_t3722313464  ___point2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_SignedDistancePlanePoint_m2566831789_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t3722313464  L_0 = ___planeNormal0;
		Vector3_t3722313464  L_1 = ___point2;
		Vector3_t3722313464  L_2 = ___planePoint1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		float L_4 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Single Math3d::SignedDotProduct(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Math3d_SignedDotProduct_m2541652821 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___vectorA0, Vector3_t3722313464  ___vectorB1, Vector3_t3722313464  ___normal2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_SignedDotProduct_m2541652821_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		Vector3_t3722313464  L_0 = ___normal2;
		Vector3_t3722313464  L_1 = ___vectorA0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Vector3_Cross_m418170344(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t3722313464  L_3 = V_0;
		Vector3_t3722313464  L_4 = ___vectorB1;
		float L_5 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = V_1;
		return L_6;
	}
}
// System.Single Math3d::AngleVectorPlane(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Math3d_AngleVectorPlane_m4246505615 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___vector0, Vector3_t3722313464  ___normal1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_AngleVectorPlane_m4246505615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Vector3_t3722313464  L_0 = ___vector0;
		Vector3_t3722313464  L_1 = ___normal1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_2 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		double L_4 = acos((((double)((double)L_3))));
		V_1 = (((float)((float)L_4)));
		float L_5 = V_1;
		return ((float)il2cpp_codegen_subtract((float)(1.57079637f), (float)L_5));
	}
}
// System.Single Math3d::DotProductAngle(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Math3d_DotProductAngle_m4066372586 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___vec10, Vector3_t3722313464  ___vec21, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_DotProductAngle_m4066372586_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	double V_1 = 0.0;
	{
		Vector3_t3722313464  L_0 = ___vec10;
		Vector3_t3722313464  L_1 = ___vec21;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_2 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (((double)((double)L_2)));
		double L_3 = V_0;
		if ((!(((double)L_3) < ((double)(-1.0)))))
		{
			goto IL_0022;
		}
	}
	{
		V_0 = (-1.0);
	}

IL_0022:
	{
		double L_4 = V_0;
		if ((!(((double)L_4) > ((double)(1.0)))))
		{
			goto IL_003b;
		}
	}
	{
		V_0 = (1.0);
	}

IL_003b:
	{
		double L_5 = V_0;
		double L_6 = acos(L_5);
		V_1 = L_6;
		double L_7 = V_1;
		return (((float)((float)L_7)));
	}
}
// System.Void Math3d::PlaneFrom3Points(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Math3d_PlaneFrom3Points_m1928210570 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464 * ___planeNormal0, Vector3_t3722313464 * ___planePoint1, Vector3_t3722313464  ___pointA2, Vector3_t3722313464  ___pointB3, Vector3_t3722313464  ___pointC4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_PlaneFrom3Points_m1928210570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t3722313464  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		Vector3_t3722313464 * L_0 = ___planeNormal0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_0 = L_1;
		Vector3_t3722313464 * L_2 = ___planePoint1;
		Vector3_t3722313464  L_3 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_2 = L_3;
		Vector3_t3722313464  L_4 = ___pointB3;
		Vector3_t3722313464  L_5 = ___pointA2;
		Vector3_t3722313464  L_6 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector3_t3722313464  L_7 = ___pointC4;
		Vector3_t3722313464  L_8 = ___pointA2;
		Vector3_t3722313464  L_9 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		Vector3_t3722313464 * L_10 = ___planeNormal0;
		Vector3_t3722313464  L_11 = V_0;
		Vector3_t3722313464  L_12 = V_1;
		Vector3_t3722313464  L_13 = Vector3_Cross_m418170344(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		Vector3_t3722313464  L_14 = Vector3_Normalize_m2941713846(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_10 = L_14;
		Vector3_t3722313464  L_15 = ___pointA2;
		Vector3_t3722313464  L_16 = V_0;
		Vector3_t3722313464  L_17 = Vector3_op_Division_m510815599(NULL /*static, unused*/, L_16, (2.0f), /*hidden argument*/NULL);
		Vector3_t3722313464  L_18 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		Vector3_t3722313464  L_19 = ___pointA2;
		Vector3_t3722313464  L_20 = V_1;
		Vector3_t3722313464  L_21 = Vector3_op_Division_m510815599(NULL /*static, unused*/, L_20, (2.0f), /*hidden argument*/NULL);
		Vector3_t3722313464  L_22 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_19, L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		Vector3_t3722313464  L_23 = ___pointC4;
		Vector3_t3722313464  L_24 = V_2;
		Vector3_t3722313464  L_25 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		V_4 = L_25;
		Vector3_t3722313464  L_26 = ___pointB3;
		Vector3_t3722313464  L_27 = V_3;
		Vector3_t3722313464  L_28 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
		V_5 = L_28;
		Vector3_t3722313464 * L_29 = ___planePoint1;
		Vector3_t3722313464  L_30 = V_2;
		Vector3_t3722313464  L_31 = V_4;
		Vector3_t3722313464  L_32 = V_3;
		Vector3_t3722313464  L_33 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Math3d_t1427253148_il2cpp_TypeInfo_var);
		Math3d_ClosestPointsOnTwoLines_m2440915867(NULL /*static, unused*/, (Vector3_t3722313464 *)L_29, (Vector3_t3722313464 *)(&V_6), L_30, L_31, L_32, L_33, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 Math3d::GetForwardVector(UnityEngine.Quaternion)
extern "C"  Vector3_t3722313464  Math3d_GetForwardVector_m983852590 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  ___q0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_GetForwardVector_m983852590_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Quaternion_t2301928331  L_0 = ___q0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Vector3_get_forward_m3100859705(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Quaternion_op_Multiply_m2607404835(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 Math3d::GetUpVector(UnityEngine.Quaternion)
extern "C"  Vector3_t3722313464  Math3d_GetUpVector_m2891280427 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  ___q0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_GetUpVector_m2891280427_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Quaternion_t2301928331  L_0 = ___q0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Quaternion_op_Multiply_m2607404835(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 Math3d::GetRightVector(UnityEngine.Quaternion)
extern "C"  Vector3_t3722313464  Math3d_GetRightVector_m493994844 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  ___q0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_GetRightVector_m493994844_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Quaternion_t2301928331  L_0 = ___q0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Vector3_get_right_m1913784872(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Quaternion_op_Multiply_m2607404835(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Quaternion Math3d::QuaternionFromMatrix(UnityEngine.Matrix4x4)
extern "C"  Quaternion_t2301928331  Math3d_QuaternionFromMatrix_m3657783017 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  ___m0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_QuaternionFromMatrix_m3657783017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector4_t3319028937  L_0 = Matrix4x4_GetColumn_m461504848((Matrix4x4_t1817901843 *)(&___m0), 2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t3319028937_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Vector4_op_Implicit_m1158564884(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Vector4_t3319028937  L_2 = Matrix4x4_GetColumn_m461504848((Matrix4x4_t1817901843 *)(&___m0), 1, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = Vector4_op_Implicit_m1158564884(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_4 = Quaternion_LookRotation_m3197602968(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector3 Math3d::PositionFromMatrix(UnityEngine.Matrix4x4)
extern "C"  Vector3_t3722313464  Math3d_PositionFromMatrix_m3866083237 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  ___m0, const RuntimeMethod* method)
{
	Vector4_t3319028937  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector4_t3319028937  L_0 = Matrix4x4_GetColumn_m461504848((Matrix4x4_t1817901843 *)(&___m0), 3, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_1();
		float L_2 = (&V_0)->get_y_2();
		float L_3 = (&V_0)->get_z_3();
		Vector3_t3722313464  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m3353183577((&L_4), L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void Math3d::LookRotationExtended(UnityEngine.GameObject&,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Math3d_LookRotationExtended_m2189285815 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 ** ___gameObjectInOut0, Vector3_t3722313464  ___alignWithVector1, Vector3_t3722313464  ___alignWithNormal2, Vector3_t3722313464  ___customForward3, Vector3_t3722313464  ___customUp4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_LookRotationExtended_m2189285815_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t2301928331  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector3_t3722313464  L_0 = ___alignWithVector1;
		Vector3_t3722313464  L_1 = ___alignWithNormal2;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_2 = Quaternion_LookRotation_m3197602968(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t3722313464  L_3 = ___customForward3;
		Vector3_t3722313464  L_4 = ___customUp4;
		Quaternion_t2301928331  L_5 = Quaternion_LookRotation_m3197602968(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		GameObject_t1113636619 ** L_6 = ___gameObjectInOut0;
		NullCheck((*((GameObject_t1113636619 **)L_6)));
		Transform_t3600365921 * L_7 = GameObject_get_transform_m1369836730((*((GameObject_t1113636619 **)L_6)), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_8 = V_0;
		Quaternion_t2301928331  L_9 = V_1;
		Quaternion_t2301928331  L_10 = Quaternion_Inverse_m1311579081(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_11 = Quaternion_op_Multiply_m1294064023(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_rotation_m3524318132(L_7, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Math3d::TransformWithParent(UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Quaternion,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  void Math3d_TransformWithParent_m2022261480 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331 * ___childRotation0, Vector3_t3722313464 * ___childPosition1, Quaternion_t2301928331  ___parentRotation2, Vector3_t3722313464  ___parentPosition3, Quaternion_t2301928331  ___startParentRotation4, Vector3_t3722313464  ___startParentPosition5, Quaternion_t2301928331  ___startChildRotation6, Vector3_t3722313464  ___startChildPosition7, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_TransformWithParent_m2022261480_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Quaternion_t2301928331 * L_0 = ___childRotation0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_1 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Quaternion_t2301928331 *)L_0 = L_1;
		Vector3_t3722313464 * L_2 = ___childPosition1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_2 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(Math3d_t1427253148_il2cpp_TypeInfo_var);
		Transform_t3600365921 * L_4 = ((Math3d_t1427253148_StaticFields*)il2cpp_codegen_static_fields_for(Math3d_t1427253148_il2cpp_TypeInfo_var))->get_tempParent_3();
		Quaternion_t2301928331  L_5 = ___startParentRotation4;
		NullCheck(L_4);
		Transform_set_rotation_m3524318132(L_4, L_5, /*hidden argument*/NULL);
		Transform_t3600365921 * L_6 = ((Math3d_t1427253148_StaticFields*)il2cpp_codegen_static_fields_for(Math3d_t1427253148_il2cpp_TypeInfo_var))->get_tempParent_3();
		Vector3_t3722313464  L_7 = ___startParentPosition5;
		NullCheck(L_6);
		Transform_set_position_m3387557959(L_6, L_7, /*hidden argument*/NULL);
		Transform_t3600365921 * L_8 = ((Math3d_t1427253148_StaticFields*)il2cpp_codegen_static_fields_for(Math3d_t1427253148_il2cpp_TypeInfo_var))->get_tempParent_3();
		Vector3_t3722313464  L_9 = Vector3_get_one_m1629952498(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_localScale_m3053443106(L_8, L_9, /*hidden argument*/NULL);
		Transform_t3600365921 * L_10 = ((Math3d_t1427253148_StaticFields*)il2cpp_codegen_static_fields_for(Math3d_t1427253148_il2cpp_TypeInfo_var))->get_tempChild_2();
		Quaternion_t2301928331  L_11 = ___startChildRotation6;
		NullCheck(L_10);
		Transform_set_rotation_m3524318132(L_10, L_11, /*hidden argument*/NULL);
		Transform_t3600365921 * L_12 = ((Math3d_t1427253148_StaticFields*)il2cpp_codegen_static_fields_for(Math3d_t1427253148_il2cpp_TypeInfo_var))->get_tempChild_2();
		Vector3_t3722313464  L_13 = ___startChildPosition7;
		NullCheck(L_12);
		Transform_set_position_m3387557959(L_12, L_13, /*hidden argument*/NULL);
		Transform_t3600365921 * L_14 = ((Math3d_t1427253148_StaticFields*)il2cpp_codegen_static_fields_for(Math3d_t1427253148_il2cpp_TypeInfo_var))->get_tempChild_2();
		Vector3_t3722313464  L_15 = Vector3_get_one_m1629952498(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_set_localScale_m3053443106(L_14, L_15, /*hidden argument*/NULL);
		Transform_t3600365921 * L_16 = ((Math3d_t1427253148_StaticFields*)il2cpp_codegen_static_fields_for(Math3d_t1427253148_il2cpp_TypeInfo_var))->get_tempParent_3();
		Quaternion_t2301928331  L_17 = ___parentRotation2;
		NullCheck(L_16);
		Transform_set_rotation_m3524318132(L_16, L_17, /*hidden argument*/NULL);
		Transform_t3600365921 * L_18 = ((Math3d_t1427253148_StaticFields*)il2cpp_codegen_static_fields_for(Math3d_t1427253148_il2cpp_TypeInfo_var))->get_tempParent_3();
		Vector3_t3722313464  L_19 = ___parentPosition3;
		NullCheck(L_18);
		Transform_set_position_m3387557959(L_18, L_19, /*hidden argument*/NULL);
		Quaternion_t2301928331 * L_20 = ___childRotation0;
		Transform_t3600365921 * L_21 = ((Math3d_t1427253148_StaticFields*)il2cpp_codegen_static_fields_for(Math3d_t1427253148_il2cpp_TypeInfo_var))->get_tempChild_2();
		NullCheck(L_21);
		Quaternion_t2301928331  L_22 = Transform_get_rotation_m3502953881(L_21, /*hidden argument*/NULL);
		*(Quaternion_t2301928331 *)L_20 = L_22;
		Vector3_t3722313464 * L_23 = ___childPosition1;
		Transform_t3600365921 * L_24 = ((Math3d_t1427253148_StaticFields*)il2cpp_codegen_static_fields_for(Math3d_t1427253148_il2cpp_TypeInfo_var))->get_tempChild_2();
		NullCheck(L_24);
		Vector3_t3722313464  L_25 = Transform_get_position_m36019626(L_24, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_23 = L_25;
		return;
	}
}
// System.Void Math3d::PreciseAlign(UnityEngine.GameObject&,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Math3d_PreciseAlign_m2550822458 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 ** ___gameObjectInOut0, Vector3_t3722313464  ___alignWithVector1, Vector3_t3722313464  ___alignWithNormal2, Vector3_t3722313464  ___alignWithPosition3, Vector3_t3722313464  ___triangleForward4, Vector3_t3722313464  ___triangleNormal5, Vector3_t3722313464  ___trianglePosition6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_PreciseAlign_m2550822458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		GameObject_t1113636619 ** L_0 = ___gameObjectInOut0;
		Vector3_t3722313464  L_1 = ___alignWithVector1;
		Vector3_t3722313464  L_2 = ___alignWithNormal2;
		Vector3_t3722313464  L_3 = ___triangleForward4;
		Vector3_t3722313464  L_4 = ___triangleNormal5;
		IL2CPP_RUNTIME_CLASS_INIT(Math3d_t1427253148_il2cpp_TypeInfo_var);
		Math3d_LookRotationExtended_m2189285815(NULL /*static, unused*/, (GameObject_t1113636619 **)L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		GameObject_t1113636619 ** L_5 = ___gameObjectInOut0;
		NullCheck((*((GameObject_t1113636619 **)L_5)));
		Transform_t3600365921 * L_6 = GameObject_get_transform_m1369836730((*((GameObject_t1113636619 **)L_5)), /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = ___trianglePosition6;
		NullCheck(L_6);
		Vector3_t3722313464  L_8 = Transform_TransformPoint_m226827784(L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		Vector3_t3722313464  L_9 = ___alignWithPosition3;
		Vector3_t3722313464  L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_11 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		GameObject_t1113636619 ** L_12 = ___gameObjectInOut0;
		NullCheck((*((GameObject_t1113636619 **)L_12)));
		Transform_t3600365921 * L_13 = GameObject_get_transform_m1369836730((*((GameObject_t1113636619 **)L_12)), /*hidden argument*/NULL);
		Vector3_t3722313464  L_14 = V_1;
		NullCheck(L_13);
		Transform_Translate_m1990195114(L_13, L_14, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Math3d::VectorsToTransform(UnityEngine.GameObject&,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Math3d_VectorsToTransform_m1296114369 (Math3d_t1427253148 * __this, GameObject_t1113636619 ** ___gameObjectInOut0, Vector3_t3722313464  ___positionVector1, Vector3_t3722313464  ___directionVector2, Vector3_t3722313464  ___normalVector3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_VectorsToTransform_m1296114369_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 ** L_0 = ___gameObjectInOut0;
		NullCheck((*((GameObject_t1113636619 **)L_0)));
		Transform_t3600365921 * L_1 = GameObject_get_transform_m1369836730((*((GameObject_t1113636619 **)L_0)), /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = ___positionVector1;
		NullCheck(L_1);
		Transform_set_position_m3387557959(L_1, L_2, /*hidden argument*/NULL);
		GameObject_t1113636619 ** L_3 = ___gameObjectInOut0;
		NullCheck((*((GameObject_t1113636619 **)L_3)));
		Transform_t3600365921 * L_4 = GameObject_get_transform_m1369836730((*((GameObject_t1113636619 **)L_3)), /*hidden argument*/NULL);
		Vector3_t3722313464  L_5 = ___directionVector2;
		Vector3_t3722313464  L_6 = ___normalVector3;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_7 = Quaternion_LookRotation_m3197602968(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_rotation_m3524318132(L_4, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Math3d::PointOnWhichSideOfLineSegment(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  int32_t Math3d_PointOnWhichSideOfLineSegment_m3215589412 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___linePoint10, Vector3_t3722313464  ___linePoint21, Vector3_t3722313464  ___point2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_PointOnWhichSideOfLineSegment_m3215589412_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	{
		Vector3_t3722313464  L_0 = ___linePoint21;
		Vector3_t3722313464  L_1 = ___linePoint10;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t3722313464  L_3 = ___point2;
		Vector3_t3722313464  L_4 = ___linePoint10;
		Vector3_t3722313464  L_5 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector3_t3722313464  L_6 = V_1;
		Vector3_t3722313464  L_7 = V_0;
		float L_8 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		float L_9 = V_2;
		if ((!(((float)L_9) > ((float)(0.0f)))))
		{
			goto IL_003a;
		}
	}
	{
		float L_10 = Vector3_get_magnitude_m27958459((Vector3_t3722313464 *)(&V_1), /*hidden argument*/NULL);
		float L_11 = Vector3_get_magnitude_m27958459((Vector3_t3722313464 *)(&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_10) <= ((float)L_11))))
		{
			goto IL_0038;
		}
	}
	{
		return 0;
	}

IL_0038:
	{
		return 2;
	}

IL_003a:
	{
		return 1;
	}
}
// System.Single Math3d::MouseDistanceToLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Math3d_MouseDistanceToLine_m163400490 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___linePoint10, Vector3_t3722313464  ___linePoint21, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_MouseDistanceToLine_m163400490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Camera_t4157153871 * V_0 = NULL;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		Camera_t4157153871 * L_0 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Input_get_mousePosition_m1616496925(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		Camera_t4157153871 * L_2 = V_0;
		Vector3_t3722313464  L_3 = ___linePoint10;
		NullCheck(L_2);
		Vector3_t3722313464  L_4 = Camera_WorldToScreenPoint_m3726311023(L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		Camera_t4157153871 * L_5 = V_0;
		Vector3_t3722313464  L_6 = ___linePoint21;
		NullCheck(L_5);
		Vector3_t3722313464  L_7 = Camera_WorldToScreenPoint_m3726311023(L_5, L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		Vector3_t3722313464  L_8 = V_2;
		Vector3_t3722313464  L_9 = V_3;
		Vector3_t3722313464  L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Math3d_t1427253148_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_11 = Math3d_ProjectPointOnLineSegment_m2179139403(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
		V_4 = L_11;
		float L_12 = (&V_4)->get_x_1();
		float L_13 = (&V_4)->get_y_2();
		Vector3__ctor_m3353183577((Vector3_t3722313464 *)(&V_4), L_12, L_13, (0.0f), /*hidden argument*/NULL);
		Vector3_t3722313464  L_14 = V_4;
		Vector3_t3722313464  L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_16 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		V_5 = L_16;
		float L_17 = Vector3_get_magnitude_m27958459((Vector3_t3722313464 *)(&V_5), /*hidden argument*/NULL);
		return L_17;
	}
}
// System.Single Math3d::MouseDistanceToCircle(UnityEngine.Vector3,System.Single)
extern "C"  float Math3d_MouseDistanceToCircle_m2071200245 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___point0, float ___radius1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_MouseDistanceToCircle_m2071200245_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Camera_t4157153871 * V_0 = NULL;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	{
		Camera_t4157153871 * L_0 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Input_get_mousePosition_m1616496925(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		Camera_t4157153871 * L_2 = V_0;
		Vector3_t3722313464  L_3 = ___point0;
		NullCheck(L_2);
		Vector3_t3722313464  L_4 = Camera_WorldToScreenPoint_m3726311023(L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = (&V_2)->get_x_1();
		float L_6 = (&V_2)->get_y_2();
		Vector3__ctor_m3353183577((Vector3_t3722313464 *)(&V_2), L_5, L_6, (0.0f), /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = V_2;
		Vector3_t3722313464  L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_9 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		float L_10 = Vector3_get_magnitude_m27958459((Vector3_t3722313464 *)(&V_3), /*hidden argument*/NULL);
		V_4 = L_10;
		float L_11 = V_4;
		float L_12 = ___radius1;
		V_5 = ((float)il2cpp_codegen_subtract((float)L_11, (float)L_12));
		float L_13 = V_5;
		return L_13;
	}
}
// System.Boolean Math3d::IsLineInRectangle(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Math3d_IsLineInRectangle_m2110013125 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___linePoint10, Vector3_t3722313464  ___linePoint21, Vector3_t3722313464  ___rectA2, Vector3_t3722313464  ___rectB3, Vector3_t3722313464  ___rectC4, Vector3_t3722313464  ___rectD5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_IsLineInRectangle_m2110013125_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	{
		V_0 = (bool)0;
		V_1 = (bool)0;
		Vector3_t3722313464  L_0 = ___linePoint10;
		Vector3_t3722313464  L_1 = ___rectA2;
		Vector3_t3722313464  L_2 = ___rectC4;
		Vector3_t3722313464  L_3 = ___rectB3;
		Vector3_t3722313464  L_4 = ___rectD5;
		IL2CPP_RUNTIME_CLASS_INIT(Math3d_t1427253148_il2cpp_TypeInfo_var);
		bool L_5 = Math3d_IsPointInRectangle_m50583159(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		bool L_6 = V_0;
		if (L_6)
		{
			goto IL_0024;
		}
	}
	{
		Vector3_t3722313464  L_7 = ___linePoint21;
		Vector3_t3722313464  L_8 = ___rectA2;
		Vector3_t3722313464  L_9 = ___rectC4;
		Vector3_t3722313464  L_10 = ___rectB3;
		Vector3_t3722313464  L_11 = ___rectD5;
		IL2CPP_RUNTIME_CLASS_INIT(Math3d_t1427253148_il2cpp_TypeInfo_var);
		bool L_12 = Math3d_IsPointInRectangle_m50583159(NULL /*static, unused*/, L_7, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
	}

IL_0024:
	{
		bool L_13 = V_0;
		if (L_13)
		{
			goto IL_007c;
		}
	}
	{
		bool L_14 = V_1;
		if (L_14)
		{
			goto IL_007c;
		}
	}
	{
		Vector3_t3722313464  L_15 = ___linePoint10;
		Vector3_t3722313464  L_16 = ___linePoint21;
		Vector3_t3722313464  L_17 = ___rectA2;
		Vector3_t3722313464  L_18 = ___rectB3;
		IL2CPP_RUNTIME_CLASS_INIT(Math3d_t1427253148_il2cpp_TypeInfo_var);
		bool L_19 = Math3d_AreLineSegmentsCrossing_m616452860(NULL /*static, unused*/, L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
		V_2 = L_19;
		Vector3_t3722313464  L_20 = ___linePoint10;
		Vector3_t3722313464  L_21 = ___linePoint21;
		Vector3_t3722313464  L_22 = ___rectB3;
		Vector3_t3722313464  L_23 = ___rectC4;
		bool L_24 = Math3d_AreLineSegmentsCrossing_m616452860(NULL /*static, unused*/, L_20, L_21, L_22, L_23, /*hidden argument*/NULL);
		V_3 = L_24;
		Vector3_t3722313464  L_25 = ___linePoint10;
		Vector3_t3722313464  L_26 = ___linePoint21;
		Vector3_t3722313464  L_27 = ___rectC4;
		Vector3_t3722313464  L_28 = ___rectD5;
		bool L_29 = Math3d_AreLineSegmentsCrossing_m616452860(NULL /*static, unused*/, L_25, L_26, L_27, L_28, /*hidden argument*/NULL);
		V_4 = L_29;
		Vector3_t3722313464  L_30 = ___linePoint10;
		Vector3_t3722313464  L_31 = ___linePoint21;
		Vector3_t3722313464  L_32 = ___rectD5;
		Vector3_t3722313464  L_33 = ___rectA2;
		bool L_34 = Math3d_AreLineSegmentsCrossing_m616452860(NULL /*static, unused*/, L_30, L_31, L_32, L_33, /*hidden argument*/NULL);
		V_5 = L_34;
		bool L_35 = V_2;
		if (L_35)
		{
			goto IL_0078;
		}
	}
	{
		bool L_36 = V_3;
		if (L_36)
		{
			goto IL_0078;
		}
	}
	{
		bool L_37 = V_4;
		if (L_37)
		{
			goto IL_0078;
		}
	}
	{
		bool L_38 = V_5;
		if (!L_38)
		{
			goto IL_007a;
		}
	}

IL_0078:
	{
		return (bool)1;
	}

IL_007a:
	{
		return (bool)0;
	}

IL_007c:
	{
		return (bool)1;
	}
}
// System.Boolean Math3d::IsPointInRectangle(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Math3d_IsPointInRectangle_m50583159 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___point0, Vector3_t3722313464  ___rectA1, Vector3_t3722313464  ___rectC2, Vector3_t3722313464  ___rectB3, Vector3_t3722313464  ___rectD4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_IsPointInRectangle_m50583159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	Vector3_t3722313464  V_6;
	memset(&V_6, 0, sizeof(V_6));
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	{
		Vector3_t3722313464  L_0 = ___rectC2;
		Vector3_t3722313464  L_1 = ___rectA1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Vector3_get_magnitude_m27958459((Vector3_t3722313464 *)(&V_0), /*hidden argument*/NULL);
		V_2 = ((-((float)((float)L_3/(float)(2.0f)))));
		Vector3_t3722313464  L_4 = V_0;
		float L_5 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Math3d_t1427253148_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_6 = Math3d_AddVectorLength_m3019376001(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector3_t3722313464  L_7 = ___rectA1;
		Vector3_t3722313464  L_8 = V_0;
		Vector3_t3722313464  L_9 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		Vector3_t3722313464  L_10 = ___rectB3;
		Vector3_t3722313464  L_11 = ___rectA1;
		Vector3_t3722313464  L_12 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_4 = L_12;
		float L_13 = Vector3_get_magnitude_m27958459((Vector3_t3722313464 *)(&V_4), /*hidden argument*/NULL);
		V_5 = ((float)((float)L_13/(float)(2.0f)));
		Vector3_t3722313464  L_14 = ___rectD4;
		Vector3_t3722313464  L_15 = ___rectA1;
		Vector3_t3722313464  L_16 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		V_6 = L_16;
		float L_17 = Vector3_get_magnitude_m27958459((Vector3_t3722313464 *)(&V_6), /*hidden argument*/NULL);
		V_7 = ((float)((float)L_17/(float)(2.0f)));
		Vector3_t3722313464  L_18 = V_3;
		Vector3_t3722313464  L_19 = Vector3_get_normalized_m2454957984((Vector3_t3722313464 *)(&V_4), /*hidden argument*/NULL);
		Vector3_t3722313464  L_20 = ___point0;
		Vector3_t3722313464  L_21 = Math3d_ProjectPointOnLine_m4192705973(NULL /*static, unused*/, L_18, L_19, L_20, /*hidden argument*/NULL);
		V_1 = L_21;
		Vector3_t3722313464  L_22 = V_1;
		Vector3_t3722313464  L_23 = ___point0;
		Vector3_t3722313464  L_24 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		V_0 = L_24;
		float L_25 = Vector3_get_magnitude_m27958459((Vector3_t3722313464 *)(&V_0), /*hidden argument*/NULL);
		V_8 = L_25;
		Vector3_t3722313464  L_26 = V_3;
		Vector3_t3722313464  L_27 = Vector3_get_normalized_m2454957984((Vector3_t3722313464 *)(&V_6), /*hidden argument*/NULL);
		Vector3_t3722313464  L_28 = ___point0;
		Vector3_t3722313464  L_29 = Math3d_ProjectPointOnLine_m4192705973(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		V_1 = L_29;
		Vector3_t3722313464  L_30 = V_1;
		Vector3_t3722313464  L_31 = ___point0;
		Vector3_t3722313464  L_32 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
		V_0 = L_32;
		float L_33 = Vector3_get_magnitude_m27958459((Vector3_t3722313464 *)(&V_0), /*hidden argument*/NULL);
		V_9 = L_33;
		float L_34 = V_9;
		float L_35 = V_5;
		if ((!(((float)L_34) <= ((float)L_35))))
		{
			goto IL_00ac;
		}
	}
	{
		float L_36 = V_8;
		float L_37 = V_7;
		if ((!(((float)L_36) <= ((float)L_37))))
		{
			goto IL_00ac;
		}
	}
	{
		return (bool)1;
	}

IL_00ac:
	{
		return (bool)0;
	}
}
// System.Boolean Math3d::AreLineSegmentsCrossing(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Math3d_AreLineSegmentsCrossing_m616452860 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___pointA10, Vector3_t3722313464  ___pointA21, Vector3_t3722313464  ___pointB12, Vector3_t3722313464  ___pointB23, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_AreLineSegmentsCrossing_m616452860_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	bool V_6 = false;
	{
		Vector3_t3722313464  L_0 = ___pointA21;
		Vector3_t3722313464  L_1 = ___pointA10;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_4 = L_2;
		Vector3_t3722313464  L_3 = ___pointB23;
		Vector3_t3722313464  L_4 = ___pointB12;
		Vector3_t3722313464  L_5 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_5 = L_5;
		Vector3_t3722313464  L_6 = ___pointA10;
		Vector3_t3722313464  L_7 = Vector3_get_normalized_m2454957984((Vector3_t3722313464 *)(&V_4), /*hidden argument*/NULL);
		Vector3_t3722313464  L_8 = ___pointB12;
		Vector3_t3722313464  L_9 = Vector3_get_normalized_m2454957984((Vector3_t3722313464 *)(&V_5), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Math3d_t1427253148_il2cpp_TypeInfo_var);
		bool L_10 = Math3d_ClosestPointsOnTwoLines_m2440915867(NULL /*static, unused*/, (Vector3_t3722313464 *)(&V_0), (Vector3_t3722313464 *)(&V_1), L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		V_6 = L_10;
		bool L_11 = V_6;
		if (!L_11)
		{
			goto IL_0056;
		}
	}
	{
		Vector3_t3722313464  L_12 = ___pointA10;
		Vector3_t3722313464  L_13 = ___pointA21;
		Vector3_t3722313464  L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Math3d_t1427253148_il2cpp_TypeInfo_var);
		int32_t L_15 = Math3d_PointOnWhichSideOfLineSegment_m3215589412(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		Vector3_t3722313464  L_16 = ___pointB12;
		Vector3_t3722313464  L_17 = ___pointB23;
		Vector3_t3722313464  L_18 = V_1;
		int32_t L_19 = Math3d_PointOnWhichSideOfLineSegment_m3215589412(NULL /*static, unused*/, L_16, L_17, L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		int32_t L_20 = V_2;
		if (L_20)
		{
			goto IL_0054;
		}
	}
	{
		int32_t L_21 = V_3;
		if (L_21)
		{
			goto IL_0054;
		}
	}
	{
		return (bool)1;
	}

IL_0054:
	{
		return (bool)0;
	}

IL_0056:
	{
		return (bool)0;
	}
}
// System.Void Math3d::AverageQuaternion(UnityEngine.Vector4&,UnityEngine.Quaternion,UnityEngine.Quaternion,System.Int32)
extern "C"  void Math3d_AverageQuaternion_m2118685977 (RuntimeObject * __this /* static, unused */, Vector4_t3319028937 * ___cumulative0, Quaternion_t2301928331  ___newRotation1, Quaternion_t2301928331  ___firstRotation2, int32_t ___addAmount3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Math3d_AverageQuaternion_m2118685977_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		V_0 = (0.0f);
		V_1 = (0.0f);
		V_2 = (0.0f);
		V_3 = (0.0f);
		Quaternion_t2301928331  L_0 = ___newRotation1;
		Quaternion_t2301928331  L_1 = ___firstRotation2;
		IL2CPP_RUNTIME_CLASS_INIT(Math3d_t1427253148_il2cpp_TypeInfo_var);
		bool L_2 = Math3d_AreQuaternionsClose_m3116464126(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		Quaternion_t2301928331  L_3 = ___newRotation1;
		IL2CPP_RUNTIME_CLASS_INIT(Math3d_t1427253148_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_4 = Math3d_InverseSignQuaternion_m7038823(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		___newRotation1 = L_4;
	}

IL_002c:
	{
		int32_t L_5 = ___addAmount3;
		V_4 = ((float)((float)(1.0f)/(float)(((float)((float)L_5)))));
		Vector4_t3319028937 * L_6 = ___cumulative0;
		Vector4_t3319028937 * L_7 = L_6;
		float L_8 = L_7->get_w_4();
		float L_9 = (&___newRotation1)->get_w_3();
		L_7->set_w_4(((float)il2cpp_codegen_add((float)L_8, (float)L_9)));
		Vector4_t3319028937 * L_10 = ___cumulative0;
		float L_11 = L_10->get_w_4();
		float L_12 = V_4;
		V_0 = ((float)il2cpp_codegen_multiply((float)L_11, (float)L_12));
		Vector4_t3319028937 * L_13 = ___cumulative0;
		Vector4_t3319028937 * L_14 = L_13;
		float L_15 = L_14->get_x_1();
		float L_16 = (&___newRotation1)->get_x_0();
		L_14->set_x_1(((float)il2cpp_codegen_add((float)L_15, (float)L_16)));
		Vector4_t3319028937 * L_17 = ___cumulative0;
		float L_18 = L_17->get_x_1();
		float L_19 = V_4;
		V_1 = ((float)il2cpp_codegen_multiply((float)L_18, (float)L_19));
		Vector4_t3319028937 * L_20 = ___cumulative0;
		Vector4_t3319028937 * L_21 = L_20;
		float L_22 = L_21->get_y_2();
		float L_23 = (&___newRotation1)->get_y_1();
		L_21->set_y_2(((float)il2cpp_codegen_add((float)L_22, (float)L_23)));
		Vector4_t3319028937 * L_24 = ___cumulative0;
		float L_25 = L_24->get_y_2();
		float L_26 = V_4;
		V_2 = ((float)il2cpp_codegen_multiply((float)L_25, (float)L_26));
		Vector4_t3319028937 * L_27 = ___cumulative0;
		Vector4_t3319028937 * L_28 = L_27;
		float L_29 = L_28->get_z_3();
		float L_30 = (&___newRotation1)->get_z_2();
		L_28->set_z_3(((float)il2cpp_codegen_add((float)L_29, (float)L_30)));
		Vector4_t3319028937 * L_31 = ___cumulative0;
		float L_32 = L_31->get_z_3();
		float L_33 = V_4;
		V_3 = ((float)il2cpp_codegen_multiply((float)L_32, (float)L_33));
		return;
	}
}
// UnityEngine.Quaternion Math3d::NormalizeQuaternion(System.Single,System.Single,System.Single,System.Single)
extern "C"  Quaternion_t2301928331  Math3d_NormalizeQuaternion_m1537677135 (RuntimeObject * __this /* static, unused */, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___w3;
		float L_1 = ___w3;
		float L_2 = ___x0;
		float L_3 = ___x0;
		float L_4 = ___y1;
		float L_5 = ___y1;
		float L_6 = ___z2;
		float L_7 = ___z2;
		V_0 = ((float)((float)(1.0f)/(float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_0, (float)L_1)), (float)((float)il2cpp_codegen_multiply((float)L_2, (float)L_3)))), (float)((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)))), (float)((float)il2cpp_codegen_multiply((float)L_6, (float)L_7))))));
		float L_8 = ___w3;
		float L_9 = V_0;
		___w3 = ((float)il2cpp_codegen_multiply((float)L_8, (float)L_9));
		float L_10 = ___x0;
		float L_11 = V_0;
		___x0 = ((float)il2cpp_codegen_multiply((float)L_10, (float)L_11));
		float L_12 = ___y1;
		float L_13 = V_0;
		___y1 = ((float)il2cpp_codegen_multiply((float)L_12, (float)L_13));
		float L_14 = ___z2;
		float L_15 = V_0;
		___z2 = ((float)il2cpp_codegen_multiply((float)L_14, (float)L_15));
		float L_16 = ___x0;
		float L_17 = ___y1;
		float L_18 = ___z2;
		float L_19 = ___w3;
		Quaternion_t2301928331  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Quaternion__ctor_m435141806((&L_20), L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Void Math3d::.cctor()
extern "C"  void Math3d__cctor_m292415024 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MenuAnimator::.ctor()
extern "C"  void MenuAnimator__ctor_m2002699033 (MenuAnimator_t2112910832 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuAnimator__ctor_m2002699033_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_0 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mVisiblePos_2(L_0);
		Vector3_t3722313464  L_1 = Vector3_get_right_m1913784872(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = Vector3_op_UnaryNegation_m1951478815(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_2, (2000.0f), /*hidden argument*/NULL);
		__this->set_mInvisiblePos_3(L_3);
		__this->set_SlidingTime_8((0.3f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuAnimator::Start()
extern "C"  void MenuAnimator_Start_m1688760679 (MenuAnimator_t2112910832 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuAnimator_Start_m1688760679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_0 = Vector3_get_right_m1913784872(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Vector3_op_UnaryNegation_m1951478815(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_1, (((float)((float)((int32_t)il2cpp_codegen_multiply((int32_t)2, (int32_t)L_2))))), /*hidden argument*/NULL);
		__this->set_mInvisiblePos_3(L_3);
		__this->set_mVisibility_4((0.0f));
		__this->set_mVisible_5((bool)0);
		Transform_t3600365921 * L_4 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_5 = __this->get_mInvisiblePos_3();
		NullCheck(L_4);
		Transform_set_position_m3387557959(L_4, L_5, /*hidden argument*/NULL);
		CanvasU5BU5D_t682926938* L_6 = Component_GetComponentsInChildren_TisCanvas_t3310196443_m1457345007(__this, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisCanvas_t3310196443_m1457345007_RuntimeMethod_var);
		NullCheck(L_6);
		int32_t L_7 = 0;
		Canvas_t3310196443 * L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		__this->set_mCanvas_6(L_8);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		MenuOptions_t1951716431 * L_9 = Object_FindObjectOfType_TisMenuOptions_t1951716431_m1050906446(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisMenuOptions_t1951716431_m1050906446_RuntimeMethod_var);
		__this->set_mMenuOptions_7(L_9);
		return;
	}
}
// System.Void MenuAnimator::Update()
extern "C"  void MenuAnimator_Update_m3695532741 (MenuAnimator_t2112910832 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuAnimator_Update_m3695532741_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_0 = Vector3_get_right_m1913784872(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Vector3_op_UnaryNegation_m1951478815(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_1, (((float)((float)L_2))), /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_3, (2.0f), /*hidden argument*/NULL);
		__this->set_mInvisiblePos_3(L_4);
		bool L_5 = __this->get_mVisible_5();
		if (!L_5)
		{
			goto IL_00be;
		}
	}
	{
		Canvas_t3310196443 * L_6 = __this->get_mCanvas_6();
		NullCheck(L_6);
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameObject_SetActive_m796801857(L_7, (bool)1, /*hidden argument*/NULL);
		Canvas_t3310196443 * L_8 = __this->get_mCanvas_6();
		NullCheck(L_8);
		bool L_9 = Behaviour_get_enabled_m753527255(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_005d;
		}
	}
	{
		Canvas_t3310196443 * L_10 = __this->get_mCanvas_6();
		NullCheck(L_10);
		Behaviour_set_enabled_m20417929(L_10, (bool)1, /*hidden argument*/NULL);
	}

IL_005d:
	{
		float L_11 = __this->get_mVisibility_4();
		if ((!(((float)L_11) < ((float)(1.0f)))))
		{
			goto IL_00b9;
		}
	}
	{
		float L_12 = __this->get_mVisibility_4();
		float L_13 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_14 = __this->get_SlidingTime_8();
		__this->set_mVisibility_4(((float)il2cpp_codegen_add((float)L_12, (float)((float)((float)L_13/(float)L_14)))));
		float L_15 = __this->get_mVisibility_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_16 = Mathf_Clamp01_m56433566(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		__this->set_mVisibility_4(L_16);
		Transform_t3600365921 * L_17 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_18 = __this->get_mInvisiblePos_3();
		Vector3_t3722313464  L_19 = __this->get_mVisiblePos_2();
		float L_20 = __this->get_mVisibility_4();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_21 = Vector3_Slerp_m802114822(NULL /*static, unused*/, L_18, L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_position_m3387557959(L_17, L_21, /*hidden argument*/NULL);
	}

IL_00b9:
	{
		goto IL_016d;
	}

IL_00be:
	{
		float L_22 = __this->get_mVisibility_4();
		if ((!(((float)L_22) > ((float)(0.0f)))))
		{
			goto IL_015c;
		}
	}
	{
		float L_23 = __this->get_mVisibility_4();
		float L_24 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_25 = __this->get_SlidingTime_8();
		__this->set_mVisibility_4(((float)il2cpp_codegen_subtract((float)L_23, (float)((float)((float)L_24/(float)L_25)))));
		float L_26 = __this->get_mVisibility_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_27 = Mathf_Clamp01_m56433566(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		__this->set_mVisibility_4(L_27);
		Transform_t3600365921 * L_28 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_29 = __this->get_mInvisiblePos_3();
		Vector3_t3722313464  L_30 = __this->get_mVisiblePos_2();
		float L_31 = __this->get_mVisibility_4();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_32 = Vector3_Slerp_m802114822(NULL /*static, unused*/, L_29, L_30, L_31, /*hidden argument*/NULL);
		NullCheck(L_28);
		Transform_set_position_m3387557959(L_28, L_32, /*hidden argument*/NULL);
		Canvas_t3310196443 * L_33 = __this->get_mCanvas_6();
		NullCheck(L_33);
		GameObject_t1113636619 * L_34 = Component_get_gameObject_m442555142(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		GameObject_SetActive_m796801857(L_34, (bool)0, /*hidden argument*/NULL);
		float L_35 = __this->get_mVisibility_4();
		if ((!(((float)L_35) < ((float)(0.01f)))))
		{
			goto IL_0157;
		}
	}
	{
		Canvas_t3310196443 * L_36 = __this->get_mCanvas_6();
		NullCheck(L_36);
		bool L_37 = Behaviour_get_enabled_m753527255(L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0157;
		}
	}
	{
		Canvas_t3310196443 * L_38 = __this->get_mCanvas_6();
		NullCheck(L_38);
		Behaviour_set_enabled_m20417929(L_38, (bool)0, /*hidden argument*/NULL);
	}

IL_0157:
	{
		goto IL_016d;
	}

IL_015c:
	{
		Transform_t3600365921 * L_39 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_40 = __this->get_mInvisiblePos_3();
		NullCheck(L_39);
		Transform_set_position_m3387557959(L_39, L_40, /*hidden argument*/NULL);
	}

IL_016d:
	{
		return;
	}
}
// System.Void MenuAnimator::Show()
extern "C"  void MenuAnimator_Show_m982174260 (MenuAnimator_t2112910832 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuAnimator_Show_m982174260_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_mVisible_5((bool)1);
		MenuOptions_t1951716431 * L_0 = __this->get_mMenuOptions_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		MenuOptions_t1951716431 * L_2 = __this->get_mMenuOptions_7();
		NullCheck(L_2);
		MenuOptions_UpdateUI_m2695051465(L_2, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void MenuAnimator::Hide()
extern "C"  void MenuAnimator_Hide_m1323916210 (MenuAnimator_t2112910832 * __this, const RuntimeMethod* method)
{
	{
		__this->set_mVisible_5((bool)0);
		return;
	}
}
// System.Boolean MenuAnimator::IsVisible()
extern "C"  bool MenuAnimator_IsVisible_m928022866 (MenuAnimator_t2112910832 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_mVisibility_4();
		return (bool)((((float)L_0) > ((float)(0.05f)))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MenuOptions::.ctor()
extern "C"  void MenuOptions__ctor_m1083420456 (MenuOptions_t1951716431 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuOptions::Start()
extern "C"  void MenuOptions_Start_m2289807458 (MenuOptions_t1951716431 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuOptions_Start_m2289807458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VuforiaARController_t1876945237 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		CameraSettings_t3152619780 * L_0 = Object_FindObjectOfType_TisCameraSettings_t3152619780_m4203198741(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisCameraSettings_t3152619780_m4203198741_RuntimeMethod_var);
		__this->set_mCamSettings_2(L_0);
		TrackableSettings_t2862243993 * L_1 = Object_FindObjectOfType_TisTrackableSettings_t2862243993_m1852430619(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisTrackableSettings_t2862243993_m1852430619_RuntimeMethod_var);
		__this->set_mTrackableSettings_3(L_1);
		MenuAnimator_t2112910832 * L_2 = Object_FindObjectOfType_TisMenuAnimator_t2112910832_m1830680649(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisMenuAnimator_t2112910832_m1830680649_RuntimeMethod_var);
		__this->set_mMenuAnim_4(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaARController_t1876945237_il2cpp_TypeInfo_var);
		VuforiaARController_t1876945237 * L_3 = VuforiaARController_get_Instance_m1100644214(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_3;
		VuforiaARController_t1876945237 * L_4 = V_0;
		intptr_t L_5 = (intptr_t)MenuOptions_OnPaused_m1134986695_RuntimeMethod_var;
		Action_1_t269755560 * L_6 = (Action_1_t269755560 *)il2cpp_codegen_object_new(Action_1_t269755560_il2cpp_TypeInfo_var);
		Action_1__ctor_m2079027267(L_6, __this, L_5, /*hidden argument*/Action_1__ctor_m2079027267_RuntimeMethod_var);
		NullCheck(L_4);
		VuforiaARController_RegisterOnPauseCallback_m1833237148(L_4, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuOptions::ShowAboutPage()
extern "C"  void MenuOptions_ShowAboutPage_m3278612116 (MenuOptions_t1951716431 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuOptions_ShowAboutPage_m3278612116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, _stringLiteral3521912105, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuOptions::ToggleAutofocus()
extern "C"  void MenuOptions_ToggleAutofocus_m677591208 (MenuOptions_t1951716431 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuOptions_ToggleAutofocus_m677591208_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Toggle_t2735377061 * V_0 = NULL;
	{
		Toggle_t2735377061 * L_0 = MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125(__this, _stringLiteral683363693, /*hidden argument*/MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125_RuntimeMethod_var);
		V_0 = L_0;
		Toggle_t2735377061 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0038;
		}
	}
	{
		CameraSettings_t3152619780 * L_3 = __this->get_mCamSettings_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		CameraSettings_t3152619780 * L_5 = __this->get_mCamSettings_2();
		Toggle_t2735377061 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = Toggle_get_isOn_m1428293607(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		CameraSettings_SwitchAutofocus_m1104105893(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void MenuOptions::ToggleTorch()
extern "C"  void MenuOptions_ToggleTorch_m1503051540 (MenuOptions_t1951716431 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuOptions_ToggleTorch_m1503051540_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Toggle_t2735377061 * V_0 = NULL;
	{
		Toggle_t2735377061 * L_0 = MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125(__this, _stringLiteral351316669, /*hidden argument*/MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125_RuntimeMethod_var);
		V_0 = L_0;
		Toggle_t2735377061 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0049;
		}
	}
	{
		CameraSettings_t3152619780 * L_3 = __this->get_mCamSettings_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0049;
		}
	}
	{
		CameraSettings_t3152619780 * L_5 = __this->get_mCamSettings_2();
		Toggle_t2735377061 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = Toggle_get_isOn_m1428293607(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		CameraSettings_SwitchFlashTorch_m1540789541(L_5, L_7, /*hidden argument*/NULL);
		Toggle_t2735377061 * L_8 = V_0;
		CameraSettings_t3152619780 * L_9 = __this->get_mCamSettings_2();
		NullCheck(L_9);
		bool L_10 = CameraSettings_IsFlashTorchEnabled_m3875252701(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		Toggle_set_isOn_m3548357404(L_8, L_10, /*hidden argument*/NULL);
	}

IL_0049:
	{
		return;
	}
}
// System.Void MenuOptions::SelectCamera(System.Boolean)
extern "C"  void MenuOptions_SelectCamera_m2158115138 (MenuOptions_t1951716431 * __this, bool ___front0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuOptions_SelectCamera_m2158115138_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Toggle_t2735377061 * V_0 = NULL;
	CameraSettings_t3152619780 * G_B3_0 = NULL;
	CameraSettings_t3152619780 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	CameraSettings_t3152619780 * G_B4_1 = NULL;
	{
		CameraSettings_t3152619780 * L_0 = __this->get_mCamSettings_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0056;
		}
	}
	{
		CameraSettings_t3152619780 * L_2 = __this->get_mCamSettings_2();
		bool L_3 = ___front0;
		G_B2_0 = L_2;
		if (!L_3)
		{
			G_B3_0 = L_2;
			goto IL_0022;
		}
	}
	{
		G_B4_0 = 2;
		G_B4_1 = G_B2_0;
		goto IL_0023;
	}

IL_0022:
	{
		G_B4_0 = 1;
		G_B4_1 = G_B3_0;
	}

IL_0023:
	{
		NullCheck(G_B4_1);
		CameraSettings_SelectCamera_m387205834(G_B4_1, G_B4_0, /*hidden argument*/NULL);
		Toggle_t2735377061 * L_4 = MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125(__this, _stringLiteral351316669, /*hidden argument*/MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125_RuntimeMethod_var);
		V_0 = L_4;
		bool L_5 = ___front0;
		if (!L_5)
		{
			goto IL_0056;
		}
	}
	{
		Toggle_t2735377061 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		Toggle_t2735377061 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = Toggle_get_isOn_m1428293607(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0056;
		}
	}
	{
		MenuOptions_ToggleTorch_m1503051540(__this, /*hidden argument*/NULL);
	}

IL_0056:
	{
		return;
	}
}
// System.Void MenuOptions::ToggleExtendedTracking()
extern "C"  void MenuOptions_ToggleExtendedTracking_m3362773293 (MenuOptions_t1951716431 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuOptions_ToggleExtendedTracking_m3362773293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Toggle_t2735377061 * V_0 = NULL;
	{
		Toggle_t2735377061 * L_0 = MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125(__this, _stringLiteral1531511922, /*hidden argument*/MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125_RuntimeMethod_var);
		V_0 = L_0;
		Toggle_t2735377061 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0038;
		}
	}
	{
		TrackableSettings_t2862243993 * L_3 = __this->get_mTrackableSettings_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		TrackableSettings_t2862243993 * L_5 = __this->get_mTrackableSettings_3();
		Toggle_t2735377061 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = Toggle_get_isOn_m1428293607(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< bool >::Invoke(4 /* System.Void TrackableSettings::SwitchExtendedTracking(System.Boolean) */, L_5, L_7);
	}

IL_0038:
	{
		return;
	}
}
// System.Void MenuOptions::ActivateDataset(System.String)
extern "C"  void MenuOptions_ActivateDataset_m942511626 (MenuOptions_t1951716431 * __this, String_t* ___datasetName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuOptions_ActivateDataset_m942511626_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableSettings_t2862243993 * L_0 = __this->get_mTrackableSettings_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		TrackableSettings_t2862243993 * L_2 = __this->get_mTrackableSettings_3();
		String_t* L_3 = ___datasetName0;
		NullCheck(L_2);
		TrackableSettings_ActivateDataSet_m4118878993(L_2, L_3, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void MenuOptions::UpdateUI()
extern "C"  void MenuOptions_UpdateUI_m2695051465 (MenuOptions_t1951716431 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuOptions_UpdateUI_m2695051465_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Toggle_t2735377061 * V_0 = NULL;
	Toggle_t2735377061 * V_1 = NULL;
	Toggle_t2735377061 * V_2 = NULL;
	Toggle_t2735377061 * V_3 = NULL;
	Toggle_t2735377061 * V_4 = NULL;
	Toggle_t2735377061 * V_5 = NULL;
	Toggle_t2735377061 * V_6 = NULL;
	{
		Toggle_t2735377061 * L_0 = MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125(__this, _stringLiteral1531511922, /*hidden argument*/MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125_RuntimeMethod_var);
		V_0 = L_0;
		Toggle_t2735377061 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0038;
		}
	}
	{
		TrackableSettings_t2862243993 * L_3 = __this->get_mTrackableSettings_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		Toggle_t2735377061 * L_5 = V_0;
		TrackableSettings_t2862243993 * L_6 = __this->get_mTrackableSettings_3();
		NullCheck(L_6);
		bool L_7 = TrackableSettings_IsExtendedTrackingEnabled_m1319587722(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Toggle_set_isOn_m3548357404(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0038:
	{
		Toggle_t2735377061 * L_8 = MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125(__this, _stringLiteral351316669, /*hidden argument*/MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125_RuntimeMethod_var);
		V_1 = L_8;
		Toggle_t2735377061 * L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0070;
		}
	}
	{
		CameraSettings_t3152619780 * L_11 = __this->get_mCamSettings_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		Toggle_t2735377061 * L_13 = V_1;
		CameraSettings_t3152619780 * L_14 = __this->get_mCamSettings_2();
		NullCheck(L_14);
		bool L_15 = CameraSettings_IsFlashTorchEnabled_m3875252701(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Toggle_set_isOn_m3548357404(L_13, L_15, /*hidden argument*/NULL);
	}

IL_0070:
	{
		Toggle_t2735377061 * L_16 = MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125(__this, _stringLiteral683363693, /*hidden argument*/MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125_RuntimeMethod_var);
		V_2 = L_16;
		Toggle_t2735377061 * L_17 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00a8;
		}
	}
	{
		CameraSettings_t3152619780 * L_19 = __this->get_mCamSettings_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00a8;
		}
	}
	{
		Toggle_t2735377061 * L_21 = V_2;
		CameraSettings_t3152619780 * L_22 = __this->get_mCamSettings_2();
		NullCheck(L_22);
		bool L_23 = CameraSettings_IsAutofocusEnabled_m386305083(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		Toggle_set_isOn_m3548357404(L_21, L_23, /*hidden argument*/NULL);
	}

IL_00a8:
	{
		Toggle_t2735377061 * L_24 = MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125(__this, _stringLiteral3549741419, /*hidden argument*/MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125_RuntimeMethod_var);
		V_3 = L_24;
		Toggle_t2735377061 * L_25 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_26 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00e0;
		}
	}
	{
		CameraSettings_t3152619780 * L_27 = __this->get_mCamSettings_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_28 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00e0;
		}
	}
	{
		Toggle_t2735377061 * L_29 = V_3;
		CameraSettings_t3152619780 * L_30 = __this->get_mCamSettings_2();
		NullCheck(L_30);
		bool L_31 = CameraSettings_IsFrontCameraActive_m282456036(L_30, /*hidden argument*/NULL);
		NullCheck(L_29);
		Toggle_set_isOn_m3548357404(L_29, L_31, /*hidden argument*/NULL);
	}

IL_00e0:
	{
		Toggle_t2735377061 * L_32 = MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125(__this, _stringLiteral3872780051, /*hidden argument*/MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125_RuntimeMethod_var);
		V_4 = L_32;
		Toggle_t2735377061 * L_33 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_34 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_011e;
		}
	}
	{
		CameraSettings_t3152619780 * L_35 = __this->get_mCamSettings_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_36 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_011e;
		}
	}
	{
		Toggle_t2735377061 * L_37 = V_4;
		CameraSettings_t3152619780 * L_38 = __this->get_mCamSettings_2();
		NullCheck(L_38);
		bool L_39 = CameraSettings_IsFrontCameraActive_m282456036(L_38, /*hidden argument*/NULL);
		NullCheck(L_37);
		Toggle_set_isOn_m3548357404(L_37, (bool)((((int32_t)L_39) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_011e:
	{
		Toggle_t2735377061 * L_40 = MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125(__this, _stringLiteral3544302583, /*hidden argument*/MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125_RuntimeMethod_var);
		V_5 = L_40;
		Toggle_t2735377061 * L_41 = MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125(__this, _stringLiteral451598385, /*hidden argument*/MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125_RuntimeMethod_var);
		V_6 = L_41;
		TrackableSettings_t2862243993 * L_42 = __this->get_mTrackableSettings_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_43 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_01ca;
		}
	}
	{
		Toggle_t2735377061 * L_44 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_45 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_0181;
		}
	}
	{
		Toggle_t2735377061 * L_46 = V_5;
		NullCheck(L_46);
		GameObject_t1113636619 * L_47 = Component_get_gameObject_m442555142(L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		bool L_48 = GameObject_get_activeInHierarchy_m2006396688(L_47, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_0181;
		}
	}
	{
		Toggle_t2735377061 * L_49 = V_5;
		TrackableSettings_t2862243993 * L_50 = __this->get_mTrackableSettings_3();
		NullCheck(L_50);
		String_t* L_51 = TrackableSettings_GetActiveDatasetName_m1771968640(L_50, /*hidden argument*/NULL);
		NullCheck(L_51);
		bool L_52 = String_Contains_m1147431944(L_51, _stringLiteral3544302583, /*hidden argument*/NULL);
		NullCheck(L_49);
		Toggle_set_isOn_m3548357404(L_49, L_52, /*hidden argument*/NULL);
	}

IL_0181:
	{
		Toggle_t2735377061 * L_53 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_54 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_01ca;
		}
	}
	{
		TrackableSettings_t2862243993 * L_55 = __this->get_mTrackableSettings_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_56 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_01ca;
		}
	}
	{
		Toggle_t2735377061 * L_57 = V_6;
		NullCheck(L_57);
		GameObject_t1113636619 * L_58 = Component_get_gameObject_m442555142(L_57, /*hidden argument*/NULL);
		NullCheck(L_58);
		bool L_59 = GameObject_get_activeInHierarchy_m2006396688(L_58, /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_01ca;
		}
	}
	{
		Toggle_t2735377061 * L_60 = V_6;
		TrackableSettings_t2862243993 * L_61 = __this->get_mTrackableSettings_3();
		NullCheck(L_61);
		String_t* L_62 = TrackableSettings_GetActiveDatasetName_m1771968640(L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		bool L_63 = String_Contains_m1147431944(L_62, _stringLiteral451598385, /*hidden argument*/NULL);
		NullCheck(L_60);
		Toggle_set_isOn_m3548357404(L_60, L_63, /*hidden argument*/NULL);
	}

IL_01ca:
	{
		return;
	}
}
// System.Void MenuOptions::CloseMenu()
extern "C"  void MenuOptions_CloseMenu_m2198615225 (MenuOptions_t1951716431 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuOptions_CloseMenu_m2198615225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MenuAnimator_t2112910832 * L_0 = __this->get_mMenuAnim_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		MenuAnimator_t2112910832 * L_2 = __this->get_mMenuAnim_4();
		NullCheck(L_2);
		MenuAnimator_Hide_m1323916210(L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void MenuOptions::OnPaused(System.Boolean)
extern "C"  void MenuOptions_OnPaused_m1134986695 (MenuOptions_t1951716431 * __this, bool ___paused0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuOptions_OnPaused_m1134986695_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Toggle_t2735377061 * V_1 = NULL;
	{
		bool L_0 = ___paused0;
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0034;
		}
	}
	{
		Toggle_t2735377061 * L_2 = MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125(__this, _stringLiteral351316669, /*hidden argument*/MenuOptions_FindUISelectableWithText_TisToggle_t2735377061_m4067184125_RuntimeMethod_var);
		V_1 = L_2;
		Toggle_t2735377061 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		Toggle_t2735377061 * L_5 = V_1;
		CameraSettings_t3152619780 * L_6 = __this->get_mCamSettings_2();
		NullCheck(L_6);
		bool L_7 = CameraSettings_IsFlashTorchEnabled_m3875252701(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Toggle_set_isOn_m3548357404(L_5, L_7, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ModelSwap::.ctor()
extern "C"  void ModelSwap__ctor_m414347635 (ModelSwap_t1632145241 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ModelSwap::Start()
extern "C"  void ModelSwap_Start_m1562037199 (ModelSwap_t1632145241 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ModelSwap_Start_m1562037199_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = Transform_Find_m1729760951(L_0, _stringLiteral3058346340, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(L_1, /*hidden argument*/NULL);
		__this->set_mDefaultModel_2(L_2);
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t3600365921 * L_4 = Transform_Find_m1729760951(L_3, _stringLiteral3974699883, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(L_4, /*hidden argument*/NULL);
		__this->set_mExtTrackedModel_3(L_5);
		GameObject_t1113636619 * L_6 = __this->get_mDefaultModel_2();
		__this->set_mActiveModel_4(L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		TrackableSettings_t2862243993 * L_7 = Object_FindObjectOfType_TisTrackableSettings_t2862243993_m1852430619(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisTrackableSettings_t2862243993_m1852430619_RuntimeMethod_var);
		__this->set_mTrackableSettings_5(L_7);
		return;
	}
}
// System.Void ModelSwap::Update()
extern "C"  void ModelSwap_Update_m1133952060 (ModelSwap_t1632145241 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ModelSwap_Update_m1133952060_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackableSettings_t2862243993 * L_0 = __this->get_mTrackableSettings_5();
		NullCheck(L_0);
		bool L_1 = TrackableSettings_IsExtendedTrackingEnabled_m1319587722(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		GameObject_t1113636619 * L_2 = __this->get_mActiveModel_4();
		GameObject_t1113636619 * L_3 = __this->get_mDefaultModel_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004f;
		}
	}
	{
		GameObject_t1113636619 * L_5 = __this->get_mDefaultModel_2();
		NullCheck(L_5);
		GameObject_SetActive_m796801857(L_5, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_6 = __this->get_mExtTrackedModel_3();
		NullCheck(L_6);
		GameObject_SetActive_m796801857(L_6, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_7 = __this->get_mExtTrackedModel_3();
		__this->set_mActiveModel_4(L_7);
		goto IL_0099;
	}

IL_004f:
	{
		TrackableSettings_t2862243993 * L_8 = __this->get_mTrackableSettings_5();
		NullCheck(L_8);
		bool L_9 = TrackableSettings_IsExtendedTrackingEnabled_m1319587722(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0099;
		}
	}
	{
		GameObject_t1113636619 * L_10 = __this->get_mActiveModel_4();
		GameObject_t1113636619 * L_11 = __this->get_mExtTrackedModel_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0099;
		}
	}
	{
		GameObject_t1113636619 * L_13 = __this->get_mExtTrackedModel_3();
		NullCheck(L_13);
		GameObject_SetActive_m796801857(L_13, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_14 = __this->get_mDefaultModel_2();
		NullCheck(L_14);
		GameObject_SetActive_m796801857(L_14, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_15 = __this->get_mDefaultModel_2();
		__this->set_mActiveModel_4(L_15);
	}

IL_0099:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void onScanController::.ctor()
extern "C"  void onScanController__ctor_m2347946813 (onScanController_t1472212342 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void onScanController::Awake()
extern "C"  void onScanController_Awake_m195501164 (onScanController_t1472212342 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (onScanController_Awake_m195501164_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	characterSlot_t990761438 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(onScanController_t1472212342_il2cpp_TypeInfo_var);
		((onScanController_t1472212342_StaticFields*)il2cpp_codegen_static_fields_for(onScanController_t1472212342_il2cpp_TypeInfo_var))->set_someoneIsPlaying_11((bool)0);
		GameObject_t1113636619 * L_0 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral778478698, /*hidden argument*/NULL);
		__this->set_uiCanvas_7(L_0);
		GameObject_t1113636619 * L_1 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral3670285835, /*hidden argument*/NULL);
		__this->set_speechText_8(L_1);
		GameObject_t1113636619 * L_2 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral2682697484, /*hidden argument*/NULL);
		__this->set_speechSymbols_9(L_2);
		GameObject_t1113636619 * L_3 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral321671482, /*hidden argument*/NULL);
		NullCheck(L_3);
		characterLoader_t1326495876 * L_4 = GameObject_GetComponent_TischaracterLoader_t1326495876_m1035246574(L_3, /*hidden argument*/GameObject_GetComponent_TischaracterLoader_t1326495876_m1035246574_RuntimeMethod_var);
		__this->set_CharacterLoader_2(L_4);
		V_0 = (characterSlot_t990761438 *)NULL;
		V_1 = 0;
		goto IL_0088;
	}

IL_0054:
	{
		characterLoader_t1326495876 * L_5 = __this->get_CharacterLoader_2();
		NullCheck(L_5);
		characterSlotU5BU5D_t2121073899* L_6 = L_5->get_characterList_2();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		characterSlot_t990761438 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_9);
		String_t* L_10 = L_9->get_CharacterID_0();
		String_t* L_11 = __this->get_CharacterID_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m920492651(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0084;
		}
	}
	{
		characterLoader_t1326495876 * L_13 = __this->get_CharacterLoader_2();
		NullCheck(L_13);
		characterSlotU5BU5D_t2121073899* L_14 = L_13->get_characterList_2();
		int32_t L_15 = V_1;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		characterSlot_t990761438 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		V_0 = L_17;
	}

IL_0084:
	{
		int32_t L_18 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_0088:
	{
		int32_t L_19 = V_1;
		characterLoader_t1326495876 * L_20 = __this->get_CharacterLoader_2();
		NullCheck(L_20);
		characterSlotU5BU5D_t2121073899* L_21 = L_20->get_characterList_2();
		NullCheck(L_21);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_21)->max_length)))))))
		{
			goto IL_0054;
		}
	}
	{
		String_t* L_22 = __this->get_CharacterID_3();
		if (!L_22)
		{
			goto IL_00be;
		}
	}
	{
		characterSlot_t990761438 * L_23 = V_0;
		NullCheck(L_23);
		GameObject_t1113636619 * L_24 = L_23->get_CharacterPrefab_1();
		Transform_t3600365921 * L_25 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_26 = Object_Instantiate_TisGameObject_t1113636619_m4130575780(NULL /*static, unused*/, L_24, L_25, (bool)0, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4130575780_RuntimeMethod_var);
		__this->set_currentCharacter_6(L_26);
	}

IL_00be:
	{
		GameObject_t1113636619 * L_27 = __this->get_currentCharacter_6();
		NullCheck(L_27);
		Animation_t3648466861 * L_28 = GameObject_GetComponentInChildren_TisAnimation_t3648466861_m235625869(L_27, /*hidden argument*/GameObject_GetComponentInChildren_TisAnimation_t3648466861_m235625869_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_29 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_28, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00e5;
		}
	}
	{
		GameObject_t1113636619 * L_30 = __this->get_currentCharacter_6();
		NullCheck(L_30);
		Animation_t3648466861 * L_31 = GameObject_GetComponentInChildren_TisAnimation_t3648466861_m235625869(L_30, /*hidden argument*/GameObject_GetComponentInChildren_TisAnimation_t3648466861_m235625869_RuntimeMethod_var);
		__this->set_mouthAnim_10(L_31);
	}

IL_00e5:
	{
		return;
	}
}
// System.Void onScanController::Update()
extern "C"  void onScanController_Update_m190167415 (onScanController_t1472212342 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (onScanController_Update_m190167415_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		AudioSource_t3935305588 * L_1 = GameObject_GetComponent_TisAudioSource_t3935305588_m625814604(L_0, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t3935305588_m625814604_RuntimeMethod_var);
		NullCheck(L_1);
		bool L_2 = AudioSource_get_isPlaying_m1896551654(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0057;
		}
	}
	{
		GameObject_t1113636619 * L_3 = __this->get_currentCharacter_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0057;
		}
	}
	{
		GameObject_t1113636619 * L_5 = __this->get_currentCharacter_6();
		NullCheck(L_5);
		Animator_t434523843 * L_6 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_6, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0057;
		}
	}
	{
		GameObject_t1113636619 * L_8 = __this->get_currentCharacter_6();
		NullCheck(L_8);
		Animator_t434523843 * L_9 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_8, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_9);
		Animator_SetBool_m234840832(L_9, _stringLiteral4045970737, (bool)1, /*hidden argument*/NULL);
		goto IL_00a9;
	}

IL_0057:
	{
		GameObject_t1113636619 * L_10 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		AudioSource_t3935305588 * L_11 = GameObject_GetComponent_TisAudioSource_t3935305588_m625814604(L_10, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t3935305588_m625814604_RuntimeMethod_var);
		NullCheck(L_11);
		bool L_12 = AudioSource_get_isPlaying_m1896551654(L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_00a9;
		}
	}
	{
		GameObject_t1113636619 * L_13 = __this->get_currentCharacter_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_13, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00a9;
		}
	}
	{
		GameObject_t1113636619 * L_15 = __this->get_currentCharacter_6();
		NullCheck(L_15);
		Animator_t434523843 * L_16 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_15, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_16, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00a9;
		}
	}
	{
		GameObject_t1113636619 * L_18 = __this->get_currentCharacter_6();
		NullCheck(L_18);
		Animator_t434523843 * L_19 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_18, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_19);
		Animator_SetBool_m234840832(L_19, _stringLiteral4045970737, (bool)0, /*hidden argument*/NULL);
	}

IL_00a9:
	{
		bool L_20 = __this->get_imPlaying_12();
		if (!L_20)
		{
			goto IL_00dd;
		}
	}
	{
		GameObject_t1113636619 * L_21 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		AudioSource_t3935305588 * L_22 = GameObject_GetComponent_TisAudioSource_t3935305588_m625814604(L_21, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t3935305588_m625814604_RuntimeMethod_var);
		NullCheck(L_22);
		bool L_23 = AudioSource_get_isPlaying_m1896551654(L_22, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_00dd;
		}
	}
	{
		__this->set_imPlaying_12((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(onScanController_t1472212342_il2cpp_TypeInfo_var);
		((onScanController_t1472212342_StaticFields*)il2cpp_codegen_static_fields_for(onScanController_t1472212342_il2cpp_TypeInfo_var))->set_someoneIsPlaying_11((bool)0);
		Behaviour_set_enabled_m20417929(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_00dd:
	{
		return;
	}
}
// System.Void onScanController::OnEnable()
extern "C"  void onScanController_OnEnable_m2731938639 (onScanController_t1472212342 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (onScanController_OnEnable_m2731938639_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	Transform_t3600365921 * V_3 = NULL;
	RuntimeObject* V_4 = NULL;
	RuntimeObject* V_5 = NULL;
	GameObject_t1113636619 * V_6 = NULL;
	Vector2_t2156229523  V_7;
	memset(&V_7, 0, sizeof(V_7));
	int32_t V_8 = 0;
	float V_9 = 0.0f;
	Rect_t2360479859  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t3722313464  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(onScanController_t1472212342_il2cpp_TypeInfo_var);
		bool L_0 = ((onScanController_t1472212342_StaticFields*)il2cpp_codegen_static_fields_for(onScanController_t1472212342_il2cpp_TypeInfo_var))->get_someoneIsPlaying_11();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		Behaviour_set_enabled_m20417929(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_0012:
	{
		GameObject_t1113636619 * L_1 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral537141514, /*hidden argument*/NULL);
		NullCheck(L_1);
		settingsManager_t2086358119 * L_2 = GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329(L_1, /*hidden argument*/GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329_RuntimeMethod_var);
		NullCheck(L_2);
		List_1_t3355509319 * L_3 = L_2->get_settingslist_2();
		GameObject_t1113636619 * L_4 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral537141514, /*hidden argument*/NULL);
		NullCheck(L_4);
		settingsManager_t2086358119 * L_5 = GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329(L_4, /*hidden argument*/GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329_RuntimeMethod_var);
		NullCheck(L_5);
		int32_t L_6 = settingsManager_findSetting_m2352407313(L_5, _stringLiteral2952584548, /*hidden argument*/NULL);
		NullCheck(L_3);
		SettingsList_t1883434577 * L_7 = List_1_get_Item_m3797380788(L_3, L_6, /*hidden argument*/List_1_get_Item_m3797380788_RuntimeMethod_var);
		NullCheck(L_7);
		bool L_8 = L_7->get_IsActive_1();
		V_0 = L_8;
		GameObject_t1113636619 * L_9 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral537141514, /*hidden argument*/NULL);
		NullCheck(L_9);
		settingsManager_t2086358119 * L_10 = GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329(L_9, /*hidden argument*/GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329_RuntimeMethod_var);
		NullCheck(L_10);
		List_1_t3355509319 * L_11 = L_10->get_settingslist_2();
		GameObject_t1113636619 * L_12 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral537141514, /*hidden argument*/NULL);
		NullCheck(L_12);
		settingsManager_t2086358119 * L_13 = GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329(L_12, /*hidden argument*/GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329_RuntimeMethod_var);
		NullCheck(L_13);
		int32_t L_14 = settingsManager_findSetting_m2352407313(L_13, _stringLiteral3987835886, /*hidden argument*/NULL);
		NullCheck(L_11);
		SettingsList_t1883434577 * L_15 = List_1_get_Item_m3797380788(L_11, L_14, /*hidden argument*/List_1_get_Item_m3797380788_RuntimeMethod_var);
		NullCheck(L_15);
		bool L_16 = L_15->get_IsActive_1();
		V_1 = L_16;
		GameObject_t1113636619 * L_17 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral537141514, /*hidden argument*/NULL);
		NullCheck(L_17);
		settingsManager_t2086358119 * L_18 = GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329(L_17, /*hidden argument*/GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329_RuntimeMethod_var);
		NullCheck(L_18);
		List_1_t3355509319 * L_19 = L_18->get_settingslist_2();
		GameObject_t1113636619 * L_20 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral537141514, /*hidden argument*/NULL);
		NullCheck(L_20);
		settingsManager_t2086358119 * L_21 = GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329(L_20, /*hidden argument*/GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329_RuntimeMethod_var);
		NullCheck(L_21);
		int32_t L_22 = settingsManager_findSetting_m2352407313(L_21, _stringLiteral2002597352, /*hidden argument*/NULL);
		NullCheck(L_19);
		SettingsList_t1883434577 * L_23 = List_1_get_Item_m3797380788(L_19, L_22, /*hidden argument*/List_1_get_Item_m3797380788_RuntimeMethod_var);
		NullCheck(L_23);
		bool L_24 = L_23->get_IsActive_1();
		V_2 = L_24;
		bool L_25 = V_0;
		if (L_25)
		{
			goto IL_00c6;
		}
	}
	{
		bool L_26 = V_1;
		if (!L_26)
		{
			goto IL_00e8;
		}
	}

IL_00c6:
	{
		GameObject_t1113636619 * L_27 = __this->get_uiCanvas_7();
		NullCheck(L_27);
		Canvas_t3310196443 * L_28 = GameObject_GetComponent_TisCanvas_t3310196443_m1808013672(L_27, /*hidden argument*/GameObject_GetComponent_TisCanvas_t3310196443_m1808013672_RuntimeMethod_var);
		NullCheck(L_28);
		Behaviour_set_enabled_m20417929(L_28, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_29 = __this->get_uiCanvas_7();
		NullCheck(L_29);
		Animation_t3648466861 * L_30 = GameObject_GetComponent_TisAnimation_t3648466861_m3766528019(L_29, /*hidden argument*/GameObject_GetComponent_TisAnimation_t3648466861_m3766528019_RuntimeMethod_var);
		NullCheck(L_30);
		Animation_Play_m2798573761(L_30, /*hidden argument*/NULL);
	}

IL_00e8:
	{
		GameObject_t1113636619 * L_31 = __this->get_speechText_8();
		NullCheck(L_31);
		Text_t1901882714 * L_32 = GameObject_GetComponent_TisText_t1901882714_m2114913816(L_31, /*hidden argument*/GameObject_GetComponent_TisText_t1901882714_m2114913816_RuntimeMethod_var);
		String_t* L_33 = __this->get_scanText_4();
		NullCheck(L_32);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_32, L_33);
		GameObject_t1113636619 * L_34 = __this->get_speechSymbols_9();
		NullCheck(L_34);
		Transform_t3600365921 * L_35 = GameObject_get_transform_m1369836730(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		RuntimeObject* L_36 = Transform_GetEnumerator_m2717073726(L_35, /*hidden argument*/NULL);
		V_4 = L_36;
	}

IL_0110:
	try
	{ // begin try (depth: 1)
		{
			goto IL_012d;
		}

IL_0115:
		{
			RuntimeObject* L_37 = V_4;
			NullCheck(L_37);
			RuntimeObject * L_38 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_37);
			V_3 = ((Transform_t3600365921 *)CastclassClass((RuntimeObject*)L_38, Transform_t3600365921_il2cpp_TypeInfo_var));
			Transform_t3600365921 * L_39 = V_3;
			NullCheck(L_39);
			GameObject_t1113636619 * L_40 = Component_get_gameObject_m442555142(L_39, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
			Object_Destroy_m565254235(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		}

IL_012d:
		{
			RuntimeObject* L_41 = V_4;
			NullCheck(L_41);
			bool L_42 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_41);
			if (L_42)
			{
				goto IL_0115;
			}
		}

IL_0139:
		{
			IL2CPP_LEAVE(0x155, FINALLY_013e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_013e;
	}

FINALLY_013e:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_43 = V_4;
			RuntimeObject* L_44 = ((RuntimeObject*)IsInst((RuntimeObject*)L_43, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			V_5 = L_44;
			if (!L_44)
			{
				goto IL_0154;
			}
		}

IL_014d:
		{
			RuntimeObject* L_45 = V_5;
			NullCheck(L_45);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_45);
		}

IL_0154:
		{
			IL2CPP_END_FINALLY(318)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(318)
	{
		IL2CPP_JUMP_TBL(0x155, IL_0155)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0155:
	{
		V_8 = 0;
		goto IL_020a;
	}

IL_015d:
	{
		GameObject_t1113636619 * L_46 = __this->get_uiCanvas_7();
		NullCheck(L_46);
		symbolPrefabDefiner_t2798204667 * L_47 = GameObject_GetComponent_TissymbolPrefabDefiner_t2798204667_m4248518593(L_46, /*hidden argument*/GameObject_GetComponent_TissymbolPrefabDefiner_t2798204667_m4248518593_RuntimeMethod_var);
		NullCheck(L_47);
		GameObject_t1113636619 * L_48 = L_47->get_symbolPrefab_2();
		GameObject_t1113636619 * L_49 = __this->get_speechSymbols_9();
		NullCheck(L_49);
		Transform_t3600365921 * L_50 = GameObject_get_transform_m1369836730(L_49, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_51 = Object_Instantiate_TisGameObject_t1113636619_m3215236302(NULL /*static, unused*/, L_48, L_50, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3215236302_RuntimeMethod_var);
		V_6 = L_51;
		GameObject_t1113636619 * L_52 = V_6;
		NullCheck(L_52);
		RectTransform_t3704657025 * L_53 = GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398(L_52, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398_RuntimeMethod_var);
		NullCheck(L_53);
		Rect_t2360479859  L_54 = RectTransform_get_rect_m574169965(L_53, /*hidden argument*/NULL);
		V_10 = L_54;
		float L_55 = Rect_get_height_m1358425599((Rect_t2360479859 *)(&V_10), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_56 = V_6;
		NullCheck(L_56);
		RectTransform_t3704657025 * L_57 = GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398(L_56, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398_RuntimeMethod_var);
		NullCheck(L_57);
		Vector3_t3722313464  L_58 = Transform_get_localScale_m129152068(L_57, /*hidden argument*/NULL);
		V_11 = L_58;
		float L_59 = (&V_11)->get_y_2();
		V_9 = ((float)il2cpp_codegen_multiply((float)L_55, (float)L_59));
		float L_60 = V_9;
		float L_61 = V_9;
		int32_t L_62 = V_8;
		float L_63 = V_9;
		SpriteU5BU5D_t2581906349* L_64 = __this->get_symbols_5();
		NullCheck(L_64);
		(&V_7)->set_x_0(((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_60, (float)(0.5f))), (float)((float)il2cpp_codegen_multiply((float)L_61, (float)(((float)((float)L_62))))))), (float)((float)((float)((float)il2cpp_codegen_multiply((float)L_63, (float)(((float)((float)(((int32_t)((int32_t)(((RuntimeArray *)L_64)->max_length)))))))))/(float)(2.0f))))));
		(&V_7)->set_y_1((0.0f));
		GameObject_t1113636619 * L_65 = V_6;
		NullCheck(L_65);
		RectTransform_t3704657025 * L_66 = GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398(L_65, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398_RuntimeMethod_var);
		Vector2_t2156229523  L_67 = V_7;
		NullCheck(L_66);
		RectTransform_set_anchoredPosition_m4126691837(L_66, L_67, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_68 = V_6;
		NullCheck(L_68);
		Image_t2670269651 * L_69 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_68, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		SpriteU5BU5D_t2581906349* L_70 = __this->get_symbols_5();
		int32_t L_71 = V_8;
		NullCheck(L_70);
		int32_t L_72 = L_71;
		Sprite_t280657092 * L_73 = (L_70)->GetAt(static_cast<il2cpp_array_size_t>(L_72));
		NullCheck(L_69);
		Image_set_sprite_m2369174689(L_69, L_73, /*hidden argument*/NULL);
		int32_t L_74 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_74, (int32_t)1));
	}

IL_020a:
	{
		int32_t L_75 = V_8;
		SpriteU5BU5D_t2581906349* L_76 = __this->get_symbols_5();
		NullCheck(L_76);
		if ((((int32_t)L_75) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_76)->max_length)))))))
		{
			goto IL_015d;
		}
	}
	{
		GameObject_t1113636619 * L_77 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_77);
		AudioSource_t3935305588 * L_78 = GameObject_GetComponent_TisAudioSource_t3935305588_m625814604(L_77, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t3935305588_m625814604_RuntimeMethod_var);
		NullCheck(L_78);
		bool L_79 = AudioSource_get_isPlaying_m1896551654(L_78, /*hidden argument*/NULL);
		if (L_79)
		{
			goto IL_0251;
		}
	}
	{
		bool L_80 = V_2;
		if (!L_80)
		{
			goto IL_0251;
		}
	}
	{
		GameObject_t1113636619 * L_81 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_81);
		AudioSource_t3935305588 * L_82 = GameObject_GetComponent_TisAudioSource_t3935305588_m625814604(L_81, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t3935305588_m625814604_RuntimeMethod_var);
		NullCheck(L_82);
		AudioSource_Play_m48294159(L_82, /*hidden argument*/NULL);
		__this->set_imPlaying_12((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(onScanController_t1472212342_il2cpp_TypeInfo_var);
		((onScanController_t1472212342_StaticFields*)il2cpp_codegen_static_fields_for(onScanController_t1472212342_il2cpp_TypeInfo_var))->set_someoneIsPlaying_11((bool)1);
	}

IL_0251:
	{
		bool L_83 = V_1;
		if (!L_83)
		{
			goto IL_026d;
		}
	}
	{
		GameObject_t1113636619 * L_84 = __this->get_speechText_8();
		NullCheck(L_84);
		Text_t1901882714 * L_85 = GameObject_GetComponent_TisText_t1901882714_m2114913816(L_84, /*hidden argument*/GameObject_GetComponent_TisText_t1901882714_m2114913816_RuntimeMethod_var);
		NullCheck(L_85);
		Behaviour_set_enabled_m20417929(L_85, (bool)1, /*hidden argument*/NULL);
		goto IL_0284;
	}

IL_026d:
	{
		bool L_86 = V_0;
		if (!L_86)
		{
			goto IL_0284;
		}
	}
	{
		GameObject_t1113636619 * L_87 = __this->get_speechSymbols_9();
		NullCheck(L_87);
		Canvas_t3310196443 * L_88 = GameObject_GetComponent_TisCanvas_t3310196443_m1808013672(L_87, /*hidden argument*/GameObject_GetComponent_TisCanvas_t3310196443_m1808013672_RuntimeMethod_var);
		NullCheck(L_88);
		Behaviour_set_enabled_m20417929(L_88, (bool)1, /*hidden argument*/NULL);
	}

IL_0284:
	{
		bool L_89 = V_2;
		if (L_89)
		{
			goto IL_0291;
		}
	}
	{
		Behaviour_set_enabled_m20417929(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_0291:
	{
		bool L_90 = __this->get_ShowTailAnimation_13();
		if (!L_90)
		{
			goto IL_02d4;
		}
	}
	{
		GameObject_t1113636619 * L_91 = __this->get_currentCharacter_6();
		NullCheck(L_91);
		Animator_t434523843 * L_92 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_91, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_92);
		Animator_SetBool_m234840832(L_92, _stringLiteral1035174073, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_93 = __this->get_currentCharacter_6();
		NullCheck(L_93);
		Animator_t434523843 * L_94 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_93, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_94);
		Animator_Play_m1697843332(L_94, _stringLiteral1628612613, /*hidden argument*/NULL);
		RuntimeObject* L_95 = onScanController_WaitAndPlayShowTail_m1087383620(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_95, /*hidden argument*/NULL);
	}

IL_02d4:
	{
		return;
	}
}
// System.Collections.IEnumerator onScanController::WaitAndPlayShowTail()
extern "C"  RuntimeObject* onScanController_WaitAndPlayShowTail_m1087383620 (onScanController_t1472212342 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (onScanController_WaitAndPlayShowTail_m1087383620_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349 * V_0 = NULL;
	{
		U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349 * L_0 = (U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349 *)il2cpp_codegen_object_new(U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349_il2cpp_TypeInfo_var);
		U3CWaitAndPlayShowTailU3Ec__Iterator0__ctor_m1252499421(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349 * L_2 = V_0;
		return L_2;
	}
}
// System.Void onScanController::.cctor()
extern "C"  void onScanController__cctor_m1086509438 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void onScanController/<WaitAndPlayShowTail>c__Iterator0::.ctor()
extern "C"  void U3CWaitAndPlayShowTailU3Ec__Iterator0__ctor_m1252499421 (U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean onScanController/<WaitAndPlayShowTail>c__Iterator0::MoveNext()
extern "C"  bool U3CWaitAndPlayShowTailU3Ec__Iterator0_MoveNext_m3400634910 (U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitAndPlayShowTailU3Ec__Iterator0_MoveNext_m3400634910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0045;
			}
		}
	}
	{
		goto IL_0098;
	}

IL_0021:
	{
		WaitForSeconds_t1699091251 * L_2 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_2, (3.0f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_0040;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0040:
	{
		goto IL_009a;
	}

IL_0045:
	{
		onScanController_t1472212342 * L_4 = __this->get_U24this_0();
		NullCheck(L_4);
		GameObject_t1113636619 * L_5 = L_4->get_currentCharacter_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_5, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0091;
		}
	}
	{
		onScanController_t1472212342 * L_7 = __this->get_U24this_0();
		NullCheck(L_7);
		GameObject_t1113636619 * L_8 = L_7->get_currentCharacter_6();
		NullCheck(L_8);
		Animator_t434523843 * L_9 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_8, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_9, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0091;
		}
	}
	{
		onScanController_t1472212342 * L_11 = __this->get_U24this_0();
		NullCheck(L_11);
		GameObject_t1113636619 * L_12 = L_11->get_currentCharacter_6();
		NullCheck(L_12);
		Animator_t434523843 * L_13 = GameObject_GetComponent_TisAnimator_t434523843_m440019408(L_12, /*hidden argument*/GameObject_GetComponent_TisAnimator_t434523843_m440019408_RuntimeMethod_var);
		NullCheck(L_13);
		Animator_SetBool_m234840832(L_13, _stringLiteral1035174073, (bool)1, /*hidden argument*/NULL);
	}

IL_0091:
	{
		__this->set_U24PC_3((-1));
	}

IL_0098:
	{
		return (bool)0;
	}

IL_009a:
	{
		return (bool)1;
	}
}
// System.Object onScanController/<WaitAndPlayShowTail>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CWaitAndPlayShowTailU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2206217087 (U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object onScanController/<WaitAndPlayShowTail>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CWaitAndPlayShowTailU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1318624475 (U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void onScanController/<WaitAndPlayShowTail>c__Iterator0::Dispose()
extern "C"  void U3CWaitAndPlayShowTailU3Ec__Iterator0_Dispose_m925566460 (U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void onScanController/<WaitAndPlayShowTail>c__Iterator0::Reset()
extern "C"  void U3CWaitAndPlayShowTailU3Ec__Iterator0_Reset_m351113022 (U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitAndPlayShowTailU3Ec__Iterator0_Reset_m351113022_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CWaitAndPlayShowTailU3Ec__Iterator0_Reset_m351113022_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void pageTurn::.ctor()
extern "C"  void pageTurn__ctor_m2167905574 (pageTurn_t132420247 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void pageTurn::Start()
extern "C"  void pageTurn_Start_m3266321620 (pageTurn_t132420247 * __this, const RuntimeMethod* method)
{
	{
		__this->set_currentPage_3(0);
		pageTurn_turnToPage_m4215582371(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void pageTurn::turnToNextPage()
extern "C"  void pageTurn_turnToNextPage_m922167039 (pageTurn_t132420247 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (pageTurn_turnToNextPage_m922167039_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2585711361 * L_0 = __this->get_Pages_2();
		int32_t L_1 = __this->get_currentPage_3();
		NullCheck(L_0);
		GameObject_t1113636619 * L_2 = List_1_get_Item_m3743125852(L_0, L_1, /*hidden argument*/List_1_get_Item_m3743125852_RuntimeMethod_var);
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_currentPage_3();
		List_1_t2585711361 * L_4 = __this->get_Pages_2();
		NullCheck(L_4);
		int32_t L_5 = List_1_get_Count_m2812834599(L_4, /*hidden argument*/List_1_get_Count_m2812834599_RuntimeMethod_var);
		if ((((int32_t)L_3) >= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_5, (int32_t)1)))))
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_6 = __this->get_currentPage_3();
		__this->set_currentPage_3(((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1)));
		goto IL_0049;
	}

IL_0042:
	{
		__this->set_currentPage_3(0);
	}

IL_0049:
	{
		List_1_t2585711361 * L_7 = __this->get_Pages_2();
		int32_t L_8 = __this->get_currentPage_3();
		NullCheck(L_7);
		GameObject_t1113636619 * L_9 = List_1_get_Item_m3743125852(L_7, L_8, /*hidden argument*/List_1_get_Item_m3743125852_RuntimeMethod_var);
		NullCheck(L_9);
		GameObject_SetActive_m796801857(L_9, (bool)1, /*hidden argument*/NULL);
		List_1_t2585711361 * L_10 = __this->get_Pages_2();
		int32_t L_11 = __this->get_currentPage_3();
		NullCheck(L_10);
		GameObject_t1113636619 * L_12 = List_1_get_Item_m3743125852(L_10, L_11, /*hidden argument*/List_1_get_Item_m3743125852_RuntimeMethod_var);
		NullCheck(L_12);
		Animation_t3648466861 * L_13 = GameObject_GetComponent_TisAnimation_t3648466861_m3766528019(L_12, /*hidden argument*/GameObject_GetComponent_TisAnimation_t3648466861_m3766528019_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_13, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00c3;
		}
	}
	{
		List_1_t2585711361 * L_15 = __this->get_Pages_2();
		int32_t L_16 = __this->get_currentPage_3();
		NullCheck(L_15);
		GameObject_t1113636619 * L_17 = List_1_get_Item_m3743125852(L_15, L_16, /*hidden argument*/List_1_get_Item_m3743125852_RuntimeMethod_var);
		NullCheck(L_17);
		Animation_t3648466861 * L_18 = GameObject_GetComponent_TisAnimation_t3648466861_m3766528019(L_17, /*hidden argument*/GameObject_GetComponent_TisAnimation_t3648466861_m3766528019_RuntimeMethod_var);
		NullCheck(L_18);
		AnimationClip_t2318505987 * L_19 = Animation_get_clip_m1019546398(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_19, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00c3;
		}
	}
	{
		List_1_t2585711361 * L_21 = __this->get_Pages_2();
		int32_t L_22 = __this->get_currentPage_3();
		NullCheck(L_21);
		GameObject_t1113636619 * L_23 = List_1_get_Item_m3743125852(L_21, L_22, /*hidden argument*/List_1_get_Item_m3743125852_RuntimeMethod_var);
		NullCheck(L_23);
		Animation_t3648466861 * L_24 = GameObject_GetComponent_TisAnimation_t3648466861_m3766528019(L_23, /*hidden argument*/GameObject_GetComponent_TisAnimation_t3648466861_m3766528019_RuntimeMethod_var);
		NullCheck(L_24);
		Animation_Play_m2798573761(L_24, /*hidden argument*/NULL);
	}

IL_00c3:
	{
		return;
	}
}
// System.Void pageTurn::turnToPage(System.Int32)
extern "C"  void pageTurn_turnToPage_m4215582371 (pageTurn_t132420247 * __this, int32_t ___page0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (pageTurn_turnToPage_m4215582371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_001d;
	}

IL_0007:
	{
		List_1_t2585711361 * L_0 = __this->get_Pages_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		GameObject_t1113636619 * L_2 = List_1_get_Item_m3743125852(L_0, L_1, /*hidden argument*/List_1_get_Item_m3743125852_RuntimeMethod_var);
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
	}

IL_001d:
	{
		int32_t L_4 = V_0;
		List_1_t2585711361 * L_5 = __this->get_Pages_2();
		NullCheck(L_5);
		int32_t L_6 = List_1_get_Count_m2812834599(L_5, /*hidden argument*/List_1_get_Count_m2812834599_RuntimeMethod_var);
		if ((((int32_t)L_4) < ((int32_t)L_6)))
		{
			goto IL_0007;
		}
	}
	{
		int32_t L_7 = ___page0;
		__this->set_currentPage_3(((int32_t)il2cpp_codegen_subtract((int32_t)L_7, (int32_t)1)));
		List_1_t2585711361 * L_8 = __this->get_Pages_2();
		int32_t L_9 = __this->get_currentPage_3();
		NullCheck(L_8);
		GameObject_t1113636619 * L_10 = List_1_get_Item_m3743125852(L_8, L_9, /*hidden argument*/List_1_get_Item_m3743125852_RuntimeMethod_var);
		NullCheck(L_10);
		GameObject_SetActive_m796801857(L_10, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void pageTurn::Update()
extern "C"  void pageTurn_Update_m1659992768 (pageTurn_t132420247 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void setSettings::.ctor()
extern "C"  void setSettings__ctor_m4109405184 (setSettings_t4116128020 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void setSettings::Start()
extern "C"  void setSettings_Start_m1838433502 (setSettings_t4116128020 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (setSettings_Start_m1838433502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral3588954345, /*hidden argument*/NULL);
		__this->set_settingsManager_2(L_0);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, _stringLiteral1836739017, /*hidden argument*/NULL);
		return;
	}
}
// System.Void setSettings::toggleSetting(System.String)
extern "C"  void setSettings_toggleSetting_m2511861156 (setSettings_t4116128020 * __this, String_t* ___calledSetting0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (setSettings_toggleSetting_m2511861156_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = __this->get_settingsManager_2();
		NullCheck(L_0);
		settingsManager_t2086358119 * L_1 = GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329(L_0, /*hidden argument*/GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329_RuntimeMethod_var);
		String_t* L_2 = ___calledSetting0;
		NullCheck(L_1);
		settingsManager_toggleSetting_m1197959296(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void setSettings::toggleSettingIfItCantCeOnAtTheSameTimeAsOtherSetting(System.String)
extern "C"  void setSettings_toggleSettingIfItCantCeOnAtTheSameTimeAsOtherSetting_m3029310343 (setSettings_t4116128020 * __this, String_t* ___calledSetting0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (setSettings_toggleSettingIfItCantCeOnAtTheSameTimeAsOtherSetting_m3029310343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___calledSetting0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m920492651(NULL /*static, unused*/, L_0, _stringLiteral3987835886, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		bool L_2 = setSettings_checkIfOn_m1363969882(__this, _stringLiteral3987835886, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0040;
		}
	}

IL_0020:
	{
		String_t* L_3 = ___calledSetting0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m920492651(NULL /*static, unused*/, L_3, _stringLiteral2952584548, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0041;
		}
	}
	{
		bool L_5 = setSettings_checkIfOn_m1363969882(__this, _stringLiteral2952584548, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0041;
		}
	}

IL_0040:
	{
		return;
	}

IL_0041:
	{
		GameObject_t1113636619 * L_6 = __this->get_settingsManager_2();
		NullCheck(L_6);
		settingsManager_t2086358119 * L_7 = GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329(L_6, /*hidden argument*/GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329_RuntimeMethod_var);
		String_t* L_8 = ___calledSetting0;
		NullCheck(L_7);
		settingsManager_toggleSetting_m1197959296(L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean setSettings::checkIfOn(System.String)
extern "C"  bool setSettings_checkIfOn_m1363969882 (setSettings_t4116128020 * __this, String_t* ___calledSetting0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (setSettings_checkIfOn_m1363969882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral537141514, /*hidden argument*/NULL);
		NullCheck(L_0);
		settingsManager_t2086358119 * L_1 = GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329(L_0, /*hidden argument*/GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329_RuntimeMethod_var);
		NullCheck(L_1);
		List_1_t3355509319 * L_2 = L_1->get_settingslist_2();
		GameObject_t1113636619 * L_3 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral537141514, /*hidden argument*/NULL);
		NullCheck(L_3);
		settingsManager_t2086358119 * L_4 = GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329(L_3, /*hidden argument*/GameObject_GetComponent_TissettingsManager_t2086358119_m3437422329_RuntimeMethod_var);
		String_t* L_5 = ___calledSetting0;
		NullCheck(L_4);
		int32_t L_6 = settingsManager_findSetting_m2352407313(L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		SettingsList_t1883434577 * L_7 = List_1_get_Item_m3797380788(L_2, L_6, /*hidden argument*/List_1_get_Item_m3797380788_RuntimeMethod_var);
		NullCheck(L_7);
		bool L_8 = L_7->get_IsActive_1();
		if (!L_8)
		{
			goto IL_003a;
		}
	}
	{
		return (bool)1;
	}

IL_003a:
	{
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SettingsList::.ctor(System.String,System.Boolean)
extern "C"  void SettingsList__ctor_m385606149 (SettingsList_t1883434577 * __this, String_t* ___newSettingName0, bool ___newIsActive1, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___newSettingName0;
		__this->set_SettingName_0(L_0);
		bool L_1 = ___newIsActive1;
		__this->set_IsActive_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void settingsManager::.ctor()
extern "C"  void settingsManager__ctor_m3188189104 (settingsManager_t2086358119 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void settingsManager::Awake()
extern "C"  void settingsManager_Awake_m2253256640 (settingsManager_t2086358119 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (settingsManager_Awake_m2253256640_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m166252750(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_0 = GameObject_FindGameObjectsWithTag_m2585173894(NULL /*static, unused*/, _stringLiteral3588954345, /*hidden argument*/NULL);
		NullCheck(L_0);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_0028;
		}
	}
	{
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_0038;
	}

IL_0028:
	{
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_set_tag_m2353670106(L_2, _stringLiteral3588954345, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void settingsManager::Start()
extern "C"  void settingsManager_Start_m3801860366 (settingsManager_t2086358119 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (settingsManager_Start_m3801860366_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioMixer_t3521020193 * L_0 = __this->get_MainMixer_3();
		float* L_1 = __this->get_address_of_orgMusicVol_4();
		NullCheck(L_0);
		AudioMixer_GetFloat_m3026076910(L_0, _stringLiteral3964750823, (float*)L_1, /*hidden argument*/NULL);
		AudioMixer_t3521020193 * L_2 = __this->get_MainMixer_3();
		float* L_3 = __this->get_address_of_orgSoundVol_5();
		NullCheck(L_2);
		AudioMixer_GetFloat_m3026076910(L_2, _stringLiteral2002597352, (float*)L_3, /*hidden argument*/NULL);
		float L_4 = __this->get_orgMusicVol_4();
		float L_5 = L_4;
		RuntimeObject * L_6 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_5);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		float L_7 = __this->get_orgSoundVol_5();
		float L_8 = L_7;
		RuntimeObject * L_9 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_8);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void settingsManager::toggleSetting(System.String)
extern "C"  void settingsManager_toggleSetting_m1197959296 (settingsManager_t2086358119 * __this, String_t* ___calledSetting0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (settingsManager_toggleSetting_m1197959296_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3355509319 * L_0 = __this->get_settingslist_2();
		String_t* L_1 = ___calledSetting0;
		int32_t L_2 = settingsManager_findSetting_m2352407313(__this, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		SettingsList_t1883434577 * L_3 = List_1_get_Item_m3797380788(L_0, L_2, /*hidden argument*/List_1_get_Item_m3797380788_RuntimeMethod_var);
		NullCheck(L_3);
		bool L_4 = L_3->get_IsActive_1();
		if (!L_4)
		{
			goto IL_0061;
		}
	}
	{
		String_t* L_5 = ___calledSetting0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m920492651(NULL /*static, unused*/, L_5, _stringLiteral3964750823, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_003c;
		}
	}
	{
		String_t* L_7 = ___calledSetting0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m920492651(NULL /*static, unused*/, L_7, _stringLiteral2002597352, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0044;
		}
	}

IL_003c:
	{
		String_t* L_9 = ___calledSetting0;
		settingsManager_mute_m3259797164(__this, L_9, (bool)1, /*hidden argument*/NULL);
	}

IL_0044:
	{
		List_1_t3355509319 * L_10 = __this->get_settingslist_2();
		String_t* L_11 = ___calledSetting0;
		int32_t L_12 = settingsManager_findSetting_m2352407313(__this, L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		SettingsList_t1883434577 * L_13 = List_1_get_Item_m3797380788(L_10, L_12, /*hidden argument*/List_1_get_Item_m3797380788_RuntimeMethod_var);
		NullCheck(L_13);
		L_13->set_IsActive_1((bool)0);
		goto IL_00a1;
	}

IL_0061:
	{
		String_t* L_14 = ___calledSetting0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Equality_m920492651(NULL /*static, unused*/, L_14, _stringLiteral3964750823, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0081;
		}
	}
	{
		String_t* L_16 = ___calledSetting0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_op_Equality_m920492651(NULL /*static, unused*/, L_16, _stringLiteral2002597352, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0089;
		}
	}

IL_0081:
	{
		String_t* L_18 = ___calledSetting0;
		settingsManager_mute_m3259797164(__this, L_18, (bool)0, /*hidden argument*/NULL);
	}

IL_0089:
	{
		List_1_t3355509319 * L_19 = __this->get_settingslist_2();
		String_t* L_20 = ___calledSetting0;
		int32_t L_21 = settingsManager_findSetting_m2352407313(__this, L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		SettingsList_t1883434577 * L_22 = List_1_get_Item_m3797380788(L_19, L_21, /*hidden argument*/List_1_get_Item_m3797380788_RuntimeMethod_var);
		NullCheck(L_22);
		L_22->set_IsActive_1((bool)1);
	}

IL_00a1:
	{
		List_1_t3355509319 * L_23 = __this->get_settingslist_2();
		String_t* L_24 = ___calledSetting0;
		int32_t L_25 = settingsManager_findSetting_m2352407313(__this, L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		SettingsList_t1883434577 * L_26 = List_1_get_Item_m3797380788(L_23, L_25, /*hidden argument*/List_1_get_Item_m3797380788_RuntimeMethod_var);
		NullCheck(L_26);
		String_t* L_27 = L_26->get_SettingName_0();
		List_1_t3355509319 * L_28 = __this->get_settingslist_2();
		String_t* L_29 = ___calledSetting0;
		int32_t L_30 = settingsManager_findSetting_m2352407313(__this, L_29, /*hidden argument*/NULL);
		NullCheck(L_28);
		SettingsList_t1883434577 * L_31 = List_1_get_Item_m3797380788(L_28, L_30, /*hidden argument*/List_1_get_Item_m3797380788_RuntimeMethod_var);
		NullCheck(L_31);
		bool L_32 = L_31->get_IsActive_1();
		bool L_33 = L_32;
		RuntimeObject * L_34 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_33);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m1715369213(NULL /*static, unused*/, L_27, _stringLiteral3787301066, L_34, /*hidden argument*/NULL);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 settingsManager::findSetting(System.String)
extern "C"  int32_t settingsManager_findSetting_m2352407313 (settingsManager_t2086358119 * __this, String_t* ___calledSetting0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (settingsManager_findSetting_m2352407313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = (bool)0;
		V_1 = 0;
		V_2 = 0;
		goto IL_002f;
	}

IL_000b:
	{
		List_1_t3355509319 * L_0 = __this->get_settingslist_2();
		int32_t L_1 = V_2;
		NullCheck(L_0);
		SettingsList_t1883434577 * L_2 = List_1_get_Item_m3797380788(L_0, L_1, /*hidden argument*/List_1_get_Item_m3797380788_RuntimeMethod_var);
		NullCheck(L_2);
		String_t* L_3 = L_2->get_SettingName_0();
		String_t* L_4 = ___calledSetting0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m920492651(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_6 = V_2;
		V_1 = L_6;
		V_0 = (bool)1;
	}

IL_002b:
	{
		int32_t L_7 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_002f:
	{
		int32_t L_8 = V_2;
		List_1_t3355509319 * L_9 = __this->get_settingslist_2();
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_m3659879520(L_9, /*hidden argument*/List_1_get_Count_m3659879520_RuntimeMethod_var);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_000b;
		}
	}
	{
		bool L_11 = V_0;
		if (!L_11)
		{
			goto IL_0048;
		}
	}
	{
		int32_t L_12 = V_1;
		return L_12;
	}

IL_0048:
	{
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, _stringLiteral1938806281, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Void settingsManager::mute(System.String,System.Boolean)
extern "C"  void settingsManager_mute_m3259797164 (settingsManager_t2086358119 * __this, String_t* ___calledSetting0, bool ___isMuted1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (settingsManager_mute_m3259797164_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___calledSetting0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m920492651(NULL /*static, unused*/, L_0, _stringLiteral3964750823, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		bool L_2 = ___isMuted1;
		if (!L_2)
		{
			goto IL_0031;
		}
	}
	{
		AudioMixer_t3521020193 * L_3 = __this->get_MainMixer_3();
		NullCheck(L_3);
		AudioMixer_SetFloat_m3036056706(L_3, _stringLiteral3964750823, (-80.0f), /*hidden argument*/NULL);
		goto IL_00c1;
	}

IL_0031:
	{
		String_t* L_4 = ___calledSetting0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m920492651(NULL /*static, unused*/, L_4, _stringLiteral3964750823, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0063;
		}
	}
	{
		bool L_6 = ___isMuted1;
		if (L_6)
		{
			goto IL_0063;
		}
	}
	{
		AudioMixer_t3521020193 * L_7 = __this->get_MainMixer_3();
		float L_8 = __this->get_orgMusicVol_4();
		NullCheck(L_7);
		AudioMixer_SetFloat_m3036056706(L_7, _stringLiteral3964750823, L_8, /*hidden argument*/NULL);
		goto IL_00c1;
	}

IL_0063:
	{
		String_t* L_9 = ___calledSetting0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m920492651(NULL /*static, unused*/, L_9, _stringLiteral2002597352, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0094;
		}
	}
	{
		bool L_11 = ___isMuted1;
		if (!L_11)
		{
			goto IL_0094;
		}
	}
	{
		AudioMixer_t3521020193 * L_12 = __this->get_MainMixer_3();
		NullCheck(L_12);
		AudioMixer_SetFloat_m3036056706(L_12, _stringLiteral2002597352, (-80.0f), /*hidden argument*/NULL);
		goto IL_00c1;
	}

IL_0094:
	{
		String_t* L_13 = ___calledSetting0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_op_Equality_m920492651(NULL /*static, unused*/, L_13, _stringLiteral2002597352, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00c1;
		}
	}
	{
		bool L_15 = ___isMuted1;
		if (L_15)
		{
			goto IL_00c1;
		}
	}
	{
		AudioMixer_t3521020193 * L_16 = __this->get_MainMixer_3();
		float L_17 = __this->get_orgSoundVol_5();
		NullCheck(L_16);
		AudioMixer_SetFloat_m3036056706(L_16, _stringLiteral2002597352, L_17, /*hidden argument*/NULL);
	}

IL_00c1:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SmoothCamera::.ctor()
extern "C"  void SmoothCamera__ctor_m526654758 (SmoothCamera_t4222009864 * __this, const RuntimeMethod* method)
{
	{
		__this->set_smoothingFrames_2(((int32_t)10));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SmoothCamera::OnInitialized()
extern "C"  void SmoothCamera_OnInitialized_m1148221488 (SmoothCamera_t4222009864 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void SmoothCamera::OnTrackablesUpdated()
extern "C"  void SmoothCamera_OnTrackablesUpdated_m2235135197 (SmoothCamera_t4222009864 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmoothCamera_OnTrackablesUpdated_m2235135197_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector4_t3319028937  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t2301928331  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t2810099927  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Enumerator_t4230485060  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Queue_1_t2148187825 * L_0 = __this->get_rotations_6();
		NullCheck(L_0);
		int32_t L_1 = Queue_1_get_Count_m566549244(L_0, /*hidden argument*/Queue_1_get_Count_m566549244_RuntimeMethod_var);
		int32_t L_2 = __this->get_smoothingFrames_2();
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_002e;
		}
	}
	{
		Queue_1_t2148187825 * L_3 = __this->get_rotations_6();
		NullCheck(L_3);
		Queue_1_Dequeue_m127687118(L_3, /*hidden argument*/Queue_1_Dequeue_m127687118_RuntimeMethod_var);
		Queue_1_t3568572958 * L_4 = __this->get_positions_7();
		NullCheck(L_4);
		Queue_1_Dequeue_m3534639416(L_4, /*hidden argument*/Queue_1_Dequeue_m3534639416_RuntimeMethod_var);
	}

IL_002e:
	{
		Queue_1_t2148187825 * L_5 = __this->get_rotations_6();
		Transform_t3600365921 * L_6 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Quaternion_t2301928331  L_7 = Transform_get_rotation_m3502953881(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Queue_1_Enqueue_m309039112(L_5, L_7, /*hidden argument*/Queue_1_Enqueue_m309039112_RuntimeMethod_var);
		Queue_1_t3568572958 * L_8 = __this->get_positions_7();
		Transform_t3600365921 * L_9 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t3722313464  L_10 = Transform_get_position_m36019626(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		Queue_1_Enqueue_m2554143986(L_8, L_10, /*hidden argument*/Queue_1_Enqueue_m2554143986_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t3319028937_il2cpp_TypeInfo_var);
		Vector4_t3319028937  L_11 = Vector4_get_zero_m1422399515(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_11;
		Queue_1_t2148187825 * L_12 = __this->get_rotations_6();
		NullCheck(L_12);
		Enumerator_t2810099927  L_13 = Queue_1_GetEnumerator_m3198989551(L_12, /*hidden argument*/Queue_1_GetEnumerator_m3198989551_RuntimeMethod_var);
		V_2 = L_13;
	}

IL_006c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0097;
		}

IL_0071:
		{
			Quaternion_t2301928331  L_14 = Enumerator_get_Current_m2950039238((Enumerator_t2810099927 *)(&V_2), /*hidden argument*/Enumerator_get_Current_m2950039238_RuntimeMethod_var);
			V_1 = L_14;
			Quaternion_t2301928331  L_15 = V_1;
			Queue_1_t2148187825 * L_16 = __this->get_rotations_6();
			NullCheck(L_16);
			Quaternion_t2301928331  L_17 = Queue_1_Peek_m1284364542(L_16, /*hidden argument*/Queue_1_Peek_m1284364542_RuntimeMethod_var);
			Queue_1_t2148187825 * L_18 = __this->get_rotations_6();
			NullCheck(L_18);
			int32_t L_19 = Queue_1_get_Count_m566549244(L_18, /*hidden argument*/Queue_1_get_Count_m566549244_RuntimeMethod_var);
			IL2CPP_RUNTIME_CLASS_INIT(Math3d_t1427253148_il2cpp_TypeInfo_var);
			Math3d_AverageQuaternion_m2118685977(NULL /*static, unused*/, (Vector4_t3319028937 *)(&V_0), L_15, L_17, L_19, /*hidden argument*/NULL);
		}

IL_0097:
		{
			bool L_20 = Enumerator_MoveNext_m459395817((Enumerator_t2810099927 *)(&V_2), /*hidden argument*/Enumerator_MoveNext_m459395817_RuntimeMethod_var);
			if (L_20)
			{
				goto IL_0071;
			}
		}

IL_00a3:
		{
			IL2CPP_LEAVE(0xB6, FINALLY_00a8);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00a8;
	}

FINALLY_00a8:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m557667343((Enumerator_t2810099927 *)(&V_2), /*hidden argument*/Enumerator_Dispose_m557667343_RuntimeMethod_var);
		IL2CPP_END_FINALLY(168)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(168)
	{
		IL2CPP_JUMP_TBL(0xB6, IL_00b6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00b6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_21 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_21;
		Queue_1_t3568572958 * L_22 = __this->get_positions_7();
		NullCheck(L_22);
		Enumerator_t4230485060  L_23 = Queue_1_GetEnumerator_m676935694(L_22, /*hidden argument*/Queue_1_GetEnumerator_m676935694_RuntimeMethod_var);
		V_5 = L_23;
	}

IL_00c9:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00e0;
		}

IL_00ce:
		{
			Vector3_t3722313464  L_24 = Enumerator_get_Current_m4117045769((Enumerator_t4230485060 *)(&V_5), /*hidden argument*/Enumerator_get_Current_m4117045769_RuntimeMethod_var);
			V_4 = L_24;
			Vector3_t3722313464  L_25 = V_3;
			Vector3_t3722313464  L_26 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
			Vector3_t3722313464  L_27 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
			V_3 = L_27;
		}

IL_00e0:
		{
			bool L_28 = Enumerator_MoveNext_m2202395141((Enumerator_t4230485060 *)(&V_5), /*hidden argument*/Enumerator_MoveNext_m2202395141_RuntimeMethod_var);
			if (L_28)
			{
				goto IL_00ce;
			}
		}

IL_00ec:
		{
			IL2CPP_LEAVE(0xFF, FINALLY_00f1);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00f1;
	}

FINALLY_00f1:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1218804216((Enumerator_t4230485060 *)(&V_5), /*hidden argument*/Enumerator_Dispose_m1218804216_RuntimeMethod_var);
		IL2CPP_END_FINALLY(241)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(241)
	{
		IL2CPP_JUMP_TBL(0xFF, IL_00ff)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00ff:
	{
		Vector3_t3722313464  L_29 = V_3;
		Queue_1_t3568572958 * L_30 = __this->get_positions_7();
		NullCheck(L_30);
		int32_t L_31 = Queue_1_get_Count_m2376062550(L_30, /*hidden argument*/Queue_1_get_Count_m2376062550_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_32 = Vector3_op_Division_m510815599(NULL /*static, unused*/, L_29, (((float)((float)L_31))), /*hidden argument*/NULL);
		V_3 = L_32;
		float L_33 = (&V_0)->get_x_1();
		float L_34 = (&V_0)->get_y_2();
		float L_35 = (&V_0)->get_z_3();
		float L_36 = (&V_0)->get_w_4();
		Quaternion_t2301928331  L_37;
		memset(&L_37, 0, sizeof(L_37));
		Quaternion__ctor_m435141806((&L_37), L_33, L_34, L_35, L_36, /*hidden argument*/NULL);
		__this->set_smoothedRotation_4(L_37);
		Vector3_t3722313464  L_38 = V_3;
		__this->set_smoothedPosition_5(L_38);
		return;
	}
}
// System.Void SmoothCamera::Start()
extern "C"  void SmoothCamera_Start_m244894020 (SmoothCamera_t4222009864 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmoothCamera_Start_m244894020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VuforiaARController_t1876945237 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_smoothingFrames_2();
		Queue_1_t2148187825 * L_1 = (Queue_1_t2148187825 *)il2cpp_codegen_object_new(Queue_1_t2148187825_il2cpp_TypeInfo_var);
		Queue_1__ctor_m1117100023(L_1, L_0, /*hidden argument*/Queue_1__ctor_m1117100023_RuntimeMethod_var);
		__this->set_rotations_6(L_1);
		int32_t L_2 = __this->get_smoothingFrames_2();
		Queue_1_t3568572958 * L_3 = (Queue_1_t3568572958 *)il2cpp_codegen_object_new(Queue_1_t3568572958_il2cpp_TypeInfo_var);
		Queue_1__ctor_m3566777400(L_3, L_2, /*hidden argument*/Queue_1__ctor_m3566777400_RuntimeMethod_var);
		__this->set_positions_7(L_3);
		VuforiaBehaviour_t2151848540 * L_4 = Component_GetComponent_TisVuforiaBehaviour_t2151848540_m3060796760(__this, /*hidden argument*/Component_GetComponent_TisVuforiaBehaviour_t2151848540_m3060796760_RuntimeMethod_var);
		__this->set_qcarBehavior_3(L_4);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaARController_t1876945237_il2cpp_TypeInfo_var);
		VuforiaARController_t1876945237 * L_5 = VuforiaARController_get_Instance_m1100644214(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_5;
		VuforiaARController_t1876945237 * L_6 = V_0;
		intptr_t L_7 = (intptr_t)SmoothCamera_OnInitialized_m1148221488_RuntimeMethod_var;
		Action_t1264377477 * L_8 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_8, __this, L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		VuforiaARController_RegisterVuforiaStartedCallback_m3258152770(L_6, L_8, /*hidden argument*/NULL);
		VuforiaARController_t1876945237 * L_9 = V_0;
		intptr_t L_10 = (intptr_t)SmoothCamera_OnTrackablesUpdated_m2235135197_RuntimeMethod_var;
		Action_t1264377477 * L_11 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_11, __this, L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		VuforiaARController_RegisterTrackablesUpdatedCallback_m2341042762(L_9, L_11, /*hidden argument*/NULL);
		intptr_t L_12 = (intptr_t)SmoothCamera_UnRegister_m1885363057_RuntimeMethod_var;
		UnityAction_2_t1262235195 * L_13 = (UnityAction_2_t1262235195 *)il2cpp_codegen_object_new(UnityAction_2_t1262235195_il2cpp_TypeInfo_var);
		UnityAction_2__ctor_m608474170(L_13, __this, L_12, /*hidden argument*/UnityAction_2__ctor_m608474170_RuntimeMethod_var);
		SceneManager_add_activeSceneChanged_m1118944293(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SmoothCamera::LateUpdate()
extern "C"  void SmoothCamera_LateUpdate_m2647874918 (SmoothCamera_t4222009864 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_1 = __this->get_smoothedRotation_4();
		NullCheck(L_0);
		Transform_set_rotation_m3524318132(L_0, L_1, /*hidden argument*/NULL);
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = __this->get_smoothedPosition_5();
		NullCheck(L_2);
		Transform_set_position_m3387557959(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SmoothCamera::UnRegister(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern "C"  void SmoothCamera_UnRegister_m1885363057 (SmoothCamera_t4222009864 * __this, Scene_t2348375561  ___scene10, Scene_t2348375561  ___scene21, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmoothCamera_UnRegister_m1885363057_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Scene_t2348375561  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Scene_t2348375561  V_1;
	memset(&V_1, 0, sizeof(V_1));
	VuforiaARController_t1876945237 * V_2 = NULL;
	{
		Scene_t2348375561  L_0 = SceneManager_GetActiveScene_m1825203488(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = Scene_get_name_m622963475((Scene_t2348375561 *)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m3937257545(NULL /*static, unused*/, L_1, _stringLiteral3886504256, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Scene_t2348375561  L_3 = SceneManager_GetActiveScene_m1825203488(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = Scene_get_name_m622963475((Scene_t2348375561 *)(&V_1), /*hidden argument*/NULL);
		bool L_5 = String_op_Equality_m920492651(NULL /*static, unused*/, L_4, _stringLiteral2619271551, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0062;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaARController_t1876945237_il2cpp_TypeInfo_var);
		VuforiaARController_t1876945237 * L_6 = VuforiaARController_get_Instance_m1100644214(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_6;
		VuforiaARController_t1876945237 * L_7 = V_2;
		intptr_t L_8 = (intptr_t)SmoothCamera_OnInitialized_m1148221488_RuntimeMethod_var;
		Action_t1264377477 * L_9 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_9, __this, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		VuforiaARController_UnregisterVuforiaStartedCallback_m3486105406(L_7, L_9, /*hidden argument*/NULL);
		VuforiaARController_t1876945237 * L_10 = V_2;
		intptr_t L_11 = (intptr_t)SmoothCamera_OnTrackablesUpdated_m2235135197_RuntimeMethod_var;
		Action_t1264377477 * L_12 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_12, __this, L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		VuforiaARController_UnregisterTrackablesUpdatedCallback_m3199567651(L_10, L_12, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void symbolPrefabDefiner::.ctor()
extern "C"  void symbolPrefabDefiner__ctor_m3192395265 (symbolPrefabDefiner_t2798204667 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void symbolsSwitch::.ctor()
extern "C"  void symbolsSwitch__ctor_m3381865028 (symbolsSwitch_t3635132447 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TapHandler::.ctor()
extern "C"  void TapHandler__ctor_m1076312137 (TapHandler_t334234343 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TapHandler::Start()
extern "C"  void TapHandler_Start_m554074027 (TapHandler_t334234343 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TapHandler_Start_m554074027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_mTapCount_5(0);
		__this->set_mTimeSinceLastTap_3((0.0f));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		MenuAnimator_t2112910832 * L_0 = Object_FindObjectOfType_TisMenuAnimator_t2112910832_m1830680649(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisMenuAnimator_t2112910832_m1830680649_RuntimeMethod_var);
		__this->set_mMenuAnim_4(L_0);
		return;
	}
}
// System.Void TapHandler::Update()
extern "C"  void TapHandler_Update_m2328515667 (TapHandler_t334234343 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TapHandler_Update_m2328515667_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MenuAnimator_t2112910832 * L_0 = __this->get_mMenuAnim_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0037;
		}
	}
	{
		MenuAnimator_t2112910832 * L_2 = __this->get_mMenuAnim_4();
		NullCheck(L_2);
		bool L_3 = MenuAnimator_IsVisible_m928022866(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0037;
		}
	}
	{
		__this->set_mTapCount_5(0);
		__this->set_mTimeSinceLastTap_3((0.0f));
		goto IL_003d;
	}

IL_0037:
	{
		TapHandler_HandleTap_m1322678531(__this, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Void TapHandler::HandleTap()
extern "C"  void TapHandler_HandleTap_m1322678531 (TapHandler_t334234343 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TapHandler_HandleTap_m1322678531_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_mTapCount_5();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_004b;
		}
	}
	{
		float L_1 = __this->get_mTimeSinceLastTap_3();
		float L_2 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mTimeSinceLastTap_3(((float)il2cpp_codegen_add((float)L_1, (float)L_2)));
		float L_3 = __this->get_mTimeSinceLastTap_3();
		if ((!(((float)L_3) > ((float)(0.5f)))))
		{
			goto IL_0046;
		}
	}
	{
		VirtActionInvoker0::Invoke(5 /* System.Void TapHandler::OnSingleTapConfirmed() */, __this);
		__this->set_mTapCount_5(0);
		__this->set_mTimeSinceLastTap_3((0.0f));
	}

IL_0046:
	{
		goto IL_006f;
	}

IL_004b:
	{
		int32_t L_4 = __this->get_mTapCount_5();
		if ((!(((uint32_t)L_4) == ((uint32_t)2))))
		{
			goto IL_006f;
		}
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void TapHandler::OnDoubleTap() */, __this);
		__this->set_mTimeSinceLastTap_3((0.0f));
		__this->set_mTapCount_5(0);
	}

IL_006f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_5 = Input_GetMouseButtonUp_m2924350851(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_009a;
		}
	}
	{
		int32_t L_6 = __this->get_mTapCount_5();
		__this->set_mTapCount_5(((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1)));
		int32_t L_7 = __this->get_mTapCount_5();
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_009a;
		}
	}
	{
		VirtActionInvoker0::Invoke(4 /* System.Void TapHandler::OnSingleTap() */, __this);
	}

IL_009a:
	{
		return;
	}
}
// System.Void TapHandler::OnSingleTap()
extern "C"  void TapHandler_OnSingleTap_m3625150319 (TapHandler_t334234343 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void TapHandler::OnSingleTapConfirmed()
extern "C"  void TapHandler_OnSingleTapConfirmed_m3372274260 (TapHandler_t334234343 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TapHandler_OnSingleTapConfirmed_m3372274260_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CameraSettings_t3152619780 * V_0 = NULL;
	{
		CameraSettings_t3152619780 * L_0 = Component_GetComponentInChildren_TisCameraSettings_t3152619780_m2209036194(__this, /*hidden argument*/Component_GetComponentInChildren_TisCameraSettings_t3152619780_m2209036194_RuntimeMethod_var);
		V_0 = L_0;
		CameraSettings_t3152619780 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		CameraSettings_t3152619780 * L_3 = V_0;
		NullCheck(L_3);
		CameraSettings_TriggerAutofocusEvent_m898525935(L_3, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void TapHandler::OnDoubleTap()
extern "C"  void TapHandler_OnDoubleTap_m2928608177 (TapHandler_t334234343 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TapHandler_OnDoubleTap_m2928608177_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MenuAnimator_t2112910832 * L_0 = __this->get_mMenuAnim_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002b;
		}
	}
	{
		MenuAnimator_t2112910832 * L_2 = __this->get_mMenuAnim_4();
		NullCheck(L_2);
		bool L_3 = MenuAnimator_IsVisible_m928022866(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_002b;
		}
	}
	{
		MenuAnimator_t2112910832 * L_4 = __this->get_mMenuAnim_4();
		NullCheck(L_4);
		MenuAnimator_Show_m982174260(L_4, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TouchCamera::.ctor()
extern "C"  void TouchCamera__ctor_m2214619079 (TouchCamera_t3780128488 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchCamera__ctor_m2214619079_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_oldTouchPositions_2(((Nullable_1U5BU5D_t3148003288*)SZArrayNew(Nullable_1U5BU5D_t3148003288_il2cpp_TypeInfo_var, (uint32_t)2)));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TouchCamera::Update()
extern "C"  void TouchCamera_Update_m4288923361 (TouchCamera_t3780128488 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchCamera_Update_m4288923361_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Nullable_1_t3878791605  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Nullable_1_t3878791605  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Touch_t1921856868  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Nullable_1_t3878791605  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2156229523  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Touch_t1921856868  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Nullable_1_t3878791605  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Nullable_1_t3878791605  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Nullable_1_t3878791605  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Nullable_1_t3878791605  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t3722313464  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Nullable_1_t3878791605  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Touch_t1921856868  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Touch_t1921856868  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Nullable_1_t3878791605  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Nullable_1_t3878791605  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Nullable_1_t3878791605  V_16;
	memset(&V_16, 0, sizeof(V_16));
	Vector2_t2156229523  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Vector2U5BU5D_t1457185986* V_18 = NULL;
	Touch_t1921856868  V_19;
	memset(&V_19, 0, sizeof(V_19));
	Touch_t1921856868  V_20;
	memset(&V_20, 0, sizeof(V_20));
	Vector2_t2156229523  V_21;
	memset(&V_21, 0, sizeof(V_21));
	float V_22 = 0.0f;
	Nullable_1_t3878791605  V_23;
	memset(&V_23, 0, sizeof(V_23));
	Nullable_1_t3878791605  V_24;
	memset(&V_24, 0, sizeof(V_24));
	Nullable_1_t3878791605  V_25;
	memset(&V_25, 0, sizeof(V_25));
	Nullable_1_t3878791605  V_26;
	memset(&V_26, 0, sizeof(V_26));
	Nullable_1_t3878791605  V_27;
	memset(&V_27, 0, sizeof(V_27));
	Nullable_1_t3878791605  V_28;
	memset(&V_28, 0, sizeof(V_28));
	Transform_t3600365921 * G_B8_0 = NULL;
	Vector3_t3722313464  G_B8_1;
	memset(&G_B8_1, 0, sizeof(G_B8_1));
	Transform_t3600365921 * G_B8_2 = NULL;
	Transform_t3600365921 * G_B7_0 = NULL;
	Vector3_t3722313464  G_B7_1;
	memset(&G_B7_1, 0, sizeof(G_B7_1));
	Transform_t3600365921 * G_B7_2 = NULL;
	Nullable_1_t3878791605  G_B9_0;
	memset(&G_B9_0, 0, sizeof(G_B9_0));
	Transform_t3600365921 * G_B9_1 = NULL;
	Vector3_t3722313464  G_B9_2;
	memset(&G_B9_2, 0, sizeof(G_B9_2));
	Transform_t3600365921 * G_B9_3 = NULL;
	Transform_t3600365921 * G_B11_0 = NULL;
	Vector3_t3722313464  G_B11_1;
	memset(&G_B11_1, 0, sizeof(G_B11_1));
	Transform_t3600365921 * G_B11_2 = NULL;
	Transform_t3600365921 * G_B10_0 = NULL;
	Vector3_t3722313464  G_B10_1;
	memset(&G_B10_1, 0, sizeof(G_B10_1));
	Transform_t3600365921 * G_B10_2 = NULL;
	Nullable_1_t3878791605  G_B12_0;
	memset(&G_B12_0, 0, sizeof(G_B12_0));
	Transform_t3600365921 * G_B12_1 = NULL;
	Vector3_t3722313464  G_B12_2;
	memset(&G_B12_2, 0, sizeof(G_B12_2));
	Transform_t3600365921 * G_B12_3 = NULL;
	Transform_t3600365921 * G_B14_0 = NULL;
	Vector3_t3722313464  G_B14_1;
	memset(&G_B14_1, 0, sizeof(G_B14_1));
	Transform_t3600365921 * G_B14_2 = NULL;
	Transform_t3600365921 * G_B13_0 = NULL;
	Vector3_t3722313464  G_B13_1;
	memset(&G_B13_1, 0, sizeof(G_B13_1));
	Transform_t3600365921 * G_B13_2 = NULL;
	Nullable_1_t3878791605  G_B15_0;
	memset(&G_B15_0, 0, sizeof(G_B15_0));
	Transform_t3600365921 * G_B15_1 = NULL;
	Vector3_t3722313464  G_B15_2;
	memset(&G_B15_2, 0, sizeof(G_B15_2));
	Transform_t3600365921 * G_B15_3 = NULL;
	Transform_t3600365921 * G_B17_0 = NULL;
	Vector3_t3722313464  G_B17_1;
	memset(&G_B17_1, 0, sizeof(G_B17_1));
	Transform_t3600365921 * G_B17_2 = NULL;
	Transform_t3600365921 * G_B16_0 = NULL;
	Vector3_t3722313464  G_B16_1;
	memset(&G_B16_1, 0, sizeof(G_B16_1));
	Transform_t3600365921 * G_B16_2 = NULL;
	Nullable_1_t3878791605  G_B18_0;
	memset(&G_B18_0, 0, sizeof(G_B18_0));
	Transform_t3600365921 * G_B18_1 = NULL;
	Vector3_t3722313464  G_B18_2;
	memset(&G_B18_2, 0, sizeof(G_B18_2));
	Transform_t3600365921 * G_B18_3 = NULL;
	Transform_t3600365921 * G_B20_0 = NULL;
	Vector3_t3722313464  G_B20_1;
	memset(&G_B20_1, 0, sizeof(G_B20_1));
	Transform_t3600365921 * G_B20_2 = NULL;
	Transform_t3600365921 * G_B19_0 = NULL;
	Vector3_t3722313464  G_B19_1;
	memset(&G_B19_1, 0, sizeof(G_B19_1));
	Transform_t3600365921 * G_B19_2 = NULL;
	Vector3_t3722313464  G_B21_0;
	memset(&G_B21_0, 0, sizeof(G_B21_0));
	Transform_t3600365921 * G_B21_1 = NULL;
	Vector3_t3722313464  G_B21_2;
	memset(&G_B21_2, 0, sizeof(G_B21_2));
	Transform_t3600365921 * G_B21_3 = NULL;
	TouchCamera_t3780128488 * G_B26_0 = NULL;
	TouchCamera_t3780128488 * G_B25_0 = NULL;
	Nullable_1_t3878791605  G_B27_0;
	memset(&G_B27_0, 0, sizeof(G_B27_0));
	TouchCamera_t3780128488 * G_B27_1 = NULL;
	Transform_t3600365921 * G_B30_0 = NULL;
	Vector3_t3722313464  G_B30_1;
	memset(&G_B30_1, 0, sizeof(G_B30_1));
	Transform_t3600365921 * G_B30_2 = NULL;
	Transform_t3600365921 * G_B29_0 = NULL;
	Vector3_t3722313464  G_B29_1;
	memset(&G_B29_1, 0, sizeof(G_B29_1));
	Transform_t3600365921 * G_B29_2 = NULL;
	Nullable_1_t3878791605  G_B31_0;
	memset(&G_B31_0, 0, sizeof(G_B31_0));
	Transform_t3600365921 * G_B31_1 = NULL;
	Vector3_t3722313464  G_B31_2;
	memset(&G_B31_2, 0, sizeof(G_B31_2));
	Transform_t3600365921 * G_B31_3 = NULL;
	Transform_t3600365921 * G_B33_0 = NULL;
	Vector3_t3722313464  G_B33_1;
	memset(&G_B33_1, 0, sizeof(G_B33_1));
	Transform_t3600365921 * G_B33_2 = NULL;
	Transform_t3600365921 * G_B32_0 = NULL;
	Vector3_t3722313464  G_B32_1;
	memset(&G_B32_1, 0, sizeof(G_B32_1));
	Transform_t3600365921 * G_B32_2 = NULL;
	Nullable_1_t3878791605  G_B34_0;
	memset(&G_B34_0, 0, sizeof(G_B34_0));
	Transform_t3600365921 * G_B34_1 = NULL;
	Vector3_t3722313464  G_B34_2;
	memset(&G_B34_2, 0, sizeof(G_B34_2));
	Transform_t3600365921 * G_B34_3 = NULL;
	Transform_t3600365921 * G_B36_0 = NULL;
	Vector3_t3722313464  G_B36_1;
	memset(&G_B36_1, 0, sizeof(G_B36_1));
	Transform_t3600365921 * G_B36_2 = NULL;
	Transform_t3600365921 * G_B35_0 = NULL;
	Vector3_t3722313464  G_B35_1;
	memset(&G_B35_1, 0, sizeof(G_B35_1));
	Transform_t3600365921 * G_B35_2 = NULL;
	Nullable_1_t3878791605  G_B37_0;
	memset(&G_B37_0, 0, sizeof(G_B37_0));
	Transform_t3600365921 * G_B37_1 = NULL;
	Vector3_t3722313464  G_B37_2;
	memset(&G_B37_2, 0, sizeof(G_B37_2));
	Transform_t3600365921 * G_B37_3 = NULL;
	Transform_t3600365921 * G_B39_0 = NULL;
	Vector3_t3722313464  G_B39_1;
	memset(&G_B39_1, 0, sizeof(G_B39_1));
	Transform_t3600365921 * G_B39_2 = NULL;
	Transform_t3600365921 * G_B38_0 = NULL;
	Vector3_t3722313464  G_B38_1;
	memset(&G_B38_1, 0, sizeof(G_B38_1));
	Transform_t3600365921 * G_B38_2 = NULL;
	Nullable_1_t3878791605  G_B40_0;
	memset(&G_B40_0, 0, sizeof(G_B40_0));
	Transform_t3600365921 * G_B40_1 = NULL;
	Vector3_t3722313464  G_B40_2;
	memset(&G_B40_2, 0, sizeof(G_B40_2));
	Transform_t3600365921 * G_B40_3 = NULL;
	Transform_t3600365921 * G_B42_0 = NULL;
	Vector3_t3722313464  G_B42_1;
	memset(&G_B42_1, 0, sizeof(G_B42_1));
	Transform_t3600365921 * G_B42_2 = NULL;
	Transform_t3600365921 * G_B41_0 = NULL;
	Vector3_t3722313464  G_B41_1;
	memset(&G_B41_1, 0, sizeof(G_B41_1));
	Transform_t3600365921 * G_B41_2 = NULL;
	Vector3_t3722313464  G_B43_0;
	memset(&G_B43_0, 0, sizeof(G_B43_0));
	Transform_t3600365921 * G_B43_1 = NULL;
	Vector3_t3722313464  G_B43_2;
	memset(&G_B43_2, 0, sizeof(G_B43_2));
	Transform_t3600365921 * G_B43_3 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m3403849067(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0043;
		}
	}
	{
		Nullable_1U5BU5D_t3148003288* L_1 = __this->get_oldTouchPositions_2();
		NullCheck(L_1);
		il2cpp_codegen_initobj((&V_0), sizeof(Nullable_1_t3878791605 ));
		Nullable_1_t3878791605  L_2 = V_0;
		*(Nullable_1_t3878791605 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_2;
		Nullable_1U5BU5D_t3148003288* L_3 = __this->get_oldTouchPositions_2();
		NullCheck(L_3);
		il2cpp_codegen_initobj((&V_0), sizeof(Nullable_1_t3878791605 ));
		Nullable_1_t3878791605  L_4 = V_0;
		*(Nullable_1_t3878791605 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_4;
		goto IL_0623;
	}

IL_0043:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		int32_t L_5 = Input_get_touchCount_m3403849067(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_0222;
		}
	}
	{
		Nullable_1U5BU5D_t3148003288* L_6 = __this->get_oldTouchPositions_2();
		NullCheck(L_6);
		V_0 = (*(Nullable_1_t3878791605 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		bool L_7 = Nullable_1_get_HasValue_m1317965445((Nullable_1_t3878791605 *)(&V_0), /*hidden argument*/Nullable_1_get_HasValue_m1317965445_RuntimeMethod_var);
		if (((((int32_t)L_7) == ((int32_t)0))? 1 : 0))
		{
			goto IL_008d;
		}
	}
	{
		Nullable_1U5BU5D_t3148003288* L_8 = __this->get_oldTouchPositions_2();
		NullCheck(L_8);
		V_1 = (*(Nullable_1_t3878791605 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))));
		bool L_9 = Nullable_1_get_HasValue_m1317965445((Nullable_1_t3878791605 *)(&V_1), /*hidden argument*/Nullable_1_get_HasValue_m1317965445_RuntimeMethod_var);
		if (!L_9)
		{
			goto IL_00d0;
		}
	}

IL_008d:
	{
		Nullable_1U5BU5D_t3148003288* L_10 = __this->get_oldTouchPositions_2();
		NullCheck(L_10);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Touch_t1921856868  L_11 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_2 = L_11;
		Vector2_t2156229523  L_12 = Touch_get_position_m3109777936((Touch_t1921856868 *)(&V_2), /*hidden argument*/NULL);
		Nullable_1_t3878791605  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Nullable_1__ctor_m680077965((&L_13), L_12, /*hidden argument*/Nullable_1__ctor_m680077965_RuntimeMethod_var);
		*(Nullable_1_t3878791605 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_13;
		Nullable_1U5BU5D_t3148003288* L_14 = __this->get_oldTouchPositions_2();
		NullCheck(L_14);
		il2cpp_codegen_initobj((&V_3), sizeof(Nullable_1_t3878791605 ));
		Nullable_1_t3878791605  L_15 = V_3;
		*(Nullable_1_t3878791605 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_15;
		goto IL_021d;
	}

IL_00d0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Touch_t1921856868  L_16 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_5 = L_16;
		Vector2_t2156229523  L_17 = Touch_get_position_m3109777936((Touch_t1921856868 *)(&V_5), /*hidden argument*/NULL);
		V_4 = L_17;
		Transform_t3600365921 * L_18 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_19 = L_18;
		NullCheck(L_19);
		Vector3_t3722313464  L_20 = Transform_get_position_m36019626(L_19, /*hidden argument*/NULL);
		Transform_t3600365921 * L_21 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Nullable_1U5BU5D_t3148003288* L_22 = __this->get_oldTouchPositions_2();
		NullCheck(L_22);
		V_3 = (*(Nullable_1_t3878791605 *)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		bool L_23 = Nullable_1_get_HasValue_m1317965445((Nullable_1_t3878791605 *)(&V_3), /*hidden argument*/Nullable_1_get_HasValue_m1317965445_RuntimeMethod_var);
		G_B7_0 = L_21;
		G_B7_1 = L_20;
		G_B7_2 = L_19;
		if (!L_23)
		{
			G_B8_0 = L_21;
			G_B8_1 = L_20;
			G_B8_2 = L_19;
			goto IL_0126;
		}
	}
	{
		Vector2_t2156229523  L_24 = Nullable_1_GetValueOrDefault_m3722452406((Nullable_1_t3878791605 *)(&V_3), /*hidden argument*/Nullable_1_GetValueOrDefault_m3722452406_RuntimeMethod_var);
		Vector2_t2156229523  L_25 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_26 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		Nullable_1_t3878791605  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Nullable_1__ctor_m680077965((&L_27), L_26, /*hidden argument*/Nullable_1__ctor_m680077965_RuntimeMethod_var);
		G_B9_0 = L_27;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		G_B9_3 = G_B7_2;
		goto IL_0130;
	}

IL_0126:
	{
		il2cpp_codegen_initobj((&V_6), sizeof(Nullable_1_t3878791605 ));
		Nullable_1_t3878791605  L_28 = V_6;
		G_B9_0 = L_28;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
	}

IL_0130:
	{
		V_6 = G_B9_0;
		bool L_29 = Nullable_1_get_HasValue_m1317965445((Nullable_1_t3878791605 *)(&V_6), /*hidden argument*/Nullable_1_get_HasValue_m1317965445_RuntimeMethod_var);
		G_B10_0 = G_B9_1;
		G_B10_1 = G_B9_2;
		G_B10_2 = G_B9_3;
		if (!L_29)
		{
			G_B11_0 = G_B9_1;
			G_B11_1 = G_B9_2;
			G_B11_2 = G_B9_3;
			goto IL_015c;
		}
	}
	{
		Vector2_t2156229523  L_30 = Nullable_1_GetValueOrDefault_m3722452406((Nullable_1_t3878791605 *)(&V_6), /*hidden argument*/Nullable_1_GetValueOrDefault_m3722452406_RuntimeMethod_var);
		Camera_t4157153871 * L_31 = Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var);
		NullCheck(L_31);
		float L_32 = Camera_get_orthographicSize_m3903216845(L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_33 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_30, L_32, /*hidden argument*/NULL);
		Nullable_1_t3878791605  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Nullable_1__ctor_m680077965((&L_34), L_33, /*hidden argument*/Nullable_1__ctor_m680077965_RuntimeMethod_var);
		G_B12_0 = L_34;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		G_B12_3 = G_B10_2;
		goto IL_0166;
	}

IL_015c:
	{
		il2cpp_codegen_initobj((&V_7), sizeof(Nullable_1_t3878791605 ));
		Nullable_1_t3878791605  L_35 = V_7;
		G_B12_0 = L_35;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
		G_B12_3 = G_B11_2;
	}

IL_0166:
	{
		V_7 = G_B12_0;
		bool L_36 = Nullable_1_get_HasValue_m1317965445((Nullable_1_t3878791605 *)(&V_7), /*hidden argument*/Nullable_1_get_HasValue_m1317965445_RuntimeMethod_var);
		G_B13_0 = G_B12_1;
		G_B13_1 = G_B12_2;
		G_B13_2 = G_B12_3;
		if (!L_36)
		{
			G_B14_0 = G_B12_1;
			G_B14_1 = G_B12_2;
			G_B14_2 = G_B12_3;
			goto IL_0193;
		}
	}
	{
		Vector2_t2156229523  L_37 = Nullable_1_GetValueOrDefault_m3722452406((Nullable_1_t3878791605 *)(&V_7), /*hidden argument*/Nullable_1_GetValueOrDefault_m3722452406_RuntimeMethod_var);
		Camera_t4157153871 * L_38 = Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var);
		NullCheck(L_38);
		int32_t L_39 = Camera_get_pixelHeight_m722276884(L_38, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_40 = Vector2_op_Division_m132623573(NULL /*static, unused*/, L_37, (((float)((float)L_39))), /*hidden argument*/NULL);
		Nullable_1_t3878791605  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Nullable_1__ctor_m680077965((&L_41), L_40, /*hidden argument*/Nullable_1__ctor_m680077965_RuntimeMethod_var);
		G_B15_0 = L_41;
		G_B15_1 = G_B13_0;
		G_B15_2 = G_B13_1;
		G_B15_3 = G_B13_2;
		goto IL_019d;
	}

IL_0193:
	{
		il2cpp_codegen_initobj((&V_8), sizeof(Nullable_1_t3878791605 ));
		Nullable_1_t3878791605  L_42 = V_8;
		G_B15_0 = L_42;
		G_B15_1 = G_B14_0;
		G_B15_2 = G_B14_1;
		G_B15_3 = G_B14_2;
	}

IL_019d:
	{
		V_8 = G_B15_0;
		bool L_43 = Nullable_1_get_HasValue_m1317965445((Nullable_1_t3878791605 *)(&V_8), /*hidden argument*/Nullable_1_get_HasValue_m1317965445_RuntimeMethod_var);
		G_B16_0 = G_B15_1;
		G_B16_1 = G_B15_2;
		G_B16_2 = G_B15_3;
		if (!L_43)
		{
			G_B17_0 = G_B15_1;
			G_B17_1 = G_B15_2;
			G_B17_2 = G_B15_3;
			goto IL_01c3;
		}
	}
	{
		Vector2_t2156229523  L_44 = Nullable_1_GetValueOrDefault_m3722452406((Nullable_1_t3878791605 *)(&V_8), /*hidden argument*/Nullable_1_GetValueOrDefault_m3722452406_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_45 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_44, (2.0f), /*hidden argument*/NULL);
		Nullable_1_t3878791605  L_46;
		memset(&L_46, 0, sizeof(L_46));
		Nullable_1__ctor_m680077965((&L_46), L_45, /*hidden argument*/Nullable_1__ctor_m680077965_RuntimeMethod_var);
		G_B18_0 = L_46;
		G_B18_1 = G_B16_0;
		G_B18_2 = G_B16_1;
		G_B18_3 = G_B16_2;
		goto IL_01cd;
	}

IL_01c3:
	{
		il2cpp_codegen_initobj((&V_9), sizeof(Nullable_1_t3878791605 ));
		Nullable_1_t3878791605  L_47 = V_9;
		G_B18_0 = L_47;
		G_B18_1 = G_B17_0;
		G_B18_2 = G_B17_1;
		G_B18_3 = G_B17_2;
	}

IL_01cd:
	{
		V_9 = G_B18_0;
		bool L_48 = Nullable_1_get_HasValue_m1317965445((Nullable_1_t3878791605 *)(&V_9), /*hidden argument*/Nullable_1_get_HasValue_m1317965445_RuntimeMethod_var);
		G_B19_0 = G_B18_1;
		G_B19_1 = G_B18_2;
		G_B19_2 = G_B18_3;
		if (!L_48)
		{
			G_B20_0 = G_B18_1;
			G_B20_1 = G_B18_2;
			G_B20_2 = G_B18_3;
			goto IL_01ec;
		}
	}
	{
		Vector2_t2156229523  L_49 = Nullable_1_GetValueOrDefault_m3722452406((Nullable_1_t3878791605 *)(&V_9), /*hidden argument*/Nullable_1_GetValueOrDefault_m3722452406_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_50 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		G_B21_0 = L_50;
		G_B21_1 = G_B19_0;
		G_B21_2 = G_B19_1;
		G_B21_3 = G_B19_2;
		goto IL_01f6;
	}

IL_01ec:
	{
		il2cpp_codegen_initobj((&V_10), sizeof(Vector3_t3722313464 ));
		Vector3_t3722313464  L_51 = V_10;
		G_B21_0 = L_51;
		G_B21_1 = G_B20_0;
		G_B21_2 = G_B20_1;
		G_B21_3 = G_B20_2;
	}

IL_01f6:
	{
		NullCheck(G_B21_1);
		Vector3_t3722313464  L_52 = Transform_TransformDirection_m3784028109(G_B21_1, G_B21_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_53 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, G_B21_2, L_52, /*hidden argument*/NULL);
		NullCheck(G_B21_3);
		Transform_set_position_m3387557959(G_B21_3, L_53, /*hidden argument*/NULL);
		Nullable_1U5BU5D_t3148003288* L_54 = __this->get_oldTouchPositions_2();
		NullCheck(L_54);
		Vector2_t2156229523  L_55 = V_4;
		Nullable_1_t3878791605  L_56;
		memset(&L_56, 0, sizeof(L_56));
		Nullable_1__ctor_m680077965((&L_56), L_55, /*hidden argument*/Nullable_1__ctor_m680077965_RuntimeMethod_var);
		*(Nullable_1_t3878791605 *)((L_54)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_56;
	}

IL_021d:
	{
		goto IL_0623;
	}

IL_0222:
	{
		Nullable_1U5BU5D_t3148003288* L_57 = __this->get_oldTouchPositions_2();
		NullCheck(L_57);
		V_11 = (*(Nullable_1_t3878791605 *)((L_57)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))));
		bool L_58 = Nullable_1_get_HasValue_m1317965445((Nullable_1_t3878791605 *)(&V_11), /*hidden argument*/Nullable_1_get_HasValue_m1317965445_RuntimeMethod_var);
		if (!((((int32_t)L_58) == ((int32_t)0))? 1 : 0))
		{
			goto IL_0311;
		}
	}
	{
		Nullable_1U5BU5D_t3148003288* L_59 = __this->get_oldTouchPositions_2();
		NullCheck(L_59);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Touch_t1921856868  L_60 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_12 = L_60;
		Vector2_t2156229523  L_61 = Touch_get_position_m3109777936((Touch_t1921856868 *)(&V_12), /*hidden argument*/NULL);
		Nullable_1_t3878791605  L_62;
		memset(&L_62, 0, sizeof(L_62));
		Nullable_1__ctor_m680077965((&L_62), L_61, /*hidden argument*/Nullable_1__ctor_m680077965_RuntimeMethod_var);
		*(Nullable_1_t3878791605 *)((L_59)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_62;
		Nullable_1U5BU5D_t3148003288* L_63 = __this->get_oldTouchPositions_2();
		NullCheck(L_63);
		Touch_t1921856868  L_64 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_13 = L_64;
		Vector2_t2156229523  L_65 = Touch_get_position_m3109777936((Touch_t1921856868 *)(&V_13), /*hidden argument*/NULL);
		Nullable_1_t3878791605  L_66;
		memset(&L_66, 0, sizeof(L_66));
		Nullable_1__ctor_m680077965((&L_66), L_65, /*hidden argument*/Nullable_1__ctor_m680077965_RuntimeMethod_var);
		*(Nullable_1_t3878791605 *)((L_63)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_66;
		Nullable_1U5BU5D_t3148003288* L_67 = __this->get_oldTouchPositions_2();
		NullCheck(L_67);
		V_14 = (*(Nullable_1_t3878791605 *)((L_67)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		bool L_68 = Nullable_1_get_HasValue_m1317965445((Nullable_1_t3878791605 *)(&V_14), /*hidden argument*/Nullable_1_get_HasValue_m1317965445_RuntimeMethod_var);
		Nullable_1U5BU5D_t3148003288* L_69 = __this->get_oldTouchPositions_2();
		NullCheck(L_69);
		V_15 = (*(Nullable_1_t3878791605 *)((L_69)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))));
		bool L_70 = Nullable_1_get_HasValue_m1317965445((Nullable_1_t3878791605 *)(&V_15), /*hidden argument*/Nullable_1_get_HasValue_m1317965445_RuntimeMethod_var);
		G_B25_0 = __this;
		if (!((int32_t)((int32_t)L_68&(int32_t)L_70)))
		{
			G_B26_0 = __this;
			goto IL_02e3;
		}
	}
	{
		Vector2_t2156229523  L_71 = Nullable_1_GetValueOrDefault_m3722452406((Nullable_1_t3878791605 *)(&V_14), /*hidden argument*/Nullable_1_GetValueOrDefault_m3722452406_RuntimeMethod_var);
		Vector2_t2156229523  L_72 = Nullable_1_GetValueOrDefault_m3722452406((Nullable_1_t3878791605 *)(&V_15), /*hidden argument*/Nullable_1_GetValueOrDefault_m3722452406_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_73 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_71, L_72, /*hidden argument*/NULL);
		Nullable_1_t3878791605  L_74;
		memset(&L_74, 0, sizeof(L_74));
		Nullable_1__ctor_m680077965((&L_74), L_73, /*hidden argument*/Nullable_1__ctor_m680077965_RuntimeMethod_var);
		G_B27_0 = L_74;
		G_B27_1 = G_B25_0;
		goto IL_02ed;
	}

IL_02e3:
	{
		il2cpp_codegen_initobj((&V_16), sizeof(Nullable_1_t3878791605 ));
		Nullable_1_t3878791605  L_75 = V_16;
		G_B27_0 = L_75;
		G_B27_1 = G_B26_0;
	}

IL_02ed:
	{
		V_16 = G_B27_0;
		Vector2_t2156229523  L_76 = Nullable_1_get_Value_m963759207((Nullable_1_t3878791605 *)(&V_16), /*hidden argument*/Nullable_1_get_Value_m963759207_RuntimeMethod_var);
		NullCheck(G_B27_1);
		G_B27_1->set_oldTouchVector_3(L_76);
		Vector2_t2156229523 * L_77 = __this->get_address_of_oldTouchVector_3();
		float L_78 = Vector2_get_magnitude_m2752892833((Vector2_t2156229523 *)L_77, /*hidden argument*/NULL);
		__this->set_oldTouchDistance_4(L_78);
		goto IL_0623;
	}

IL_0311:
	{
		Camera_t4157153871 * L_79 = Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var);
		NullCheck(L_79);
		int32_t L_80 = Camera_get_pixelWidth_m1110053668(L_79, /*hidden argument*/NULL);
		Camera_t4157153871 * L_81 = Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var);
		NullCheck(L_81);
		int32_t L_82 = Camera_get_pixelHeight_m722276884(L_81, /*hidden argument*/NULL);
		Vector2__ctor_m3970636864((Vector2_t2156229523 *)(&V_17), (((float)((float)L_80))), (((float)((float)L_82))), /*hidden argument*/NULL);
		Vector2U5BU5D_t1457185986* L_83 = ((Vector2U5BU5D_t1457185986*)SZArrayNew(Vector2U5BU5D_t1457185986_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_83);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Touch_t1921856868  L_84 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_19 = L_84;
		Vector2_t2156229523  L_85 = Touch_get_position_m3109777936((Touch_t1921856868 *)(&V_19), /*hidden argument*/NULL);
		*(Vector2_t2156229523 *)((L_83)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_85;
		Vector2U5BU5D_t1457185986* L_86 = L_83;
		NullCheck(L_86);
		Touch_t1921856868  L_87 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_20 = L_87;
		Vector2_t2156229523  L_88 = Touch_get_position_m3109777936((Touch_t1921856868 *)(&V_20), /*hidden argument*/NULL);
		*(Vector2_t2156229523 *)((L_86)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_88;
		V_18 = L_86;
		Vector2U5BU5D_t1457185986* L_89 = V_18;
		NullCheck(L_89);
		Vector2U5BU5D_t1457185986* L_90 = V_18;
		NullCheck(L_90);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_91 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, (*(Vector2_t2156229523 *)((L_89)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), (*(Vector2_t2156229523 *)((L_90)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))), /*hidden argument*/NULL);
		V_21 = L_91;
		float L_92 = Vector2_get_magnitude_m2752892833((Vector2_t2156229523 *)(&V_21), /*hidden argument*/NULL);
		V_22 = L_92;
		Transform_t3600365921 * L_93 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_94 = L_93;
		NullCheck(L_94);
		Vector3_t3722313464  L_95 = Transform_get_position_m36019626(L_94, /*hidden argument*/NULL);
		Transform_t3600365921 * L_96 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Nullable_1U5BU5D_t3148003288* L_97 = __this->get_oldTouchPositions_2();
		NullCheck(L_97);
		V_23 = (*(Nullable_1_t3878791605 *)((L_97)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		bool L_98 = Nullable_1_get_HasValue_m1317965445((Nullable_1_t3878791605 *)(&V_23), /*hidden argument*/Nullable_1_get_HasValue_m1317965445_RuntimeMethod_var);
		Nullable_1U5BU5D_t3148003288* L_99 = __this->get_oldTouchPositions_2();
		NullCheck(L_99);
		V_24 = (*(Nullable_1_t3878791605 *)((L_99)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))));
		bool L_100 = Nullable_1_get_HasValue_m1317965445((Nullable_1_t3878791605 *)(&V_24), /*hidden argument*/Nullable_1_get_HasValue_m1317965445_RuntimeMethod_var);
		G_B29_0 = L_96;
		G_B29_1 = L_95;
		G_B29_2 = L_94;
		if (!((int32_t)((int32_t)L_98&(int32_t)L_100)))
		{
			G_B30_0 = L_96;
			G_B30_1 = L_95;
			G_B30_2 = L_94;
			goto IL_03fe;
		}
	}
	{
		Vector2_t2156229523  L_101 = Nullable_1_GetValueOrDefault_m3722452406((Nullable_1_t3878791605 *)(&V_23), /*hidden argument*/Nullable_1_GetValueOrDefault_m3722452406_RuntimeMethod_var);
		Vector2_t2156229523  L_102 = Nullable_1_GetValueOrDefault_m3722452406((Nullable_1_t3878791605 *)(&V_24), /*hidden argument*/Nullable_1_GetValueOrDefault_m3722452406_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_103 = Vector2_op_Addition_m800700293(NULL /*static, unused*/, L_101, L_102, /*hidden argument*/NULL);
		Nullable_1_t3878791605  L_104;
		memset(&L_104, 0, sizeof(L_104));
		Nullable_1__ctor_m680077965((&L_104), L_103, /*hidden argument*/Nullable_1__ctor_m680077965_RuntimeMethod_var);
		G_B31_0 = L_104;
		G_B31_1 = G_B29_0;
		G_B31_2 = G_B29_1;
		G_B31_3 = G_B29_2;
		goto IL_0408;
	}

IL_03fe:
	{
		il2cpp_codegen_initobj((&V_25), sizeof(Nullable_1_t3878791605 ));
		Nullable_1_t3878791605  L_105 = V_25;
		G_B31_0 = L_105;
		G_B31_1 = G_B30_0;
		G_B31_2 = G_B30_1;
		G_B31_3 = G_B30_2;
	}

IL_0408:
	{
		V_25 = G_B31_0;
		bool L_106 = Nullable_1_get_HasValue_m1317965445((Nullable_1_t3878791605 *)(&V_25), /*hidden argument*/Nullable_1_get_HasValue_m1317965445_RuntimeMethod_var);
		G_B32_0 = G_B31_1;
		G_B32_1 = G_B31_2;
		G_B32_2 = G_B31_3;
		if (!L_106)
		{
			G_B33_0 = G_B31_1;
			G_B33_1 = G_B31_2;
			G_B33_2 = G_B31_3;
			goto IL_042b;
		}
	}
	{
		Vector2_t2156229523  L_107 = Nullable_1_GetValueOrDefault_m3722452406((Nullable_1_t3878791605 *)(&V_25), /*hidden argument*/Nullable_1_GetValueOrDefault_m3722452406_RuntimeMethod_var);
		Vector2_t2156229523  L_108 = V_17;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_109 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_107, L_108, /*hidden argument*/NULL);
		Nullable_1_t3878791605  L_110;
		memset(&L_110, 0, sizeof(L_110));
		Nullable_1__ctor_m680077965((&L_110), L_109, /*hidden argument*/Nullable_1__ctor_m680077965_RuntimeMethod_var);
		G_B34_0 = L_110;
		G_B34_1 = G_B32_0;
		G_B34_2 = G_B32_1;
		G_B34_3 = G_B32_2;
		goto IL_0435;
	}

IL_042b:
	{
		il2cpp_codegen_initobj((&V_26), sizeof(Nullable_1_t3878791605 ));
		Nullable_1_t3878791605  L_111 = V_26;
		G_B34_0 = L_111;
		G_B34_1 = G_B33_0;
		G_B34_2 = G_B33_1;
		G_B34_3 = G_B33_2;
	}

IL_0435:
	{
		V_26 = G_B34_0;
		bool L_112 = Nullable_1_get_HasValue_m1317965445((Nullable_1_t3878791605 *)(&V_26), /*hidden argument*/Nullable_1_get_HasValue_m1317965445_RuntimeMethod_var);
		G_B35_0 = G_B34_1;
		G_B35_1 = G_B34_2;
		G_B35_2 = G_B34_3;
		if (!L_112)
		{
			G_B36_0 = G_B34_1;
			G_B36_1 = G_B34_2;
			G_B36_2 = G_B34_3;
			goto IL_0461;
		}
	}
	{
		Vector2_t2156229523  L_113 = Nullable_1_GetValueOrDefault_m3722452406((Nullable_1_t3878791605 *)(&V_26), /*hidden argument*/Nullable_1_GetValueOrDefault_m3722452406_RuntimeMethod_var);
		Camera_t4157153871 * L_114 = Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var);
		NullCheck(L_114);
		float L_115 = Camera_get_orthographicSize_m3903216845(L_114, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_116 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_113, L_115, /*hidden argument*/NULL);
		Nullable_1_t3878791605  L_117;
		memset(&L_117, 0, sizeof(L_117));
		Nullable_1__ctor_m680077965((&L_117), L_116, /*hidden argument*/Nullable_1__ctor_m680077965_RuntimeMethod_var);
		G_B37_0 = L_117;
		G_B37_1 = G_B35_0;
		G_B37_2 = G_B35_1;
		G_B37_3 = G_B35_2;
		goto IL_046b;
	}

IL_0461:
	{
		il2cpp_codegen_initobj((&V_27), sizeof(Nullable_1_t3878791605 ));
		Nullable_1_t3878791605  L_118 = V_27;
		G_B37_0 = L_118;
		G_B37_1 = G_B36_0;
		G_B37_2 = G_B36_1;
		G_B37_3 = G_B36_2;
	}

IL_046b:
	{
		V_27 = G_B37_0;
		bool L_119 = Nullable_1_get_HasValue_m1317965445((Nullable_1_t3878791605 *)(&V_27), /*hidden argument*/Nullable_1_get_HasValue_m1317965445_RuntimeMethod_var);
		G_B38_0 = G_B37_1;
		G_B38_1 = G_B37_2;
		G_B38_2 = G_B37_3;
		if (!L_119)
		{
			G_B39_0 = G_B37_1;
			G_B39_1 = G_B37_2;
			G_B39_2 = G_B37_3;
			goto IL_0493;
		}
	}
	{
		Vector2_t2156229523  L_120 = Nullable_1_GetValueOrDefault_m3722452406((Nullable_1_t3878791605 *)(&V_27), /*hidden argument*/Nullable_1_GetValueOrDefault_m3722452406_RuntimeMethod_var);
		float L_121 = (&V_17)->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_122 = Vector2_op_Division_m132623573(NULL /*static, unused*/, L_120, L_121, /*hidden argument*/NULL);
		Nullable_1_t3878791605  L_123;
		memset(&L_123, 0, sizeof(L_123));
		Nullable_1__ctor_m680077965((&L_123), L_122, /*hidden argument*/Nullable_1__ctor_m680077965_RuntimeMethod_var);
		G_B40_0 = L_123;
		G_B40_1 = G_B38_0;
		G_B40_2 = G_B38_1;
		G_B40_3 = G_B38_2;
		goto IL_049d;
	}

IL_0493:
	{
		il2cpp_codegen_initobj((&V_28), sizeof(Nullable_1_t3878791605 ));
		Nullable_1_t3878791605  L_124 = V_28;
		G_B40_0 = L_124;
		G_B40_1 = G_B39_0;
		G_B40_2 = G_B39_1;
		G_B40_3 = G_B39_2;
	}

IL_049d:
	{
		V_28 = G_B40_0;
		bool L_125 = Nullable_1_get_HasValue_m1317965445((Nullable_1_t3878791605 *)(&V_28), /*hidden argument*/Nullable_1_get_HasValue_m1317965445_RuntimeMethod_var);
		G_B41_0 = G_B40_1;
		G_B41_1 = G_B40_2;
		G_B41_2 = G_B40_3;
		if (!L_125)
		{
			G_B42_0 = G_B40_1;
			G_B42_1 = G_B40_2;
			G_B42_2 = G_B40_3;
			goto IL_04bc;
		}
	}
	{
		Vector2_t2156229523  L_126 = Nullable_1_GetValueOrDefault_m3722452406((Nullable_1_t3878791605 *)(&V_28), /*hidden argument*/Nullable_1_GetValueOrDefault_m3722452406_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_127 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_126, /*hidden argument*/NULL);
		G_B43_0 = L_127;
		G_B43_1 = G_B41_0;
		G_B43_2 = G_B41_1;
		G_B43_3 = G_B41_2;
		goto IL_04c6;
	}

IL_04bc:
	{
		il2cpp_codegen_initobj((&V_10), sizeof(Vector3_t3722313464 ));
		Vector3_t3722313464  L_128 = V_10;
		G_B43_0 = L_128;
		G_B43_1 = G_B42_0;
		G_B43_2 = G_B42_1;
		G_B43_3 = G_B42_2;
	}

IL_04c6:
	{
		NullCheck(G_B43_1);
		Vector3_t3722313464  L_129 = Transform_TransformDirection_m3784028109(G_B43_1, G_B43_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_130 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, G_B43_2, L_129, /*hidden argument*/NULL);
		NullCheck(G_B43_3);
		Transform_set_position_m3387557959(G_B43_3, L_130, /*hidden argument*/NULL);
		Transform_t3600365921 * L_131 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_132 = L_131;
		NullCheck(L_132);
		Quaternion_t2301928331  L_133 = Transform_get_localRotation_m3487911431(L_132, /*hidden argument*/NULL);
		Vector2_t2156229523 * L_134 = __this->get_address_of_oldTouchVector_3();
		float L_135 = L_134->get_y_1();
		float L_136 = (&V_21)->get_x_0();
		Vector2_t2156229523 * L_137 = __this->get_address_of_oldTouchVector_3();
		float L_138 = L_137->get_x_0();
		float L_139 = (&V_21)->get_y_1();
		float L_140 = __this->get_oldTouchDistance_4();
		float L_141 = V_22;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_142 = Mathf_Clamp_m3350697880(NULL /*static, unused*/, ((float)((float)((float)((float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_135, (float)L_136)), (float)((float)il2cpp_codegen_multiply((float)L_138, (float)L_139))))/(float)L_140))/(float)L_141)), (-1.0f), (1.0f), /*hidden argument*/NULL);
		float L_143 = asinf(L_142);
		Vector3_t3722313464  L_144;
		memset(&L_144, 0, sizeof(L_144));
		Vector3__ctor_m3353183577((&L_144), (0.0f), (0.0f), ((float)((float)L_143/(float)(0.0174532924f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_145 = Quaternion_Euler_m1803555822(NULL /*static, unused*/, L_144, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_146 = Quaternion_op_Multiply_m1294064023(NULL /*static, unused*/, L_133, L_145, /*hidden argument*/NULL);
		NullCheck(L_132);
		Transform_set_localRotation_m19445462(L_132, L_146, /*hidden argument*/NULL);
		Camera_t4157153871 * L_147 = Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var);
		Camera_t4157153871 * L_148 = L_147;
		NullCheck(L_148);
		float L_149 = Camera_get_orthographicSize_m3903216845(L_148, /*hidden argument*/NULL);
		float L_150 = __this->get_oldTouchDistance_4();
		float L_151 = V_22;
		NullCheck(L_148);
		Camera_set_orthographicSize_m76971700(L_148, ((float)il2cpp_codegen_multiply((float)L_149, (float)((float)((float)L_150/(float)L_151)))), /*hidden argument*/NULL);
		Transform_t3600365921 * L_152 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_153 = L_152;
		NullCheck(L_153);
		Vector3_t3722313464  L_154 = Transform_get_position_m36019626(L_153, /*hidden argument*/NULL);
		Transform_t3600365921 * L_155 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector2U5BU5D_t1457185986* L_156 = V_18;
		NullCheck(L_156);
		Vector2U5BU5D_t1457185986* L_157 = V_18;
		NullCheck(L_157);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_158 = Vector2_op_Addition_m800700293(NULL /*static, unused*/, (*(Vector2_t2156229523 *)((L_156)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), (*(Vector2_t2156229523 *)((L_157)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))), /*hidden argument*/NULL);
		Vector2_t2156229523  L_159 = V_17;
		Vector2_t2156229523  L_160 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_158, L_159, /*hidden argument*/NULL);
		Camera_t4157153871 * L_161 = Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var);
		NullCheck(L_161);
		float L_162 = Camera_get_orthographicSize_m3903216845(L_161, /*hidden argument*/NULL);
		Vector2_t2156229523  L_163 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_160, L_162, /*hidden argument*/NULL);
		float L_164 = (&V_17)->get_y_1();
		Vector2_t2156229523  L_165 = Vector2_op_Division_m132623573(NULL /*static, unused*/, L_163, L_164, /*hidden argument*/NULL);
		Vector3_t3722313464  L_166 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_165, /*hidden argument*/NULL);
		NullCheck(L_155);
		Vector3_t3722313464  L_167 = Transform_TransformDirection_m3784028109(L_155, L_166, /*hidden argument*/NULL);
		Vector3_t3722313464  L_168 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_154, L_167, /*hidden argument*/NULL);
		NullCheck(L_153);
		Transform_set_position_m3387557959(L_153, L_168, /*hidden argument*/NULL);
		Nullable_1U5BU5D_t3148003288* L_169 = __this->get_oldTouchPositions_2();
		NullCheck(L_169);
		Vector2U5BU5D_t1457185986* L_170 = V_18;
		NullCheck(L_170);
		Nullable_1_t3878791605  L_171;
		memset(&L_171, 0, sizeof(L_171));
		Nullable_1__ctor_m680077965((&L_171), (*(Vector2_t2156229523 *)((L_170)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/Nullable_1__ctor_m680077965_RuntimeMethod_var);
		*(Nullable_1_t3878791605 *)((L_169)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_171;
		Nullable_1U5BU5D_t3148003288* L_172 = __this->get_oldTouchPositions_2();
		NullCheck(L_172);
		Vector2U5BU5D_t1457185986* L_173 = V_18;
		NullCheck(L_173);
		Nullable_1_t3878791605  L_174;
		memset(&L_174, 0, sizeof(L_174));
		Nullable_1__ctor_m680077965((&L_174), (*(Vector2_t2156229523 *)((L_173)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))), /*hidden argument*/Nullable_1__ctor_m680077965_RuntimeMethod_var);
		*(Nullable_1_t3878791605 *)((L_172)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_174;
		Vector2_t2156229523  L_175 = V_21;
		__this->set_oldTouchVector_3(L_175);
		float L_176 = V_22;
		__this->set_oldTouchDistance_4(L_176);
	}

IL_0623:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TrackableSettings::.ctor()
extern "C"  void TrackableSettings__ctor_m1376286375 (TrackableSettings_t2862243993 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean TrackableSettings::IsExtendedTrackingEnabled()
extern "C"  bool TrackableSettings_IsExtendedTrackingEnabled_m1319587722 (TrackableSettings_t2862243993 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_mExtTrackingEnabled_2();
		return L_0;
	}
}
// System.Void TrackableSettings::SwitchExtendedTracking(System.Boolean)
extern "C"  void TrackableSettings_SwitchExtendedTracking_m1275921245 (TrackableSettings_t2862243993 * __this, bool ___extTrackingEnabled0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableSettings_SwitchExtendedTracking_m1275921245_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StateManager_t1982749557 * V_0 = NULL;
	bool V_1 = false;
	TrackableBehaviour_t1113559212 * V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	ImageTargetBehaviour_t2200418350 * V_4 = NULL;
	MultiTargetBehaviour_t2061511750 * V_5 = NULL;
	CylinderTargetBehaviour_t822809409 * V_6 = NULL;
	ObjectTargetBehaviour_t728125005 * V_7 = NULL;
	VuMarkBehaviour_t1178230459 * V_8 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	TrackableSettings_t2862243993 * G_B43_0 = NULL;
	TrackableSettings_t2862243993 * G_B42_0 = NULL;
	int32_t G_B44_0 = 0;
	TrackableSettings_t2862243993 * G_B44_1 = NULL;
	{
		RuntimeObject* L_0 = TrackerManager_get_Instance_m777262631(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		StateManager_t1982749557 * L_1 = InterfaceFuncInvoker0< StateManager_t1982749557 * >::Invoke(3 /* Vuforia.StateManager Vuforia.ITrackerManager::GetStateManager() */, ITrackerManager_t607206903_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		V_1 = (bool)1;
		StateManager_t1982749557 * L_2 = V_0;
		NullCheck(L_2);
		RuntimeObject* L_3 = StateManager_GetTrackableBehaviours_m3331997292(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour>::GetEnumerator() */, IEnumerable_1_t93412101_il2cpp_TypeInfo_var, L_3);
		V_3 = L_4;
	}

IL_0019:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01dd;
		}

IL_001e:
		{
			RuntimeObject* L_5 = V_3;
			NullCheck(L_5);
			TrackableBehaviour_t1113559212 * L_6 = InterfaceFuncInvoker0< TrackableBehaviour_t1113559212 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Vuforia.TrackableBehaviour>::get_Current() */, IEnumerator_1_t1546129680_il2cpp_TypeInfo_var, L_5);
			V_2 = L_6;
			TrackableBehaviour_t1113559212 * L_7 = V_2;
			if (!((ImageTargetBehaviour_t2200418350 *)IsInstClass((RuntimeObject*)L_7, ImageTargetBehaviour_t2200418350_il2cpp_TypeInfo_var)))
			{
				goto IL_007e;
			}
		}

IL_0030:
		{
			TrackableBehaviour_t1113559212 * L_8 = V_2;
			V_4 = ((ImageTargetBehaviour_t2200418350 *)IsInstClass((RuntimeObject*)L_8, ImageTargetBehaviour_t2200418350_il2cpp_TypeInfo_var));
			bool L_9 = ___extTrackingEnabled0;
			if (!L_9)
			{
				goto IL_006c;
			}
		}

IL_003e:
		{
			ImageTargetBehaviour_t2200418350 * L_10 = V_4;
			NullCheck(L_10);
			RuntimeObject* L_11 = ImageTargetBehaviour_get_ImageTarget_m2707173779(L_10, /*hidden argument*/NULL);
			NullCheck(L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean Vuforia.ObjectTarget::StartExtendedTracking() */, ObjectTarget_t3212252422_il2cpp_TypeInfo_var, L_11);
			if (L_12)
			{
				goto IL_0067;
			}
		}

IL_004f:
		{
			V_1 = (bool)0;
			ImageTargetBehaviour_t2200418350 * L_13 = V_4;
			NullCheck(L_13);
			String_t* L_14 = TrackableBehaviour_get_TrackableName_m3644057705(L_13, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_15 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2304731189, L_14, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_LogError_m2850623458(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		}

IL_0067:
		{
			goto IL_0079;
		}

IL_006c:
		{
			ImageTargetBehaviour_t2200418350 * L_16 = V_4;
			NullCheck(L_16);
			RuntimeObject* L_17 = ImageTargetBehaviour_get_ImageTarget_m2707173779(L_16, /*hidden argument*/NULL);
			NullCheck(L_17);
			InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean Vuforia.ObjectTarget::StopExtendedTracking() */, ObjectTarget_t3212252422_il2cpp_TypeInfo_var, L_17);
		}

IL_0079:
		{
			goto IL_01dd;
		}

IL_007e:
		{
			TrackableBehaviour_t1113559212 * L_18 = V_2;
			if (!((MultiTargetBehaviour_t2061511750 *)IsInstClass((RuntimeObject*)L_18, MultiTargetBehaviour_t2061511750_il2cpp_TypeInfo_var)))
			{
				goto IL_00d7;
			}
		}

IL_0089:
		{
			TrackableBehaviour_t1113559212 * L_19 = V_2;
			V_5 = ((MultiTargetBehaviour_t2061511750 *)IsInstClass((RuntimeObject*)L_19, MultiTargetBehaviour_t2061511750_il2cpp_TypeInfo_var));
			bool L_20 = ___extTrackingEnabled0;
			if (!L_20)
			{
				goto IL_00c5;
			}
		}

IL_0097:
		{
			MultiTargetBehaviour_t2061511750 * L_21 = V_5;
			NullCheck(L_21);
			RuntimeObject* L_22 = MultiTargetBehaviour_get_MultiTarget_m1306973800(L_21, /*hidden argument*/NULL);
			NullCheck(L_22);
			bool L_23 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean Vuforia.ObjectTarget::StartExtendedTracking() */, ObjectTarget_t3212252422_il2cpp_TypeInfo_var, L_22);
			if (L_23)
			{
				goto IL_00c0;
			}
		}

IL_00a8:
		{
			V_1 = (bool)0;
			MultiTargetBehaviour_t2061511750 * L_24 = V_5;
			NullCheck(L_24);
			String_t* L_25 = TrackableBehaviour_get_TrackableName_m3644057705(L_24, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_26 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2304731189, L_25, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_LogError_m2850623458(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		}

IL_00c0:
		{
			goto IL_00d2;
		}

IL_00c5:
		{
			MultiTargetBehaviour_t2061511750 * L_27 = V_5;
			NullCheck(L_27);
			RuntimeObject* L_28 = MultiTargetBehaviour_get_MultiTarget_m1306973800(L_27, /*hidden argument*/NULL);
			NullCheck(L_28);
			InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean Vuforia.ObjectTarget::StopExtendedTracking() */, ObjectTarget_t3212252422_il2cpp_TypeInfo_var, L_28);
		}

IL_00d2:
		{
			goto IL_01dd;
		}

IL_00d7:
		{
			TrackableBehaviour_t1113559212 * L_29 = V_2;
			if (!((CylinderTargetBehaviour_t822809409 *)IsInstClass((RuntimeObject*)L_29, CylinderTargetBehaviour_t822809409_il2cpp_TypeInfo_var)))
			{
				goto IL_0130;
			}
		}

IL_00e2:
		{
			TrackableBehaviour_t1113559212 * L_30 = V_2;
			V_6 = ((CylinderTargetBehaviour_t822809409 *)IsInstClass((RuntimeObject*)L_30, CylinderTargetBehaviour_t822809409_il2cpp_TypeInfo_var));
			bool L_31 = ___extTrackingEnabled0;
			if (!L_31)
			{
				goto IL_011e;
			}
		}

IL_00f0:
		{
			CylinderTargetBehaviour_t822809409 * L_32 = V_6;
			NullCheck(L_32);
			RuntimeObject* L_33 = CylinderTargetBehaviour_get_CylinderTarget_m2779698828(L_32, /*hidden argument*/NULL);
			NullCheck(L_33);
			bool L_34 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean Vuforia.ObjectTarget::StartExtendedTracking() */, ObjectTarget_t3212252422_il2cpp_TypeInfo_var, L_33);
			if (L_34)
			{
				goto IL_0119;
			}
		}

IL_0101:
		{
			V_1 = (bool)0;
			CylinderTargetBehaviour_t822809409 * L_35 = V_6;
			NullCheck(L_35);
			String_t* L_36 = TrackableBehaviour_get_TrackableName_m3644057705(L_35, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_37 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2304731189, L_36, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_LogError_m2850623458(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		}

IL_0119:
		{
			goto IL_012b;
		}

IL_011e:
		{
			CylinderTargetBehaviour_t822809409 * L_38 = V_6;
			NullCheck(L_38);
			RuntimeObject* L_39 = CylinderTargetBehaviour_get_CylinderTarget_m2779698828(L_38, /*hidden argument*/NULL);
			NullCheck(L_39);
			InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean Vuforia.ObjectTarget::StopExtendedTracking() */, ObjectTarget_t3212252422_il2cpp_TypeInfo_var, L_39);
		}

IL_012b:
		{
			goto IL_01dd;
		}

IL_0130:
		{
			TrackableBehaviour_t1113559212 * L_40 = V_2;
			if (!((ObjectTargetBehaviour_t728125005 *)IsInstClass((RuntimeObject*)L_40, ObjectTargetBehaviour_t728125005_il2cpp_TypeInfo_var)))
			{
				goto IL_0189;
			}
		}

IL_013b:
		{
			TrackableBehaviour_t1113559212 * L_41 = V_2;
			V_7 = ((ObjectTargetBehaviour_t728125005 *)IsInstClass((RuntimeObject*)L_41, ObjectTargetBehaviour_t728125005_il2cpp_TypeInfo_var));
			bool L_42 = ___extTrackingEnabled0;
			if (!L_42)
			{
				goto IL_0177;
			}
		}

IL_0149:
		{
			ObjectTargetBehaviour_t728125005 * L_43 = V_7;
			NullCheck(L_43);
			RuntimeObject* L_44 = ObjectTargetBehaviour_get_ObjectTarget_m1153386099(L_43, /*hidden argument*/NULL);
			NullCheck(L_44);
			bool L_45 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean Vuforia.ObjectTarget::StartExtendedTracking() */, ObjectTarget_t3212252422_il2cpp_TypeInfo_var, L_44);
			if (L_45)
			{
				goto IL_0172;
			}
		}

IL_015a:
		{
			V_1 = (bool)0;
			ObjectTargetBehaviour_t728125005 * L_46 = V_7;
			NullCheck(L_46);
			String_t* L_47 = TrackableBehaviour_get_TrackableName_m3644057705(L_46, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_48 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2304731189, L_47, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_LogError_m2850623458(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		}

IL_0172:
		{
			goto IL_0184;
		}

IL_0177:
		{
			ObjectTargetBehaviour_t728125005 * L_49 = V_7;
			NullCheck(L_49);
			RuntimeObject* L_50 = ObjectTargetBehaviour_get_ObjectTarget_m1153386099(L_49, /*hidden argument*/NULL);
			NullCheck(L_50);
			InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean Vuforia.ObjectTarget::StopExtendedTracking() */, ObjectTarget_t3212252422_il2cpp_TypeInfo_var, L_50);
		}

IL_0184:
		{
			goto IL_01dd;
		}

IL_0189:
		{
			TrackableBehaviour_t1113559212 * L_51 = V_2;
			if (!((VuMarkBehaviour_t1178230459 *)IsInstClass((RuntimeObject*)L_51, VuMarkBehaviour_t1178230459_il2cpp_TypeInfo_var)))
			{
				goto IL_01dd;
			}
		}

IL_0194:
		{
			TrackableBehaviour_t1113559212 * L_52 = V_2;
			V_8 = ((VuMarkBehaviour_t1178230459 *)IsInstClass((RuntimeObject*)L_52, VuMarkBehaviour_t1178230459_il2cpp_TypeInfo_var));
			bool L_53 = ___extTrackingEnabled0;
			if (!L_53)
			{
				goto IL_01d0;
			}
		}

IL_01a2:
		{
			VuMarkBehaviour_t1178230459 * L_54 = V_8;
			NullCheck(L_54);
			RuntimeObject* L_55 = VuMarkBehaviour_get_VuMarkTemplate_m3771121130(L_54, /*hidden argument*/NULL);
			NullCheck(L_55);
			bool L_56 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean Vuforia.ObjectTarget::StartExtendedTracking() */, ObjectTarget_t3212252422_il2cpp_TypeInfo_var, L_55);
			if (L_56)
			{
				goto IL_01cb;
			}
		}

IL_01b3:
		{
			V_1 = (bool)0;
			VuMarkBehaviour_t1178230459 * L_57 = V_8;
			NullCheck(L_57);
			String_t* L_58 = TrackableBehaviour_get_TrackableName_m3644057705(L_57, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_59 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2304731189, L_58, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_LogError_m2850623458(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		}

IL_01cb:
		{
			goto IL_01dd;
		}

IL_01d0:
		{
			VuMarkBehaviour_t1178230459 * L_60 = V_8;
			NullCheck(L_60);
			RuntimeObject* L_61 = VuMarkBehaviour_get_VuMarkTemplate_m3771121130(L_60, /*hidden argument*/NULL);
			NullCheck(L_61);
			InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean Vuforia.ObjectTarget::StopExtendedTracking() */, ObjectTarget_t3212252422_il2cpp_TypeInfo_var, L_61);
		}

IL_01dd:
		{
			RuntimeObject* L_62 = V_3;
			NullCheck(L_62);
			bool L_63 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_62);
			if (L_63)
			{
				goto IL_001e;
			}
		}

IL_01e8:
		{
			IL2CPP_LEAVE(0x1FA, FINALLY_01ed);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_01ed;
	}

FINALLY_01ed:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_64 = V_3;
			if (!L_64)
			{
				goto IL_01f9;
			}
		}

IL_01f3:
		{
			RuntimeObject* L_65 = V_3;
			NullCheck(L_65);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_65);
		}

IL_01f9:
		{
			IL2CPP_END_FINALLY(493)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(493)
	{
		IL2CPP_JUMP_TBL(0x1FA, IL_01fa)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_01fa:
	{
		bool L_66 = V_1;
		G_B42_0 = __this;
		if (!L_66)
		{
			G_B43_0 = __this;
			goto IL_0204;
		}
	}
	{
		bool L_67 = ___extTrackingEnabled0;
		G_B44_0 = ((int32_t)(L_67));
		G_B44_1 = G_B42_0;
		goto IL_0205;
	}

IL_0204:
	{
		G_B44_0 = 0;
		G_B44_1 = G_B43_0;
	}

IL_0205:
	{
		NullCheck(G_B44_1);
		G_B44_1->set_mExtTrackingEnabled_2((bool)G_B44_0);
		return;
	}
}
// System.String TrackableSettings::GetActiveDatasetName()
extern "C"  String_t* TrackableSettings_GetActiveDatasetName_m1771968640 (TrackableSettings_t2862243993 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableSettings_GetActiveDatasetName_m1771968640_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectTracker_t4177997237 * V_0 = NULL;
	List_1_t463142320 * V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	{
		RuntimeObject* L_0 = TrackerManager_get_Instance_m777262631(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectTracker_t4177997237 * L_1 = GenericInterfaceFuncInvoker0< ObjectTracker_t4177997237 * >::Invoke(ITrackerManager_GetTracker_TisObjectTracker_t4177997237_m1548597255_RuntimeMethod_var, L_0);
		V_0 = L_1;
		ObjectTracker_t4177997237 * L_2 = V_0;
		NullCheck(L_2);
		RuntimeObject* L_3 = ObjectTracker_GetActiveDataSets_m254019146(L_2, /*hidden argument*/NULL);
		List_1_t463142320 * L_4 = Enumerable_ToList_TisDataSet_t3286034874_m1984750843(NULL /*static, unused*/, L_3, /*hidden argument*/Enumerable_ToList_TisDataSet_t3286034874_m1984750843_RuntimeMethod_var);
		V_1 = L_4;
		List_1_t463142320 * L_5 = V_1;
		NullCheck(L_5);
		int32_t L_6 = List_1_get_Count_m3978133574(L_5, /*hidden argument*/List_1_get_Count_m3978133574_RuntimeMethod_var);
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0055;
		}
	}
	{
		List_1_t463142320 * L_7 = V_1;
		DataSet_t3286034874 * L_8 = Enumerable_ElementAt_TisDataSet_t3286034874_m1304043092(NULL /*static, unused*/, L_7, 0, /*hidden argument*/Enumerable_ElementAt_TisDataSet_t3286034874_m1304043092_RuntimeMethod_var);
		NullCheck(L_8);
		String_t* L_9 = DataSet_get_Path_m176743276(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		String_t* L_10 = V_2;
		String_t* L_11 = V_2;
		NullCheck(L_11);
		int32_t L_12 = String_LastIndexOf_m2676535141(L_11, _stringLiteral3452614529, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_13 = String_Substring_m2848979100(L_10, ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1)), /*hidden argument*/NULL);
		V_3 = L_13;
		String_t* L_14 = V_3;
		NullCheck(_stringLiteral3932363835);
		CharU5BU5D_t3528271667* L_15 = String_ToCharArray_m1492846834(_stringLiteral3932363835, /*hidden argument*/NULL);
		NullCheck(L_14);
		String_t* L_16 = String_TrimEnd_m3824727301(L_14, L_15, /*hidden argument*/NULL);
		return L_16;
	}

IL_0055:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		return L_17;
	}
}
// System.Void TrackableSettings::ActivateDataSet(System.String)
extern "C"  void TrackableSettings_ActivateDataSet_m4118878993 (TrackableSettings_t2862243993 * __this, String_t* ___datasetName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackableSettings_ActivateDataSet_m4118878993_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectTracker_t4177997237 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	List_1_t463142320 * V_3 = NULL;
	DataSet_t3286034874 * V_4 = NULL;
	Enumerator_t2352386197  V_5;
	memset(&V_5, 0, sizeof(V_5));
	DataSet_t3286034874 * V_6 = NULL;
	RuntimeObject* V_7 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeObject* L_0 = TrackerManager_get_Instance_m777262631(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectTracker_t4177997237 * L_1 = GenericInterfaceFuncInvoker0< ObjectTracker_t4177997237 * >::Invoke(ITrackerManager_GetTracker_TisObjectTracker_t4177997237_m1548597255_RuntimeMethod_var, L_0);
		V_0 = L_1;
		ObjectTracker_t4177997237 * L_2 = V_0;
		NullCheck(L_2);
		RuntimeObject* L_3 = ObjectTracker_GetDataSets_m1245718448(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		ObjectTracker_t4177997237 * L_4 = V_0;
		NullCheck(L_4);
		RuntimeObject* L_5 = ObjectTracker_GetActiveDataSets_m254019146(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		RuntimeObject* L_6 = V_2;
		List_1_t463142320 * L_7 = Enumerable_ToList_TisDataSet_t3286034874_m1984750843(NULL /*static, unused*/, L_6, /*hidden argument*/Enumerable_ToList_TisDataSet_t3286034874_m1984750843_RuntimeMethod_var);
		V_3 = L_7;
		List_1_t463142320 * L_8 = V_3;
		NullCheck(L_8);
		Enumerator_t2352386197  L_9 = List_1_GetEnumerator_m6144716(L_8, /*hidden argument*/List_1_GetEnumerator_m6144716_RuntimeMethod_var);
		V_5 = L_9;
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003f;
		}

IL_002d:
		{
			DataSet_t3286034874 * L_10 = Enumerator_get_Current_m1064371760((Enumerator_t2352386197 *)(&V_5), /*hidden argument*/Enumerator_get_Current_m1064371760_RuntimeMethod_var);
			V_4 = L_10;
			ObjectTracker_t4177997237 * L_11 = V_0;
			DataSet_t3286034874 * L_12 = V_4;
			NullCheck(L_11);
			ObjectTracker_DeactivateDataSet_m2094146088(L_11, L_12, /*hidden argument*/NULL);
		}

IL_003f:
		{
			bool L_13 = Enumerator_MoveNext_m1234730920((Enumerator_t2352386197 *)(&V_5), /*hidden argument*/Enumerator_MoveNext_m1234730920_RuntimeMethod_var);
			if (L_13)
			{
				goto IL_002d;
			}
		}

IL_004b:
		{
			IL2CPP_LEAVE(0x5E, FINALLY_0050);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0050;
	}

FINALLY_0050:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1247034833((Enumerator_t2352386197 *)(&V_5), /*hidden argument*/Enumerator_Dispose_m1247034833_RuntimeMethod_var);
		IL2CPP_END_FINALLY(80)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(80)
	{
		IL2CPP_JUMP_TBL(0x5E, IL_005e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_005e:
	{
		ObjectTracker_t4177997237 * L_14 = V_0;
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(5 /* System.Void Vuforia.Tracker::Stop() */, L_14);
		RuntimeObject* L_15 = V_1;
		NullCheck(L_15);
		RuntimeObject* L_16 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Vuforia.DataSet>::GetEnumerator() */, IEnumerable_1_t2265887763_il2cpp_TypeInfo_var, L_15);
		V_7 = L_16;
	}

IL_006c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0095;
		}

IL_0071:
		{
			RuntimeObject* L_17 = V_7;
			NullCheck(L_17);
			DataSet_t3286034874 * L_18 = InterfaceFuncInvoker0< DataSet_t3286034874 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Vuforia.DataSet>::get_Current() */, IEnumerator_1_t3718605342_il2cpp_TypeInfo_var, L_17);
			V_6 = L_18;
			DataSet_t3286034874 * L_19 = V_6;
			NullCheck(L_19);
			String_t* L_20 = DataSet_get_Path_m176743276(L_19, /*hidden argument*/NULL);
			String_t* L_21 = ___datasetName0;
			NullCheck(L_20);
			bool L_22 = String_Contains_m1147431944(L_20, L_21, /*hidden argument*/NULL);
			if (!L_22)
			{
				goto IL_0095;
			}
		}

IL_008c:
		{
			ObjectTracker_t4177997237 * L_23 = V_0;
			DataSet_t3286034874 * L_24 = V_6;
			NullCheck(L_23);
			ObjectTracker_ActivateDataSet_m3009523340(L_23, L_24, /*hidden argument*/NULL);
		}

IL_0095:
		{
			RuntimeObject* L_25 = V_7;
			NullCheck(L_25);
			bool L_26 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_25);
			if (L_26)
			{
				goto IL_0071;
			}
		}

IL_00a1:
		{
			IL2CPP_LEAVE(0xB5, FINALLY_00a6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00a6;
	}

FINALLY_00a6:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_27 = V_7;
			if (!L_27)
			{
				goto IL_00b4;
			}
		}

IL_00ad:
		{
			RuntimeObject* L_28 = V_7;
			NullCheck(L_28);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_28);
		}

IL_00b4:
		{
			IL2CPP_END_FINALLY(166)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(166)
	{
		IL2CPP_JUMP_TBL(0xB5, IL_00b5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00b5:
	{
		ObjectTracker_t4177997237 * L_29 = V_0;
		NullCheck(L_29);
		VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean Vuforia.Tracker::Start() */, L_29);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void triggerAudio::.ctor()
extern "C"  void triggerAudio__ctor_m409198648 (triggerAudio_t1885395141 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void triggerAudio::OnEnable()
extern "C"  void triggerAudio_OnEnable_m4092205599 (triggerAudio_t1885395141 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (triggerAudio_OnEnable_m4092205599_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_soundBusy_2();
		if (L_0)
		{
			goto IL_002f;
		}
	}
	{
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		AudioSource_t3935305588 * L_2 = GameObject_GetComponent_TisAudioSource_t3935305588_m625814604(L_1, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t3935305588_m625814604_RuntimeMethod_var);
		NullCheck(L_2);
		AudioSource_Play_m48294159(L_2, /*hidden argument*/NULL);
		RuntimeObject* L_3 = triggerAudio_SoundCooldown_m3107703534(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_3, /*hidden argument*/NULL);
		Behaviour_set_enabled_m20417929(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Collections.IEnumerator triggerAudio::SoundCooldown()
extern "C"  RuntimeObject* triggerAudio_SoundCooldown_m3107703534 (triggerAudio_t1885395141 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (triggerAudio_SoundCooldown_m3107703534_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSoundCooldownU3Ec__Iterator0_t604438267 * V_0 = NULL;
	{
		U3CSoundCooldownU3Ec__Iterator0_t604438267 * L_0 = (U3CSoundCooldownU3Ec__Iterator0_t604438267 *)il2cpp_codegen_object_new(U3CSoundCooldownU3Ec__Iterator0_t604438267_il2cpp_TypeInfo_var);
		U3CSoundCooldownU3Ec__Iterator0__ctor_m968837215(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSoundCooldownU3Ec__Iterator0_t604438267 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		U3CSoundCooldownU3Ec__Iterator0_t604438267 * L_2 = V_0;
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void triggerAudio/<SoundCooldown>c__Iterator0::.ctor()
extern "C"  void U3CSoundCooldownU3Ec__Iterator0__ctor_m968837215 (U3CSoundCooldownU3Ec__Iterator0_t604438267 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean triggerAudio/<SoundCooldown>c__Iterator0::MoveNext()
extern "C"  bool U3CSoundCooldownU3Ec__Iterator0_MoveNext_m3232324018 (U3CSoundCooldownU3Ec__Iterator0_t604438267 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSoundCooldownU3Ec__Iterator0_MoveNext_m3232324018_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0072;
			}
		}
	}
	{
		goto IL_0085;
	}

IL_0021:
	{
		triggerAudio_t1885395141 * L_2 = __this->get_U24this_1();
		NullCheck(L_2);
		L_2->set_soundBusy_2((bool)1);
		triggerAudio_t1885395141 * L_3 = __this->get_U24this_1();
		NullCheck(L_3);
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Animation_t3648466861 * L_5 = GameObject_GetComponent_TisAnimation_t3648466861_m3766528019(L_4, /*hidden argument*/GameObject_GetComponent_TisAnimation_t3648466861_m3766528019_RuntimeMethod_var);
		NullCheck(L_5);
		AnimationClip_t2318505987 * L_6 = Animation_get_clip_m1019546398(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		float L_7 = AnimationClip_get_length_m3296085482(L_6, /*hidden argument*/NULL);
		__this->set_U3CanimationLengthU3E__0_0(L_7);
		float L_8 = __this->get_U3CanimationLengthU3E__0_0();
		WaitForSeconds_t1699091251 * L_9 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_9, L_8, /*hidden argument*/NULL);
		__this->set_U24current_2(L_9);
		bool L_10 = __this->get_U24disposing_3();
		if (L_10)
		{
			goto IL_006d;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_006d:
	{
		goto IL_0087;
	}

IL_0072:
	{
		triggerAudio_t1885395141 * L_11 = __this->get_U24this_1();
		NullCheck(L_11);
		L_11->set_soundBusy_2((bool)0);
		__this->set_U24PC_4((-1));
	}

IL_0085:
	{
		return (bool)0;
	}

IL_0087:
	{
		return (bool)1;
	}
}
// System.Object triggerAudio/<SoundCooldown>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CSoundCooldownU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1841473112 (U3CSoundCooldownU3Ec__Iterator0_t604438267 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object triggerAudio/<SoundCooldown>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CSoundCooldownU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1988765353 (U3CSoundCooldownU3Ec__Iterator0_t604438267 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void triggerAudio/<SoundCooldown>c__Iterator0::Dispose()
extern "C"  void U3CSoundCooldownU3Ec__Iterator0_Dispose_m1957076382 (U3CSoundCooldownU3Ec__Iterator0_t604438267 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void triggerAudio/<SoundCooldown>c__Iterator0::Reset()
extern "C"  void U3CSoundCooldownU3Ec__Iterator0_Reset_m1882823724 (U3CSoundCooldownU3Ec__Iterator0_t604438267 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSoundCooldownU3Ec__Iterator0_Reset_m1882823724_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CSoundCooldownU3Ec__Iterator0_Reset_m1882823724_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void tutorialManager::.ctor()
extern "C"  void tutorialManager__ctor_m438860424 (tutorialManager_t3421782323 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void tutorialManager::Awake()
extern "C"  void tutorialManager_Awake_m2139072952 (tutorialManager_t3421782323 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (tutorialManager_Awake_m2139072952_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m166252750(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		GameObjectU5BU5D_t3328599146* L_0 = GameObject_FindGameObjectsWithTag_m2585173894(NULL /*static, unused*/, _stringLiteral1271059651, /*hidden argument*/NULL);
		NullCheck(L_0);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_0028;
		}
	}
	{
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_0038;
	}

IL_0028:
	{
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_set_tag_m2353670106(L_2, _stringLiteral1271059651, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void tutorialManager::Start()
extern "C"  void tutorialManager_Start_m1569919508 (tutorialManager_t3421782323 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasShowed_3();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		tutorialManager_Show_m1645464898(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void tutorialManager::Show()
extern "C"  void tutorialManager_Show_m1645464898 (tutorialManager_t3421782323 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (tutorialManager_Show_m1645464898_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_hasShowed_3((bool)1);
		GameObject_t1113636619 * L_0 = __this->get_tutorial_2();
		NullCheck(L_0);
		Canvas_t3310196443 * L_1 = GameObject_GetComponent_TisCanvas_t3310196443_m1808013672(L_0, /*hidden argument*/GameObject_GetComponent_TisCanvas_t3310196443_m1808013672_RuntimeMethod_var);
		NullCheck(L_1);
		Behaviour_set_enabled_m20417929(L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
