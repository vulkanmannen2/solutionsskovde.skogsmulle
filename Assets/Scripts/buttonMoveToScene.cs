﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class buttonMoveToScene : MonoBehaviour {

    public int nextLvlBuildIndex;

    public void LoadLevel ()
    {
        SceneManager.LoadScene(nextLvlBuildIndex);
    }
}
