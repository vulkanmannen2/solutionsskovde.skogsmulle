﻿using UnityEngine;
using System.Collections;
using System.Linq;
[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour {

    private Quaternion startRotation;
    private Vector3 startPosition;
    private Vector3 parentStartPosition;
#if UNITY_EDITOR
    private Vector2 prevMousePos;
#endif

    // Use this for initialization
    void Start () {
        parentStartPosition = transform.parent.position;
        startRotation = transform.rotation;
        startPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        var touches = Input.touches.Select(t => t.deltaPosition).ToArray();
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            prevMousePos = Input.mousePosition;
        }
        if (Input.GetMouseButton(0))
        {
            touches = new[] { (Vector2)Input.mousePosition - prevMousePos };
        }
#endif
        switch(touches.Length)
        {
            case 2:
                UpdatePanning(touches[0], touches[1]);
                break;
            case 1:
                UpdateRotate(touches[0]);
                break;
            default:
                break;
        }
#if UNITY_EDITOR
        if(Input.GetMouseButton(0))
        {
            prevMousePos = Input.mousePosition;
        }
        
#endif
    }

    private void UpdateRotate(Vector2 touch0)
    {
        transform.RotateAround(transform.parent.position, Vector3.up, touch0.x);
        transform.RotateAround(transform.parent.position, transform.right * (-1f), touch0.y);
    }

    private void UpdatePanning(Vector2 touch0, Vector2 touch1)
    {
        transform.parent.position += transform.up * touch1.y* -0.01f + transform.right * touch1.x * -0.01f;
    }


    public void ResetToStart()
    {
        transform.parent.position = parentStartPosition;
        transform.rotation = startRotation;
        transform.position = startPosition;
    }
}
