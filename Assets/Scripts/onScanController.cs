﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class onScanController : MonoBehaviour {

    characterLoader CharacterLoader;
    public string CharacterID;

    [TextArea(3, 10)]
    public string scanText;

    public Sprite[] symbols;

    GameObject currentCharacter;

    GameObject uiCanvas;
    GameObject speechText;
    GameObject speechSymbols;

    public Animation mouthAnim;

    static bool someoneIsPlaying = false;
    bool imPlaying = false;

    public bool ShowTailAnimation = false;
   

    private void Awake()
    {
        someoneIsPlaying = false;

        uiCanvas = GameObject.FindGameObjectWithTag("speechUI");
        speechText = GameObject.FindGameObjectWithTag("speechText");
        speechSymbols = GameObject.FindGameObjectWithTag("speechSymbols");

        CharacterLoader = GameObject.FindGameObjectWithTag("characterLoader").GetComponent<characterLoader>();

        characterSlot currentCharacterPrefab = null;

        for (int i = 0; i < CharacterLoader.characterList.Length; i ++)
        {
            if (CharacterLoader.characterList[i].CharacterID == CharacterID)
                currentCharacterPrefab = CharacterLoader.characterList[i];
        }

        if (CharacterID != null)
        {
            currentCharacter = Instantiate(currentCharacterPrefab.CharacterPrefab, transform, false);
        }

        if (currentCharacter.GetComponentInChildren<Animation>() != null)
        {
            mouthAnim = currentCharacter.GetComponentInChildren<Animation>();
        }
    }

    private void Update()
    {
        if (gameObject.GetComponent<AudioSource>().isPlaying && currentCharacter != null && currentCharacter.GetComponent<Animator>() != null)
        {
            currentCharacter.GetComponent<Animator>().SetBool("isTalking", true);
           /* if (mouthAnim != null)
                if (!mouthAnim.isPlaying)
                    mouthAnim.Play();*/
            //print(currentCharacter.name + " is talking");
        }
        else if (!gameObject.GetComponent<AudioSource>().isPlaying && currentCharacter != null && currentCharacter.GetComponent<Animator>() != null)
        {
            currentCharacter.GetComponent<Animator>().SetBool("isTalking", false);
            /*if (mouthAnim != null)
                if (mouthAnim.isPlaying)
                {
                    mouthAnim.Stop();
                    mouthAnim[mouthAnim.clip.name].time = 0;
                    currentCharacter.GetComponentInChildren<animateTexture>().changeToTexture(0);
                }*/
        }

        if(imPlaying && !gameObject.GetComponent<AudioSource>().isPlaying)
        {
            imPlaying = false;
            someoneIsPlaying = false;
            this.enabled = false;
        }
    }

    void OnEnable()
    {
        if(someoneIsPlaying)
        {
            this.enabled = false;
            return;
        }

        //Debug.Log("On Enable");


        // Looks into settingsmanager for setting called "Symbols" and "Text".
        bool usingSymbols = GameObject.Find("_SettingsManager").GetComponent<settingsManager>().settingslist[GameObject.Find("_SettingsManager").GetComponent<settingsManager>().findSetting("Symbols")].IsActive;
        bool usingText = GameObject.Find("_SettingsManager").GetComponent<settingsManager>().settingslist[GameObject.Find("_SettingsManager").GetComponent<settingsManager>().findSetting("Text")].IsActive;
        bool usingSound = GameObject.Find("_SettingsManager").GetComponent<settingsManager>().settingslist[GameObject.Find("_SettingsManager").GetComponent<settingsManager>().findSetting("Sound")].IsActive;

        if (usingSymbols || usingText)
        {
            uiCanvas.GetComponent<Canvas>().enabled = true;
            uiCanvas.GetComponent<Animation>().Play();
        }
        speechText.GetComponent<UnityEngine.UI.Text>().text = scanText;

        foreach (Transform child in speechSymbols.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        //Puts the symbols in a row when displayed. Uses height rather than width to determin spacing between symbols to fix some issues with UI scaling.
        //IMPORTANT: Symbols must have the same hieght and width. Otherwise, the spacing in the will row be incorrect.
        GameObject tempSymbol;
        Vector2 symbolOffset;
        for (int i = 0; i < symbols.Length; i++)
        {
            tempSymbol = Instantiate(uiCanvas.GetComponent<symbolPrefabDefiner>().symbolPrefab, speechSymbols.transform);
            float symbolHeight = tempSymbol.GetComponent<RectTransform>().rect.height * tempSymbol.GetComponent<RectTransform>().localScale.y;
            symbolOffset.x = (symbolHeight * 0.5f) + (symbolHeight * i) - (symbolHeight * symbols.Length / 2);
            symbolOffset.y = 0;
            tempSymbol.GetComponent<RectTransform>().anchoredPosition = symbolOffset;
            tempSymbol.GetComponent<UnityEngine.UI.Image>().sprite = symbols[i];
        }

        //Debug.Log("Play sound");

        if (!gameObject.GetComponent<AudioSource>().isPlaying && usingSound)
        {
            gameObject.GetComponent<AudioSource>().Play();
            imPlaying = true;
            someoneIsPlaying = true;
        }

        if(usingText)
        {
            speechText.GetComponent<UnityEngine.UI.Text>().enabled = true;
        }
        else if(usingSymbols)
        {
            speechSymbols.GetComponent<Canvas>().enabled = true;
        }

        if(!usingSound)
        {
            this.enabled = false;
        }

        //--------- Show tail ------------
        if(ShowTailAnimation)
        {
            currentCharacter.GetComponent<Animator>().SetBool("foundTail", false);
            currentCharacter.GetComponent<Animator>().Play("Idle");
            StartCoroutine(WaitAndPlayShowTail());
        }
    }

    IEnumerator WaitAndPlayShowTail()
    {
        yield return new WaitForSeconds(3);

        if (currentCharacter != null && currentCharacter.GetComponent<Animator>() != null)
        {
            currentCharacter.GetComponent<Animator>().SetBool("foundTail", true);
        }
    }
}
