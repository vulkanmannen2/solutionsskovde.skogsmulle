﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerAudio : MonoBehaviour {

    bool soundBusy = false;

    void OnEnable()
    {
        if (!soundBusy)
        {
            gameObject.GetComponent<AudioSource>().Play();
            StartCoroutine(SoundCooldown());
            enabled = false;
        }
    }

    private IEnumerator SoundCooldown()
    {
        soundBusy = true;

        float animationLength = gameObject.GetComponent<Animation>().clip.length;
        yield return new WaitForSeconds(animationLength);

        soundBusy = false;
    }
}
