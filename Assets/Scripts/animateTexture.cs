﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animateTexture : MonoBehaviour {

    public List<Texture> textureFrames;

    Material objectMaterial;

    int currentIndex = 0;

    // Use this for initialization
    void Start () {
        if (GetComponent<Renderer>() != null)
            objectMaterial = GetComponent<Renderer>().material;
    }
	
	// Update is called once per frame
	public void changeToTexture (int index) {
        objectMaterial.mainTexture = textureFrames[index];
        currentIndex = index;
	}

    public void changeToNextTexture()
    {
        if (currentIndex < textureFrames.Count -1)
            currentIndex++;
        else
            currentIndex = 0;
        objectMaterial.mainTexture = textureFrames[currentIndex];
    }
}
