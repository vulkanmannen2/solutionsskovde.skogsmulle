﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonTriggerAnimation : MonoBehaviour {

    bool animationBusy = false;

	// Use this for initialization
	public void PlayAnimation ()
    {
        if (!animationBusy)
        {
            gameObject.GetComponent<Animation>().Play();
            StartCoroutine(AnimationCooldown());
        }
        
    }

    // Stops new animations from starting whiles another is playing
    private IEnumerator AnimationCooldown ()
    {
        animationBusy = true;

        float animationLength = gameObject.GetComponent<Animation>().clip.length;
        yield return new WaitForSeconds(animationLength);

        animationBusy = false;
    }
}
