﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonColorToggle : MonoBehaviour {

    public bool toggleOn;

    public bool useSprite;

    public string settingName; //a string to find what the setting is called in SettingsManager. Is used to check whether the option is on or off upon returning to the menu.

    public Color OnColor = new Vector4(0,0.7f,0,1);
    public Color OffColor = new Vector4(0.7f, 0, 0, 1);

    public Sprite OnSprite;
    public Sprite OffSprite;

    public string OnText = "On";
    public string OffText = "Off";

    string buttonText;

    private void Start()
    {
        toggleOn = !checkIfOn(settingName);
        buttonText = transform.GetComponentInChildren<UnityEngine.UI.Text>().text;
        toggleColor();
    }

    public void toggleColor(bool turnOfOtherSettingCall = false)
    {
        if(turnOfOtherSettingCall && !toggleOn)
        {
            return;
        }

        if (toggleOn)
        {
            if (!useSprite)
                GetComponent<UnityEngine.UI.Image>().color = OffColor;
            else
                GetComponent<UnityEngine.UI.Image>().sprite = OffSprite;

            transform.GetComponentInChildren<UnityEngine.UI.Text>().text = buttonText + " " + OffText;
            toggleOn = false;
        } else
        {
            if (!useSprite)
                GetComponent<UnityEngine.UI.Image>().color = OnColor;
            else
                GetComponent<UnityEngine.UI.Image>().sprite = OnSprite;

            transform.GetComponentInChildren<UnityEngine.UI.Text>().text = buttonText + " " + OnText;
            toggleOn = true;
        }
    }

    // Looks into settingsmanager for the called setting and returns wether the setting is true or false.
    bool checkIfOn (string calledSetting)
    {
        if (GameObject.Find("_SettingsManager").GetComponent<settingsManager>().settingslist[GameObject.Find("_SettingsManager").GetComponent<settingsManager>().findSetting(calledSetting)].IsActive)
        {
            return true;
        }
        else
            return false;
    }
}
