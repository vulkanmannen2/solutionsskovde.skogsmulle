﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animationScaling : MonoBehaviour {

    Vector3 orgSize;

    public Vector3 animaionSize = new Vector3(1,1,1);

	// Use this for initialization
	void Start () {
        orgSize = transform.localScale;
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 updateSize;

        updateSize.x = orgSize.x * animaionSize.x;
        updateSize.y = orgSize.y * animaionSize.y;
        updateSize.z = orgSize.z * animaionSize.z;

        transform.localScale = updateSize;
	}
}
