﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class characterSlot {

    public string CharacterID;
    public GameObject CharacterPrefab;

    public characterSlot(string newCharacterID, GameObject newCharacterPrefab)
    {
        CharacterID = newCharacterID;
        CharacterPrefab = newCharacterPrefab;
    }
}
