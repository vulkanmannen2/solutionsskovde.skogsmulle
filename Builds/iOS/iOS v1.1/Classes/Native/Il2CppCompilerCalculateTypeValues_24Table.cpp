﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// buttonTriggerAnimation
struct buttonTriggerAnimation_t4121295644;
// CameraSettings
struct CameraSettings_t3152619780;
// triggerAudio
struct triggerAudio_t1885395141;
// onScanController
struct onScanController_t1472212342;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Analytics.TrackableField
struct TrackableField_t1772682203;
// UnityEngine.Analytics.ValueProperty
struct ValueProperty_t1868393739;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t2151848540;
// System.Collections.Generic.Queue`1<UnityEngine.Quaternion>
struct Queue_1_t2148187825;
// System.Collections.Generic.Queue`1<UnityEngine.Vector3>
struct Queue_1_t3568572958;
// System.Collections.Generic.List`1<SettingsList>
struct List_1_t3355509319;
// UnityEngine.Audio.AudioMixer
struct AudioMixer_t3521020193;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1113559212;
// MenuAnimator
struct MenuAnimator_t2112910832;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// System.Collections.Generic.List`1<UnityEngine.Texture>
struct List_1_t839070149;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// MenuOptions
struct MenuOptions_t1951716431;
// TrackableSettings
struct TrackableSettings_t2862243993;
// UnityEngine.Animation
struct Animation_t3648466861;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// characterLoader
struct characterLoader_t1326495876;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// System.Func`2<UnityEngine.Touch,UnityEngine.Vector2>
struct Func_2_t528868469;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// characterSlot[]
struct characterSlotU5BU5D_t2121073899;
// System.Nullable`1<UnityEngine.Vector2>[]
struct Nullable_1U5BU5D_t3148003288;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.UI.FontData
struct FontData_t746620069;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745550_H
#define U3CMODULEU3E_T692745550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745550 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745550_H
#ifndef CHARACTERSLOT_T990761438_H
#define CHARACTERSLOT_T990761438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// characterSlot
struct  characterSlot_t990761438  : public RuntimeObject
{
public:
	// System.String characterSlot::CharacterID
	String_t* ___CharacterID_0;
	// UnityEngine.GameObject characterSlot::CharacterPrefab
	GameObject_t1113636619 * ___CharacterPrefab_1;

public:
	inline static int32_t get_offset_of_CharacterID_0() { return static_cast<int32_t>(offsetof(characterSlot_t990761438, ___CharacterID_0)); }
	inline String_t* get_CharacterID_0() const { return ___CharacterID_0; }
	inline String_t** get_address_of_CharacterID_0() { return &___CharacterID_0; }
	inline void set_CharacterID_0(String_t* value)
	{
		___CharacterID_0 = value;
		Il2CppCodeGenWriteBarrier((&___CharacterID_0), value);
	}

	inline static int32_t get_offset_of_CharacterPrefab_1() { return static_cast<int32_t>(offsetof(characterSlot_t990761438, ___CharacterPrefab_1)); }
	inline GameObject_t1113636619 * get_CharacterPrefab_1() const { return ___CharacterPrefab_1; }
	inline GameObject_t1113636619 ** get_address_of_CharacterPrefab_1() { return &___CharacterPrefab_1; }
	inline void set_CharacterPrefab_1(GameObject_t1113636619 * value)
	{
		___CharacterPrefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___CharacterPrefab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERSLOT_T990761438_H
#ifndef U3CLOADNEXTSCENEAFTERU3EC__ITERATOR0_T994227170_H
#define U3CLOADNEXTSCENEAFTERU3EC__ITERATOR0_T994227170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0
struct  U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170  : public RuntimeObject
{
public:
	// System.Single AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0::seconds
	float ___seconds_0;
	// System.Object AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 AsyncSceneLoader/<LoadNextSceneAfter>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_seconds_0() { return static_cast<int32_t>(offsetof(U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170, ___seconds_0)); }
	inline float get_seconds_0() const { return ___seconds_0; }
	inline float* get_address_of_seconds_0() { return &___seconds_0; }
	inline void set_seconds_0(float value)
	{
		___seconds_0 = value;
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADNEXTSCENEAFTERU3EC__ITERATOR0_T994227170_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CANIMATIONCOOLDOWNU3EC__ITERATOR0_T1141888348_H
#define U3CANIMATIONCOOLDOWNU3EC__ITERATOR0_T1141888348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// buttonTriggerAnimation/<AnimationCooldown>c__Iterator0
struct  U3CAnimationCooldownU3Ec__Iterator0_t1141888348  : public RuntimeObject
{
public:
	// System.Single buttonTriggerAnimation/<AnimationCooldown>c__Iterator0::<animationLength>__0
	float ___U3CanimationLengthU3E__0_0;
	// buttonTriggerAnimation buttonTriggerAnimation/<AnimationCooldown>c__Iterator0::$this
	buttonTriggerAnimation_t4121295644 * ___U24this_1;
	// System.Object buttonTriggerAnimation/<AnimationCooldown>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean buttonTriggerAnimation/<AnimationCooldown>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 buttonTriggerAnimation/<AnimationCooldown>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CanimationLengthU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimationCooldownU3Ec__Iterator0_t1141888348, ___U3CanimationLengthU3E__0_0)); }
	inline float get_U3CanimationLengthU3E__0_0() const { return ___U3CanimationLengthU3E__0_0; }
	inline float* get_address_of_U3CanimationLengthU3E__0_0() { return &___U3CanimationLengthU3E__0_0; }
	inline void set_U3CanimationLengthU3E__0_0(float value)
	{
		___U3CanimationLengthU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAnimationCooldownU3Ec__Iterator0_t1141888348, ___U24this_1)); }
	inline buttonTriggerAnimation_t4121295644 * get_U24this_1() const { return ___U24this_1; }
	inline buttonTriggerAnimation_t4121295644 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(buttonTriggerAnimation_t4121295644 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CAnimationCooldownU3Ec__Iterator0_t1141888348, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CAnimationCooldownU3Ec__Iterator0_t1141888348, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CAnimationCooldownU3Ec__Iterator0_t1141888348, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATIONCOOLDOWNU3EC__ITERATOR0_T1141888348_H
#ifndef U3CRESTOREORIGINALFOCUSMODEU3EC__ITERATOR0_T2912012229_H
#define U3CRESTOREORIGINALFOCUSMODEU3EC__ITERATOR0_T2912012229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraSettings/<RestoreOriginalFocusMode>c__Iterator0
struct  U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229  : public RuntimeObject
{
public:
	// CameraSettings CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::$this
	CameraSettings_t3152619780 * ___U24this_0;
	// System.Object CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 CameraSettings/<RestoreOriginalFocusMode>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229, ___U24this_0)); }
	inline CameraSettings_t3152619780 * get_U24this_0() const { return ___U24this_0; }
	inline CameraSettings_t3152619780 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(CameraSettings_t3152619780 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESTOREORIGINALFOCUSMODEU3EC__ITERATOR0_T2912012229_H
#ifndef TRIGGERMETHOD_T582536534_H
#define TRIGGERMETHOD_T582536534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerMethod
struct  TriggerMethod_t582536534  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERMETHOD_T582536534_H
#ifndef TRACKABLETRIGGER_T621205209_H
#define TRACKABLETRIGGER_T621205209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableTrigger
struct  TrackableTrigger_t621205209  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Analytics.TrackableTrigger::m_Target
	GameObject_t1113636619 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackableTrigger::m_MethodPath
	String_t* ___m_MethodPath_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_Target_0)); }
	inline GameObject_t1113636619 * get_m_Target_0() const { return ___m_Target_0; }
	inline GameObject_t1113636619 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(GameObject_t1113636619 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodPath_1() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_MethodPath_1)); }
	inline String_t* get_m_MethodPath_1() const { return ___m_MethodPath_1; }
	inline String_t** get_address_of_m_MethodPath_1() { return &___m_MethodPath_1; }
	inline void set_m_MethodPath_1(String_t* value)
	{
		___m_MethodPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodPath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLETRIGGER_T621205209_H
#ifndef SETTINGSLIST_T1883434577_H
#define SETTINGSLIST_T1883434577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsList
struct  SettingsList_t1883434577  : public RuntimeObject
{
public:
	// System.String SettingsList::SettingName
	String_t* ___SettingName_0;
	// System.Boolean SettingsList::IsActive
	bool ___IsActive_1;

public:
	inline static int32_t get_offset_of_SettingName_0() { return static_cast<int32_t>(offsetof(SettingsList_t1883434577, ___SettingName_0)); }
	inline String_t* get_SettingName_0() const { return ___SettingName_0; }
	inline String_t** get_address_of_SettingName_0() { return &___SettingName_0; }
	inline void set_SettingName_0(String_t* value)
	{
		___SettingName_0 = value;
		Il2CppCodeGenWriteBarrier((&___SettingName_0), value);
	}

	inline static int32_t get_offset_of_IsActive_1() { return static_cast<int32_t>(offsetof(SettingsList_t1883434577, ___IsActive_1)); }
	inline bool get_IsActive_1() const { return ___IsActive_1; }
	inline bool* get_address_of_IsActive_1() { return &___IsActive_1; }
	inline void set_IsActive_1(bool value)
	{
		___IsActive_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSLIST_T1883434577_H
#ifndef U3CSOUNDCOOLDOWNU3EC__ITERATOR0_T604438267_H
#define U3CSOUNDCOOLDOWNU3EC__ITERATOR0_T604438267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// triggerAudio/<SoundCooldown>c__Iterator0
struct  U3CSoundCooldownU3Ec__Iterator0_t604438267  : public RuntimeObject
{
public:
	// System.Single triggerAudio/<SoundCooldown>c__Iterator0::<animationLength>__0
	float ___U3CanimationLengthU3E__0_0;
	// triggerAudio triggerAudio/<SoundCooldown>c__Iterator0::$this
	triggerAudio_t1885395141 * ___U24this_1;
	// System.Object triggerAudio/<SoundCooldown>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean triggerAudio/<SoundCooldown>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 triggerAudio/<SoundCooldown>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CanimationLengthU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSoundCooldownU3Ec__Iterator0_t604438267, ___U3CanimationLengthU3E__0_0)); }
	inline float get_U3CanimationLengthU3E__0_0() const { return ___U3CanimationLengthU3E__0_0; }
	inline float* get_address_of_U3CanimationLengthU3E__0_0() { return &___U3CanimationLengthU3E__0_0; }
	inline void set_U3CanimationLengthU3E__0_0(float value)
	{
		___U3CanimationLengthU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CSoundCooldownU3Ec__Iterator0_t604438267, ___U24this_1)); }
	inline triggerAudio_t1885395141 * get_U24this_1() const { return ___U24this_1; }
	inline triggerAudio_t1885395141 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(triggerAudio_t1885395141 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CSoundCooldownU3Ec__Iterator0_t604438267, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CSoundCooldownU3Ec__Iterator0_t604438267, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CSoundCooldownU3Ec__Iterator0_t604438267, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSOUNDCOOLDOWNU3EC__ITERATOR0_T604438267_H
#ifndef U3CWAITANDPLAYSHOWTAILU3EC__ITERATOR0_T581090349_H
#define U3CWAITANDPLAYSHOWTAILU3EC__ITERATOR0_T581090349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// onScanController/<WaitAndPlayShowTail>c__Iterator0
struct  U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349  : public RuntimeObject
{
public:
	// onScanController onScanController/<WaitAndPlayShowTail>c__Iterator0::$this
	onScanController_t1472212342 * ___U24this_0;
	// System.Object onScanController/<WaitAndPlayShowTail>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean onScanController/<WaitAndPlayShowTail>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 onScanController/<WaitAndPlayShowTail>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349, ___U24this_0)); }
	inline onScanController_t1472212342 * get_U24this_0() const { return ___U24this_0; }
	inline onScanController_t1472212342 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(onScanController_t1472212342 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITANDPLAYSHOWTAILU3EC__ITERATOR0_T581090349_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TRIGGEROPERATOR_T3611898925_H
#define TRIGGEROPERATOR_T3611898925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerOperator
struct  TriggerOperator_t3611898925 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerOperator::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerOperator_t3611898925, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEROPERATOR_T3611898925_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef CAMERADIRECTION_T637748435_H
#define CAMERADIRECTION_T637748435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/CameraDirection
struct  CameraDirection_t637748435 
{
public:
	// System.Int32 Vuforia.CameraDevice/CameraDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraDirection_t637748435, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADIRECTION_T637748435_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef TRIGGERRULE_T1946298321_H
#define TRIGGERRULE_T1946298321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerRule
struct  TriggerRule_t1946298321  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.TriggerRule::m_Target
	TrackableField_t1772682203 * ___m_Target_0;
	// UnityEngine.Analytics.TriggerOperator UnityEngine.Analytics.TriggerRule::m_Operator
	int32_t ___m_Operator_1;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value
	ValueProperty_t1868393739 * ___m_Value_2;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value2
	ValueProperty_t1868393739 * ___m_Value2_3;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Target_0)); }
	inline TrackableField_t1772682203 * get_m_Target_0() const { return ___m_Target_0; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(TrackableField_t1772682203 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Operator_1() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Operator_1)); }
	inline int32_t get_m_Operator_1() const { return ___m_Operator_1; }
	inline int32_t* get_address_of_m_Operator_1() { return &___m_Operator_1; }
	inline void set_m_Operator_1(int32_t value)
	{
		___m_Operator_1 = value;
	}

	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value_2)); }
	inline ValueProperty_t1868393739 * get_m_Value_2() const { return ___m_Value_2; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(ValueProperty_t1868393739 * value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_2), value);
	}

	inline static int32_t get_offset_of_m_Value2_3() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value2_3)); }
	inline ValueProperty_t1868393739 * get_m_Value2_3() const { return ___m_Value2_3; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value2_3() { return &___m_Value2_3; }
	inline void set_m_Value2_3(ValueProperty_t1868393739 * value)
	{
		___m_Value2_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERRULE_T1946298321_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ONTRIGGER_T4184125570_H
#define ONTRIGGER_T4184125570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger/OnTrigger
struct  OnTrigger_t4184125570  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRIGGER_T4184125570_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SMOOTHCAMERA_T4222009864_H
#define SMOOTHCAMERA_T4222009864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmoothCamera
struct  SmoothCamera_t4222009864  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 SmoothCamera::smoothingFrames
	int32_t ___smoothingFrames_2;
	// Vuforia.VuforiaBehaviour SmoothCamera::qcarBehavior
	VuforiaBehaviour_t2151848540 * ___qcarBehavior_3;
	// UnityEngine.Quaternion SmoothCamera::smoothedRotation
	Quaternion_t2301928331  ___smoothedRotation_4;
	// UnityEngine.Vector3 SmoothCamera::smoothedPosition
	Vector3_t3722313464  ___smoothedPosition_5;
	// System.Collections.Generic.Queue`1<UnityEngine.Quaternion> SmoothCamera::rotations
	Queue_1_t2148187825 * ___rotations_6;
	// System.Collections.Generic.Queue`1<UnityEngine.Vector3> SmoothCamera::positions
	Queue_1_t3568572958 * ___positions_7;

public:
	inline static int32_t get_offset_of_smoothingFrames_2() { return static_cast<int32_t>(offsetof(SmoothCamera_t4222009864, ___smoothingFrames_2)); }
	inline int32_t get_smoothingFrames_2() const { return ___smoothingFrames_2; }
	inline int32_t* get_address_of_smoothingFrames_2() { return &___smoothingFrames_2; }
	inline void set_smoothingFrames_2(int32_t value)
	{
		___smoothingFrames_2 = value;
	}

	inline static int32_t get_offset_of_qcarBehavior_3() { return static_cast<int32_t>(offsetof(SmoothCamera_t4222009864, ___qcarBehavior_3)); }
	inline VuforiaBehaviour_t2151848540 * get_qcarBehavior_3() const { return ___qcarBehavior_3; }
	inline VuforiaBehaviour_t2151848540 ** get_address_of_qcarBehavior_3() { return &___qcarBehavior_3; }
	inline void set_qcarBehavior_3(VuforiaBehaviour_t2151848540 * value)
	{
		___qcarBehavior_3 = value;
		Il2CppCodeGenWriteBarrier((&___qcarBehavior_3), value);
	}

	inline static int32_t get_offset_of_smoothedRotation_4() { return static_cast<int32_t>(offsetof(SmoothCamera_t4222009864, ___smoothedRotation_4)); }
	inline Quaternion_t2301928331  get_smoothedRotation_4() const { return ___smoothedRotation_4; }
	inline Quaternion_t2301928331 * get_address_of_smoothedRotation_4() { return &___smoothedRotation_4; }
	inline void set_smoothedRotation_4(Quaternion_t2301928331  value)
	{
		___smoothedRotation_4 = value;
	}

	inline static int32_t get_offset_of_smoothedPosition_5() { return static_cast<int32_t>(offsetof(SmoothCamera_t4222009864, ___smoothedPosition_5)); }
	inline Vector3_t3722313464  get_smoothedPosition_5() const { return ___smoothedPosition_5; }
	inline Vector3_t3722313464 * get_address_of_smoothedPosition_5() { return &___smoothedPosition_5; }
	inline void set_smoothedPosition_5(Vector3_t3722313464  value)
	{
		___smoothedPosition_5 = value;
	}

	inline static int32_t get_offset_of_rotations_6() { return static_cast<int32_t>(offsetof(SmoothCamera_t4222009864, ___rotations_6)); }
	inline Queue_1_t2148187825 * get_rotations_6() const { return ___rotations_6; }
	inline Queue_1_t2148187825 ** get_address_of_rotations_6() { return &___rotations_6; }
	inline void set_rotations_6(Queue_1_t2148187825 * value)
	{
		___rotations_6 = value;
		Il2CppCodeGenWriteBarrier((&___rotations_6), value);
	}

	inline static int32_t get_offset_of_positions_7() { return static_cast<int32_t>(offsetof(SmoothCamera_t4222009864, ___positions_7)); }
	inline Queue_1_t3568572958 * get_positions_7() const { return ___positions_7; }
	inline Queue_1_t3568572958 ** get_address_of_positions_7() { return &___positions_7; }
	inline void set_positions_7(Queue_1_t3568572958 * value)
	{
		___positions_7 = value;
		Il2CppCodeGenWriteBarrier((&___positions_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHCAMERA_T4222009864_H
#ifndef SYMBOLPREFABDEFINER_T2798204667_H
#define SYMBOLPREFABDEFINER_T2798204667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// symbolPrefabDefiner
struct  symbolPrefabDefiner_t2798204667  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject symbolPrefabDefiner::symbolPrefab
	GameObject_t1113636619 * ___symbolPrefab_2;

public:
	inline static int32_t get_offset_of_symbolPrefab_2() { return static_cast<int32_t>(offsetof(symbolPrefabDefiner_t2798204667, ___symbolPrefab_2)); }
	inline GameObject_t1113636619 * get_symbolPrefab_2() const { return ___symbolPrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_symbolPrefab_2() { return &___symbolPrefab_2; }
	inline void set_symbolPrefab_2(GameObject_t1113636619 * value)
	{
		___symbolPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___symbolPrefab_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMBOLPREFABDEFINER_T2798204667_H
#ifndef VUFORIAMONOBEHAVIOUR_T1150221792_H
#define VUFORIAMONOBEHAVIOUR_T1150221792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VuforiaMonoBehaviour
struct  VuforiaMonoBehaviour_t1150221792  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMONOBEHAVIOUR_T1150221792_H
#ifndef SETTINGSMANAGER_T2086358119_H
#define SETTINGSMANAGER_T2086358119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// settingsManager
struct  settingsManager_t2086358119  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<SettingsList> settingsManager::settingslist
	List_1_t3355509319 * ___settingslist_2;
	// UnityEngine.Audio.AudioMixer settingsManager::MainMixer
	AudioMixer_t3521020193 * ___MainMixer_3;
	// System.Single settingsManager::orgMusicVol
	float ___orgMusicVol_4;
	// System.Single settingsManager::orgSoundVol
	float ___orgSoundVol_5;

public:
	inline static int32_t get_offset_of_settingslist_2() { return static_cast<int32_t>(offsetof(settingsManager_t2086358119, ___settingslist_2)); }
	inline List_1_t3355509319 * get_settingslist_2() const { return ___settingslist_2; }
	inline List_1_t3355509319 ** get_address_of_settingslist_2() { return &___settingslist_2; }
	inline void set_settingslist_2(List_1_t3355509319 * value)
	{
		___settingslist_2 = value;
		Il2CppCodeGenWriteBarrier((&___settingslist_2), value);
	}

	inline static int32_t get_offset_of_MainMixer_3() { return static_cast<int32_t>(offsetof(settingsManager_t2086358119, ___MainMixer_3)); }
	inline AudioMixer_t3521020193 * get_MainMixer_3() const { return ___MainMixer_3; }
	inline AudioMixer_t3521020193 ** get_address_of_MainMixer_3() { return &___MainMixer_3; }
	inline void set_MainMixer_3(AudioMixer_t3521020193 * value)
	{
		___MainMixer_3 = value;
		Il2CppCodeGenWriteBarrier((&___MainMixer_3), value);
	}

	inline static int32_t get_offset_of_orgMusicVol_4() { return static_cast<int32_t>(offsetof(settingsManager_t2086358119, ___orgMusicVol_4)); }
	inline float get_orgMusicVol_4() const { return ___orgMusicVol_4; }
	inline float* get_address_of_orgMusicVol_4() { return &___orgMusicVol_4; }
	inline void set_orgMusicVol_4(float value)
	{
		___orgMusicVol_4 = value;
	}

	inline static int32_t get_offset_of_orgSoundVol_5() { return static_cast<int32_t>(offsetof(settingsManager_t2086358119, ___orgSoundVol_5)); }
	inline float get_orgSoundVol_5() const { return ___orgSoundVol_5; }
	inline float* get_address_of_orgSoundVol_5() { return &___orgSoundVol_5; }
	inline void set_orgSoundVol_5(float value)
	{
		___orgSoundVol_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSMANAGER_T2086358119_H
#ifndef TUTORIALMANAGER_T3421782323_H
#define TUTORIALMANAGER_T3421782323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// tutorialManager
struct  tutorialManager_t3421782323  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject tutorialManager::tutorial
	GameObject_t1113636619 * ___tutorial_2;
	// System.Boolean tutorialManager::hasShowed
	bool ___hasShowed_3;

public:
	inline static int32_t get_offset_of_tutorial_2() { return static_cast<int32_t>(offsetof(tutorialManager_t3421782323, ___tutorial_2)); }
	inline GameObject_t1113636619 * get_tutorial_2() const { return ___tutorial_2; }
	inline GameObject_t1113636619 ** get_address_of_tutorial_2() { return &___tutorial_2; }
	inline void set_tutorial_2(GameObject_t1113636619 * value)
	{
		___tutorial_2 = value;
		Il2CppCodeGenWriteBarrier((&___tutorial_2), value);
	}

	inline static int32_t get_offset_of_hasShowed_3() { return static_cast<int32_t>(offsetof(tutorialManager_t3421782323, ___hasShowed_3)); }
	inline bool get_hasShowed_3() const { return ___hasShowed_3; }
	inline bool* get_address_of_hasShowed_3() { return &___hasShowed_3; }
	inline void set_hasShowed_3(bool value)
	{
		___hasShowed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALMANAGER_T3421782323_H
#ifndef DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#define DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultTrackableEventHandler
struct  DefaultTrackableEventHandler_t1588957063  : public MonoBehaviour_t3962482529
{
public:
	// Vuforia.TrackableBehaviour DefaultTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t1113559212 * ___mTrackableBehaviour_2;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_2() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___mTrackableBehaviour_2)); }
	inline TrackableBehaviour_t1113559212 * get_mTrackableBehaviour_2() const { return ___mTrackableBehaviour_2; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mTrackableBehaviour_2() { return &___mTrackableBehaviour_2; }
	inline void set_mTrackableBehaviour_2(TrackableBehaviour_t1113559212 * value)
	{
		___mTrackableBehaviour_2 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef SYMBOLSSWITCH_T3635132447_H
#define SYMBOLSSWITCH_T3635132447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// symbolsSwitch
struct  symbolsSwitch_t3635132447  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean symbolsSwitch::useSymbols
	bool ___useSymbols_2;

public:
	inline static int32_t get_offset_of_useSymbols_2() { return static_cast<int32_t>(offsetof(symbolsSwitch_t3635132447, ___useSymbols_2)); }
	inline bool get_useSymbols_2() const { return ___useSymbols_2; }
	inline bool* get_address_of_useSymbols_2() { return &___useSymbols_2; }
	inline void set_useSymbols_2(bool value)
	{
		___useSymbols_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMBOLSSWITCH_T3635132447_H
#ifndef TRIGGERAUDIO_T1885395141_H
#define TRIGGERAUDIO_T1885395141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// triggerAudio
struct  triggerAudio_t1885395141  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean triggerAudio::soundBusy
	bool ___soundBusy_2;

public:
	inline static int32_t get_offset_of_soundBusy_2() { return static_cast<int32_t>(offsetof(triggerAudio_t1885395141, ___soundBusy_2)); }
	inline bool get_soundBusy_2() const { return ___soundBusy_2; }
	inline bool* get_address_of_soundBusy_2() { return &___soundBusy_2; }
	inline void set_soundBusy_2(bool value)
	{
		___soundBusy_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERAUDIO_T1885395141_H
#ifndef SETSETTINGS_T4116128020_H
#define SETSETTINGS_T4116128020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// setSettings
struct  setSettings_t4116128020  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject setSettings::settingsManager
	GameObject_t1113636619 * ___settingsManager_2;

public:
	inline static int32_t get_offset_of_settingsManager_2() { return static_cast<int32_t>(offsetof(setSettings_t4116128020, ___settingsManager_2)); }
	inline GameObject_t1113636619 * get_settingsManager_2() const { return ___settingsManager_2; }
	inline GameObject_t1113636619 ** get_address_of_settingsManager_2() { return &___settingsManager_2; }
	inline void set_settingsManager_2(GameObject_t1113636619 * value)
	{
		___settingsManager_2 = value;
		Il2CppCodeGenWriteBarrier((&___settingsManager_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETSETTINGS_T4116128020_H
#ifndef TRACKABLESETTINGS_T2862243993_H
#define TRACKABLESETTINGS_T2862243993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrackableSettings
struct  TrackableSettings_t2862243993  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean TrackableSettings::mExtTrackingEnabled
	bool ___mExtTrackingEnabled_2;

public:
	inline static int32_t get_offset_of_mExtTrackingEnabled_2() { return static_cast<int32_t>(offsetof(TrackableSettings_t2862243993, ___mExtTrackingEnabled_2)); }
	inline bool get_mExtTrackingEnabled_2() const { return ___mExtTrackingEnabled_2; }
	inline bool* get_address_of_mExtTrackingEnabled_2() { return &___mExtTrackingEnabled_2; }
	inline void set_mExtTrackingEnabled_2(bool value)
	{
		___mExtTrackingEnabled_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLESETTINGS_T2862243993_H
#ifndef TAPHANDLER_T334234343_H
#define TAPHANDLER_T334234343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TapHandler
struct  TapHandler_t334234343  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TapHandler::mTimeSinceLastTap
	float ___mTimeSinceLastTap_3;
	// MenuAnimator TapHandler::mMenuAnim
	MenuAnimator_t2112910832 * ___mMenuAnim_4;
	// System.Int32 TapHandler::mTapCount
	int32_t ___mTapCount_5;

public:
	inline static int32_t get_offset_of_mTimeSinceLastTap_3() { return static_cast<int32_t>(offsetof(TapHandler_t334234343, ___mTimeSinceLastTap_3)); }
	inline float get_mTimeSinceLastTap_3() const { return ___mTimeSinceLastTap_3; }
	inline float* get_address_of_mTimeSinceLastTap_3() { return &___mTimeSinceLastTap_3; }
	inline void set_mTimeSinceLastTap_3(float value)
	{
		___mTimeSinceLastTap_3 = value;
	}

	inline static int32_t get_offset_of_mMenuAnim_4() { return static_cast<int32_t>(offsetof(TapHandler_t334234343, ___mMenuAnim_4)); }
	inline MenuAnimator_t2112910832 * get_mMenuAnim_4() const { return ___mMenuAnim_4; }
	inline MenuAnimator_t2112910832 ** get_address_of_mMenuAnim_4() { return &___mMenuAnim_4; }
	inline void set_mMenuAnim_4(MenuAnimator_t2112910832 * value)
	{
		___mMenuAnim_4 = value;
		Il2CppCodeGenWriteBarrier((&___mMenuAnim_4), value);
	}

	inline static int32_t get_offset_of_mTapCount_5() { return static_cast<int32_t>(offsetof(TapHandler_t334234343, ___mTapCount_5)); }
	inline int32_t get_mTapCount_5() const { return ___mTapCount_5; }
	inline int32_t* get_address_of_mTapCount_5() { return &___mTapCount_5; }
	inline void set_mTapCount_5(int32_t value)
	{
		___mTapCount_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPHANDLER_T334234343_H
#ifndef LOADINGSCREEN_T2154736699_H
#define LOADINGSCREEN_T2154736699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadingScreen
struct  LoadingScreen_t2154736699  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean LoadingScreen::mChangeLevel
	bool ___mChangeLevel_2;
	// UnityEngine.UI.RawImage LoadingScreen::mUISpinner
	RawImage_t3182918964 * ___mUISpinner_3;

public:
	inline static int32_t get_offset_of_mChangeLevel_2() { return static_cast<int32_t>(offsetof(LoadingScreen_t2154736699, ___mChangeLevel_2)); }
	inline bool get_mChangeLevel_2() const { return ___mChangeLevel_2; }
	inline bool* get_address_of_mChangeLevel_2() { return &___mChangeLevel_2; }
	inline void set_mChangeLevel_2(bool value)
	{
		___mChangeLevel_2 = value;
	}

	inline static int32_t get_offset_of_mUISpinner_3() { return static_cast<int32_t>(offsetof(LoadingScreen_t2154736699, ___mUISpinner_3)); }
	inline RawImage_t3182918964 * get_mUISpinner_3() const { return ___mUISpinner_3; }
	inline RawImage_t3182918964 ** get_address_of_mUISpinner_3() { return &___mUISpinner_3; }
	inline void set_mUISpinner_3(RawImage_t3182918964 * value)
	{
		___mUISpinner_3 = value;
		Il2CppCodeGenWriteBarrier((&___mUISpinner_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADINGSCREEN_T2154736699_H
#ifndef ANIMATETEXTURE_T712563028_H
#define ANIMATETEXTURE_T712563028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// animateTexture
struct  animateTexture_t712563028  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Texture> animateTexture::textureFrames
	List_1_t839070149 * ___textureFrames_2;
	// UnityEngine.Material animateTexture::objectMaterial
	Material_t340375123 * ___objectMaterial_3;
	// System.Int32 animateTexture::currentIndex
	int32_t ___currentIndex_4;

public:
	inline static int32_t get_offset_of_textureFrames_2() { return static_cast<int32_t>(offsetof(animateTexture_t712563028, ___textureFrames_2)); }
	inline List_1_t839070149 * get_textureFrames_2() const { return ___textureFrames_2; }
	inline List_1_t839070149 ** get_address_of_textureFrames_2() { return &___textureFrames_2; }
	inline void set_textureFrames_2(List_1_t839070149 * value)
	{
		___textureFrames_2 = value;
		Il2CppCodeGenWriteBarrier((&___textureFrames_2), value);
	}

	inline static int32_t get_offset_of_objectMaterial_3() { return static_cast<int32_t>(offsetof(animateTexture_t712563028, ___objectMaterial_3)); }
	inline Material_t340375123 * get_objectMaterial_3() const { return ___objectMaterial_3; }
	inline Material_t340375123 ** get_address_of_objectMaterial_3() { return &___objectMaterial_3; }
	inline void set_objectMaterial_3(Material_t340375123 * value)
	{
		___objectMaterial_3 = value;
		Il2CppCodeGenWriteBarrier((&___objectMaterial_3), value);
	}

	inline static int32_t get_offset_of_currentIndex_4() { return static_cast<int32_t>(offsetof(animateTexture_t712563028, ___currentIndex_4)); }
	inline int32_t get_currentIndex_4() const { return ___currentIndex_4; }
	inline int32_t* get_address_of_currentIndex_4() { return &___currentIndex_4; }
	inline void set_currentIndex_4(int32_t value)
	{
		___currentIndex_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATETEXTURE_T712563028_H
#ifndef BUTTONMOVETOSCENE_T2573241184_H
#define BUTTONMOVETOSCENE_T2573241184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// buttonMoveToScene
struct  buttonMoveToScene_t2573241184  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 buttonMoveToScene::nextLvlBuildIndex
	int32_t ___nextLvlBuildIndex_2;

public:
	inline static int32_t get_offset_of_nextLvlBuildIndex_2() { return static_cast<int32_t>(offsetof(buttonMoveToScene_t2573241184, ___nextLvlBuildIndex_2)); }
	inline int32_t get_nextLvlBuildIndex_2() const { return ___nextLvlBuildIndex_2; }
	inline int32_t* get_address_of_nextLvlBuildIndex_2() { return &___nextLvlBuildIndex_2; }
	inline void set_nextLvlBuildIndex_2(int32_t value)
	{
		___nextLvlBuildIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONMOVETOSCENE_T2573241184_H
#ifndef BUTTONCOLORTOGGLE_T2066501000_H
#define BUTTONCOLORTOGGLE_T2066501000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// buttonColorToggle
struct  buttonColorToggle_t2066501000  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean buttonColorToggle::toggleOn
	bool ___toggleOn_2;
	// System.Boolean buttonColorToggle::useSprite
	bool ___useSprite_3;
	// System.String buttonColorToggle::settingName
	String_t* ___settingName_4;
	// UnityEngine.Color buttonColorToggle::OnColor
	Color_t2555686324  ___OnColor_5;
	// UnityEngine.Color buttonColorToggle::OffColor
	Color_t2555686324  ___OffColor_6;
	// UnityEngine.Sprite buttonColorToggle::OnSprite
	Sprite_t280657092 * ___OnSprite_7;
	// UnityEngine.Sprite buttonColorToggle::OffSprite
	Sprite_t280657092 * ___OffSprite_8;
	// System.String buttonColorToggle::OnText
	String_t* ___OnText_9;
	// System.String buttonColorToggle::OffText
	String_t* ___OffText_10;
	// System.String buttonColorToggle::buttonText
	String_t* ___buttonText_11;

public:
	inline static int32_t get_offset_of_toggleOn_2() { return static_cast<int32_t>(offsetof(buttonColorToggle_t2066501000, ___toggleOn_2)); }
	inline bool get_toggleOn_2() const { return ___toggleOn_2; }
	inline bool* get_address_of_toggleOn_2() { return &___toggleOn_2; }
	inline void set_toggleOn_2(bool value)
	{
		___toggleOn_2 = value;
	}

	inline static int32_t get_offset_of_useSprite_3() { return static_cast<int32_t>(offsetof(buttonColorToggle_t2066501000, ___useSprite_3)); }
	inline bool get_useSprite_3() const { return ___useSprite_3; }
	inline bool* get_address_of_useSprite_3() { return &___useSprite_3; }
	inline void set_useSprite_3(bool value)
	{
		___useSprite_3 = value;
	}

	inline static int32_t get_offset_of_settingName_4() { return static_cast<int32_t>(offsetof(buttonColorToggle_t2066501000, ___settingName_4)); }
	inline String_t* get_settingName_4() const { return ___settingName_4; }
	inline String_t** get_address_of_settingName_4() { return &___settingName_4; }
	inline void set_settingName_4(String_t* value)
	{
		___settingName_4 = value;
		Il2CppCodeGenWriteBarrier((&___settingName_4), value);
	}

	inline static int32_t get_offset_of_OnColor_5() { return static_cast<int32_t>(offsetof(buttonColorToggle_t2066501000, ___OnColor_5)); }
	inline Color_t2555686324  get_OnColor_5() const { return ___OnColor_5; }
	inline Color_t2555686324 * get_address_of_OnColor_5() { return &___OnColor_5; }
	inline void set_OnColor_5(Color_t2555686324  value)
	{
		___OnColor_5 = value;
	}

	inline static int32_t get_offset_of_OffColor_6() { return static_cast<int32_t>(offsetof(buttonColorToggle_t2066501000, ___OffColor_6)); }
	inline Color_t2555686324  get_OffColor_6() const { return ___OffColor_6; }
	inline Color_t2555686324 * get_address_of_OffColor_6() { return &___OffColor_6; }
	inline void set_OffColor_6(Color_t2555686324  value)
	{
		___OffColor_6 = value;
	}

	inline static int32_t get_offset_of_OnSprite_7() { return static_cast<int32_t>(offsetof(buttonColorToggle_t2066501000, ___OnSprite_7)); }
	inline Sprite_t280657092 * get_OnSprite_7() const { return ___OnSprite_7; }
	inline Sprite_t280657092 ** get_address_of_OnSprite_7() { return &___OnSprite_7; }
	inline void set_OnSprite_7(Sprite_t280657092 * value)
	{
		___OnSprite_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnSprite_7), value);
	}

	inline static int32_t get_offset_of_OffSprite_8() { return static_cast<int32_t>(offsetof(buttonColorToggle_t2066501000, ___OffSprite_8)); }
	inline Sprite_t280657092 * get_OffSprite_8() const { return ___OffSprite_8; }
	inline Sprite_t280657092 ** get_address_of_OffSprite_8() { return &___OffSprite_8; }
	inline void set_OffSprite_8(Sprite_t280657092 * value)
	{
		___OffSprite_8 = value;
		Il2CppCodeGenWriteBarrier((&___OffSprite_8), value);
	}

	inline static int32_t get_offset_of_OnText_9() { return static_cast<int32_t>(offsetof(buttonColorToggle_t2066501000, ___OnText_9)); }
	inline String_t* get_OnText_9() const { return ___OnText_9; }
	inline String_t** get_address_of_OnText_9() { return &___OnText_9; }
	inline void set_OnText_9(String_t* value)
	{
		___OnText_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnText_9), value);
	}

	inline static int32_t get_offset_of_OffText_10() { return static_cast<int32_t>(offsetof(buttonColorToggle_t2066501000, ___OffText_10)); }
	inline String_t* get_OffText_10() const { return ___OffText_10; }
	inline String_t** get_address_of_OffText_10() { return &___OffText_10; }
	inline void set_OffText_10(String_t* value)
	{
		___OffText_10 = value;
		Il2CppCodeGenWriteBarrier((&___OffText_10), value);
	}

	inline static int32_t get_offset_of_buttonText_11() { return static_cast<int32_t>(offsetof(buttonColorToggle_t2066501000, ___buttonText_11)); }
	inline String_t* get_buttonText_11() const { return ___buttonText_11; }
	inline String_t** get_address_of_buttonText_11() { return &___buttonText_11; }
	inline void set_buttonText_11(String_t* value)
	{
		___buttonText_11 = value;
		Il2CppCodeGenWriteBarrier((&___buttonText_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONCOLORTOGGLE_T2066501000_H
#ifndef ANIMATIONSCALING_T1595398591_H
#define ANIMATIONSCALING_T1595398591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// animationScaling
struct  animationScaling_t1595398591  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 animationScaling::orgSize
	Vector3_t3722313464  ___orgSize_2;
	// UnityEngine.Vector3 animationScaling::animaionSize
	Vector3_t3722313464  ___animaionSize_3;

public:
	inline static int32_t get_offset_of_orgSize_2() { return static_cast<int32_t>(offsetof(animationScaling_t1595398591, ___orgSize_2)); }
	inline Vector3_t3722313464  get_orgSize_2() const { return ___orgSize_2; }
	inline Vector3_t3722313464 * get_address_of_orgSize_2() { return &___orgSize_2; }
	inline void set_orgSize_2(Vector3_t3722313464  value)
	{
		___orgSize_2 = value;
	}

	inline static int32_t get_offset_of_animaionSize_3() { return static_cast<int32_t>(offsetof(animationScaling_t1595398591, ___animaionSize_3)); }
	inline Vector3_t3722313464  get_animaionSize_3() const { return ___animaionSize_3; }
	inline Vector3_t3722313464 * get_address_of_animaionSize_3() { return &___animaionSize_3; }
	inline void set_animaionSize_3(Vector3_t3722313464  value)
	{
		___animaionSize_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSCALING_T1595398591_H
#ifndef INITERRORHANDLER_T2159361531_H
#define INITERRORHANDLER_T2159361531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InitErrorHandler
struct  InitErrorHandler_t2159361531  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text InitErrorHandler::errorText
	Text_t1901882714 * ___errorText_2;
	// UnityEngine.Canvas InitErrorHandler::errorCanvas
	Canvas_t3310196443 * ___errorCanvas_3;
	// System.String InitErrorHandler::key
	String_t* ___key_4;

public:
	inline static int32_t get_offset_of_errorText_2() { return static_cast<int32_t>(offsetof(InitErrorHandler_t2159361531, ___errorText_2)); }
	inline Text_t1901882714 * get_errorText_2() const { return ___errorText_2; }
	inline Text_t1901882714 ** get_address_of_errorText_2() { return &___errorText_2; }
	inline void set_errorText_2(Text_t1901882714 * value)
	{
		___errorText_2 = value;
		Il2CppCodeGenWriteBarrier((&___errorText_2), value);
	}

	inline static int32_t get_offset_of_errorCanvas_3() { return static_cast<int32_t>(offsetof(InitErrorHandler_t2159361531, ___errorCanvas_3)); }
	inline Canvas_t3310196443 * get_errorCanvas_3() const { return ___errorCanvas_3; }
	inline Canvas_t3310196443 ** get_address_of_errorCanvas_3() { return &___errorCanvas_3; }
	inline void set_errorCanvas_3(Canvas_t3310196443 * value)
	{
		___errorCanvas_3 = value;
		Il2CppCodeGenWriteBarrier((&___errorCanvas_3), value);
	}

	inline static int32_t get_offset_of_key_4() { return static_cast<int32_t>(offsetof(InitErrorHandler_t2159361531, ___key_4)); }
	inline String_t* get_key_4() const { return ___key_4; }
	inline String_t** get_address_of_key_4() { return &___key_4; }
	inline void set_key_4(String_t* value)
	{
		___key_4 = value;
		Il2CppCodeGenWriteBarrier((&___key_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITERRORHANDLER_T2159361531_H
#ifndef FRAMERATESETTINGS_T3598747490_H
#define FRAMERATESETTINGS_T3598747490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FrameRateSettings
struct  FrameRateSettings_t3598747490  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMERATESETTINGS_T3598747490_H
#ifndef CAMERASETTINGS_T3152619780_H
#define CAMERASETTINGS_T3152619780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraSettings
struct  CameraSettings_t3152619780  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CameraSettings::mVuforiaStarted
	bool ___mVuforiaStarted_2;
	// System.Boolean CameraSettings::mAutofocusEnabled
	bool ___mAutofocusEnabled_3;
	// System.Boolean CameraSettings::mFlashTorchEnabled
	bool ___mFlashTorchEnabled_4;
	// Vuforia.CameraDevice/CameraDirection CameraSettings::mActiveDirection
	int32_t ___mActiveDirection_5;

public:
	inline static int32_t get_offset_of_mVuforiaStarted_2() { return static_cast<int32_t>(offsetof(CameraSettings_t3152619780, ___mVuforiaStarted_2)); }
	inline bool get_mVuforiaStarted_2() const { return ___mVuforiaStarted_2; }
	inline bool* get_address_of_mVuforiaStarted_2() { return &___mVuforiaStarted_2; }
	inline void set_mVuforiaStarted_2(bool value)
	{
		___mVuforiaStarted_2 = value;
	}

	inline static int32_t get_offset_of_mAutofocusEnabled_3() { return static_cast<int32_t>(offsetof(CameraSettings_t3152619780, ___mAutofocusEnabled_3)); }
	inline bool get_mAutofocusEnabled_3() const { return ___mAutofocusEnabled_3; }
	inline bool* get_address_of_mAutofocusEnabled_3() { return &___mAutofocusEnabled_3; }
	inline void set_mAutofocusEnabled_3(bool value)
	{
		___mAutofocusEnabled_3 = value;
	}

	inline static int32_t get_offset_of_mFlashTorchEnabled_4() { return static_cast<int32_t>(offsetof(CameraSettings_t3152619780, ___mFlashTorchEnabled_4)); }
	inline bool get_mFlashTorchEnabled_4() const { return ___mFlashTorchEnabled_4; }
	inline bool* get_address_of_mFlashTorchEnabled_4() { return &___mFlashTorchEnabled_4; }
	inline void set_mFlashTorchEnabled_4(bool value)
	{
		___mFlashTorchEnabled_4 = value;
	}

	inline static int32_t get_offset_of_mActiveDirection_5() { return static_cast<int32_t>(offsetof(CameraSettings_t3152619780, ___mActiveDirection_5)); }
	inline int32_t get_mActiveDirection_5() const { return ___mActiveDirection_5; }
	inline int32_t* get_address_of_mActiveDirection_5() { return &___mActiveDirection_5; }
	inline void set_mActiveDirection_5(int32_t value)
	{
		___mActiveDirection_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERASETTINGS_T3152619780_H
#ifndef MENUANIMATOR_T2112910832_H
#define MENUANIMATOR_T2112910832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuAnimator
struct  MenuAnimator_t2112910832  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 MenuAnimator::mVisiblePos
	Vector3_t3722313464  ___mVisiblePos_2;
	// UnityEngine.Vector3 MenuAnimator::mInvisiblePos
	Vector3_t3722313464  ___mInvisiblePos_3;
	// System.Single MenuAnimator::mVisibility
	float ___mVisibility_4;
	// System.Boolean MenuAnimator::mVisible
	bool ___mVisible_5;
	// UnityEngine.Canvas MenuAnimator::mCanvas
	Canvas_t3310196443 * ___mCanvas_6;
	// MenuOptions MenuAnimator::mMenuOptions
	MenuOptions_t1951716431 * ___mMenuOptions_7;
	// System.Single MenuAnimator::SlidingTime
	float ___SlidingTime_8;

public:
	inline static int32_t get_offset_of_mVisiblePos_2() { return static_cast<int32_t>(offsetof(MenuAnimator_t2112910832, ___mVisiblePos_2)); }
	inline Vector3_t3722313464  get_mVisiblePos_2() const { return ___mVisiblePos_2; }
	inline Vector3_t3722313464 * get_address_of_mVisiblePos_2() { return &___mVisiblePos_2; }
	inline void set_mVisiblePos_2(Vector3_t3722313464  value)
	{
		___mVisiblePos_2 = value;
	}

	inline static int32_t get_offset_of_mInvisiblePos_3() { return static_cast<int32_t>(offsetof(MenuAnimator_t2112910832, ___mInvisiblePos_3)); }
	inline Vector3_t3722313464  get_mInvisiblePos_3() const { return ___mInvisiblePos_3; }
	inline Vector3_t3722313464 * get_address_of_mInvisiblePos_3() { return &___mInvisiblePos_3; }
	inline void set_mInvisiblePos_3(Vector3_t3722313464  value)
	{
		___mInvisiblePos_3 = value;
	}

	inline static int32_t get_offset_of_mVisibility_4() { return static_cast<int32_t>(offsetof(MenuAnimator_t2112910832, ___mVisibility_4)); }
	inline float get_mVisibility_4() const { return ___mVisibility_4; }
	inline float* get_address_of_mVisibility_4() { return &___mVisibility_4; }
	inline void set_mVisibility_4(float value)
	{
		___mVisibility_4 = value;
	}

	inline static int32_t get_offset_of_mVisible_5() { return static_cast<int32_t>(offsetof(MenuAnimator_t2112910832, ___mVisible_5)); }
	inline bool get_mVisible_5() const { return ___mVisible_5; }
	inline bool* get_address_of_mVisible_5() { return &___mVisible_5; }
	inline void set_mVisible_5(bool value)
	{
		___mVisible_5 = value;
	}

	inline static int32_t get_offset_of_mCanvas_6() { return static_cast<int32_t>(offsetof(MenuAnimator_t2112910832, ___mCanvas_6)); }
	inline Canvas_t3310196443 * get_mCanvas_6() const { return ___mCanvas_6; }
	inline Canvas_t3310196443 ** get_address_of_mCanvas_6() { return &___mCanvas_6; }
	inline void set_mCanvas_6(Canvas_t3310196443 * value)
	{
		___mCanvas_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCanvas_6), value);
	}

	inline static int32_t get_offset_of_mMenuOptions_7() { return static_cast<int32_t>(offsetof(MenuAnimator_t2112910832, ___mMenuOptions_7)); }
	inline MenuOptions_t1951716431 * get_mMenuOptions_7() const { return ___mMenuOptions_7; }
	inline MenuOptions_t1951716431 ** get_address_of_mMenuOptions_7() { return &___mMenuOptions_7; }
	inline void set_mMenuOptions_7(MenuOptions_t1951716431 * value)
	{
		___mMenuOptions_7 = value;
		Il2CppCodeGenWriteBarrier((&___mMenuOptions_7), value);
	}

	inline static int32_t get_offset_of_SlidingTime_8() { return static_cast<int32_t>(offsetof(MenuAnimator_t2112910832, ___SlidingTime_8)); }
	inline float get_SlidingTime_8() const { return ___SlidingTime_8; }
	inline float* get_address_of_SlidingTime_8() { return &___SlidingTime_8; }
	inline void set_SlidingTime_8(float value)
	{
		___SlidingTime_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUANIMATOR_T2112910832_H
#ifndef ASYNCSCENELOADER_T621267272_H
#define ASYNCSCENELOADER_T621267272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AsyncSceneLoader
struct  AsyncSceneLoader_t621267272  : public MonoBehaviour_t3962482529
{
public:
	// System.Single AsyncSceneLoader::loadingDelay
	float ___loadingDelay_2;

public:
	inline static int32_t get_offset_of_loadingDelay_2() { return static_cast<int32_t>(offsetof(AsyncSceneLoader_t621267272, ___loadingDelay_2)); }
	inline float get_loadingDelay_2() const { return ___loadingDelay_2; }
	inline float* get_address_of_loadingDelay_2() { return &___loadingDelay_2; }
	inline void set_loadingDelay_2(float value)
	{
		___loadingDelay_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCSCENELOADER_T621267272_H
#ifndef ABOUTSCREEN_T2183797299_H
#define ABOUTSCREEN_T2183797299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AboutScreen
struct  AboutScreen_t2183797299  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABOUTSCREEN_T2183797299_H
#ifndef MENUOPTIONS_T1951716431_H
#define MENUOPTIONS_T1951716431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuOptions
struct  MenuOptions_t1951716431  : public MonoBehaviour_t3962482529
{
public:
	// CameraSettings MenuOptions::mCamSettings
	CameraSettings_t3152619780 * ___mCamSettings_2;
	// TrackableSettings MenuOptions::mTrackableSettings
	TrackableSettings_t2862243993 * ___mTrackableSettings_3;
	// MenuAnimator MenuOptions::mMenuAnim
	MenuAnimator_t2112910832 * ___mMenuAnim_4;

public:
	inline static int32_t get_offset_of_mCamSettings_2() { return static_cast<int32_t>(offsetof(MenuOptions_t1951716431, ___mCamSettings_2)); }
	inline CameraSettings_t3152619780 * get_mCamSettings_2() const { return ___mCamSettings_2; }
	inline CameraSettings_t3152619780 ** get_address_of_mCamSettings_2() { return &___mCamSettings_2; }
	inline void set_mCamSettings_2(CameraSettings_t3152619780 * value)
	{
		___mCamSettings_2 = value;
		Il2CppCodeGenWriteBarrier((&___mCamSettings_2), value);
	}

	inline static int32_t get_offset_of_mTrackableSettings_3() { return static_cast<int32_t>(offsetof(MenuOptions_t1951716431, ___mTrackableSettings_3)); }
	inline TrackableSettings_t2862243993 * get_mTrackableSettings_3() const { return ___mTrackableSettings_3; }
	inline TrackableSettings_t2862243993 ** get_address_of_mTrackableSettings_3() { return &___mTrackableSettings_3; }
	inline void set_mTrackableSettings_3(TrackableSettings_t2862243993 * value)
	{
		___mTrackableSettings_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableSettings_3), value);
	}

	inline static int32_t get_offset_of_mMenuAnim_4() { return static_cast<int32_t>(offsetof(MenuOptions_t1951716431, ___mMenuAnim_4)); }
	inline MenuAnimator_t2112910832 * get_mMenuAnim_4() const { return ___mMenuAnim_4; }
	inline MenuAnimator_t2112910832 ** get_address_of_mMenuAnim_4() { return &___mMenuAnim_4; }
	inline void set_mMenuAnim_4(MenuAnimator_t2112910832 * value)
	{
		___mMenuAnim_4 = value;
		Il2CppCodeGenWriteBarrier((&___mMenuAnim_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUOPTIONS_T1951716431_H
#ifndef BUTTONTRIGGERANIMATION_T4121295644_H
#define BUTTONTRIGGERANIMATION_T4121295644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// buttonTriggerAnimation
struct  buttonTriggerAnimation_t4121295644  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean buttonTriggerAnimation::animationBusy
	bool ___animationBusy_2;

public:
	inline static int32_t get_offset_of_animationBusy_2() { return static_cast<int32_t>(offsetof(buttonTriggerAnimation_t4121295644, ___animationBusy_2)); }
	inline bool get_animationBusy_2() const { return ___animationBusy_2; }
	inline bool* get_address_of_animationBusy_2() { return &___animationBusy_2; }
	inline void set_animationBusy_2(bool value)
	{
		___animationBusy_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONTRIGGERANIMATION_T4121295644_H
#ifndef FINDTUTORIALMANAGER_T1037408675_H
#define FINDTUTORIALMANAGER_T1037408675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// findTutorialManager
struct  findTutorialManager_t1037408675  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINDTUTORIALMANAGER_T1037408675_H
#ifndef ENABLEMOUTHANIM_T2443685136_H
#define ENABLEMOUTHANIM_T2443685136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnableMouthAnim
struct  EnableMouthAnim_t2443685136  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animation EnableMouthAnim::mouthAnim
	Animation_t3648466861 * ___mouthAnim_2;

public:
	inline static int32_t get_offset_of_mouthAnim_2() { return static_cast<int32_t>(offsetof(EnableMouthAnim_t2443685136, ___mouthAnim_2)); }
	inline Animation_t3648466861 * get_mouthAnim_2() const { return ___mouthAnim_2; }
	inline Animation_t3648466861 ** get_address_of_mouthAnim_2() { return &___mouthAnim_2; }
	inline void set_mouthAnim_2(Animation_t3648466861 * value)
	{
		___mouthAnim_2 = value;
		Il2CppCodeGenWriteBarrier((&___mouthAnim_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENABLEMOUTHANIM_T2443685136_H
#ifndef DESTORYONAWAKE_T1274961527_H
#define DESTORYONAWAKE_T1274961527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// destoryOnAwake
struct  destoryOnAwake_t1274961527  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean destoryOnAwake::on
	bool ___on_2;

public:
	inline static int32_t get_offset_of_on_2() { return static_cast<int32_t>(offsetof(destoryOnAwake_t1274961527, ___on_2)); }
	inline bool get_on_2() const { return ___on_2; }
	inline bool* get_address_of_on_2() { return &___on_2; }
	inline void set_on_2(bool value)
	{
		___on_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTORYONAWAKE_T1274961527_H
#ifndef MATH3D_T1427253148_H
#define MATH3D_T1427253148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Math3d
struct  Math3d_t1427253148  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Math3d_t1427253148_StaticFields
{
public:
	// UnityEngine.Transform Math3d::tempChild
	Transform_t3600365921 * ___tempChild_2;
	// UnityEngine.Transform Math3d::tempParent
	Transform_t3600365921 * ___tempParent_3;

public:
	inline static int32_t get_offset_of_tempChild_2() { return static_cast<int32_t>(offsetof(Math3d_t1427253148_StaticFields, ___tempChild_2)); }
	inline Transform_t3600365921 * get_tempChild_2() const { return ___tempChild_2; }
	inline Transform_t3600365921 ** get_address_of_tempChild_2() { return &___tempChild_2; }
	inline void set_tempChild_2(Transform_t3600365921 * value)
	{
		___tempChild_2 = value;
		Il2CppCodeGenWriteBarrier((&___tempChild_2), value);
	}

	inline static int32_t get_offset_of_tempParent_3() { return static_cast<int32_t>(offsetof(Math3d_t1427253148_StaticFields, ___tempParent_3)); }
	inline Transform_t3600365921 * get_tempParent_3() const { return ___tempParent_3; }
	inline Transform_t3600365921 ** get_address_of_tempParent_3() { return &___tempParent_3; }
	inline void set_tempParent_3(Transform_t3600365921 * value)
	{
		___tempParent_3 = value;
		Il2CppCodeGenWriteBarrier((&___tempParent_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATH3D_T1427253148_H
#ifndef PAGETURN_T132420247_H
#define PAGETURN_T132420247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// pageTurn
struct  pageTurn_t132420247  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> pageTurn::Pages
	List_1_t2585711361 * ___Pages_2;
	// System.Int32 pageTurn::currentPage
	int32_t ___currentPage_3;

public:
	inline static int32_t get_offset_of_Pages_2() { return static_cast<int32_t>(offsetof(pageTurn_t132420247, ___Pages_2)); }
	inline List_1_t2585711361 * get_Pages_2() const { return ___Pages_2; }
	inline List_1_t2585711361 ** get_address_of_Pages_2() { return &___Pages_2; }
	inline void set_Pages_2(List_1_t2585711361 * value)
	{
		___Pages_2 = value;
		Il2CppCodeGenWriteBarrier((&___Pages_2), value);
	}

	inline static int32_t get_offset_of_currentPage_3() { return static_cast<int32_t>(offsetof(pageTurn_t132420247, ___currentPage_3)); }
	inline int32_t get_currentPage_3() const { return ___currentPage_3; }
	inline int32_t* get_address_of_currentPage_3() { return &___currentPage_3; }
	inline void set_currentPage_3(int32_t value)
	{
		___currentPage_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAGETURN_T132420247_H
#ifndef ONSCANCONTROLLER_T1472212342_H
#define ONSCANCONTROLLER_T1472212342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// onScanController
struct  onScanController_t1472212342  : public MonoBehaviour_t3962482529
{
public:
	// characterLoader onScanController::CharacterLoader
	characterLoader_t1326495876 * ___CharacterLoader_2;
	// System.String onScanController::CharacterID
	String_t* ___CharacterID_3;
	// System.String onScanController::scanText
	String_t* ___scanText_4;
	// UnityEngine.Sprite[] onScanController::symbols
	SpriteU5BU5D_t2581906349* ___symbols_5;
	// UnityEngine.GameObject onScanController::currentCharacter
	GameObject_t1113636619 * ___currentCharacter_6;
	// UnityEngine.GameObject onScanController::uiCanvas
	GameObject_t1113636619 * ___uiCanvas_7;
	// UnityEngine.GameObject onScanController::speechText
	GameObject_t1113636619 * ___speechText_8;
	// UnityEngine.GameObject onScanController::speechSymbols
	GameObject_t1113636619 * ___speechSymbols_9;
	// UnityEngine.Animation onScanController::mouthAnim
	Animation_t3648466861 * ___mouthAnim_10;
	// System.Boolean onScanController::imPlaying
	bool ___imPlaying_12;
	// System.Boolean onScanController::ShowTailAnimation
	bool ___ShowTailAnimation_13;

public:
	inline static int32_t get_offset_of_CharacterLoader_2() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___CharacterLoader_2)); }
	inline characterLoader_t1326495876 * get_CharacterLoader_2() const { return ___CharacterLoader_2; }
	inline characterLoader_t1326495876 ** get_address_of_CharacterLoader_2() { return &___CharacterLoader_2; }
	inline void set_CharacterLoader_2(characterLoader_t1326495876 * value)
	{
		___CharacterLoader_2 = value;
		Il2CppCodeGenWriteBarrier((&___CharacterLoader_2), value);
	}

	inline static int32_t get_offset_of_CharacterID_3() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___CharacterID_3)); }
	inline String_t* get_CharacterID_3() const { return ___CharacterID_3; }
	inline String_t** get_address_of_CharacterID_3() { return &___CharacterID_3; }
	inline void set_CharacterID_3(String_t* value)
	{
		___CharacterID_3 = value;
		Il2CppCodeGenWriteBarrier((&___CharacterID_3), value);
	}

	inline static int32_t get_offset_of_scanText_4() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___scanText_4)); }
	inline String_t* get_scanText_4() const { return ___scanText_4; }
	inline String_t** get_address_of_scanText_4() { return &___scanText_4; }
	inline void set_scanText_4(String_t* value)
	{
		___scanText_4 = value;
		Il2CppCodeGenWriteBarrier((&___scanText_4), value);
	}

	inline static int32_t get_offset_of_symbols_5() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___symbols_5)); }
	inline SpriteU5BU5D_t2581906349* get_symbols_5() const { return ___symbols_5; }
	inline SpriteU5BU5D_t2581906349** get_address_of_symbols_5() { return &___symbols_5; }
	inline void set_symbols_5(SpriteU5BU5D_t2581906349* value)
	{
		___symbols_5 = value;
		Il2CppCodeGenWriteBarrier((&___symbols_5), value);
	}

	inline static int32_t get_offset_of_currentCharacter_6() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___currentCharacter_6)); }
	inline GameObject_t1113636619 * get_currentCharacter_6() const { return ___currentCharacter_6; }
	inline GameObject_t1113636619 ** get_address_of_currentCharacter_6() { return &___currentCharacter_6; }
	inline void set_currentCharacter_6(GameObject_t1113636619 * value)
	{
		___currentCharacter_6 = value;
		Il2CppCodeGenWriteBarrier((&___currentCharacter_6), value);
	}

	inline static int32_t get_offset_of_uiCanvas_7() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___uiCanvas_7)); }
	inline GameObject_t1113636619 * get_uiCanvas_7() const { return ___uiCanvas_7; }
	inline GameObject_t1113636619 ** get_address_of_uiCanvas_7() { return &___uiCanvas_7; }
	inline void set_uiCanvas_7(GameObject_t1113636619 * value)
	{
		___uiCanvas_7 = value;
		Il2CppCodeGenWriteBarrier((&___uiCanvas_7), value);
	}

	inline static int32_t get_offset_of_speechText_8() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___speechText_8)); }
	inline GameObject_t1113636619 * get_speechText_8() const { return ___speechText_8; }
	inline GameObject_t1113636619 ** get_address_of_speechText_8() { return &___speechText_8; }
	inline void set_speechText_8(GameObject_t1113636619 * value)
	{
		___speechText_8 = value;
		Il2CppCodeGenWriteBarrier((&___speechText_8), value);
	}

	inline static int32_t get_offset_of_speechSymbols_9() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___speechSymbols_9)); }
	inline GameObject_t1113636619 * get_speechSymbols_9() const { return ___speechSymbols_9; }
	inline GameObject_t1113636619 ** get_address_of_speechSymbols_9() { return &___speechSymbols_9; }
	inline void set_speechSymbols_9(GameObject_t1113636619 * value)
	{
		___speechSymbols_9 = value;
		Il2CppCodeGenWriteBarrier((&___speechSymbols_9), value);
	}

	inline static int32_t get_offset_of_mouthAnim_10() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___mouthAnim_10)); }
	inline Animation_t3648466861 * get_mouthAnim_10() const { return ___mouthAnim_10; }
	inline Animation_t3648466861 ** get_address_of_mouthAnim_10() { return &___mouthAnim_10; }
	inline void set_mouthAnim_10(Animation_t3648466861 * value)
	{
		___mouthAnim_10 = value;
		Il2CppCodeGenWriteBarrier((&___mouthAnim_10), value);
	}

	inline static int32_t get_offset_of_imPlaying_12() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___imPlaying_12)); }
	inline bool get_imPlaying_12() const { return ___imPlaying_12; }
	inline bool* get_address_of_imPlaying_12() { return &___imPlaying_12; }
	inline void set_imPlaying_12(bool value)
	{
		___imPlaying_12 = value;
	}

	inline static int32_t get_offset_of_ShowTailAnimation_13() { return static_cast<int32_t>(offsetof(onScanController_t1472212342, ___ShowTailAnimation_13)); }
	inline bool get_ShowTailAnimation_13() const { return ___ShowTailAnimation_13; }
	inline bool* get_address_of_ShowTailAnimation_13() { return &___ShowTailAnimation_13; }
	inline void set_ShowTailAnimation_13(bool value)
	{
		___ShowTailAnimation_13 = value;
	}
};

struct onScanController_t1472212342_StaticFields
{
public:
	// System.Boolean onScanController::someoneIsPlaying
	bool ___someoneIsPlaying_11;

public:
	inline static int32_t get_offset_of_someoneIsPlaying_11() { return static_cast<int32_t>(offsetof(onScanController_t1472212342_StaticFields, ___someoneIsPlaying_11)); }
	inline bool get_someoneIsPlaying_11() const { return ___someoneIsPlaying_11; }
	inline bool* get_address_of_someoneIsPlaying_11() { return &___someoneIsPlaying_11; }
	inline void set_someoneIsPlaying_11(bool value)
	{
		___someoneIsPlaying_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSCANCONTROLLER_T1472212342_H
#ifndef MODELSWAP_T1632145241_H
#define MODELSWAP_T1632145241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModelSwap
struct  ModelSwap_t1632145241  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ModelSwap::mDefaultModel
	GameObject_t1113636619 * ___mDefaultModel_2;
	// UnityEngine.GameObject ModelSwap::mExtTrackedModel
	GameObject_t1113636619 * ___mExtTrackedModel_3;
	// UnityEngine.GameObject ModelSwap::mActiveModel
	GameObject_t1113636619 * ___mActiveModel_4;
	// TrackableSettings ModelSwap::mTrackableSettings
	TrackableSettings_t2862243993 * ___mTrackableSettings_5;

public:
	inline static int32_t get_offset_of_mDefaultModel_2() { return static_cast<int32_t>(offsetof(ModelSwap_t1632145241, ___mDefaultModel_2)); }
	inline GameObject_t1113636619 * get_mDefaultModel_2() const { return ___mDefaultModel_2; }
	inline GameObject_t1113636619 ** get_address_of_mDefaultModel_2() { return &___mDefaultModel_2; }
	inline void set_mDefaultModel_2(GameObject_t1113636619 * value)
	{
		___mDefaultModel_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDefaultModel_2), value);
	}

	inline static int32_t get_offset_of_mExtTrackedModel_3() { return static_cast<int32_t>(offsetof(ModelSwap_t1632145241, ___mExtTrackedModel_3)); }
	inline GameObject_t1113636619 * get_mExtTrackedModel_3() const { return ___mExtTrackedModel_3; }
	inline GameObject_t1113636619 ** get_address_of_mExtTrackedModel_3() { return &___mExtTrackedModel_3; }
	inline void set_mExtTrackedModel_3(GameObject_t1113636619 * value)
	{
		___mExtTrackedModel_3 = value;
		Il2CppCodeGenWriteBarrier((&___mExtTrackedModel_3), value);
	}

	inline static int32_t get_offset_of_mActiveModel_4() { return static_cast<int32_t>(offsetof(ModelSwap_t1632145241, ___mActiveModel_4)); }
	inline GameObject_t1113636619 * get_mActiveModel_4() const { return ___mActiveModel_4; }
	inline GameObject_t1113636619 ** get_address_of_mActiveModel_4() { return &___mActiveModel_4; }
	inline void set_mActiveModel_4(GameObject_t1113636619 * value)
	{
		___mActiveModel_4 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveModel_4), value);
	}

	inline static int32_t get_offset_of_mTrackableSettings_5() { return static_cast<int32_t>(offsetof(ModelSwap_t1632145241, ___mTrackableSettings_5)); }
	inline TrackableSettings_t2862243993 * get_mTrackableSettings_5() const { return ___mTrackableSettings_5; }
	inline TrackableSettings_t2862243993 ** get_address_of_mTrackableSettings_5() { return &___mTrackableSettings_5; }
	inline void set_mTrackableSettings_5(TrackableSettings_t2862243993 * value)
	{
		___mTrackableSettings_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableSettings_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELSWAP_T1632145241_H
#ifndef CAMERACONTROLLER_T3346819214_H
#define CAMERACONTROLLER_T3346819214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraController
struct  CameraController_t3346819214  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Quaternion CameraController::startRotation
	Quaternion_t2301928331  ___startRotation_2;
	// UnityEngine.Vector3 CameraController::startPosition
	Vector3_t3722313464  ___startPosition_3;
	// UnityEngine.Vector3 CameraController::parentStartPosition
	Vector3_t3722313464  ___parentStartPosition_4;

public:
	inline static int32_t get_offset_of_startRotation_2() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___startRotation_2)); }
	inline Quaternion_t2301928331  get_startRotation_2() const { return ___startRotation_2; }
	inline Quaternion_t2301928331 * get_address_of_startRotation_2() { return &___startRotation_2; }
	inline void set_startRotation_2(Quaternion_t2301928331  value)
	{
		___startRotation_2 = value;
	}

	inline static int32_t get_offset_of_startPosition_3() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___startPosition_3)); }
	inline Vector3_t3722313464  get_startPosition_3() const { return ___startPosition_3; }
	inline Vector3_t3722313464 * get_address_of_startPosition_3() { return &___startPosition_3; }
	inline void set_startPosition_3(Vector3_t3722313464  value)
	{
		___startPosition_3 = value;
	}

	inline static int32_t get_offset_of_parentStartPosition_4() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___parentStartPosition_4)); }
	inline Vector3_t3722313464  get_parentStartPosition_4() const { return ___parentStartPosition_4; }
	inline Vector3_t3722313464 * get_address_of_parentStartPosition_4() { return &___parentStartPosition_4; }
	inline void set_parentStartPosition_4(Vector3_t3722313464  value)
	{
		___parentStartPosition_4 = value;
	}
};

struct CameraController_t3346819214_StaticFields
{
public:
	// System.Func`2<UnityEngine.Touch,UnityEngine.Vector2> CameraController::<>f__am$cache0
	Func_2_t528868469 * ___U3CU3Ef__amU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(CameraController_t3346819214_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Func_2_t528868469 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Func_2_t528868469 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Func_2_t528868469 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONTROLLER_T3346819214_H
#ifndef CHARACTERCYCLER_T3521054542_H
#define CHARACTERCYCLER_T3521054542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterCycler
struct  CharacterCycler_t3521054542  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 CharacterCycler::currentVal
	int32_t ___currentVal_2;
	// UnityEngine.GameObject[] CharacterCycler::charList
	GameObjectU5BU5D_t3328599146* ___charList_3;
	// System.Int32 CharacterCycler::n
	int32_t ___n_4;

public:
	inline static int32_t get_offset_of_currentVal_2() { return static_cast<int32_t>(offsetof(CharacterCycler_t3521054542, ___currentVal_2)); }
	inline int32_t get_currentVal_2() const { return ___currentVal_2; }
	inline int32_t* get_address_of_currentVal_2() { return &___currentVal_2; }
	inline void set_currentVal_2(int32_t value)
	{
		___currentVal_2 = value;
	}

	inline static int32_t get_offset_of_charList_3() { return static_cast<int32_t>(offsetof(CharacterCycler_t3521054542, ___charList_3)); }
	inline GameObjectU5BU5D_t3328599146* get_charList_3() const { return ___charList_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_charList_3() { return &___charList_3; }
	inline void set_charList_3(GameObjectU5BU5D_t3328599146* value)
	{
		___charList_3 = value;
		Il2CppCodeGenWriteBarrier((&___charList_3), value);
	}

	inline static int32_t get_offset_of_n_4() { return static_cast<int32_t>(offsetof(CharacterCycler_t3521054542, ___n_4)); }
	inline int32_t get_n_4() const { return ___n_4; }
	inline int32_t* get_address_of_n_4() { return &___n_4; }
	inline void set_n_4(int32_t value)
	{
		___n_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERCYCLER_T3521054542_H
#ifndef CHARACTERLOADER_T1326495876_H
#define CHARACTERLOADER_T1326495876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// characterLoader
struct  characterLoader_t1326495876  : public MonoBehaviour_t3962482529
{
public:
	// characterSlot[] characterLoader::characterList
	characterSlotU5BU5D_t2121073899* ___characterList_2;

public:
	inline static int32_t get_offset_of_characterList_2() { return static_cast<int32_t>(offsetof(characterLoader_t1326495876, ___characterList_2)); }
	inline characterSlotU5BU5D_t2121073899* get_characterList_2() const { return ___characterList_2; }
	inline characterSlotU5BU5D_t2121073899** get_address_of_characterList_2() { return &___characterList_2; }
	inline void set_characterList_2(characterSlotU5BU5D_t2121073899* value)
	{
		___characterList_2 = value;
		Il2CppCodeGenWriteBarrier((&___characterList_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERLOADER_T1326495876_H
#ifndef CAMERAFOCUSCONTROLLER_T1033776956_H
#define CAMERAFOCUSCONTROLLER_T1033776956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFocusController
struct  CameraFocusController_t1033776956  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAFOCUSCONTROLLER_T1033776956_H
#ifndef TOUCHCAMERA_T3780128488_H
#define TOUCHCAMERA_T3780128488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchCamera
struct  TouchCamera_t3780128488  : public MonoBehaviour_t3962482529
{
public:
	// System.Nullable`1<UnityEngine.Vector2>[] TouchCamera::oldTouchPositions
	Nullable_1U5BU5D_t3148003288* ___oldTouchPositions_2;
	// UnityEngine.Vector2 TouchCamera::oldTouchVector
	Vector2_t2156229523  ___oldTouchVector_3;
	// System.Single TouchCamera::oldTouchDistance
	float ___oldTouchDistance_4;

public:
	inline static int32_t get_offset_of_oldTouchPositions_2() { return static_cast<int32_t>(offsetof(TouchCamera_t3780128488, ___oldTouchPositions_2)); }
	inline Nullable_1U5BU5D_t3148003288* get_oldTouchPositions_2() const { return ___oldTouchPositions_2; }
	inline Nullable_1U5BU5D_t3148003288** get_address_of_oldTouchPositions_2() { return &___oldTouchPositions_2; }
	inline void set_oldTouchPositions_2(Nullable_1U5BU5D_t3148003288* value)
	{
		___oldTouchPositions_2 = value;
		Il2CppCodeGenWriteBarrier((&___oldTouchPositions_2), value);
	}

	inline static int32_t get_offset_of_oldTouchVector_3() { return static_cast<int32_t>(offsetof(TouchCamera_t3780128488, ___oldTouchVector_3)); }
	inline Vector2_t2156229523  get_oldTouchVector_3() const { return ___oldTouchVector_3; }
	inline Vector2_t2156229523 * get_address_of_oldTouchVector_3() { return &___oldTouchVector_3; }
	inline void set_oldTouchVector_3(Vector2_t2156229523  value)
	{
		___oldTouchVector_3 = value;
	}

	inline static int32_t get_offset_of_oldTouchDistance_4() { return static_cast<int32_t>(offsetof(TouchCamera_t3780128488, ___oldTouchDistance_4)); }
	inline float get_oldTouchDistance_4() const { return ___oldTouchDistance_4; }
	inline float* get_address_of_oldTouchDistance_4() { return &___oldTouchDistance_4; }
	inline void set_oldTouchDistance_4(float value)
	{
		___oldTouchDistance_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHCAMERA_T3780128488_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t2598313366 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H
#define DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultInitializationErrorHandler
struct  DefaultInitializationErrorHandler_t3109936861  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// System.String DefaultInitializationErrorHandler::mErrorText
	String_t* ___mErrorText_2;
	// System.Boolean DefaultInitializationErrorHandler::mErrorOccurred
	bool ___mErrorOccurred_3;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::bodyStyle
	GUIStyle_t3956901511 * ___bodyStyle_5;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::headerStyle
	GUIStyle_t3956901511 * ___headerStyle_6;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::footerStyle
	GUIStyle_t3956901511 * ___footerStyle_7;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::bodyTexture
	Texture2D_t3840446185 * ___bodyTexture_8;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::headerTexture
	Texture2D_t3840446185 * ___headerTexture_9;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::footerTexture
	Texture2D_t3840446185 * ___footerTexture_10;

public:
	inline static int32_t get_offset_of_mErrorText_2() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___mErrorText_2)); }
	inline String_t* get_mErrorText_2() const { return ___mErrorText_2; }
	inline String_t** get_address_of_mErrorText_2() { return &___mErrorText_2; }
	inline void set_mErrorText_2(String_t* value)
	{
		___mErrorText_2 = value;
		Il2CppCodeGenWriteBarrier((&___mErrorText_2), value);
	}

	inline static int32_t get_offset_of_mErrorOccurred_3() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___mErrorOccurred_3)); }
	inline bool get_mErrorOccurred_3() const { return ___mErrorOccurred_3; }
	inline bool* get_address_of_mErrorOccurred_3() { return &___mErrorOccurred_3; }
	inline void set_mErrorOccurred_3(bool value)
	{
		___mErrorOccurred_3 = value;
	}

	inline static int32_t get_offset_of_bodyStyle_5() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___bodyStyle_5)); }
	inline GUIStyle_t3956901511 * get_bodyStyle_5() const { return ___bodyStyle_5; }
	inline GUIStyle_t3956901511 ** get_address_of_bodyStyle_5() { return &___bodyStyle_5; }
	inline void set_bodyStyle_5(GUIStyle_t3956901511 * value)
	{
		___bodyStyle_5 = value;
		Il2CppCodeGenWriteBarrier((&___bodyStyle_5), value);
	}

	inline static int32_t get_offset_of_headerStyle_6() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___headerStyle_6)); }
	inline GUIStyle_t3956901511 * get_headerStyle_6() const { return ___headerStyle_6; }
	inline GUIStyle_t3956901511 ** get_address_of_headerStyle_6() { return &___headerStyle_6; }
	inline void set_headerStyle_6(GUIStyle_t3956901511 * value)
	{
		___headerStyle_6 = value;
		Il2CppCodeGenWriteBarrier((&___headerStyle_6), value);
	}

	inline static int32_t get_offset_of_footerStyle_7() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___footerStyle_7)); }
	inline GUIStyle_t3956901511 * get_footerStyle_7() const { return ___footerStyle_7; }
	inline GUIStyle_t3956901511 ** get_address_of_footerStyle_7() { return &___footerStyle_7; }
	inline void set_footerStyle_7(GUIStyle_t3956901511 * value)
	{
		___footerStyle_7 = value;
		Il2CppCodeGenWriteBarrier((&___footerStyle_7), value);
	}

	inline static int32_t get_offset_of_bodyTexture_8() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___bodyTexture_8)); }
	inline Texture2D_t3840446185 * get_bodyTexture_8() const { return ___bodyTexture_8; }
	inline Texture2D_t3840446185 ** get_address_of_bodyTexture_8() { return &___bodyTexture_8; }
	inline void set_bodyTexture_8(Texture2D_t3840446185 * value)
	{
		___bodyTexture_8 = value;
		Il2CppCodeGenWriteBarrier((&___bodyTexture_8), value);
	}

	inline static int32_t get_offset_of_headerTexture_9() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___headerTexture_9)); }
	inline Texture2D_t3840446185 * get_headerTexture_9() const { return ___headerTexture_9; }
	inline Texture2D_t3840446185 ** get_address_of_headerTexture_9() { return &___headerTexture_9; }
	inline void set_headerTexture_9(Texture2D_t3840446185 * value)
	{
		___headerTexture_9 = value;
		Il2CppCodeGenWriteBarrier((&___headerTexture_9), value);
	}

	inline static int32_t get_offset_of_footerTexture_10() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___footerTexture_10)); }
	inline Texture2D_t3840446185 * get_footerTexture_10() const { return ___footerTexture_10; }
	inline Texture2D_t3840446185 ** get_address_of_footerTexture_10() { return &___footerTexture_10; }
	inline void set_footerTexture_10(Texture2D_t3840446185 * value)
	{
		___footerTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___footerTexture_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef TEXT_T1901882714_H
#define TEXT_T1901882714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t1901882714  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t746620069 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t3211863866 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t3211863866 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t1981460040* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_FontData_28)); }
	inline FontData_t746620069 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t746620069 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t746620069 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCache_30)); }
	inline TextGenerator_t3211863866 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t3211863866 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t3211863866 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t3211863866 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t1981460040* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t1981460040* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t1901882714_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t340375123 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t1901882714_StaticFields, ___s_DefaultText_32)); }
	inline Material_t340375123 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t340375123 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t340375123 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T1901882714_H
#ifndef FPSCOUNT_T2935661334_H
#define FPSCOUNT_T2935661334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FpsCount
struct  FpsCount_t2935661334  : public Text_t1901882714
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNT_T2935661334_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (OnTrigger_t4184125570), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (TrackableTrigger_t621205209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2401[2] = 
{
	TrackableTrigger_t621205209::get_offset_of_m_Target_0(),
	TrackableTrigger_t621205209::get_offset_of_m_MethodPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (TriggerMethod_t582536534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (TriggerRule_t1946298321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2403[4] = 
{
	TriggerRule_t1946298321::get_offset_of_m_Target_0(),
	TriggerRule_t1946298321::get_offset_of_m_Operator_1(),
	TriggerRule_t1946298321::get_offset_of_m_Value_2(),
	TriggerRule_t1946298321::get_offset_of_m_Value2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (U3CModuleU3E_t692745550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (CameraSettings_t3152619780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2405[4] = 
{
	CameraSettings_t3152619780::get_offset_of_mVuforiaStarted_2(),
	CameraSettings_t3152619780::get_offset_of_mAutofocusEnabled_3(),
	CameraSettings_t3152619780::get_offset_of_mFlashTorchEnabled_4(),
	CameraSettings_t3152619780::get_offset_of_mActiveDirection_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2406[4] = 
{
	U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229::get_offset_of_U24this_0(),
	U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229::get_offset_of_U24current_1(),
	U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229::get_offset_of_U24disposing_2(),
	U3CRestoreOriginalFocusModeU3Ec__Iterator0_t2912012229::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (FrameRateSettings_t3598747490), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (InitErrorHandler_t2159361531), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2408[3] = 
{
	InitErrorHandler_t2159361531::get_offset_of_errorText_2(),
	InitErrorHandler_t2159361531::get_offset_of_errorCanvas_3(),
	InitErrorHandler_t2159361531::get_offset_of_key_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (MenuAnimator_t2112910832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2409[7] = 
{
	MenuAnimator_t2112910832::get_offset_of_mVisiblePos_2(),
	MenuAnimator_t2112910832::get_offset_of_mInvisiblePos_3(),
	MenuAnimator_t2112910832::get_offset_of_mVisibility_4(),
	MenuAnimator_t2112910832::get_offset_of_mVisible_5(),
	MenuAnimator_t2112910832::get_offset_of_mCanvas_6(),
	MenuAnimator_t2112910832::get_offset_of_mMenuOptions_7(),
	MenuAnimator_t2112910832::get_offset_of_SlidingTime_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (MenuOptions_t1951716431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2410[3] = 
{
	MenuOptions_t1951716431::get_offset_of_mCamSettings_2(),
	MenuOptions_t1951716431::get_offset_of_mTrackableSettings_3(),
	MenuOptions_t1951716431::get_offset_of_mMenuAnim_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (AboutScreen_t2183797299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (AsyncSceneLoader_t621267272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2412[1] = 
{
	AsyncSceneLoader_t621267272::get_offset_of_loadingDelay_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2413[4] = 
{
	U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170::get_offset_of_seconds_0(),
	U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170::get_offset_of_U24current_1(),
	U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170::get_offset_of_U24disposing_2(),
	U3CLoadNextSceneAfterU3Ec__Iterator0_t994227170::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (LoadingScreen_t2154736699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2414[2] = 
{
	LoadingScreen_t2154736699::get_offset_of_mChangeLevel_2(),
	LoadingScreen_t2154736699::get_offset_of_mUISpinner_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (TapHandler_t334234343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2415[4] = 
{
	0,
	TapHandler_t334234343::get_offset_of_mTimeSinceLastTap_3(),
	TapHandler_t334234343::get_offset_of_mMenuAnim_4(),
	TapHandler_t334234343::get_offset_of_mTapCount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (TrackableSettings_t2862243993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2416[1] = 
{
	TrackableSettings_t2862243993::get_offset_of_mExtTrackingEnabled_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (animateTexture_t712563028), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2417[3] = 
{
	animateTexture_t712563028::get_offset_of_textureFrames_2(),
	animateTexture_t712563028::get_offset_of_objectMaterial_3(),
	animateTexture_t712563028::get_offset_of_currentIndex_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (animationScaling_t1595398591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2418[2] = 
{
	animationScaling_t1595398591::get_offset_of_orgSize_2(),
	animationScaling_t1595398591::get_offset_of_animaionSize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (buttonColorToggle_t2066501000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2419[10] = 
{
	buttonColorToggle_t2066501000::get_offset_of_toggleOn_2(),
	buttonColorToggle_t2066501000::get_offset_of_useSprite_3(),
	buttonColorToggle_t2066501000::get_offset_of_settingName_4(),
	buttonColorToggle_t2066501000::get_offset_of_OnColor_5(),
	buttonColorToggle_t2066501000::get_offset_of_OffColor_6(),
	buttonColorToggle_t2066501000::get_offset_of_OnSprite_7(),
	buttonColorToggle_t2066501000::get_offset_of_OffSprite_8(),
	buttonColorToggle_t2066501000::get_offset_of_OnText_9(),
	buttonColorToggle_t2066501000::get_offset_of_OffText_10(),
	buttonColorToggle_t2066501000::get_offset_of_buttonText_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (buttonMoveToScene_t2573241184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2420[1] = 
{
	buttonMoveToScene_t2573241184::get_offset_of_nextLvlBuildIndex_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (buttonTriggerAnimation_t4121295644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2421[1] = 
{
	buttonTriggerAnimation_t4121295644::get_offset_of_animationBusy_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (U3CAnimationCooldownU3Ec__Iterator0_t1141888348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2422[5] = 
{
	U3CAnimationCooldownU3Ec__Iterator0_t1141888348::get_offset_of_U3CanimationLengthU3E__0_0(),
	U3CAnimationCooldownU3Ec__Iterator0_t1141888348::get_offset_of_U24this_1(),
	U3CAnimationCooldownU3Ec__Iterator0_t1141888348::get_offset_of_U24current_2(),
	U3CAnimationCooldownU3Ec__Iterator0_t1141888348::get_offset_of_U24disposing_3(),
	U3CAnimationCooldownU3Ec__Iterator0_t1141888348::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (CameraFocusController_t1033776956), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (TouchCamera_t3780128488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2424[3] = 
{
	TouchCamera_t3780128488::get_offset_of_oldTouchPositions_2(),
	TouchCamera_t3780128488::get_offset_of_oldTouchVector_3(),
	TouchCamera_t3780128488::get_offset_of_oldTouchDistance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (CameraController_t3346819214), -1, sizeof(CameraController_t3346819214_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2425[4] = 
{
	CameraController_t3346819214::get_offset_of_startRotation_2(),
	CameraController_t3346819214::get_offset_of_startPosition_3(),
	CameraController_t3346819214::get_offset_of_parentStartPosition_4(),
	CameraController_t3346819214_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (CharacterCycler_t3521054542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2426[3] = 
{
	CharacterCycler_t3521054542::get_offset_of_currentVal_2(),
	CharacterCycler_t3521054542::get_offset_of_charList_3(),
	CharacterCycler_t3521054542::get_offset_of_n_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (characterLoader_t1326495876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2427[1] = 
{
	characterLoader_t1326495876::get_offset_of_characterList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (characterSlot_t990761438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2428[2] = 
{
	characterSlot_t990761438::get_offset_of_CharacterID_0(),
	characterSlot_t990761438::get_offset_of_CharacterPrefab_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (destoryOnAwake_t1274961527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2429[1] = 
{
	destoryOnAwake_t1274961527::get_offset_of_on_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (EnableMouthAnim_t2443685136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2430[1] = 
{
	EnableMouthAnim_t2443685136::get_offset_of_mouthAnim_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (findTutorialManager_t1037408675), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (FpsCount_t2935661334), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (Math3d_t1427253148), -1, sizeof(Math3d_t1427253148_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2433[2] = 
{
	Math3d_t1427253148_StaticFields::get_offset_of_tempChild_2(),
	Math3d_t1427253148_StaticFields::get_offset_of_tempParent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (ModelSwap_t1632145241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2434[4] = 
{
	ModelSwap_t1632145241::get_offset_of_mDefaultModel_2(),
	ModelSwap_t1632145241::get_offset_of_mExtTrackedModel_3(),
	ModelSwap_t1632145241::get_offset_of_mActiveModel_4(),
	ModelSwap_t1632145241::get_offset_of_mTrackableSettings_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (onScanController_t1472212342), -1, sizeof(onScanController_t1472212342_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2435[12] = 
{
	onScanController_t1472212342::get_offset_of_CharacterLoader_2(),
	onScanController_t1472212342::get_offset_of_CharacterID_3(),
	onScanController_t1472212342::get_offset_of_scanText_4(),
	onScanController_t1472212342::get_offset_of_symbols_5(),
	onScanController_t1472212342::get_offset_of_currentCharacter_6(),
	onScanController_t1472212342::get_offset_of_uiCanvas_7(),
	onScanController_t1472212342::get_offset_of_speechText_8(),
	onScanController_t1472212342::get_offset_of_speechSymbols_9(),
	onScanController_t1472212342::get_offset_of_mouthAnim_10(),
	onScanController_t1472212342_StaticFields::get_offset_of_someoneIsPlaying_11(),
	onScanController_t1472212342::get_offset_of_imPlaying_12(),
	onScanController_t1472212342::get_offset_of_ShowTailAnimation_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2436[4] = 
{
	U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349::get_offset_of_U24this_0(),
	U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349::get_offset_of_U24current_1(),
	U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349::get_offset_of_U24disposing_2(),
	U3CWaitAndPlayShowTailU3Ec__Iterator0_t581090349::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (pageTurn_t132420247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2437[2] = 
{
	pageTurn_t132420247::get_offset_of_Pages_2(),
	pageTurn_t132420247::get_offset_of_currentPage_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (setSettings_t4116128020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2438[1] = 
{
	setSettings_t4116128020::get_offset_of_settingsManager_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (SettingsList_t1883434577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2439[2] = 
{
	SettingsList_t1883434577::get_offset_of_SettingName_0(),
	SettingsList_t1883434577::get_offset_of_IsActive_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (settingsManager_t2086358119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2440[4] = 
{
	settingsManager_t2086358119::get_offset_of_settingslist_2(),
	settingsManager_t2086358119::get_offset_of_MainMixer_3(),
	settingsManager_t2086358119::get_offset_of_orgMusicVol_4(),
	settingsManager_t2086358119::get_offset_of_orgSoundVol_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (SmoothCamera_t4222009864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2441[6] = 
{
	SmoothCamera_t4222009864::get_offset_of_smoothingFrames_2(),
	SmoothCamera_t4222009864::get_offset_of_qcarBehavior_3(),
	SmoothCamera_t4222009864::get_offset_of_smoothedRotation_4(),
	SmoothCamera_t4222009864::get_offset_of_smoothedPosition_5(),
	SmoothCamera_t4222009864::get_offset_of_rotations_6(),
	SmoothCamera_t4222009864::get_offset_of_positions_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (symbolPrefabDefiner_t2798204667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2442[1] = 
{
	symbolPrefabDefiner_t2798204667::get_offset_of_symbolPrefab_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (symbolsSwitch_t3635132447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2443[1] = 
{
	symbolsSwitch_t3635132447::get_offset_of_useSymbols_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (triggerAudio_t1885395141), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2444[1] = 
{
	triggerAudio_t1885395141::get_offset_of_soundBusy_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (U3CSoundCooldownU3Ec__Iterator0_t604438267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2445[5] = 
{
	U3CSoundCooldownU3Ec__Iterator0_t604438267::get_offset_of_U3CanimationLengthU3E__0_0(),
	U3CSoundCooldownU3Ec__Iterator0_t604438267::get_offset_of_U24this_1(),
	U3CSoundCooldownU3Ec__Iterator0_t604438267::get_offset_of_U24current_2(),
	U3CSoundCooldownU3Ec__Iterator0_t604438267::get_offset_of_U24disposing_3(),
	U3CSoundCooldownU3Ec__Iterator0_t604438267::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (tutorialManager_t3421782323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2446[2] = 
{
	tutorialManager_t3421782323::get_offset_of_tutorial_2(),
	tutorialManager_t3421782323::get_offset_of_hasShowed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (DefaultInitializationErrorHandler_t3109936861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2447[9] = 
{
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_mErrorText_2(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_mErrorOccurred_3(),
	0,
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_bodyStyle_5(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_headerStyle_6(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_footerStyle_7(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_bodyTexture_8(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_headerTexture_9(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_footerTexture_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (DefaultTrackableEventHandler_t1588957063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2448[1] = 
{
	DefaultTrackableEventHandler_t1588957063::get_offset_of_mTrackableBehaviour_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
