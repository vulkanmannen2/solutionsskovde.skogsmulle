﻿using UnityEngine;
using System.Collections;


public class CharacterCycler : MonoBehaviour {

    int currentVal = 0;
    public GameObject[] charList;
    int n;

    // Use this for initialization
    void Start () {
	
        for (n = 0; n < charList.Length; n++)
        {
            charList[n].SetActive(false);

        }
        n = 0;
        charList[0].SetActive(true);
        Debug.Log(charList.Length);
        Debug.Log(n);
	}


    public void cycle()
    {
        charList[n].SetActive(false);
        n++;
        if (n == charList.Length)
        {
            n = 0;
        }
        charList[n].SetActive(true);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
