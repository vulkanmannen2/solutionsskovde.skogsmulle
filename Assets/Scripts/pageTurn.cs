﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pageTurn : MonoBehaviour {

    public List<GameObject> Pages;

    int currentPage;

    private void Start()
    {
        currentPage = 0;

        turnToPage(1);

    }

    public void turnToNextPage()
    {
        Pages[currentPage].SetActive(false);

        if (currentPage < Pages.Count - 1)
            currentPage++;
        else
            currentPage = 0;

        Pages[currentPage].SetActive(true);

        if (Pages[currentPage].GetComponent<Animation>() != null)
        {
            if (Pages[currentPage].GetComponent<Animation>().clip != null)
            {
                Pages[currentPage].GetComponent<Animation>().Play();
            }
        }
    }

    public void turnToPage(int page)
    {
        for (int i = 0; i < Pages.Count; i++)
        {
            Pages[i].SetActive(false);
        }

        currentPage = page - 1;
        Pages[currentPage].SetActive(true);

        /*
        if (Pages[currentPage].GetComponent<Animation>() != null)
        {
            if (Pages[currentPage].GetComponent<Animation>().clip != null)
            {
                Pages[currentPage].GetComponent<Animation>().Play();
            }
        }
        */
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
