﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tutorialManager : MonoBehaviour {


    public GameObject tutorial;
    bool hasShowed = false;

	// Use this for initialization
	void Awake () {
        DontDestroyOnLoad(this);

        // Stops _SettingsManager from duplicating when reloading menu
        if (GameObject.FindGameObjectsWithTag("tutorial").Length == 1)
        {
            Destroy(gameObject);
        }
        else
            gameObject.tag = "tutorial";
    }
	
	void Start () {
        if (!hasShowed)
            Show();
	}

    public void Show()
    {
        hasShowed = true;
        tutorial.GetComponent<Canvas>().enabled = true;
    }
}
