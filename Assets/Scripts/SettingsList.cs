﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class SettingsList
{
    public string SettingName;
    public bool IsActive;

	public SettingsList(string newSettingName, bool newIsActive)
    {
        SettingName = newSettingName;
        IsActive = newIsActive;
    }
}
