﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class setSettings : MonoBehaviour {

    GameObject settingsManager;

    private void Start()
    {

        // Sets up the settingsmanager to avoid duplication bugs when reloading menu
        settingsManager = GameObject.FindGameObjectWithTag("settings");
        print ("found _SettingsManager!");
    }

    //Calls on the functions in settingsmanager rather than changing the bools from here, in order to keep the settings functionality in one place

    public void toggleSetting (string calledSetting)
    {
        settingsManager.GetComponent<settingsManager>().toggleSetting(calledSetting);
    }

    public void toggleSettingIfItCantCeOnAtTheSameTimeAsOtherSetting(string calledSetting)
    {
        if (calledSetting == "Text" && !checkIfOn("Text") || calledSetting == "Symbols" && !checkIfOn("Symbols"))
        {
            return;
        }

        settingsManager.GetComponent<settingsManager>().toggleSetting(calledSetting);
    }

    // Looks into settingsmanager for the called setting and returns wether the setting is true or false.
    bool checkIfOn(string calledSetting)
    {
        if (GameObject.Find("_SettingsManager").GetComponent<settingsManager>().settingslist[GameObject.Find("_SettingsManager").GetComponent<settingsManager>().findSetting(calledSetting)].IsActive)
        {
            return true;
        }
        else
            return false;
    }
}
