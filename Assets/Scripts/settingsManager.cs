﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class settingsManager : MonoBehaviour {

    public List<SettingsList> settingslist;

    public AudioMixer MainMixer;

    float orgMusicVol;
    float orgSoundVol;

    void Awake()
    {

        DontDestroyOnLoad(this);

        // Stops _SettingsManager from duplicating when reloading menu
        if (GameObject.FindGameObjectsWithTag("settings").Length  == 1)
        {
            Destroy(gameObject);
        }
        else
            gameObject.tag = "settings";
    }

    private void Start()
    {
        // Defines the avilable settings

        MainMixer.GetFloat("Music", out orgMusicVol);
        MainMixer.GetFloat("Sound", out orgSoundVol);

        print(orgMusicVol);
        print(orgSoundVol);
    }

    // If you're using buttons to set the changes, use apply the "setSettings"-script on the button, and change the setting via that script. Otherwise the button may have trouble finding this script upon returning to the menu.

    // Looks for setting with string, then toggles it on/off.
    public void toggleSetting(string calledSetting)
    {
        if (settingslist[findSetting(calledSetting)].IsActive)
        {
            if (calledSetting == "Music" || calledSetting == "Sound")
                mute(calledSetting, true);
            settingslist[findSetting(calledSetting)].IsActive = false;
        }
            
        else
        {
            if (calledSetting == "Music" || calledSetting == "Sound")
                mute(calledSetting, false);
            settingslist[findSetting(calledSetting)].IsActive = true;
        }
            
            print(settingslist[findSetting(calledSetting)].SettingName + " = " + settingslist[findSetting(calledSetting)].IsActive);
    }

   public int findSetting (string calledSetting)
    {
        bool foundSetting = false;
        int settingIndex = 0;

        for (int i = 0; i < settingslist.Count; i++)
        {
            if (settingslist[i].SettingName == calledSetting)
            {
                settingIndex = i;
                foundSetting = true;
            }
        }

        if (foundSetting)
            return settingIndex;
        else
        {
            print("Setting name not found. Setting index to 0 as int's can't return null.");
            return 0;
        }
            
    }

    void mute(string calledSetting, bool isMuted)
    {
        if (calledSetting == "Music" && isMuted)
            MainMixer.SetFloat("Music", -80);
        else if (calledSetting == "Music" && !isMuted)
            MainMixer.SetFloat("Music", orgMusicVol);
        else if (calledSetting == "Sound" && isMuted)
            MainMixer.SetFloat("Sound", -80);
        else if (calledSetting == "Sound" && !isMuted)
            MainMixer.SetFloat("Sound", orgSoundVol);
    }
}
