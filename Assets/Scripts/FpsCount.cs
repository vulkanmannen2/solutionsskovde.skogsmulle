﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FpsCount : Text {
    
	
	// Update is called once per frame
	void Update () {
        text = "FPS: " + 1f / Time.deltaTime;
	}
}
